<!DOCTYPE html>
<html>
	<head>
    	<title>Liverpool</title>
        <meta charset="utf-8">
        <meta name="author" content="SIOrbita">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:300&subset=latin,greek' rel='stylesheet' type='text/css'>
        <script src="js/modernizr.custom.js"></script>
	    <script src="js/respond.min.js"></script>
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="js/sio.js"></script>
		<script>
			var token='clailvoerrepx';
			var id=0;
			var resultado;
			var calentadorID=1;
			var calentadoreslength=0;
			var resp_obtenerInfo=function(resp){
				//console.log(resp);
				resultado=JSON.parse(resp);
				codigo=document.getElementById('lsPreguntas').innerHTML;
				texto=generarHTML(resultado,codigo);
				document.getElementById('preguntas').innerHTML=texto;
			}
			var obtenerInfo=function(){
				texto="<ul><h6>Cargando... Gracias por tu paciencia</h6></ul>";
				document.getElementById('preguntas').innerHTML=texto;
				paramSend('&accion=obtenerPreguntas&id='+id,resp_obtenerInfo);
			}
			var generar=function(variable,valor){
				localStorage.setItem(variable,valor);
				id++;
				render();
			}
			var return1=function(){
				id--;
				render();
			}
			var render=function(){
				console.log('ID:'+id);
				switch(id){
					case 0:
						id=1;
					case 1:
						document.getElementById('anteriorCal').style.display="none";
						document.getElementById('siguienteCal').style.display="none";
						document.getElementById('regresar').style.display="none";
						obtenerInfo();
					break;	
					case 2:
					case 3:
					case 4:
						obtenerInfo();
						document.getElementById('regresar').style.display="block";
						document.getElementById('siguiente').style.display="none";
					break;	
					case 5:
						texto=document.getElementById('cuestionario').innerHTML;
						document.getElementById('preguntas').innerHTML=texto;
						document.getElementById('siguiente').style.display="block";
						document.getElementById('titDerecho').style.display="none"
						document.getElementById('imgLateralDer').src="img/divCuestDerecha.png";
						calentadorID=1;
						document.getElementById('anteriorCal').style.display="none";
						document.getElementById('siguienteCal').style.display="none";
					break;
					case 6:
						calentadoreslength=resultado["rows"].length;
						codigo=document.getElementById('calentadores1').innerHTML;
						texto=generarHTML(resultado,codigo,calentadorID-1,calentadorID);
						document.getElementById('preguntas').innerHTML=texto;
						codigo=document.getElementById('calentadores2').innerHTML;
						texto=generarHTML(resultado,codigo,calentadorID,calentadorID+1);
						document.getElementById('preguntas').innerHTML+=texto;
						document.getElementById('siguiente').style.display="none";
						if (calentadorID==1){
							document.getElementById('siguienteCal').style.display="block";
						}
					break;
					case 7:
						texto=document.getElementById('formulario').innerHTML;
						document.getElementById('preguntas').innerHTML=texto;
					break;
				}
			}
			var resp_obtenerResultados=function(resp){
				resultado=JSON.parse(resp);
				if(resultado["status"]==false){
					id=7;
				}else{
					id=6;
				}
				render();
			}
			var obtenerResultados=function(){
				var param='&accion=obtenerResultados';
				param+="&tipo="+localStorage.getItem('tipo');
				param+="&presion="+localStorage.getItem('presion');
				param+="&regadera="+localStorage.getItem('regadera');
				param+="&distancia="+localStorage.getItem('distancia');
				param+="&regaderas="+document.getElementById('banos').value;
				param+="&lavabos="+document.getElementById('mbanos').value;
				param+="&tina="+document.getElementById('tinas').value;
				param+="&lavadoras="+document.getElementById('lavadoras').value;
				paramSend(param,resp_obtenerResultados);
				texto="<ul><h6>Cargando... Gracias por tu paciencia</h6></ul>";
				document.getElementById('preguntas').innerHTML=texto;
			}
			var siguienteCalentador=function(){
				if(calentadorID>=calentadoreslength-1){
					return false;
				}
				document.getElementById('anteriorCal').style.display="block";
				calentadorID++;
				if(calentadorID>=calentadoreslength-1){
					document.getElementById('siguienteCal').style.display="none";
				}
				render();
			}
			var anteriorCalentador=function(){
				if(calentadorID<=1){
					return false;
				}
				document.getElementById('siguienteCal').style.display="block";
				calentadorID--;
				if(calentadorID<=1){
					document.getElementById('anteriorCal').style.display="none";	
				}
				render();
			}
		</script>
    </head>
    <body ondragstart="return false;" ondrop="return false;" onLoad="generar()" class="ns">
    <div class="contenedor">
            <div class="encabezado">
                <img src="img/header.png">
            </div>
            <div class="clearfix"></div>
            <div class="cuerpoIzq">
                <div id="imgIzq">
                	<img src="img/imgLateral.png" id="imgLateralIzq">
                </div>
            </div>
            
            <div class="cuerpoDer">
            	<div id="imgDer">
                	<img src="img/divCuestDerecha.png" id="imgLateralDer">  
                </div>
                <div id="formContacto">
                	<div id="titForm">
                		<h6>Para ofrecerte una solución de acuerdo a tus necesidades, déjanos tus datos</h6>
                    </div>
                	<form>
                    	<label >* Nombre completo</label>
                        <input required>
                        <label >* Teléfono</label>
                        <input required>
                        <label >* Estado</label>
                        <input required>
                        <div class="form3">
                        <label class="form2">* Correo electrónico</label>
                        <input class="form2" required>
                        <label class="form2">* Celular</label>
                        <input class="form2" required>
                        <label class="form2">* Ciudad</label>
                        <input class="form2" required>
                        </div>
                        <label >Mensaje</label>
                        <textarea></textarea>
                    </form>
                    <div id="btnEnviar">
                    	<p>(*) Campos Obligatorios</p>
                        <img src="img/enviar.png">
                    </div>
                </div>
 
    
    
    </body>
    </html>