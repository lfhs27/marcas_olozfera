<!DOCTYPE html>
<html>
	<head>
    	<title>Liverpool</title>
        <meta charset="utf-8">
        <meta name="author" content="SIOrbita">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:300&subset=latin,greek' rel='stylesheet' type='text/css'>
        <script src="js/modernizr.custom.js"></script>
	    <script src="js/respond.min.js"></script>
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    </head>
    <body>
    	<div class="contenedor">
            <div class="encabezado">
                <img src="img/header.png">
            </div>
            <div class="clearfix"></div>
            <div class="cuerpoIzq">
                <div id="imgIzq">
                	<img src="img/imgLateral.png" id="imgLateralIzq">
                </div>
            </div>
            
            <div class="cuerpoDer">
            	<div id="imgDer">
                	<img src="img/divCuestDerecha.png" id="imgLateralDer">  
                </div>
            	<div class="listDer">
                    <form>
                    	<label>¿Cuántos baños completos tienes?</label>
                        <input type="number"><img src="img/regadera.png"class="iconImg"><img src="img/lavabo.png" class="iconImg1">
                        <label>¿Cuántos medios baños tienes?</label>
                        <input type="number" ><img src="img/lavabo.png" class="iconImg">
                        <label>¿Cuántas tinas tienes?</label>
                        <input type="number"><img src="img/tina.png" class="iconImg">
                        <label>¿Cuántas lavadoras tienes?</label>
                        <input type="number"><img src="img/lavadora.png" class="iconImg">
                    </form>
                </div>
                <div id="divBtnRegresar1">
                	<img src="img/btnRegresar.png" alt="Regresar" id="btnRegresar">
                    <img src="img/btnSiguiente.png" alt="Siguiente" id="btnSiguiente">
                </div>
                
           <!-- <div id="divBtnSiguiente">
                	<img src="img/btnSiguiente.png" alt="Siguiente">
                </div> -->
            </div>
            <div class="clearfix"></div>
            <div class="pie">
            	
            	<img src="img/footer.png">
                <p id="titFoot">Copyright 2015 calorex | GIS | Preguntas Frecuentes |</p>
            </div>
            
        </div>
    </body> 
</html>