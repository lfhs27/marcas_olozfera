<?php
header("charset:utf-8");
$mysqli=mysqli_connect('localhost','softdep4_liver','liverpool15%','softdep4_liverpool');
if ($mysqli->connect_error) {
    die('Error de conexión: ' . $mysqli->connect_error);
}
$mysqli->set_charset("utf8");
date_default_timezone_set('America/Mexico_City');
function enviarMail($to,$subject,$message,$headers=false,$params=false){
	if(!$headers){
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= 'From: Liverpool/Calorex <noreplay@liv-cal.com>' . "\r\n";

	}
	if(!$params){
		$params='-fnoreplay@honda.com';
	}
	mail($to,$subject,$message,$headers,$params);
}
function sel($qry){
	global $mysqli;
	$respuesta["qry"]=$qry;
	$obj_res=$mysqli->query($qry) or die($mysqli->error);
	//return json_encode($obj_res);
	if($obj_res->num_rows){
		$respuesta['status']=true;
	}else{
		$respuesta['status']=false;
	}
	while($f=$obj_res->fetch_field()){
		$respuesta['cols'][]=$f->name;
	}
	while($f=$obj_res->fetch_object()){
		$respuesta['rows'][]=$f;
	}
	return json_encode($respuesta,JSON_UNESCAPED_UNICODE);
}
function insupd($qry){
	global $mysqli;
	$respuesta["qry"]=$qry;
	$obj_res=$mysqli->query($qry);
	$resppuesta["status"]=true;
	$respuesta["resultado"]=$obj_res;
	$respuesta["affected"]=$mysqli->affected_rows;
	$respuesta["id"]=$mysqli->insert_id;
	return json_encode($respuesta);
}
function obtenerPreguntas(){
	$id=$_POST['id'];
	return sel("SELECT * FROM pregunta WHERE pregunta_id='$id'");
}
function obtenerResultados(){
	$partner='liverpool';
	$brand='calorex';
	$regadera=$_POST['regadera'];
	$distancia=$_POST['distancia'];
	$presion=$_POST['presion'];
	$tipo=$_POST['tipo'];
	if($tipo=='solar'){
		return sel("SELECT calentador_id,calentador_tipo,calentador_descripcion,$partner,calentador_imagen,liverpool_LP,liverpool_Nat FROM calentador WHERE  (liverpool_LP!='0' OR liverpool_Nat!='0') AND calentador_tipo='solar' ORDER BY $partner DESC,calentador_regaderas");
	}
	$tina=$_POST['tina']*4;
	$lavavos=$_POST['lavabos']/2;
	$regaderas=$_POST['regaderas'];
	$lavadoras=$_POST['lavadoras'];
	if($distancia>=2){
		$tina+=1;
	}
	$sum1=$lavadoras+$regaderas*$regadera+$tina;
	$total=$lavabos+$sum1;
	if($total>2){
		$total=round($total,0);
	}
	return sel("SELECT calentador_id,calentador_tipo,calentador_descripcion,$partner,calentador_imagen,liverpool_LP,liverpool_Nat FROM calentador WHERE  (liverpool_LP!='0' OR liverpool_Nat!='0') AND calentador_regaderas='$total' AND calentador_marca='$brand' ORDER BY $partner DESC,calentador_regaderas");
}
function mandarCorreo(){
	$to="configurador@goblincreative.com";
	$subject="Formulario de contacto Liverpool";
	$message='
		<table>
			<caption>Formulario de Contacto</caption>
			<tbody>
				<tr>
					<td>Nombre:</td>
					<td>'.$_POST['nombre'].'</td>
				</tr>
				<tr>
					<td>Correo:</td>
					<td>'.$_POST['correo'].'</td>
				</tr>
				<tr>
					<td>Telefono:</td>
					<td>'.$_POST['telefono'].'</td>
				</tr>
				<tr>
					<td>Celular:</td>
					<td>'.$_POST['celular'].'</td>
				</tr>
				<tr>
					<td>Estado:</td>
					<td>'.$_POST['estado'].'</td>
				</tr>
				<tr>
					<td>Ciudad:</td>
					<td>'.$_POST['ciudad'].'</td>
				</tr>
				<tr>
					<td>Mensaje:</td>
					<td>'.$_POST['mensaje'].'</td>
				</tr>
			</tbody>
		</table>
	';
	return enviarMail($to,$subject,$message);
}
//echo $_POST['accion'];
echo $_POST['accion']();
?>