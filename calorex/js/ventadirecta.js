$(document).ready(function() {
	var allPanels = $('#sidebar nav .submenu').hide();

	$('#sidebar nav li a.toggle-panel').click(function(){
		var itemSiblings = $(this).parent().siblings();
		$('.submenu',itemSiblings).slideUp()
		$('.active',itemSiblings).removeClass('active');

		$(this).addClass('active');
		$(this).next('.submenu').slideDown()

		return false;
	});

	$('.product').on('mouseenter',function(){
		$('.subtitle-overlay',this).fadeIn();
	}).on('mouseleave',function(){
		$('.subtitle-overlay',this).fadeOut();
	})
});

$(document).ready(function() {
	$('#payment-options-collapsible').on('shown.bs.collapse',function(){
		var expandedEl = $('[aria-expanded=true]',this);

		$('.glyphicon',expandedEl).removeClass('glyphicon-menu-right').addClass('glyphicon-menu-down');
	});

	$('#payment-options-collapsible').on('hide.bs.collapse',function(){
		var expandedEl = $('[aria-expanded=true]',this);		
		$('.glyphicon',expandedEl).removeClass('glyphicon-menu-down').addClass('glyphicon-menu-right');
	});


	$(window).on('scroll',function(){
		if ($(window).width() > 768) {
			var st = $(window).scrollTop();
			var opacity = st / 80;
			if (opacity < 0.7) {
				$('header.opacity').css({
					opacity: 0.7
				})
			}
			if (opacity > 0.7) {
				$('header.opacity').css({
					opacity: opacity
				})
			};
		}
	});

	if ($(window).width() < 767 ) {
		$('.mobile-cycle').cycle({
			slides: '>.col-xs-12',
			fx: 'scrollHorz',
			swipe: true
		});
	};
});

// $(window).load(function(){
// 	if ($('body').hasClass('template-login')) {
// 		var winH = $(window).height();
// 		var blockH = $('.login-content').height();
// 		var marginTop = ((winH - blockH) / 2) - 170;
// 		console.log(winH);
// 		console.log(blockH);

// 		$('.login-content').css({
// 			marginTop: marginTop
// 		});
// 	}
// });