$(window).load(function() {
	$('#products-carousel-container').owlCarousel({
		loop: true,
		margin: 15,
		items: 4,
		pagination : true
	});
});

$(document).ready(function(){
	var allPanels = $('#sidebar nav .submenu').hide();

	$('#sidebar nav li a.toggle-panel').click(function(){
		var itemSiblings = $(this).parent().siblings();
		$('.submenu',itemSiblings).slideUp()
		$('.active',itemSiblings).removeClass('active');

		$(this).addClass('active');
		$(this).next('.submenu').slideDown()

		return false;
	});

	$('.product').on('mouseenter',function(){
		$('.subtitle-overlay',this).fadeIn();
	}).on('mouseleave',function(){
		$('.subtitle-overlay',this).fadeOut();
	});

	$('#products-carousel .product').on('mouseenter',function(){
		$('.product-overlay',this).fadeIn();
	}).on('mouseleave',function(){
		$('.product-overlay',this).fadeOut();
	})

	$('#products-carousel .product .product-overlay').on('click',function(){
		var nextUrl = $('a',this).attr('href');

		top.location = nextUrl;

		return false;
	})

	if ($(window).width() < 768) {
		$('#related-products .products-cycle').cycle({
			slides: '>.col-xs-12'
		});
	};

});