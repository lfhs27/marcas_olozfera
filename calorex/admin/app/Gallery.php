<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
	use SoftDeletes;

	protected $fillable = ['product_id'];

    public function images()
    {
    	return $this->morphMany('CMS\Image','imageable');
    }

    public function product()
    {
    	return $this->belongsTo('CMS\Product');
    }
}
