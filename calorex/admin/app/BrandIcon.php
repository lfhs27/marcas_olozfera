<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;

class BrandIcon extends Model
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'brand_icons';

	public function product()
	{
		return $this->belongsTo('CMS\Product');
	}
}