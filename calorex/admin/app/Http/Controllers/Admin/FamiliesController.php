<?php

namespace CMS\Http\Controllers\Admin;

use Gate;
use Session;

use Illuminate\Http\Request;

use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;

use CMS\Family;
use CMS\Brand;

class FamiliesController extends Controller
{
	public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$families = Family::orderBy("order", "asc")->get();
		$brands = Brand::all();
		return view('admin.families.index')
			->with('brands',$brands)
			->with('families',$families);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if ($request->exists('name') && $request->exists('brand_id') && $request->input('brand_id') != 0) {
			// Check if exists
			$family = Family::firstOrNew(['name' => $request->input('name'),'brand_id' => $request->input('brand_id')]);

			if (!$family->exists) {
				$family->front_name = $request->exists('front_name') ? $request->input('front_name') : $request->input('name');
				$family->business_unit_id = Session::get('UsingBusinessUnit');
				$family->slug = str_slug($request->input('name'));
				$family->created_by = \Auth::user()->id;
				$family->save();
			}
		}

		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$family = Family::find($id);
		$brands = Brand::all();

		if ($family) {
			return view('admin.families.edit')
				->with('brands',$brands) 
				->with('family',$family);   

			return redirect()->back();
		} else {
			return redirect('/admin/families');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$family = Family::find($id);

		if ($family) {
			// $family->name = $request->exists('name') ? $request->input('name') : $family->name;
			$family->front_name = $request->exists('front_name') ? $request->input('front_name') : $family->front_name;
			$family->brand_id = $request->exists('brand_id') ? $request->input('brand_id') : $family->brand_id;
			$family->hovertext = $request->exists('hovertext') ? $request->input('hovertext') : $family->hovertext;
			$family->description = $request->exists('description') ? $request->input('description') : $family->description;

			$family->save();

			return redirect()->back();
		} else {
			return redirect('/admin/families');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$family = Family::find($id);

		if ($family) {
			$family->delete();
			return redirect()->back();
		} else {
			return redirect('/admin/families');
		}
	}
	
	public function updateOrder(Request $request)
	{
		if ($request->has('families_order')) {
			$families = json_decode($request->input("families_order"));
			
			if(COUNT($families)){
				foreach($families as $idFamily){
					$family = Family::find($idFamily->id);
					$family->order = $idFamily->order;
					$family->save();
				}
			}
		}

		return redirect()->back();
	}
}
