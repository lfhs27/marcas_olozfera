<?php

namespace GIS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use GIS\Http\Requests;
use GIS\Http\Controllers\Controller;
use GIS\GISWebServiceDB\Product;
use GIS\GISWebServiceDB\Inventory;
use GIS\GISWebServiceDB\Price;

class WebServiceController extends Controller
{
	public function getProductInfo(Request $request)
	{
		$itemId = $request->input('productId');

		$product = Product::where('INVENTORY_ITEM_ID',$itemId)->get()->toArray();

		$response = $product;

		return response()->json($response);
	}

	public function getObjects(Request $request)
	{
		$brandFilter = $request->input('brands');
		$sectorFilter = $request->input('sectors');
		$familyFilter = $request->input('families');

		$response["sectors"] =  array();
		$response["families"] =  array();
		$response["products"] =  array();

		if ($brandFilter != "") {
			$response['sectors'] = Product::getBrandSectors($brandFilter);	
		}

		if ($sectorFilter != "") {
			$response["families"] = Product::getBrandSectorFamilies($brandFilter,$sectorFilter);
		}

		if ($familyFilter != "") {
			$response["products"] = Product::where(array(
				"MARCA"				=> $brandFilter,
				"LINEA_MERCADO"		=> $sectorFilter,
				"LINEA"			=> $familyFilter
				))->groupBy('SEGMENT1')->get()->toArray();
		}

		return response()->json($response);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
