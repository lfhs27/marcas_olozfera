<?php

namespace CMS\Http\Controllers\Admin;

use Gate;
use Session;

use Illuminate\Http\Request;

use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;

use CMS\Line;
use CMS\Brand;
use CMS\Family;

class LinesController extends Controller
{
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lines = Line::orderBy("order", "asc")->get();
		$families = Family::all();
		$brands = Brand::all();
		
        return view('admin.lines.index')
            ->with('families',$families)
            ->with('brands',$brands)
            ->with('lines',$lines);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->exists('name')) {
            $businessUnitId = Session::get('UsingBusinessUnit');
            // $usingBusinessUnit = BusinessUnit::find($businessUnitId);

            $line = new Line;
            $line->name = $request->input('name');
            $line->front_name = $request->input('name');
            $line->slug = str_slug($request->input('name'));
            $line->description = $request->exists('description') ? $request->input('description') : '';
            $line->business_unit_id = $businessUnitId;
            $line->brand_id = $request->exists('brand_id') ? $request->input('brand_id') : 0;
            $line->family_id = $request->exists('family_id') ? $request->input('family_id') : 0;

            $line->save();
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $line = Line::find($id);

        if ($line) {
            $family = Family::find($request->input('family_id'));

            $line->front_name = $request->exists('name') ? $request->input('name') : $line->name;
            $line->description = $request->exists('description') ? $request->input('description') : $line->description;
            $line->family_id = $family->id;
            $line->brand_id = $family->brand->id;

            $line->save();

            return redirect()->back();
        }

        return redirect('/admin/lines');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $line = Line::find($id);

        if ($line) {
            $line->delete();
        }

        return redirect()->back();
    }
	
    public function updateOrder(Request $request)
    {
        if ($request->has('lines_order')) {
            $lines = json_decode($request->input("lines_order"));
			
			if(COUNT($lines)){
				foreach($lines as $idLine){
					$line = Line::find($idLine->id);
					$line->order = $idLine->order;
					$line->save();
				}
			}
        }

        return redirect()->back();
    }
}
