<?php

namespace CMS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;
Use CMS\User;
Use CMS\Banner;

use Input;
use Validator;
use Redirect;
use Session;

class BannersController extends Controller
{
	protected $user;

	public function __construct()
	{
		$this->user = \Auth::user();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$banners = Banner::where(["type" => "banner"])->get();
		return view('admin.banners.index')
				->with('user',$this->user)
				->with('banners',$banners);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$destinationPath = 'uploads/'.$request->type; // upload path
		$fileName = null;
		// getting all of the post data
		$file = array('image' => Input::file('image'));
		// setting up rules
		$rules = array();
		if(!isset($request->id) || $request->id == 0)
			$rules = array('image' => 'required'); //mimes:jpeg,bmp,png and for max size max:10000
		// doing the validation, passing post data, rules and the messages
		$validator = Validator::make($file, $rules);
		if ($validator->fails()) {
			// send back to the page with the input data and errors
			return Redirect::to($request->viewCallback)->withInput()->withErrors($validator);
		} else {
			if(Input::hasfile('image')){
				// checking file is valid.
				if (Input::file('image')->isValid()) {
					$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
					$fileName = rand(11111,99999).'.'.$extension; // renameing image
					Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
				}  else {
					// sending back with error message.
					Session::flash('error', 'uploaded file is not valid');
					return Redirect::to('banners');
				}
			}
		}
		// sending back with message
		Session::flash('success', 'El Banner ha sido guardado'); 
		if(0 === strpos($request->type, 'bg-')) {
			$fondo = Banner::where(["type" => $request->type])->first();
			if(is_null($fondo)){
				$fondo = null;
				if(isset($request->id) && $request->id > 0)
					$fondo = Banner::find($request->id);
				else
					$fondo = new Banner;
			}
			if($fileName)
				$fondo->src = $destinationPath.'/'.$fileName;
				
			$fondo->type = $request->type;
			$fondo->save();
			
			return Redirect::to('backgrounds');
		}else{
			$banner = null;
			if(isset($request->id) && $request->id > 0)
				$banner = Banner::find($request->id);
			else
				$banner = new Banner;
				
			if($fileName)
				$banner->src = $destinationPath.'/'.$fileName;
			
			$link = $request->liga;
			if (strpos($link,'http://') === false){
				$link = 'http://'.$link;
			}
			$banner->link = $link;
			$banner->title = $request->title;
			$banner->type = $request->type;
			$banner->save();
			
			if(strcmp($request->type, "banner") == 0)
				return Redirect::to('banners');
			else if(strcmp($request->type, "slide") == 0)
				return Redirect::to('/');
			else if(strcmp($request->type, "producto") == 0)
				return Redirect::to('dashboard/productos');
			else if(strcmp($request->type, "index-bg") == 0)
				return Redirect::to('backgrounds/index');
			else
				return Redirect::back()->with('message','Operation Successful !');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$banner = Banner::find($id);
		$banner->delete();
		
		return response()->json(['status' => '200']);
	}
	
	public function getAllBanners(){
		$banners = Banner::where(["type" => "banner"])->get();
		
		echo json_encode($banners);
	}
}