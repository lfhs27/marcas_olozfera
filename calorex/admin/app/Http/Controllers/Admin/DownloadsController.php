<?php

namespace CMS\Http\Controllers\Admin;

use Gate;

use Illuminate\Http\Request;

use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;
use CMS\Download;

class DownloadsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		
		if ($request->hasFile('download')) {
			$file = $request->file('download');
			$path = 'downloads';
			$dName = $request->input('name');
			$fName = $file->getClientOriginalName();
			$downloadableId = $request->input('downloadable_id');
			$downloadableType = $request->input('downloadable_type');

			$download = new Download;
			$download->name = $dName;
			$download->filename = $fName;
			$download->filepath = $path;
			$download->extension = $file->getClientOriginalExtension();
			$download->mimetype = $file->getClientMimeType();
			$download->created_by = \Auth::user()->id;
			$download->downloadable_id = $downloadableId;
			$download->downloadable_type = $downloadableType;
			$download->type = $request->input('dtype');

			$file->move($path,$fName);

			$download->save();

			$request->session()->flash('notification', 'Archivo guardado correctamente.');
			$request->session()->flash('type', 'success');
		}

		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$download = Download::find($id);

		if ($download) {
			$download->delete();
		}

		return redirect()->back();
	}
}
