<?php

namespace TiendaGis\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Gate;
use Auth;
use Session;
use TiendaGis\Http\Requests;
use TiendaGis\Http\Controllers\Controller;
use TiendaGis\BusinessUnit;
use TiendaGis\Image;
use TiendaGis\Logo;
use TiendaGis\Pricelist;
use TiendaGis\Slider;
use TiendaGis\Center;

class BusinessUnitsController extends Controller
{
	public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		if (Gate::denies('Business_Unit_Read')) {
			abort(403);
		}

		$uBunits = BusinessUnit::all();
		return view('admin.businessunits.index')
			->with('businessUnits',$uBunits);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		if (Gate::denies('Business_Unit_Publish')) {
			abort(403);
		}

		return view('admin.businessunits.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (Gate::denies('Business_Unit_Publish')) {
			abort(403);
		}

		$this->validate($request,[
			'name'		=> 'required'
			]);

		$bUnit = new BusinessUnit;
		$bUnit->name = $request->input('name');
		$bUnit->description = $request->input('description');
		$bUnit->slug = str_slug($request->input('name'));
		$bUnit->created_by = Auth::user()->id;


		if ($bUnit->save()) {
			$request->session()->flash('notification', 'Unidad de negocio creada correctamente.');
			$request->session()->flash('type', 'success');
			return redirect()->back();
		} else {
			$request->session()->flash('notification', 'No se pudo guardar la unidad de negocio');
			$request->session()->flash('type', 'warning');
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if (Gate::denies('Business_Unit_Read')) {
			abort(403);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if (Gate::denies('Business_Unit_Write')) {
			abort(403);
		}

		$businessUnit = BusinessUnit::find($id);
		$pricelists = Pricelist::all();
		$sliders = Slider::all();
		$cedisList = Center::all();

		if ($businessUnit) {
			return view('admin.businessunits.edit')
				->with('businessUnit',$businessUnit)
				->with('pricelists',$pricelists)
				->with('sliders',$sliders)
				->with('cedisList',$cedisList);
		} else {
			return redirect('admin/businessunits')->with('notification','No existe la unidad de negocio solicitada');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if (Gate::denies('Business_Unit_Write')) {
			abort(403);
		}

		// dd($request->all(),$id);
		$bUnit = BusinessUnit::find($id);

		if ($bUnit) {
			$bUnit->slider_id = $request->exists('mainSlider') ? $request->input('mainSlider') : $bUnit->slider_id;
			$bUnit->menu_id = $request->exists('mainmenu') ? $request->input('mainmenu') : $bUnit->menu_id;

			if ($request->exists('mainSlider')) {
				$selectedSlider = Slider::find($request->input('mainSlider'));

				if ($selectedSlider) {
					$selectedSlider->business_unit_id = $request->input('mainSlider');
					$selectedSlider->save();
				}
			}

			if ($request->exists('bannerUrl')) {
				$bannerIndex = $request->input('bannerIndex');
				$newUrl = $request->input('bannerUrl');

				switch ($request->input('bannerPosition')) {
					case 'normal':
						$bannerContent = json_decode($bUnit->banners_json,TRUE);
						$bannerContent[$bannerIndex]['url'] = $newUrl;
						$bUnit->banners_json = json_encode($bannerContent);
						
						break;

					case 'bottom':
						$bannerContent = json_decode($bUnit->banners_bottom_json,TRUE);
						$bannerContent[$bannerIndex]['url'] = $newUrl;
						$bUnit->banners_bottom_json = json_encode($bannerContent);
						break;
					
					default:
						return false;
						break;
				}

				$bUnit->save();
				return response()->json(['status' => 200,'message' => 'Banner Editado correctamente.']);
			}

			if ($request->exists('removeCedis')) {
				$selectedCedis = Center::find($request->input('removeCedis'));
				$selectedCedis->business_unit_id = NULL;
				$selectedCedis->save();
			}

			if ($request->exists('cedis_id')) {
				$selectedCedis = Center::find($request->input('cedis_id'));
				$selectedCedis->business_unit_id = $bUnit->id;
				// $selectedCedis->name = $request->exists('cedis_name') ? $request->input('cedis_name') : '';
				$selectedCedis->save();
			}

			if ($request->hasFile('bannerImage') || $request->hasFile('bannerBottomImage')) {
				if ($request->hasFile('bannerImage')) {
					$bannerImage = $request->file('bannerImage');
				}

				if ($request->exists('bannerBottomImage')) {
					$bannerImage = $request->file('bannerBottomImage');
				}

				$fName = $bannerImage->getClientOriginalName();
				$fExt = $bannerImage->getClientOriginalExtension();

				$destName = time() . '-' . $fName;
				$destPath = 'uploads/banners';

				$bannerImage->move($destPath,$destName);

				list($width, $height) = getimagesize($destPath . '/' . $destName);

				$image = new Image;
				$image->name = $destName;
				$image->filename = $destName;
				$image->filepath = $destPath;
				$image->description = $destName;
				$image->alt = $destName;
				$image->width = $width;
				$image->height = $height;
				$image->extension = $fExt;
				$image->created_by = \Auth::user()->id;
				$image->imageable_type = 'TiendaGis\Banner';
				$image->save();

				if ($request->hasFile('bannerImage')) {
					$bUnitsBanners = json_decode($bUnit->banners_json,TRUE);	
					$bUnitsBanners[] = ['image_id' => $image->id,'url' => $request->input('url')];
					$bUnit->banners_json = json_encode($bUnitsBanners);
				}

				if ($request->exists('bannerBottomImage')) {
					$bUnitsBanners = json_decode($bUnit->banners_bottom_json,TRUE);	
					$bUnitsBanners[] = ['image_id' => $image->id,'url' => $request->input('url')];
					$bUnit->banners_bottom_json = json_encode($bUnitsBanners);
				}
			}

			if ($request->exists('org_id')) {
				$bUnit->org_id = $request->input('org_id');
			}
			if ($request->exists('ship_from_org')) {
				$bUnit->ship_from_org = $request->input('ship_from_org');
			}

			if ($request->exists('ship_from_org_saltillo')) {
				$bUnit->ship_from_org_saltillo = $request->input('ship_from_org_saltillo');
			}

			if ($request->exists('ship_from_org_mexico')) {
				$bUnit->ship_from_org_mexico = $request->input('ship_from_org_mexico');
			}

			if ($request->exists('context')) {
				$bUnit->context = $request->input('context');
			}

			if ($request->exists('order_type_credito_saltillo')) {
				$bUnit->order_type_credito_saltillo = $request->input('order_type_credito_saltillo');
			}

			if ($request->exists('order_type_credito_mexico')) {
				$bUnit->order_type_credito_mexico = $request->input('order_type_credito_mexico');
			}

			if ($request->exists('order_type_venta_saltillo')) {
				$bUnit->order_type_venta_saltillo = $request->input('order_type_venta_saltillo');
			}

			if ($request->exists('order_type_venta_mexico')) {
				$bUnit->order_type_venta_mexico = $request->input('order_type_venta_mexico');
			}

			if ($request->hasFile('logo')) {
				$buLogo = new Logo;
				$buLogo->business_unit_id = $bUnit->id;
				$buLogo->created_by = \Auth::user()->id;
				$buLogo->save();

				$logo = $request->file('logo');

				$fName = $logo->getClientOriginalName();
				$fExt = $logo->getClientOriginalExtension();

				$destName = time() . '-' . $fName;
				$destPath = 'uploads/businessunits/logos';

				$logo->move($destPath,$destName);

				list($width, $height) = getimagesize($destPath . '/' . $destName);

				$buLogoImage = new Image;
				$buLogoImage->name = $destName;
				$buLogoImage->filename = $destName;
				$buLogoImage->filepath = $destPath;
				$buLogoImage->description = $destName;
				$buLogoImage->alt = $destName;
				$buLogoImage->width = $width;
				$buLogoImage->height = $height;
				$buLogoImage->extension = $fExt;
				$buLogoImage->created_by = \Auth::user()->id;
				$buLogoImage->imageable_type = 'TiendaGis\Logo';
				$buLogoImage->imageable_id = $buLogo->id;
				$buLogoImage->save();
			}

			if ($request->hasFile('brandImage')) {
				$brandImage = $request->file('brandImage');

				$fName = $brandImage->getClientOriginalName();
				$fExt = $brandImage->getClientOriginalExtension();

				$destName = time() . '-' . $fName;
				$destPath = 'uploads/brands';

				$brandImage->move($destPath,$destName);

				list($width, $height) = getimagesize($destPath . '/' . $destName);

				$image = new Image;
				$image->name = $destName;
				$image->filename = $destName;
				$image->filepath = $destPath;
				$image->description = $destName;
				$image->alt = $destName;
				$image->width = $width;
				$image->height = $height;
				$image->extension = $fExt;
				$image->created_by = \Auth::user()->id;
				// $image->imageable_type = 'TiendaGis\Banner';
				$image->save();

				if ($request->hasFile('brandImageMouseover')) {
					$destPath = 'uploads/brands/on';
					$brandImageMouseover = $request->file('brandImageMouseover');

					$brandImageMouseover->move($destPath,$destName);

					list($width, $height) = getimagesize($destPath . '/' . $destName);

					$imageOn = new Image;
					$imageOn->name = $destName;
					$imageOn->filename = $destName;
					$imageOn->filepath = $destPath;
					$imageOn->description = $destName;
					$imageOn->alt = $destName;
					$imageOn->width = $width;
					$imageOn->height = $height;
					$imageOn->extension = $fExt;
					$imageOn->created_by = \Auth::user()->id;
					// $image->imageable_type = 'TiendaGis\Banner';
					$imageOn->save();
				}
				
				$bUnitBrands = json_decode($bUnit->brands_json,TRUE);	
				$bUnitBrands[] = [
					'image_id_off'	=> $image->id,
					'image_id_on'	=> $imageOn->id ? $imageOn->id : '',
					'url' => $request->input('url')
				];
				$bUnit->brands_json = json_encode($bUnitBrands);
			}

			if ($request->exists('deleteBanner')) {
				$bannerIndex = $request->input('deleteBanner');
				$bUnitBanners = json_decode($bUnit->banners_json,TRUE);

				unset($bUnitBanners[$bannerIndex]);

				$bUnit->banners_json = json_encode($bUnitBanners);
			}

			if ($request->exists('deleteBannerBottom')) {
				$bannerIndex = $request->input('deleteBannerBottom');
				$bUnitBanners = json_decode($bUnit->banners_bottom_json,TRUE);

				unset($bUnitBanners[$bannerIndex]);

				$bUnit->banners_bottom_json = json_encode($bUnitBanners);
			}

			if ($request->exists('deleteBrand')) {
				$brandIndex = $request->input('deleteBrand');
				$bUnitBrands = json_decode($bUnit->brands_json,TRUE);

				unset($bUnitBrands[$brandIndex]);

				$bUnit->brands_json = json_encode($bUnitBrands);
			}

			$bUnit->save();

			$request->session()->flash('notification', 'Cambios guardados correctamente.');
			$request->session()->flash('type', 'success');
		} else {
			$request->session()->flash('notification', 'Unidad de negocio no encontrada.');
			$request->session()->flash('type', 'warning');
		}

		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function set(Request $request)
	{
		$businessUnitId = $request->input('businessUnitId');
		// $businessUnit = BusinessUnit::find($businessUnitId);

		if ($businessUnitId) {
			Session::put('UsingBusinessUnit',$businessUnitId);
			return redirect()->back();
		} else {
			return redirect()->back()->with('notification','No existe la unidad de negocio seleccionada');
		}
	}
	
	public function saveHeaderMenu(Request $request){
		$id = $request->input('id');
		$menu = json_encode($request->input('menu'));
		
		$bu = BusinessUnit::find($id);
		$bu->header_menu = $menu;
		$bu->save();
		
		return response()->json(['code' => '100']);
	}
}
