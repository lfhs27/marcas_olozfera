<?php

namespace GIS\Http\Controllers\Admin;

use Illuminate\Http\Request;

use GIS\Http\Requests;
use GIS\Http\Controllers\Controller;
use GIS\Sector;
use GIS\Image;

class SectorsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	public function showJson(Request $request)
	{
		$sector = Sector::find($request->get('sector_id'));
		$sector->load(['image']);

		return response()->json($sector);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$fields = $request->all();

		$sector = Sector::find($id);

		$sector->front_name = $fields['sector']['name'];
		$sector->description = $fields['sector']['description'];
		$sector->hovertext = $fields['sector']['hovertext'];

		if (isset($fields['sectorImage'])) {
			$destinationPath = 'uploads/sectors/';
			$extension = $fields['sectorImage']->getClientOriginalExtension();
			$fileName = time() . rand(1111,9999) . '.' . $extension;
			$fields['sectorImage']->move($destinationPath,$fileName);
			$imgSize = getimagesize($destinationPath . '/' . $fileName);

			$image = Image::firstOrNew(['sector_id'=>$sector->id]);
			$image->name = $fields['sectorImage']->getClientOriginalName();
			$image->filename = $fileName;
			$image->sector_id = $sector->id;
			$image->path = $destinationPath;
			$image->width = $imgSize[0];
			$image->height = $imgSize[1];
			$image->location = 'listing';

			$image->save();

			$sector->image_id = $image->id;
		}

		$sector->save();

		return back()->with('notification','Línea de mercado guardada correctamente');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
