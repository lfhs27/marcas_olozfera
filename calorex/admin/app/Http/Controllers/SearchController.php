<?php

namespace CMS\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use View;

use CMS\Http\Requests;
use CMS\BusinessUnit;
use CMS\Brand;
use CMS\Family;
use CMS\Line;
use CMS\Product;
use CMS\User;

class SearchController extends Controller
{
	protected $esRefacciones = false;

	public function __construct()
	{
		$srvHostParts = explode(".",$_SERVER['HTTP_HOST']);
		$subdomains = array_shift($srvHostParts);
		$this->esRefacciones = (strcmp($subdomains, "refacciones") == 0) ? true : false;
	}
	
	// Búsqueda en Front End de la TIENDA - Búsqueda general
	public function buscar($slug = null)
	{
		$query = urldecode($_GET['query']);

		switch ($query) {
			case 'depaso':
				$query = 'de paso';
				break;
			case 'solares':
				$query = 'solar';
				break;
			case 'calefactores':
				$query = 'calefactor';
				break;
			case 'boiler':
			case 'calentadores':
				return redirect('/calentadores');
				break;
			// case 'bombas':
			// 	$query = 'bomba';
			// 	break;
			// case 'tanques':
			// 	$query = 'tanque';
			// 	break;
			// case 'funcosa':
			// 	return redirect('/fluida/marcas/funcosa');
			// 	break;
			// case 'cifunsa':
			// 	return redirect('/fluida/marcas/cifunsa');
			// 	break;
		}
		
		if(is_null($slug)){
			$families = Family::select('id')->where('slug','like','%' . strtolower($query) . '%')
								->orWhere('name','like','%' . strtolower($query) . '%')
								->orWhere('front_name','like','%' . strtolower($query) . '%')
								->get()->lists('id')->toArray();
		}else{			
			$families = Family::select('id')->where('slug',$slug)
								->get()->lists('id')->toArray();
		}

		// $family = Line::whereIn('family_id',$families)->get();

		if($this->esRefacciones){
			$products = Product::whereIn('family_id',$families)->where('name', 'LIKE', '%'.$query.'%')->get();
		}else{
			$products = Product::whereIn('family_id',$families)->get()->filter(function($product) use ($slug){
				if (strcmp($slug, "refacciones") == 0 || ($product->family && $product->family->name != 'REFACCIONES')) { 
					return $product;
				}
			});
		}
	
		// $products = DB::select('select * from products where lower(name) like "%'.strtolower($query).'%"');
		
		// $products = array();
		// foreach($results as $product){
		// 	$p = Product::find($product->id);
		// 	if($p)
		// 		array_push($products, $p);
		// }

		View::share('businessUnits',BusinessUnit::all());
		
		return view('store.products')->with('products',$products);
	}

	public function brandFamilies(Request $request)
	{
		$families = Family::where(['brand_id' => $request->input('objectId')])->get();

		return response()->json($families);
	}

	public function familyLines(Request $request)
	{
		$lines = Line::where(['family_id' => $request->input('objectId')])->get();

		return response()->json($lines);
	}

	public function postLiveSearchProducts(Request $request)
	{
		$productQuery = $request->exists('productQuery') ? $request->input('productQuery') : false;

		$productDbSearch = Product::where('name','like',"%".$productQuery."%")
			->orWhere('segment1','like',"%".$productQuery."%")
			->orWhere('original_name','like',"%".$productQuery."%")
			->get();

		// $productDbSearch->load(['brand','family','line']);

		return response()->json($productDbSearch);
	}

	public function postLiveSearchSiblings(Request $request)
	{
		$query = $request->get('productId');

		$product = Product::find($request->input('productId'));

		$siblings = Product::where([
			'brand_id'		=> $product->brand_id,
			'family_id'		=> $product->family_id,
			'line_id'		=> $product->line_id
			])->get();

		return response()->json($siblings);
	}


	public function postLiveSearch(Request $request)
	{
		$query = $request->get('query');
		$scope = $request->get('scope');

		switch ($scope) {
			case 'bunit':
				$dbQuery = BusinessUnit::where('name','like',"%".$query."%")
					->orWhere('slug','like',"%".$query."%");
				break;
			case 'brand':
				$dbQuery = Brand::where('name','like',"%".$query."%")
					->orWhere('front_name','like',"%".$query."%")
					->orWhere('slug','like',"%".$query."%");
				break;

			case 'family':
				$dbQuery = Family::where('name','like',"%".$query."%")
					->orWhere('front_name','like',"%".$query."%")
					->orWhere('slug','like',"%".$query."%");
				break;

			case 'line':
				$dbQuery = Line::where('name','like',"%".$query."%")
					->orWhere('front_name','like',"%".$query."%")
					->orWhere('slug','like',"%".$query."%");
				break;

			case 'product':
				$dbQuery = Product::where('name','like','%'.$query.'%')
					->orWhere('short_name','like','%'.$query.'%');
				break;
		}

		$results = $dbQuery->get();

		return response()->json(['query'=>$query,'scope'=>$scope,'results' => $results]);
	}

	public function getUserSearch(Request $request)
	{
		$searchTerm = $request->get('searchTerm');

		$results = User::select(['id','name','lastname','employee_number','email','company_id','user_group_id'])->where('email','like',"%".$searchTerm."%")
			->orWhere('name','like',"%".$searchTerm."%")
			->orWhere('lastname','like',"%".$searchTerm."%")
			->orWhere('employee_number','like',"%".$searchTerm."%")
			->get();

		$results->load(['company','group']);

		return response()->json($results);
	}

	public function getProductSearch(Request $request)
	{
		$productQuery = $request->get('productQuery');

		$productDbSearch = Product::select(['id','name','short_name','segment1','business_unit_id','brand_id','family_id','line_id'])
									->where('name','LIKE',"%".$productQuery."%")
									->orWhere('short_name','LIKE',"%".$productQuery."%")
									->orWhere('original_name','LIKE',"%".$productQuery."%")
									->orWhere('description','LIKE',"%".$productQuery."%")
									->whereHas('family',function($q){
										$q->where('slug','<>','refacciones');
									})
									->get();

		$productDbSearch->load(['brand','family','line'])->toArray();

		return response()->json($productDbSearch);
	}
	
	public function getIdealModels(Request $request){
		DB::connection()->enableQueryLog();
		$options = $request->input('request');
		unset($options["filtros_categorias"]);
		unset($options["filtros_marcas"]);
		$site = $request->input('site');
		$brands = $request->input('brands');
		$families = $request->input('families');
		$models = array();
		$idModels = array(); //var_dump($brands);
		$modulantes = Family::where(["slug" => "instantaneo-modulante"])->first();
		$calefactores = Family::where(["slug" => "calefactores"])->first();
		$titanio = Family::where(["slug" => "titanio"])->first();
		$exclude = array($titanio->id,$calefactores->id);
		if(strcmp($site, "liverpool") == 0){
			$calorex = Brand::where(["slug" => "calorex"])->first();
			$brands = array($calorex->id);
		}
		foreach($options as $attr => $option){
			if($brands && $families){
				$models[$attr] = DB::table("products")
									->join('business_units', 'products.business_unit_id', '=', 'business_units.id')
									->join('families', 'products.family_id', '=', 'families.id')
									->where("business_units.slug", 'calentadores')
									->where("families.slug", '!=', 'refacciones')
									->where("products.".$attr, 'LIKE', '%'.$option.'%')
									->whereIn("products.brand_id", $brands)
									->whereIn("products.family_id", $families)
									->select("products.id");
									
				if(strcmp($site, "liverpool") == 0)
					$models[$attr] = $models[$attr]->whereNotIn("products.family_id", $exclude)->get();
					//$models[$attr] = $models[$attr]->where("products.family_id", "!=", $modulantes->id)->get();
				else
					$models[$attr] = $models[$attr]->get();
			}else if($brands){
				$models[$attr] = DB::table("products")
									->join('business_units', 'products.business_unit_id', '=', 'business_units.id')
									->join('families', 'products.family_id', '=', 'families.id')
									->where("business_units.slug", 'calentadores')
									->where("families.slug", '!=', 'refacciones')
									->where("products.".$attr, 'LIKE', '%'.$option.'%')
									->whereIn("products.brand_id", $brands)
									->select("products.id");
									
				if(strcmp($site, "liverpool") == 0)
					$models[$attr] = $models[$attr]->whereNotIn("products.family_id", $exclude)->get();
				else
					$models[$attr] = $models[$attr]->get();
			}else if($families){
				$models[$attr] = DB::table("products")
									->join('business_units', 'products.business_unit_id', '=', 'business_units.id')
									->join('families', 'products.family_id', '=', 'families.id')
									->where("business_units.slug", 'calentadores')
									->where("families.slug", '!=', 'refacciones')
									->where("products.".$attr, 'LIKE', '%'.$option.'%')
									->whereIn("products.family_id", $families)
									->select("products.id");
									
				if(strcmp($site, "liverpool") == 0)
					$models[$attr] = $models[$attr]->whereNotIn("products.family_id", $exclude)->get();
				else
					$models[$attr] = $models[$attr]->get();
			}else{ //echo "ENTRO!!!";
				//$models[$attr] = Product::where($attr, 'LIKE', '%'.$option.'%');
				$models[$attr] = DB::table("products")
									->join('business_units', 'products.business_unit_id', '=', 'business_units.id')
									->join('families', 'products.family_id', '=', 'families.id')
									->where("business_units.slug", 'calentadores')
									->where("families.slug", '!=', 'refacciones')
									->where("products.".$attr, 'LIKE', '%'.$option.'%')
									->select("products.id");
									
				if(strcmp($site, "liverpool") == 0)
					$models[$attr] = $models[$attr]->whereNotIn("products.family_id", $exclude)->get();
				else
					$models[$attr] = $models[$attr]->get();
			}
			$idModels[$attr] = array();
			$query = DB::getQueryLog();
			$lastq = end($query);
			//var_dump($lastq);
		}
		foreach($models as $attr => $model){
			if(count($model) > 0){
				foreach($model as $m){
					array_push($idModels[$attr], $m->id);
				}
			}else{
				unset($idModels[$attr]);
			}
		}
		//array_push($idModels, array($this, "custom_intersect"));
		//var_dump($idModels);
		if(count($idModels) > 1)
			$response = call_user_func_array("array_intersect", $idModels);
		else
			$response = array_pop($idModels);
			
		/*for($i = 0; $i < count($response); $i++){
			$response[$i] = ProductModel::find($response[$i]);
			if($response[$i])
				$response[$i]->load("product")->load("product.gallery");
			else
				unset($response[$i]);
		}*/
		if(!is_null($response)){
			foreach($response as $i => $val){
				$response[$i] = Product::select(['id','liverpool_url_alt','liverpool_url','brand_id','family_id','name'])->find($response[$i]);
				if($response[$i])
					$response[$i]->load("gallery")->load("gallery.images");
				else
					unset($response[$i]);
			}
		}
		return response()->json($response);
	}
	
	public function custom_intersect($a, $b){
		
		return $a->id == $b->id;
	}
	
	public function getBrandCatalog($brandId)
	{
		$brand = Brand::find($brandId);
		//$brand->load(['families.products.gallery' , 'products.gallery' , 'products.family' ]);
		$brand->load(['families' ]);

		$page = array();

		$page['brand'] = $brand;

		return response()->json($page);
	}

	public function comparador(Request $request)
	{
		$families = Family::select('id')->where('slug',$request->input('familySlug'))
								->get()->lists('id')->toArray();

		if ( $request->exists('brand') ) {
			$brandLines = Line::whereBrandId($request->input('brand'))->whereIn('family_id',$families)->get();

			return response()->json($brandLines);
		}

		if ( $request->exists('line') ) {
			$lineProducts = Product::select(['name','id'])->whereLineId($request->input('line'))->get();

			return response()->json($lineProducts);
		}

		if ( $request->exists('product') ) {
			$selectedProduct = Product::find($request->input('product'));

			$productSpecs = $selectedProduct->specifications;

			return response()->json($productSpecs);
		}
	}
}
