<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;

class Specification extends Model
{
	public function product()
	{
		$this->belongsTo('CMS\Product');
	}
}
