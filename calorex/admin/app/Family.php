<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Family extends Model
{
	use SoftDeletes;

    public $fillable = ['name','business_unit_id','brand_id'];

    public function businessUnit()
	{
		return $this->belongsTo('CMS\BusinessUnit');
	}

	public function products()
	{
		return $this->hasMany('CMS\Product')->where(['is_visible' => 1]);
	}

	public function author()
	{
		return $this->belongsTo('CMS\User','created_by');
	}

	public function brand()
	{
		return $this->belongsTo('CMS\Brand');
	}

	public function lines()
	{
		return $this->hasMany('CMS\Line')->orderBy("order", "asc");
	}
}
