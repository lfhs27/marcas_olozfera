<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Line extends Model
{
	use SoftDeletes;

    public $fillable = ['name','business_unit_id','brand_id','family_id'];

    public function businessUnit()
	{
		return $this->belongsTo('CMS\BusinessUnit');
	}

	public function products()
	{
		return $this->hasMany('CMS\Product')->where(['is_visible' => 1]);
	}

	public function firstProduct()
	{
		return $this->products()->whereHas('family',function($query){
			$query->where('slug','!=','refacciones');
		})->first();
	}

	public function author()
	{
		return $this->belongsTo('CMS\User','created_by');
	}

	public function brand()
	{
		return $this->belongsTo('CMS\Brand');
	}

	public function family()
	{
		return $this->belongsTo('CMS\Family');
	}
}
