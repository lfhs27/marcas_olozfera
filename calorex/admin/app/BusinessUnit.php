<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessUnit extends Model
{
	use SoftDeletes;
	
	public function brands()
	{
		return $this->hasMany('CMS\Brand')->orderBy('created_at');
	}

	public function logo()
	{
		return $this->hasOne('CMS\Logo');
	}

	public function families()
	{
		return $this->hasMany('CMS\Family');
	}

	public function lines()
	{
		return $this->hasMany('CMS\Line');
	}

	public function products()
	{
		return $this->hasMany('CMS\Product');
	}

	public function author()
	{
		return $this->belongsTo('CMS\User','created_by');
	}

	public function sliders()
	{
		return $this->hasMany('CMS\Slider');
	}

	public function menus()
	{
		return $this->hasMany('CMS\Menu');
	}

	public function mainMenu()
	{
		return $this->hasOne('CMS\Menu');
	}

	// public function businessUnits()
	// {
	// 	return $this->belongsToMany('CMS\BusinessUnit');
	// }
	
	public function orders()
	{
		return $this->belongsToMany('CMS\Order')->orderBy('id','DESC');
	}

	public function centers()
	{
		return $this->hasMany('CMS\Center');
	}

	public function billingGroups()
	{
		return $this->belongsToMany('CMS\BillingGroup')->withPivot('sold_to_org','ship_to_org','invoice_to_org');
	}

	public function userGroups()
	{
		return $this->belongsToMany('CMS\Role');
	}
}
