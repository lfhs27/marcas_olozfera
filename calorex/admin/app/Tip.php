<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tip extends Model
{
	
	public $table = "tips";

    public function tags()
    {
    	return $this->hasMany('CMS\Tip_tag');
    }

    public function metadata()
    {
    	return $this->hasMany('CMS\Tip_metadata');
    }
}
