<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tip_metadata extends Model
{
	
	public $table = "tips_metadata";

    public function tip()
    {
    	return $this->belongsTo('CMS\Tip');
    }
}
