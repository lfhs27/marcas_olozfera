				<div id="mainnav">

					<!--Menu-->
					<!--================================-->
					<div id="mainnav-menu-wrap">
						<div class="nano">
							<div class="nano-content">
								<ul id="mainnav-menu" class="list-group">
						
									<!--Category name-->
									<li class="list-header">Menú Principal</li>						
									<!--Menu list item-->
									<li>
										<a href="{{ action('Admin\DashboardController@index') }}">
											<i class="fa fa-dashboard"></i>
											<span class="menu-title">
												<span class="menu-title">Inicio</span>
											</span>
										</a>
										<!--Submenu-->
										<ul class="collapse" aria-expanded="false" style="">
											<li><a href="{{ action('Admin\DashboardController@index') }}">Slideshow</a></li>
											<li><a href="{{ action('Admin\BackgroundsController@index_backgrounds') }}">Fondos</a></li>
											<li><a href="{{ action('Admin\DashboardController@productos') }}">Productos</a></li>
											<li><a href="{{ action('Admin\DashboardController@indexBanners') }}">Banners</a></li>
										</ul>
									</li>
									<li>
										<a href="{{ action('Admin\BannersController@index') }}">
											<i class="fa fa-file-image-o"></i>
											<span class="menu-title">
												<span class="menu-title">Banners</span>
											</span>
										</a>
									</li>
									<li>
										<a href="{{ action('Admin\BackgroundsController@index') }}">
											<i class="fa fa-file-image-o"></i>
											<span class="menu-title">
												<span class="menu-title">Fondos</span>
											</span>
										</a>
									</li>
									<li>
										<a href="{{ action('Admin\AddressesController@index') }}">
											<i class="fa fa-truck"></i>
											<span class="menu-title">
												<span class="menu-title">Distribuidores</span>
											</span>
										</a>
									</li>
									<li>
										<a href="{{ action('Admin\TipsController@index') }}">
											<i class="fa fa-list"></i>
											<span class="menu-title">
												<span class="menu-title">Blog</span>
											</span>
										</a>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-briefcase"></i>
											<span class="menu-title"><strong>Productos</strong></span>
											<i class="arrow"></i>
										</a>
						
										
										<ul class="collapse">
											<li><a href="{{ action('Admin\ProductsController@index') }}">Listado</a></li>
											<li class="list-divider"></li>
											<li><a href="{{action('Admin\BrandsController@index')}}">Marcas</a></li>
											<li><a href="{{ action('Admin\FamiliesController@index') }}">Categorías</a></li>
											<li><a href="{{ action('Admin\LinesController@index') }}">Subcategorías</a></li>

											<li><a href="{{ action('Admin\AdditionalItemsController@index') }}">Items Adicionales</a></li>

											<!-- <li><a href="#">Grupos</a></li> -->
										</ul>
									</li>
								</ul>

							</div>
						</div>
					</div>
					<!--================================-->
					<!--End menu-->

				</div>