<div class="modal fade" id="modal-add-company">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Agregar Empresa</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(array('url' => '/admin/companies','id'=>'form-add-company','class'=>'form-horizontal')) !!}
				{{-- <form action="#" method="POST" role="form" class="form-horizontal" id="form-add-company"> --}}
					<div class="form-group">
						<label class="control-label col-sm-4">Nombre Comercial</label>
						<div class="col-sm-7">
							<input name="companyName" type="text" class="form-control" id="" placeholder="Calorex">
							<div class="checkbox">
								<label>
									<input name="belongsGISVerify" type="checkbox" value="1">
									Pertenece a GIS
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-4">Código</label>
						<div class="col-sm-7">
							<input name="companyCode" type="text" class="form-control" id="" placeholder="CXcalentadores">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-4">Dominio</label>
						<div class="col-sm-7">
							<input name="companyDomain" type="text" class="form-control" id="" placeholder="calorex.com.mx">
							<div class="checkbox">
								<label>
									<input name="companyDomainVerify" type="checkbox" value="1">
									Verificar Dominio
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-4">Tipo de Pago</label>
						<div class="col-sm-7">
							<div class="checkbox">
								<label>
									<input name="allowed_payment_methods[]" type="checkbox" value="PAYMENT_METHOD_PAYROLL">
									Nómina
								</label>
							</div>

							<div class="checkbox">
								<label>
									<input name="allowed_payment_methods[]" type="checkbox" value="PAYMENT_METHOD_PAYU">
									PAYU
								</label>
							</div>

							<div class="checkbox">
								<label>
									<input name="allowed_payment_methods[]" type="checkbox" value="PAYMENT_METHOD_PAYPAL">
									PAYPAL
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-4">Envío</label>
						<div class="col-sm-7">
							<div class="checkbox">
								<label>
									<input name="allowed_shipping_methods[]" type="checkbox" value="SHIPPING_METHOD_IMMEDIATE_DELIVERY">
									Entrega Inmediata
								</label>
							</div>

							<div class="checkbox">
								<label>
									<input name="allowed_shipping_methods[]" type="checkbox" value="SHIPPING_METHOD_CEDIS">
									CEDIS
								</label>
							</div>

							<div class="checkbox">
								<label>
									<input name="allowed_shipping_methods[]" type="checkbox" value="SHIPPING_METHOD_HOME_DELIVERY">
									Domicilio
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-4">Cuota Mensual por Empleado</label>
						<div class="col-sm-7">
							<input name="employeeMonthlyFee" type="number" class="form-control" id="" placeholder="1000" step="0.01" min="0">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-4">Número de Invitados</label>
						<div class="col-sm-7">
							<input name="allows_guests" type="number" class="form-control" id="" placeholder="0" />
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<a href="#form-add-company" class="btn-save btn btn-primary">Guardar</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-import-employees">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h4 class="modal-title">Importar Empleados</h4>
</div>
{!! Form::open(array('url'=>'/admin/companies/csv','method'=>'POST', 'files'=>true)) !!}
<input type="hidden" name="company_id" value="{{ $company->id }}" />
<div class="modal-body">
<div class="control-group">
<div class="controls">
{!! Form::file('file') !!}
<p class="errors">{!!$errors->first('image')!!}</p>
@if(Session::has('error'))
<p class="errors">{!! Session::get('error') !!}</p>
@endif
</div>
</div>
<div id="success"> </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
{!! Form::submit('Importar', array('class'=>'btn btn-primary')) !!}
</div>
{!! Form::close() !!}
</div>
</div>
</div>