<div class="modal fade" id="modal-version-elect220">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Selecciona el modelo 220v</h4>
			</div>
			<div class="modal-body">
				<input type="text" name="modelo220v" value="" placeholder="INGRESE SKU" class="form-control" id="input_modelo220V">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary btn-guardar-version-220v">Guardar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-create-product-object">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Guardar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-add-version-natural">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Selecciona el modelo GAS NATURAL</h4>
			</div>
			<div class="modal-body">
				<form action="/admin/products" class="form" id="product-filter" data-which="altern">
				{{-- {!! Form::open(array('url' => 'admin/products', 'class'=>'form', 'id'=>'product-filter', 'data-which' => 'altern')) !!} --}}
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label><strong>MARCA</strong></label><br>
								<select name="brands" class="form-control">
									<option value="0">--</option>
									<?php foreach ($brands as $brand): ?>
										<option value="<?php echo $brand['MARCA']; ?>"><?php echo $brand['MARCA']; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12">
							<label><strong>SECTOR</strong></label><br>
							<select name="sectors" class="form-control">
								
							</select>
						</div>

						<div class="col-xs-12 col-sm-12">
							<label><strong>LÍNEA</strong></label><br>
							<select name="families" class="form-control">
								
							</select>
						</div>

						<div class="col-xs-12 col-sm-12">
							<label><strong>PRODUCTOS</strong></label><br>
							<select name="products" class="form-control">
								
							</select>
							<div class="row">
								<div class="col-sm-6 pad-all">
									<strong>Puede seleccionar varias opciones haciendo
									click y arrastrando el mouse.</strong>
								</div>
							</div>
						</div>
					</div>
				</form>
				{{-- {!! Form::close() !!}				 --}}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary btn-add-altern-version">Agregar</button>
			</div>
		</div>
	</div>
</div>