@extends('layouts.admin')

@section('content')

<div class="panel panel-default">
	<div class="panel-body">
		<table class="table table-hover" data-toggle="table" data-search="true" data-pagination="true" id="demo-dt-basic">
		{{-- <table class="table table-hover" data-toggle="table" data-search="true" id="datatable-users"> --}}
			<thead>
				<tr>
					<th>#</th>
					<th>N&uacute;mero de Empleado</th>
					<th>Nombre</th>
					<th>Email</th>
					<th>Teléfono Celular</th>
					<th>Empresa</th>
					<th>Tipo de Usuario</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php $flag = 1; ?>
				@foreach ($usersList as $singleUser)
				<tr>
					<td>{{ $flag }}</td>
					<td>{{ isset($singleUser->employee_number) ? $singleUser->employee_number : '' }}</td>
					<td>{{ isset($singleUser->name) ? $singleUser->name : '' }} {{ isset($singleUser->lastname) ? $singleUser->lastname : '' }}</td>
					<td>{{ isset($singleUser->email) ? $singleUser->email : '' }}</td>
					<td>{{ isset($singleUser->phone_mobile) ? $singleUser->phone_mobile : '' }}</td>
					<td>{{ isset($singleUser->Company->name) ? $singleUser->Company->name : '' }}</td>
					<td>{{ isset($singleUser->type) ? $singleUser->type : '' }}</td>
					<td>
						<?php if ($user->hasRole('Users_Write')): ?>
							{!! Form::open(['action'=>['Admin\UsersController@destroy',$singleUser->id],'method'=>'DELETE']) !!}
							<a class="btn btn-xs btn-info pull-left" href="{{url('/admin/users/' . $singleUser->id . '/edit')}}" title="Editar"><i class="fa fa-pencil"></i></a>
							<button type="submit" class="btn btn-warning btn-xs pull-right"><i class="fa fa-trash"></i></button>
							{!! Form::close() !!}
						<?php endif ?>
					</td>
				</tr>
				<?php $flag++; ?>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

@endsection