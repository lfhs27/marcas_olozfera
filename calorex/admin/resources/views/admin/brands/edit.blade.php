@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
<!--===================================================-->
<!--div id="content-container"-->
	
	@include('admin.shared.page-title')		

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		
		@include('admin.shared.page-section-title')

		<a class="btn-link" href="{{ action('Admin\BrandsController@index') }}"><i class="fa fa-angle-left"></i> Regresar al listado</a>
		<br>
		<div class="panel panel-default">
			<div class="panel-body">
				{!! Form::open(['action'=>['Admin\BrandsController@update',$brand->id],'method' => 'PATCH']) !!}
				<div class="form-group">
					<label for="" class="control-label">Nombre</label>
					<input type="text" name="name" id="inputName" class="form-control" value="{{$brand->front_name}}" required="required">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<!--===================================================-->
	<!--End page content-->


<!--/div-->
<!--===================================================-->
<!--END CONTENT CONTAINER-->

@endsection