	<!--JAVASCRIPT-->
	<!--=================================================-->

	<!-- <script src="{{ asset('assets/admin/js/jquery-loadTemplate/jquery.loadTemplate-1.5.0.min.js') }}"></script> -->

	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="{{ asset('assets/nifty/js/bootstrap.min.js') }}"></script>


	<!--Fast Click [ OPTIONAL ]-->
	<script src="{{ asset('assets/nifty/plugins/fast-click/fastclick.min.js') }}"></script>

	
	<!--Nifty Admin [ RECOMMENDED ]-->
	<script src="{{ asset('assets/nifty/js/nifty.min.js') }}"></script>


	<!--Switchery [ OPTIONAL ]-->
	<script src="{{ asset('assets/nifty/plugins/switchery/switchery.min.js') }}"></script>


	<!--Bootstrap Select [ OPTIONAL ]-->
	<script src="{{ asset('assets/nifty/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>

	<!--Bootbox Modals [ OPTIONAL ]-->
	<script src="{{ asset('assets/nifty/plugins/bootbox/bootbox.min.js') }}"></script>

	<!--Bootstrap Wizard [ OPTIONAL ]-->
	<script src="{{ asset('assets/nifty/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>

	<!-- Dropzone -->
	<script src="{{ asset('assets/nifty/plugins/dropzone/dropzone.min.js') }}"></script>

	<script src="{{ asset('assets/nifty/plugins/summernote/summernote.min.js') }}"></script>


	<!--DataTables [ OPTIONAL ]-->
	<script src="{{ asset('assets/nifty/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('assets/nifty/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
	<script src="{{ asset('assets/nifty/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>



	<!--Demo script [ DEMONSTRATION ]-->
	<script src="{{ asset('assets/nifty/js/demo/nifty-demo.min.js') }}"></script>
	<script src="{{ asset('assets/nifty/js/demo/tables-datatables.js') }}"></script>

	<script src="{{ asset('assets/lib/bootstrap-colorpicker-master/dist/js/bootstrap-colorpicker.min.js') }}"></script>

	<script src="{{ asset('assets/admin/js/gis.js') }}"></script>

	<script src="{{ asset('assets/admin/js/gis.brands.js') }}"></script>
	<script src="{{ asset('assets/admin/js/gis.orders.js') }}"></script>
	<script src="{{ asset('assets/admin/js/gis.products.js') }}"></script>
	<script src="{{ asset('assets/admin/js/tinymce/tinymce.min.js') }}"></script>

	<script src="{{ asset('vendors/jquery-ui-1.11.4.custom/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('vendors/jquery-ui-1.11.4.custom/jquery-ui-autocomplete.min.js') }}"></script>
	<script src="{{ asset('vendors/jquery-tabledit-1.2.3/jquery.tabledit.min.js') }}"></script>
	
	<!--

	REQUIRED
	You must include this in your project.

	RECOMMENDED
	This category must be included but you may modify which plugins or components which should be included in your project.

	OPTIONAL
	Optional plugins. You may choose whether to include it in your project or not.

	DEMONSTRATION
	This is to be removed, used for demonstration purposes only. This category must not be included in your project.

	SAMPLE
	Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


	Detailed information and more samples can be found in the document.

	-->