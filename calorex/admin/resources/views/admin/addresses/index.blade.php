@extends('layouts.admin')

@section('content')

<span class="btn btn-success" data-toggle="modal" data-target="#addressModal">Agregar Distribuidor</span>
<span class="btn btn-primary" data-toggle="modal" data-target="#importModal">Importar Distribuidores</span><form id="fOrder" action="/addresses/update_order" method="post" style="display:inline;">	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">	<input type="hidden" name="addresses_order" value="" />	<input type="submit" class="btn btn-warning" value="Guardar Orden" /></form>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
	<div class="eq-height">
		<div class="col-sm-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						Distribuidores Registrados
					</h3>
				</div>
				<div class="panel-body">
					<table id="table-addresses" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Nombre Comercial</th>
								<th>Sucursal</th>
								<th>Direcci&oacute;n</th>
								<th>Estado</th>
								<th>Municipio</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody class="boxes">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="Direcci&oacute;n">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Direcci&oacute;n</h4>
			</div>
			<div class="modal-body">
				<form id="fAddress">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id" value="0" />
					<div class="row form-group">
						<div class="col-xs-4">
							<input type="text" class="form-control" name="nombre_comercial" placeholder="Nombre Comercial" />
						</div>
						<div class="col-xs-4">
							<input type="text" class="form-control" name="razon_social" placeholder="Raz&oacute;n Social" />
						</div>
						<div class="col-xs-4">
							<input type="text" class="form-control" name="sucursal" placeholder="Sucursal" />
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-6">
							<input type="text" class="form-control" name="direccion" placeholder="Direcci&oacute;n" />
						</div>
						<div class="col-xs-3">
							<input type="text" class="form-control" name="colonia" placeholder="Colonia" />
						</div>
						<div class="col-xs-3">
							<input type="text" class="form-control" name="codigo_postal" placeholder="C&oacute;digo Postal" />
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-6">
							<select class="form-control" name="estado">
								<option value="0">Estado</option>
								@foreach($estados as $estado)
									<option value="{{ $estado->id }}">{{ $estado->name }}</value>
								@endforeach
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="municipio">
								<option>Municipio</option>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-6">
							<input type="text" class="form-control" name="telefono" placeholder="Tel&eacute;fono" />
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="saveAddress()">Guardar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="importModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Importar Distribuidores</h4>
			</div>
			<form method="POST" action="/addresses/csv" accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="modal-body">
				<div class="control-group">
					<div class="controls">
						<input name="file" type="file">
						<p class="errors"></p>
					</div>
				</div>
				<div id="success"> </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<input class="btn btn-primary" type="submit" value="Importar">
			</div>
			</form>
		</div>
	</div>
</div>
<script>
var table;
$(document).ready(function(){
	//table = $('#table-addresses').DataTable();		$("#fOrder").submit(function(e){		sorted = $('.boxes').sortable('toArray',{attribute: 'data-model-id'});		sortedObj = $.map(sorted, function(val, i){			return {					'id' : val,					'order' : (parseInt(i) + 1)				};		});		$(this).find("[name=addresses_order]").val(JSON.stringify(sortedObj));			});
	getAddresses();
	$("select[name=estado]").change(function(e){
		idEstado = $(this).val();
		
		if(idEstado != 0){
			loadMunicipios(idEstado, 0);
		}
	});
	$(".deleteBanner").click(function(e){
		e.preventDefault();
		elm = $(this);
		
		$.ajax({
			url : "banners/delete/"+elm.attr("data-id"),
			method : "GET",
			success : function(data){
				location.reload();
			}
		});
		
		return false;
	});
});
function saveAddress(){
	var token = $('input[name=_token]').val();
	data = $("#fAddress").serialize();
	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : token }
	});
		
	$.ajax({
		url: '/addresses/saveAddress',
		type: 'GET',
		data: data,
		dataType: 'json',
		contentType: false,
		processData: false
	})
	.done(function(response) {
		resetModal();
		getAddresses();
	});
}
function getAddresses(){
	//table.clear();
	$.ajax({
		url : "addresses/getAllAddresses",
		method : "GET",
		success : function(data){
			html = '';
			if($.isArray(data)){				
				$.each(data, function(i, val){									html += '<tr data-model-id="'+val.id+'">';					html += '	<td>'+val.nombre_comercial+'</td>';					html += '	<td>'+val.sucursal+'</td>';					html += '	<td>'+val.direccion+'</td>';					html += '	<td>'+val.municipio.estado.name+'</td>';					html += '	<td>'+val.municipio.municipio+'</td>';					html += '	<td><span class="btn btn-purple pull-left btn-sm" style="margin-right: 5px;" onclick="loadAddress('+val.id+')"><i class="fa fa-pencil"></i></span><span class="btn btn-danger pull-left btn-sm" style="margin-right: 5px;" onclick="deleteAddress('+val.id+')"><i class="fa fa-times"></i></span></td>';					html += '</tr>';
					/*table.row.add( [
							val.nombre_comercial,
							val.sucursal,
							val.direccion,
							val.municipio.estado.name,
							val.municipio.municipio,
							'<span class="btn btn-purple pull-left btn-sm" style="margin-right: 5px;" onclick="loadAddress('+val.id+')"><i class="fa fa-pencil"></i></span><span class="btn btn-danger pull-left btn-sm" style="margin-right: 5px;" onclick="deleteAddress('+val.id+')"><i class="fa fa-times"></i></span>'
						] ).draw( true );*/
				});
			}			$('#table-addresses tbody').html(html);			$('.boxes').sortable({				opacity: 0.6,				tolerance: "pointer"			});
		}
	});
}
function loadAddress(id){
	$.ajax({
		url : "addresses/getAddress/"+id,
		method : "GET",
		dataType : 'json',
		success : function(data){
			$("#fAddress [name=id]").val(data.id);
			$("#fAddress [name=nombre_comercial]").val(data.nombre_comercial);
			$("#fAddress [name=razon_social]").val(data.razon_social);
			$("#fAddress [name=sucursal]").val(data.sucursal);
			colonia = data.direccion.split(", ");
			$("#fAddress [name=direccion]").val(colonia[0]);
			$("#fAddress [name=colonia]").val(colonia[colonia.length - 1]);
			$("#fAddress [name=codigo_postal]").val(data.codigo_postal);
			$("#fAddress [name=estado]").val(data.municipio.estado_id);
			loadMunicipios(data.municipio.estado_id, data.municipio_id);
			$("#fAddress [name=telefono]").val(data.telefono);
			
			$("#addressModal").modal("show");
		}
	});
}
function loadMunicipios(estado, municipio){
	$.ajax({
		url : "addresses/getMunicipios/"+estado,
		method : "GET",
		success : function(data){
			html = '';
			if($.isArray(data.municipios)){
				$.each(data.municipios, function(i, val){
					html += '<option value="'+val.id+'">'+val.municipio+'</option>';
				});
			}
			$("select[name=municipio]").html(html);
			if(municipio > 0){
				$("select[name=municipio]").val(municipio);
			}
		}
	});
}
function deleteAddress(id){
	if(confirm("\u00BFEst\u00e1 seguro que quiere eliminar al distribuidor?")){
		$.ajax({
			url : "addresses/delete/"+id,
			method : "GET",
			success : function(data){
				getAddresses();
			}
		});
	}
}
function resetModal(){
	$("#fAddress [name=id]").val(0);
	$("#fAddress [name=nombre_comercial]").val("");
	$("#fAddress [name=razon_social]").val("");
	$("#fAddress [name=sucursal]").val("");
	$("#fAddress [name=direccion]").val("");
	$("#fAddress [name=colonia]").val("");
	$("#fAddress [name=codigo_postal]").val("");
	$("#fAddress [name=estado]").val("");
	$("#fAddress [name=municipio]").html("<option>Elegir un Estado</option>");
	$("#fAddress [name=telefono]").val("");
	$("#fAddress [name=calorex]").prop("checked", false);
	$("#fAddress [name=cinsa]").prop("checked", false);
	
	$("#addressModal").modal("hide");
}
</script>

{{-- @include('admin.modals.company') --}}

@endsection