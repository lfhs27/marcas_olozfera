@extends('layouts.admin')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
	<div class="eq-height">
		@if(Session::has('success'))
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">{!! Session::get('success') !!}</div>
		</div>
		@endif
		@foreach($backgrounds as $i => $bg)
		@if($i%2 == 1 && $i > 1)
		</div>
		@endif
		@if($i%2 == 1)
		<div class="row">
		@endif
		<div class="col-sm-6">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						{{ $bg["titulo"] }}
					</h3>
				</div>
				<div class="panel-body">
					<img src="http://admincalorex.softdepotserver2.com/{{ $bg['src']['src'] }}" alt="" style="width:100%;" />
					<form method="POST" action="banners/upload" accept-charset="UTF-8" enctype="multipart/form-data">
						<input name="_token" type="hidden" value="{{ csrf_token() }}">
						<div class="control-group">
							<div class="controls">
								<input name="image" type="file">
								<p class="errors">{!!$errors->first('image')!!}</p>
								@if(Session::has('error'))
								<p class="errors">{!! Session::get('error') !!}</p>
								@endif
								<input type="hidden" name="liga" value="" />
								<input type="hidden" name="type" value="{{ $bg['type'] }}" />
								<input type="hidden" name="title" value="" />
							</div>
						</div>
						<div id="success"> </div>
						<input class="btn btn-primary send-btn" type="submit" value="Guardar">
					</form>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
<script>
$(document).ready(function(){
	$(".deleteBanner").click(function(e){
		e.preventDefault();
		elm = $(this);
		
		$.ajax({
			url : "banners/delete/"+elm.attr("data-id"),
			method : "GET",
			success : function(data){
				location.reload();
			}
		});
		
		return false;
	});
});
</script>

{{-- @include('admin.modals.company') --}}

@endsection