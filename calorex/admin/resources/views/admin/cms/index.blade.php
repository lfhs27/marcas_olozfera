@extends('layouts.admin')

@section('content')

<h1>Administración de Contenido</h1>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
	<div class="eq-height">
		<div class="col-sm-6 eq-box-sm">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						Banner Superior
					</h3>
				</div>
				<div class="panel-body">
					{!! Form::open(array('url'=>'apply/upload','method'=>'POST', 'files'=>true)) !!}
					<div class="control-group">
						<div class="controls">
							{!! Form::file('image') !!}
							<p class="errors">{!!$errors->first('image')!!}</p>
							@if(Session::has('error'))
							<p class="errors">{!! Session::get('error') !!}</p>
							@endif
						</div>
					</div>
					<div id="success"> </div>
					{!! Form::submit('Submit', array('class'=>'send-btn')) !!}
					{!! Form::close() !!}
				</div>
				<div class="panel-footer">Panel footer</div>
			</div>
		</div>
		<div class="col-sm-6 eq-box-sm">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						Banner Inferior
					</h3>
				</div>
				<div class="panel-body">
					<p>Lorem ipsum dolor sit amet.</p>
				</div>
				<div class="panel-footer">Panel footer</div>
			</div>
		</div>
	</div>
</div>


{{-- @include('admin.modals.company') --}}

@endsection