@extends('layouts.admin')

@section('content')

<span class="btn btn-success" data-toggle="modal" data-target="#tipModal">Agregar Entrada</span>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
	<div class="eq-height">
		<div class="col-sm-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						Entradas
					</h3>
				</div>
				<div class="panel-body">
					<table id="table-tips" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>T&iacute;tulo</th>
								<th>Introducci&oacute;n</th>
								<th>Fecha</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="tipModal" tabindex="-1" role="dialog" aria-labelledby="Tip">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Entrada</h4>
			</div>
			<div class="modal-body">
				<iframe id="form_target" name="form_target" style="display:none"></iframe>
				<iframe id="full_form_target" name="full_form_target" style="display:none"></iframe>
				<form id="my_form" action="/upload" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input name="image" type="file" onchange="SubmitWithCallback($('#my_form'), $('#form_target'), fillFile);this.value='';" />
				</form>
				<form id="tform" action="/tips/save" target="full_form_target" method="post" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id" value="0" />
					<div class="row form-group">
						<div class="col-xs-6">
							<label style="width:100%;">T&iacute;tulo
								<input type="text" class="form-control" name="title" placeholder="T&iacute;tulo" />
							</label>
						</div>
						<div class="col-xs-6">
							<label style="width:100%;">Im&aacute;gen
								<input name="image" class="form-control" type="file" />
							</label>
						</div>
						<div class="col-xs-12">
							<label style="width:100%;">Tags
								<input name="tags" id="tags" value="" /> 
							</label>
						</div>
						<div class="col-xs-12">
							<label style="width:100%;">Metadata
								<input name="metadata" id="metadata" value="" /> 
							</label>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-12">
							<label style="width:100%;">Introducci&oacute;n:
								<textarea id="intro" name="intro" class="form-control"></textarea>
							</label>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-12">
							<label style="width:100%;">Mensaje:
								<textarea id="text" name="text"></textarea>
							</label>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="SubmitWithCallback($('#tform'), $('#full_form_target'), submitCallback);">Guardar</button>
			</div>
		</div>
	</div>
</div>
<script>
var table;
$(document).ready(function(){
	tinymce.init({
	    selector: "textarea",
	    resize: "both",
	    relative_urls: false,
		remove_script_host: false,
	    plugins: ["autoresize", "image", "code", "lists", "code","example", "link", 'textcolor'],
	    indentation : '20pt',
		width: '100%',
	    file_browser_callback: function(field_name, url, type, win) {
	        if (type == 'image') $('#my_form input').click();
	    },
	    image_list: "/uploads/tips",
		toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor',
	});
	$('#tags, #metadata').tagit({
		allowSpaces: true
	});
	table = $('#table-tips').DataTable();
	getTips();
	$("select[name=estado]").change(function(e){
		idEstado = $(this).val();
		
		if(idEstado != 0){
			loadMunicipios(idEstado, 0);
		}
	});
	$(".deleteBanner").click(function(e){
		e.preventDefault();
		elm = $(this);
		
		$.ajax({
			url : "banners/delete/"+elm.attr("data-id"),
			method : "GET",
			success : function(data){
				location.reload();
			}
		});
		
		return false;
	});
});
function getTips(){
	table.clear();
	$.ajax({
		url : "tips/getTips",
		method : "GET",
		dataType : 'json',
		success : function(data){
			html = '';
			if($.isArray(data)){
				$.each(data, function(i, val){			
					table.row.add( [
							val.title,
							val.intro,
							val.created_at,
							'<a href="/tips/edit/'+val.id+'" class="btn btn-purple pull-left btn-sm" style="margin-right: 5px;"><i class="fa fa-pencil"></i></a><span class="btn btn-danger pull-left btn-sm" style="margin-right: 5px;" onclick="deleteAddress('+val.id+')"><i class="fa fa-times"></i></span>'
						] ).draw( true );
				});
			}
		}
	});
}
function loadAddress(id){
	$.ajax({
		url : "addresses/getAddress/"+id,
		method : "GET",
		dataType : 'json',
		success : function(data){
			$("#fAddress [name=id]").val(data.id);
			$("#fAddress [name=nombre_comercial]").val(data.nombre_comercial);
			$("#fAddress [name=razon_social]").val(data.razon_social);
			$("#fAddress [name=sucursal]").val(data.sucursal);
			colonia = data.direccion.split(", ");
			$("#fAddress [name=direccion]").val(colonia[0]);
			$("#fAddress [name=colonia]").val(colonia[colonia.length - 1]);
			$("#fAddress [name=codigo_postal]").val(data.codigo_postal);
			$("#fAddress [name=estado]").val(data.municipio.estado_id);
			loadMunicipios(data.municipio.estado_id, data.municipio_id);
			$("#fAddress [name=telefono]").val(data.telefono);
			
			$("#addressModal").modal("show");
		}
	});
}
function loadMunicipios(estado, municipio){
	$.ajax({
		url : "addresses/getMunicipios/"+estado,
		method : "GET",
		success : function(data){
			html = '';
			if($.isArray(data.municipios)){
				$.each(data.municipios, function(i, val){
					html += '<option value="'+val.id+'">'+val.municipio+'</option>';
				});
			}
			$("select[name=municipio]").html(html);
			if(municipio > 0){
				$("select[name=municipio]").val(municipio);
			}
		}
	});
}
function deleteAddress(id){
	if(confirm("Est\u00e1s seguro que quieres eliminar esta entrada?")){
		$.ajax({
			url : "tips/delete/"+id, 
			method : "GET",
			success : function(data){
				getTips();
			}
		});
	}
}
function fillFile(){
	fileName = $('#form_target').contents().find("input[id=fileName]").val();
	
	$('.mce-btn.mce-open').parent().find('.mce-textbox').val('/uploads/tips/'+fileName).closest('.mce-window').find('.mce-primary').click();
}
function submitCallback(){
	location.reload(); 
}
function SubmitWithCallback(form, frame, successFunction) {
   var callback = function () {
       if(successFunction)
           successFunction();
       frame.unbind('load', callback);
   };
   
   frame.bind('load', callback);
   form.submit();
}
</script>

@endsection