@extends('layouts.admin')

@section('content')

<h1>Pedidos</h1>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="panel">
	<div class="panel-body">
		<table class="table table-striped table-bordered dataTable no-footer dtr-inline" id="demo-dt-basic2" data-page-length='100'>
			<thead>
				<tr>
					<th>#</th>
					<th>Nombre de Persona</th>
					<th>Email</th>
					<th>Empresa</th>
					<th>Monto</th>
					<th>Tipo de Pago</th>
					<th>Autorización</th>
					<th>Fecha y Hora</th>
					<th>Status</th>
					<th>Tipo de Entrega</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				@foreach($orders as $order)
				<tr>
					<td>{{ $order->id }}</td>
					<td>{{ $order->user->name . ' ' . $order->user->lastname }}</td>
					<td>{{ $order->user->email }}</td>
					<td>{{ $order->user->company->name }}</td>
					<td>{{number_format($order->amount, 2)}}</td>
					<td>{{ $order->payment_type }}</td>
					<td>{{ $order->authorization_status }}</td>
					<td>{{ $order->created_at }}</td>
					<td>{{ $order->delivery_status }}</td>
					<td>
						{{ $order->shipping_type == 'HOME_DELIVERY' ?  'Entrega a Domicilio' : ($order->selected_pickup == 1 ? 'Recoge en Lago de Guadalupe' : ($order->selected_pickup == 2 ? 'Recoge en Ramos Arizpe' : '')) }}<br>
						{{ $order->shipping_type == 'HOME_DELIVERY' ? $order->address->zip_code->city->name : '' }}
					</td>
					<td><a href="{{ url('/admin/orders/' . $order->id) }}" class="btn btn-info">Ver</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

@endsection