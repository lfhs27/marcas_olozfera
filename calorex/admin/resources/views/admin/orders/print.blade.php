@extends('layouts.print')

@section('content')

<h1>Información de Pedido</h1>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if($order->shipping_type == 'PICKUP')
	<div class="alert alert-success">
		<h3 style="margin-top: 0">
			Usuario recoge en:
			@if($order->selected_pickup == 1)
				Lago de Guadalupe
			@elseif($order->selected_pickup == 2)
				Ramos Arizpe
			@endif
		</h3>
		</div>
		@endif

		<table class="table table-hover">
			<thead>
				<tr>
					<th>Nº de Órden</th>
					<th>Nombre</th>
					<th>Email</th>
					<th>Teléfono</th>
					<th>Empresa</th>
					<th>Monto</th>
					<th>Tipo Pago</th>
					<th>Autorización</th>
					<th>Fecha y Hora</th>
					<th>Status</th>
					{{-- <th>&nbsp;</th> --}}
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $order->id }}</td>
					<td>{{ $order->user->name . ' ' . $order->user->lastname }}</td>
					<td>{{ $order->user->email }}</td>
					<td>{{ $order->user->phone_mobile }}</td>
					<td>{{ $order->user->company->name }}</td>
					<td>{{money_format('%(#10n',$order->amount)}}</td>
					<td>{{ $order->payment_type }}</td>
					<td>
						@if(($order->getOriginal('payment_type') === 'PAYMENT_METHOD_PAYPAL') && $order->getOriginal('authorization_status') == 1)
							{{$order->authorization_status}}
						@else
						<?php $autStatus = $order->getOriginal('authorization_status'); ?>
							{!! Form::open(['admin/orders/changestatus']) !!}
							<select name="selAutStatus" id="input_selAutStatus" data-order-id="{{ $order->id }}" data-fieldname="authorization_status" class="input_selStatus">
								<option value="-1" {{ $autStatus == -1 ? 'selected="selected"' : '' }}>No Aprobado</option>
								<option value="1" {{ $autStatus == 1 ? 'selected="selected"' : '' }}>Aprobado</option>
								<option value="0" {{ $autStatus == 0 ? 'selected="selected"' : '' }}>En espera</option>
							</select>
							<img src="{{asset('assets/admin/img/ajax-loader.gif')}}" alt="loading" class="loadingimg hidden">
							{!! Form::close() !!}
						@endif
					</td>
					<td>{{ $order->created_at }}</td>
					<td>
						<?php $delivStatus = $order->getOriginal('delivery_status'); ?>					
						{!! Form::open(['admin/orders/changestatus']) !!}	
						<select name="selDelivStatus" id="input_selDelivStatus" data-order-id="{{ $order->id }}" data-fieldname="delivery_status" class="input_selStatus">
							<option value="-1" {{$delivStatus == -1 ? 'selected="selected"' : ''}}>NO Facturar</option>
							<option value="0" {{$delivStatus == 0 ? 'selected="selected"' : ''}}>Por Facturar</option>
							<option value="1" {{$delivStatus == 1 ? 'selected="selected"' : ''}}>Por Entregar</option>
							<option value="2" {{$delivStatus == 2 ? 'selected="selected"' : ''}}>Entregado</option>
						</select>
						<img src="{{asset('assets/admin/img/ajax-loader.gif')}}" alt="loading" class="loadingimg hidden">
						{!! Form::close() !!}
					</td>
					{{-- <td><a href="{{ url('/admin/orders/' . $order->id) }}" class="btn btn-info">Ver</a></td> --}}
				</tr>
			</tbody>
		</table>

		@if(isset($order->address_id))
		<h3>Dirección de Envío</h3>

		<table class="table table-hover">
			<thead>
				<tr>
					<th>Alias</th>
					<th>Calle</th>
					<th>Núm. Ext.</th>
					<th>Núm. Int.</th>
					<th>Colonia</th>
					<th>Código Postal</th>
					<th>Ciudad</th>
					<th>Estado</th>
					<th>Persona que recibe</th>
					<th>Instrucciones</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $order->address->alias }}</td>
					<td>{{ $order->address->calle }}</td>
					<td>{{ $order->address->num_ext }}</td>
					<td>{{ $order->address->num_int }}</td>
					<td>{{ $order->address->colonia }}</td>
					<td>{{ $order->address->zip_code->code }}</td>
					<td>{{ $order->address->zip_code->city->name }}</td>
					<td>{{ $order->address->zip_code->city->state->name }}</td>
					<td>{{ $order->address->receptor }}</td>
					<td>{{ $order->address->instrucciones }}</td>
				</tr>
			</tbody>
		</table>
		@endif

		<h3>Productos en el Pedido</h3>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>SKU</th>
					<th>Marca</th>
					<th>Cantidad</th>
					<th>Precio Unitario</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($order->items as $item): ?>
					<tr>
						<td>{{ $item->product_name }}</td>
						<td>{{ $item->sku }}</td>
						<td>{{ $item->marca }}</td>
						<td>{{ $item->quantity }}</td>
						<td>{{money_format('%(#10n',$item->price_per_product)}}</td>
						<td>{{money_format('%(#10n',$item->price_per_set)}}</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>

		@if($order->getOriginal('payment_type') != 'PAYMENT_METHOD_PAYROLL' && $order->reqfactura == 1)
		<?php $facturaInfo = json_decode($order->facturainfo); ?>
		<h3>Información de Facturación</h3>
		<div class="facturainfocont">
			<div class="row mar-btm">
				<div class="col-sm-3"><strong>Razón Social</strong> {{ $facturaInfo->razonsocial }}</div>
				<div class="col-sm-3"><strong>RFC</strong>  {{ $facturaInfo->factura_rfc }}</div>
				<div class="col-sm-3"><strong>Lugar de Expedición</strong>  {{ $facturaInfo->lugar_expedicion }}</div>
				<div class="col-sm-3"><strong>Correo de Facturación</strong> {{ $facturaInfo->correo }}</div>
			</div>
			<div class="row mar-btm">
				<div class="col-sm-3"><strong>Calle</strong> {{ $facturaInfo->calle }}</div>
				<div class="col-sm-3"><strong>Número Exterior</strong> {{ $facturaInfo->numext }}</div>
				<div class="col-sm-3"><strong>Número Interior</strong> {{ $facturaInfo->numint }}</div>
				<div class="col-sm-3"><strong>Colonia</strong> {{ $facturaInfo->colonia }}</div>
			</div>
			<div class="row mar-btm">
				<div class="col-sm-3"><strong>Estado</strong> {{ $facturaInfo->estado }}</div>
				<div class="col-sm-3"><strong>Municipio/Delegación</strong> {{ $facturaInfo->municipio }}</div>
				<div class="col-sm-3"><strong>Estado</strong> {{ $facturaInfo->ciudad }}</div>
				<div class="col-sm-3"><strong>Código Postal</strong> {{ $facturaInfo->zipcode }}</div>
			</div>
		</div>
		@endif
	

@endsection