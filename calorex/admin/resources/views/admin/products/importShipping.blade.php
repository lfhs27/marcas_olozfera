@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	
	@include('admin.shared.page-title')		

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		
		@include('admin.shared.page-section-title')

		<div class="panel panel-default">
			<div class="panel-body">
				<p><strong>Seleccione el archivo que desea importar.</strong></p>

				{!! Form::open(['action'=>'Admin\ProductsController@postImportShipping','files' => true]) !!}
				<p>
				<input type="file" name="excelproducts">
				</p>
				<p>
					<button type="submit" name="Importar" value="Importar" class="btn btn-primary">Importar</button>
				</p>
				{!! Form::close() !!}
			</div>
		</div>
		
	</div>
	<!--===================================================-->
	<!--End page content-->


</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

@endsection