@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
<!--===================================================-->
<!--div id="content-container"-->
	
	@include('admin.shared.page-title')		

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		
		@include('admin.shared.page-section-title')
		
		<div class="row">
			<div class="col-sm-4">
			{!! Form::open(['action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH','files' => true]) !!}
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							@if(isset($product->businessUnit) && isset($product->brand) && isset($product->family) && isset($product->line) && isset($product->slug))
								<a href="http://www.calorex.com.mx/calentadores/{{ $product->brand->slug }}/{{ $product->family->slug }}/{{ $product->line->slug }}/{{ $product->slug }}" target="_blank" class="btn btn-primary btn-lg btn-block mar-btm">Ver en Portal</a>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Liga</label>
								<input type="text" id="" class="form-control" value="http://www.calorex.com.mx/calentadores/{{ $product->brand->slug }}/{{ $product->family->slug }}/{{ $product->line->slug }}/{{ $product->slug }}" />
							</div>
							<div class="form-group">
								<label class="control-label">Nombre</label>
								<input type="text" name="name" id="inputName" class="form-control" value="{{$product->name}}" required="required">
							</div>
						
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="" class="control-label">Nombre Corto</label>
										<input type="text" name="short_name" id="inputShort_name" class="form-control" value="{{$product->short_name}}">
									</div>
									<div class="col-md-6">
										<label for="" class="control-label">Precio al público</label>
										<input type="text" name="fake_price" id="inputFake_price" class="form-control" value="{{ $product->fake_price }}">
									</div>
								</div>
							</div>
						
							<div class="form-group">
								<label for="" class="control-label">Subtítulo</label>
								<input type="text" name="description" id="inputDescription" class="form-control" value="{{ $product->description }}">
							</div>
						
							<div class="form-group">
								<label for="" class="control-label">Marca</label>
								<select name="brand_id" id="inputBrand_id" class="form-control">
									@if($brands)
									@foreach($brands as $brand)
									<option {!!$product->brand_id == $brand->id ? 'selected="selected"' : "" !!} value="{{$brand->id}}">{{$brand->name}}</option>
									@endforeach
									@endif
								</select>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="" class="control-label">Categoría</label>
										<select name="family_id" id="inputFamily_id" class="form-control">
											@if($families)							
											<option>-- Seleccione una categoría --</option>
											@foreach($families as $family)
											<option {{ $product->family_id == $family->id ? 'selected="selected"' : '' }} value="{{ $family->id }}">{{ ($family->brand?$family->brand->front_name:'') }} - {{ $family->front_name }}</option>
											@endforeach
											@endif
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="" class="control-label">Subcategoría</label>
										<select name="line_id" id="inputLine_id" class="form-control">
											<option>-- Seleccione una Subcategoría --</option>
											@if($lines)
												@foreach($lines as $line)
													<option {{$product->line_id == $line->id ? 'selected="selected"' : ''}} value="{{$line->id}}">{{ $line->brand ? ($line->brand?$line->brand->front_name:'') : '' }} {{ $line->front_name }}</option>
												@endforeach
											@endif
										</select>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</div>
			{!! Form::close() !!}
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<p>
								<label for="" class="control-label">Galería de Fotos</label>
								<form action="/products/photos" class="dropzone" id="galleryDropzone" method="post" enctype="multipart/form-data">
									{{ csrf_field() }}
									<input type="hidden" name="productId" value="{{ $product->id }}" />
									Arrastre sus archivos a este recuadro
								</form>
							</p>
						</div>
						<div class="form-group">
							<p>
								<label for="" class="control-label">Foto Principal:</label>
								<a class="btn btn-primary" data-toggle="modal" href='#modal-add-featured-image'>Seleccionar Imagen Principal</a>
							</p>
						</div>
					</div>
				</div>
			</div>						<div class="col-sm-4">				<div class="panel panel-default">					<div class="panel-body">						<p>Video</p>						<p>Introduzca la liga de youtube</p>						{!! Form::open(['action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH','files' => true]) !!}						<p>							<input type="text" class="form-control" name="video" placeholder="URL" value="{{ strcmp($product->video, "") == 0 ? '' : 'https://www.youtube.com/v/'.$product->video }}" />						</p>						<p>							<iframe id="ytplayer" type="text/html" width="100%" src="https://www.youtube.com/embed/{{ $product->video }}?autoplay=0" frameborder="0"></iframe>						</p>						<p>							<button type="submit" class="btn btn-primary">Guardar</button>						</p>						{!! Form::close() !!}											</div>				</div>			</div>
			@if(!$product->isCombo)
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<p>Modelos</p>
						<p>Los modelos que seleccione aparecerán en los modelos de la página del producto, como se ve en la imagen:</p>
						<p>
							<img src="{{ asset('admin_assets/img/prod-mods.png') }}" alt="" class="img-responsive">
						</p>
						
						{!! Form::open(['action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH','files' => true]) !!}
						<?php 
							$productSiblingProducts = json_decode($product->getOriginal('sibling_products')); 
							/*$siblingsArr = array();
							if(count($productSiblingProducts)){
								foreach($productSiblingProducts as $sibling){
									array_push($siblingsArr, $sibling->id);
								}
							}*/
						?>
						@if($product->line && $product->line->products)
							@foreach($product->line->products as $siblingProduct)
							<div class="checkbox">
								<label>
									<input type="checkbox" name="siblings[]" value="{{$siblingProduct->id}}"{{ !empty($productSiblingProducts) ? (in_array($siblingProduct->id, $productSiblingProducts) ? 'checked="checked"' : '') : '' }}>
									{{empty($siblingProduct->original_name) ? $siblingProduct->name : $siblingProduct->original_name}}
								</label>
							</div>
							@endforeach
						@endif
						<p>
							<button type="submit" class="btn btn-primary">Guardar</button>
						</p>
						{!! Form::close() !!}
						
					</div>
				</div>
			</div>
			@else
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<p>Incluye</p>
						{!! Form::open(['action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH','files' => true]) !!}
						<input type="hidden" name="combo_update" value="1" />
						<table id="combo-list" style="width:100%;">
							<thead>
								<tr>
									<th>SKU / SEGMENT1</th>
									<th>Cantidad</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($product->combo_products as $it => $comboElement)
								<tr>
									<td>
										<select name="sku_combo[]" class="form-control">
											<option value="0"> - Elegir SKU - </option>
											@foreach($all_products as $combo_part)
											<option value="{{ $combo_part->id }}" {{ $comboElement->id==$combo_part->id?"selected":"" }}>{{ $combo_part->segment1 }}</option>
											@endforeach
										</select>
									</td>
									<td>
										<select name="cantidad_sku_combo[]" class="form-control">
											@for($i=1; $i<=20; $i++)
											<option value="{{ $i }}" {{ $comboElement->pivot->cantidad==$i?"selected":"" }}>{{ $i }}</option>
											@endfor
										</select>
									</td>
									<td>
										@if($it > 0)
										<button type="button" class="btn btn-danger" style="width:100%;" onclick="$(this).parent().parent().remove()">Borrar</button>
										@endif
									</td>
								</tr>
								@endforeach
								<tr>
									<td>
										<select name="sku_combo[]" class="form-control">
											<option value="0"> - Elegir SKU - </option>
											@foreach($all_products as $combo_part)
											<option value="{{ $combo_part->id }}">{{ $combo_part->segment1 }}</option>
											@endforeach
										</select>
									</td>
									<td>
										<select name="cantidad_sku_combo[]" class="form-control">
											@for($i=1; $i<=20; $i++)
											<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</td>
									<td>
										@if(COUNT($product->combo_products))
										<button type="button" class="btn btn-danger" style="width:100%;" onclick="$(this).parent().parent().remove()">Borrar</button>
										@endif
									</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td><button type="button" class="btn btn-warning" onclick="addComboElement()" style="width:100%;">Agregar Fila</button></td>
									<td><button type="submit" class="btn btn-primary" style="width:100%;">Guardar</button></td>
									<td></td>
								</tr>
							</tfoot>
						</table>
						<p>
							
							
						</p>
						{!! Form::close() !!}						
					</div>
				</div>
			</div>
			@endif
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="tab-base">
					<div role="tabpanel">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#tab-description" aria-controls="tab-description" role="tab" data-toggle="tab">Descripción</a>
							</li>
							<li role="presentation">
								<a href="#tab-specs" aria-controls="tab-specs" role="tab" data-toggle="tab">Especificaciones</a>
							</li>
							<li role="presentation">
								<a href="#tab-documentacion" aria-controls="tab-documentacion" role="tab" data-toggle="tab">Documentación</a>
							</li>
							<li role="presentation">
								<a href="#tab-productos-recomendados" aria-controls="tab-productos-recomendados" role="tab" data-toggle="tab">Productos Recomendados</a>
							</li>
							<li role="presentation">
								<a href="#tab-orden" aria-controls="tab-orden" role="tab" data-toggle="tab">Orden Modelos</a>
							</li>
							<!--li role="presentation">
								<a href="#tab-calentador" aria-controls="tab-calentador" role="tab" data-toggle="tab">Calentador Ideal</a>
							</li-->
							@if(strcmp($product->brand->slug, 'calorex') == 0 || strcmp($product->brand->slug, 'cinsa') == 0)
							<li role="presentation">
								<a href="#tab-brandicons" aria-controls="tab-brandicons" role="tab" data-toggle="tab">Íconos para Marcas</a>
							</li>
							@endif
						</ul>
					
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="tab-description">
								{!! Form::open(['action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH','files' => true]) !!}
								<div class="row">
									<div class="col-sm-3">
										@if($product->blueprint)
										<img src="{{ asset($product->blueprint->filepath .'/'.$product->blueprint->filename) }}" class="img-responsive">
										@endif
										<div class="form-group">
											<label for="" class="control-label">Blueprint</label>
											<input type="file" name="blueprint">
										</div>
									</div>
									<div class="col-sm-9">
										{{-- <div class="form-group">
											<label for="" class="control-label">Íconos</label>
											@forelse($product->specIcons as $specIcon)
											@empty
											<p>No existen especificaciones con íconos</p>
											@endforelse
											<a class="btn btn-primary" data-toggle="modal" href='#modal-add-spec-icon'>Agregar</a>
										</div> --}}
										<div class="form-group">
											<label for="" class="control-label">Descripción</label>
											<textarea name="descriptionHtml" id="inputDescriptionHtml" class="form-control" rows="3">{{ $product->description_html }}</textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<button type="submit" class="btn btn-primary">Guardar</button>
									</div>
								</div>
								{!! Form::close() !!}
							</div>

							<div role="tabpanel" class="tab-pane" id="tab-specs">
								<div class="row">
									<div class="col-sm-6">
										{!! Form::open(['action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH','files' => true]) !!}
											<p><strong>Servicios Simultáneos</strong></p>
											<div class="row">
												<div class="col-md-2">
													<div class="form-group">
														<label for="" class="control-label">Regaderas</label>
														<input type="text" name="servicios_regaderas" value="{{ $product->num_regaderas ? $product->num_regaderas : '' }}" class="form-control">
													</div>
												</div>
												<div class="col-md-2">
													<div class="form-group">
														<label for="" class="control-label">Lavabos</label>
														<input type="text" name="servicios_lavabos" value="{{ $product->num_lavabos ? $product->num_lavabos : '' }}" class="form-control">
													</div>
												</div>
												<div class="col-md-2">
													<div class="form-group">
														<label for="" class="control-label">Tina Estándar</label>
														<input type="text" name="servicios_tinaestandar" id="inputServicios_tinaestandar" class="form-control" value="{{ $product->num_tina ? $product->num_tina : '' }}">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-md-4">
													<div class="form-group">
														<button type="submit" class="btn btn-primary">Guardar</button>
													</div>
												</div>
											</div>
										{!! Form::close() !!}

										<h3>Especificaciones con Ícono</h3>
										<table class="table">
											<thead>
												<tr>
													<th>Ícono</th>
													<th>Título</th>
													<th>Texto</th>
													<th>&nbsp;</th>
												</tr>
											</thead>
											<tbody>
												@forelse($product->specificationIcons as $specIcon)
													<tr>
														<td><img src="{{ asset($specIcon->image->filepath . '/' . $specIcon->image->filename) }}"></td>
														<td>{{ $specIcon->name }}</td>
														<td>{{ $specIcon->description }}</td>
														<td>
															{!! Form::open(['action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH']) !!}
																<input type="hidden" name="deleteSpecIcon" value="{{ $specIcon->id }}">
																<button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
															{!! Form::close() !!}
														</td>
													</tr>
												@empty
													<tr>
														<td colspan="4">NO EXISTEN ESPECIFICACIONES CON ÍCONO</td>
													</tr>
												@endforelse
											</tbody>
										</table>

										<a class="btn btn-primary" data-toggle="modal" href='#add-spec-icon'>Agregar Especificación con Ícono</a>
									</div>
									<div class="col-sm-6">
										{!! Form::open(['action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH','files' => true]) !!}
										<div class="form-group">
											<label for="" class="control-label">Importar de Excel</label>
											<input type="file" name="specsFileExcel">
										</div>	
										<div class="form-group">
											<button type="submit" class="btn btn-primary">Importar Especificaciones</button>
										</div>
										{!! Form::close() !!}
										<p><strong>Especificaciones</strong></p>
										<table class="table table-hover" id="specifications-editable">
											<thead>
												<tr>
													<th>ID</th>
													<th>Nombre</th>
													<th>Valor</th>
												</tr>
											</thead>
											<tbody>
												@forelse($product->specifications as $spec)
												<tr>
													<td>{{ $spec->id }}</td>
													<td>{{ $spec->name }}</td>
													<td>{{ $spec->value }}</td>
												</tr>
												@empty
												<tr>
													<td colspan="2"><div class="pad-all text-center">No existen especificaciones para este producto.</div></td>
												</tr>
												@endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane" id="tab-documentacion">
								<div class="row">
									<div class="col-sm-12">

										<h3>Folleto electrónico</h3>
										<table class="table table-bordered">
											<tbody>
												@foreach($product->downloads as $download)
													@if($download->type === 'SPECSHEET')
													<tr>
														<td><a href="{{ asset($download->filepath . '/' . $download->filename) }}" target="_blank">{{ $download->name }}</a></td>
														<td>{{ $download->filename }}</td>
														<td>
															{!! Form::open(['action' => ['Admin\DownloadsController@destroy',$download->id],'method' => 'DELETE']) !!}
																<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
															{!! Form::close() !!}
														</td>
													</tr>
													@endif
												@endforeach
											</tbody>
										</table>

										{!! Form::open(['action' => 'Admin\DownloadsController@store','files' => true]) !!}
											<input type="hidden" name="name" value="Folleto electrónico de {{ $product->name }}">
											<input type="hidden" name="downloadable_id" value="{{ $product->id }}">
											<input type="hidden" name="downloadable_type" value="TiendaGIS\Product">
											<input type="hidden" name="dtype" value="SPECSHEET">
											<div class="form-group">
												<p>
													<label for="" class="control-label">Folleto</label>
													<input type="file" name="download">
												</p>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-primary">Guardar</button>
											</div>
										{!! Form::close() !!}

										<h3>Manual de Usuario</h3>

										<table class="table table-bordered">
											<tbody>
												@foreach($product->downloads as $download)
													@if($download->type === 'MANUAL')
													<tr>
														<td><a href="{{ asset($download->filepath . '/' . $download->filename) }}" target="_blank">{{ $download->name }}</a></td>
														<td>{{ $download->filename }}</td>
														<td>
															{!! Form::open(['action' => ['Admin\DownloadsController@destroy',$download->id],'method' => 'DELETE']) !!}
																<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
															{!! Form::close() !!}
														</td>
													</tr>
													@endif
												@endforeach
											</tbody>
										</table>

										{!! Form::open(['action' => 'Admin\DownloadsController@store','files' => true]) !!}
											<input type="hidden" name="name" value="Manual de Usuario de {{ $product->name }}">
											<input type="hidden" name="downloadable_id" value="{{ $product->id }}">
											<input type="hidden" name="downloadable_type" value="TiendaGIS\Product">
											<input type="hidden" name="dtype" value="MANUAL">
											<div class="form-group">
												<p>
													<label for="" class="control-label">Manual</label>
													<input type="file" name="download">
												</p>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-primary">Guardar</button>
											</div>
										{!! Form::close() !!}

									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane" id="tab-productos-recomendados">
								<div class="row">
									<div class="col-sm-12">
										<div class="table-responsive">
											<table class="table table-hover">
												<thead>
													<tr>
														<th>ID</th>
														<th>Nombre</th>
														<th>&nbsp;</th>
													</tr>
												</thead>
												<tbody>
													@if(!empty($product->prods_rec))
														<?php $prodsRec = json_decode($product->prods_rec); ?>
														@foreach($prodsRec as $key => $prodRec)
														<tr>
															<td>{{ $prodRec->id }}</td>
															<td>{{ $prodRec->name }}</td>
															<td>
																<a href="#" class="btn btn-danger btn-del-prodrec"><i class="fa fa-trash"></i></a>
															</td>
														</tr>
														@endforeach
													@endif
												</tbody>
											</table>
										</div>

										<a class="btn btn-primary" data-toggle="modal" href='#modal-add-recommended-product'>Agregar Producto Recomendado</a>
									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane" id="tab-orden">
								<div class="row">
									<div class="col-sm-12">										
										{!! Form::open(['id' => 'fOrder', 'action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH','files' => true]) !!}
										<input type="hidden" name="sibling_products" value="" />
										<div class="model-boxes clearfix ui-sortable">
											@foreach($siblings as $sibling)
												<div class="box ui-sortable-handle" data-model-id="{{ $sibling->id }}">
													{{ $sibling->name }}
												</div>
											@endforeach
										</div>
										<input type="submit" class="btn btn-primary" value="Guardar Orden" />
										{!! form::close() !!}
									</div>
								</div>
							</div>
							
							<div role="tabpanel" class="tab-pane" id="tab-brandicons">
								{!! Form::open(['action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH','files' => true]) !!}
									<div class="row brand-icon-row"></div>
									@foreach($product->brand_icons as $brand_icon)
									<div class="row brand-icon-row">
										<input type="hidden" name="brand_icon_id[]" value="{{ $brand_icon->id }}" />
										<div class="col-sm-3">
											@if($brand_icon->icon)
											<img src="{{ asset($brand_icon->icon) }}" class="img-responsive">
											@endif
											<div class="form-group">
												<label for="" class="control-label">Ícono</label>
												<input type="file" name="brand_icon[]">
											</div>
										</div>
										<div class="col-sm-7">
											<div class="form-group">
												<label for="" class="control-label">Descripción</label>
												<textarea name="brand_icon_description[]" class="brand_icon_description" class="form-control" rows="3">{{ $brand_icon->description }}</textarea>
											</div>
										</div>
										<div class="col-sm-2">
											<a href="/products/brand-icons/delete/{{ $brand_icon->id }}" class="btn btn-danger">Eliminar</a>
										</div>
									</div>
									@endforeach
									<div class="row">
										<div class="col-sm-4">
											<span class="btn btn-success" onclick="addBrandIcon()">Agregar Nuevo Ícono</span>
										</div>
										<div class="col-sm-4">
											<button type="submit" class="btn btn-primary">Guardar</button>
										</div>
									</div>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<!--===================================================-->
	<!--End page content-->


<!--/div-->
<!--===================================================-->
<!--END CONTENT CONTAINER-->


<div class="modal fade" id="modal-items-adicionales">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Items Adicionales</h4>
			</div>
			<div class="modal-body">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						@forelse($additionalItems as $item)
							<tr>
								<td>{{ $item->id }}</td>
								<td>{{ $item->name }}</td>
								<td>{{ $item->description }}</td>
								<td><a href="#" data-item-id="{{ $item->id }}" class="btn btn-add-addional-item btn-primary"><i class="fa fa-plus"></i></a></td>
							</tr>
						@empty

						@endforelse
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	$('.order-boxes,.model-boxes').sortable({
		opacity: 0.6,
		tolerance: "pointer"
	});
	
	$('#tags').tagit({
		allowSpaces: true
	});
	
	$("#fOrder").submit(function(e){
		//e.preventDefault();
		sorted = $('.model-boxes').sortable('toArray',{attribute: 'data-model-id'});
		
		$(this).find("[name=sibling_products]").val(JSON.stringify(sorted));
		
		console.log("sorted", sorted);
		return true;
	});
	
	$("#fZips").submit(function(e){
		//e.preventDefault();
		
		var tags = $("#tags").val();
		tags = tags.split(",");
		$(this).find("[name=disallowed_zipcodes]").val(JSON.stringify(tags));
		
		return true;
	});
	
});
</script>
<div class="modal fade" id="modal-add-spec-icon">
	<div class="modal-dialog">
		<div class="modal-content">
			{!! Form::open(['action' => ['Admin\ProductsController@update',$product->id],'method' =>'PATCH']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Agregar Ícono</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="" class="control-label">Ícono</label>
					<input type="file" name="specIconFile">
				</div>
				<div class="form-group">
					<label for="" class="control-label">Título</label>
					<input type="text" name="specIconTitle" id="inputSpecIconTitle" class="form-control" value="">
				</div>
				<div class="form-group">
					<label for="" class="control-label">Descripción</label>
					<input type="text" name="specIconText" id="inputSpecIconText" class="form-control" value="">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-add-recommended-product">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Producto Recomendado</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<input type="text" name="productoRecomendado" id="inputProductoRecomendado" class="form-control" value="" placeholder="Escriba el nombre de un producto.">
					</div>
				</div>
				<div class="row" id="livesearch-products-results-row">
					<div class="table-responsive" style="height: 400px; overflow: scroll;">
						<table class="table table-hover" id="livesearch-products-table-results">
							<thead>
								<tr>
									<th>ID</th>
									<th>Nombre</th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>	
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-add-featured-image">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Seleccionar Foto Principal</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(['action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH','id' => 'form-add-featured-image']) !!}
				<input type="hidden" name="featuredImage" id="inputFeaturedImage" value="">
				<div class="row">
				@if($product->gallery)
					@foreach($product->gallery->images as $image)
					<div class="col-sm-3">
						<a data-id="{{ $image->id }}" class="featured-selector-link" href="#"><img src="{{asset($image->filepath .'/'. $image->filename)}}" alt="" class="img-responsive"></a>
					</div>
					@endforeach
				@endif
				</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary">Guardar</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).on('click','.featured-selector-link',function(event){
		event.preventDefault();
		var featuredImageId = $(this).data('id');

		$('#inputFeaturedImage').val(featuredImageId);

		$('#form-add-featured-image').submit();

	});

	$(document).on('click', '.btn-add-addional-item', function(event) {
		event.preventDefault();
	
		var itemId = $(this).data('item-id');
		var formData = new FormData();
		var btn = $(this);

		formData.append('item',itemId);
		formData.append('product',{{$product->id}});

		$.ajax({
			url: '{{action('Admin\ProductsController@addAdditionalItem')}}',
			type: 'POST',
			contentType: false,
			processData: false,
			data: formData
		})
		.done(function(response) {
			console.log("success");
			console.log(response);
			if (response.code === 200) {
				var newTr;
				btn.parents('tr').remove();
				$('#modal-items-adicionales').hide();

				newTr += '<tr>';
					newTr += '<td>';
						newTr += response.item.id
					newTr += '</td>';

					newTr += '<td>';
						newTr += response.item.name
					newTr += '</td>';

					newTr += '<td>';
						newTr += response.item.description
					newTr += '</td>';

					newTr += '<td>';
						newTr += response.item.price
					newTr += '</td>';

					newTr += '<td>';
						newTr += '{!! Form::open(['action' => ['Admin\ProductsController@update',$product->id],'method' => 'PATCH']) !!}<input type="hidden" name="removeAdditionalItem" value="'+ response.item.id +'"><button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>{!! Form::close() !!}'
					newTr += '</td>';
				newTr += '<tr>';

				$('#current-itemsadicionales tbody').append(newTr);
			}
		})
		.fail(function(response) {
			console.log("error");
			console.log(response);
			$('body').html(response.responseText);
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$(document).on('click', '.add-prod-rec', function(event) {
		event.preventDefault();
		
		var prodId = $(this).data('product-id');
		var prodName = $(this).data('product-name');
		var prodRow = $(this).parents('tr');

		var formData = new FormData();

		formData.append('prodId',prodId);
		formData.append('prodName',prodName);
		formData.append('addRecProd','1');
		formData.append('_method','PATCH');

		$.ajax({
			url: '/products/{{ $product->id }}',
			type: 'POST',
			processData: false,
			contentType: false,
			data: formData,
		})
		.done(function(response) {
			console.log("success");
			// console.log(response);

			var addedRowHtml = '<tr><td>'+prodId+'</td><td>'+prodName+'</td></tr>';
			$('#tab-productos-recomendados table tbody').append(addedRowHtml);

			prodRow.remove();

			alert('Se ha agregado producto recomendado: ' + prodName);

		})
		.fail(function(response) {
			console.log("error");
			$('body').html(response.responseText);
		})
		.always(function() {
			console.log("complete");
		});
		
	});
	$(document).ready(function() {
		$('.brand_icon_description').summernote({height: 150});
		$('#inputDescriptionHtml').summernote({height: 250});

		$('#inputProductoRecomendado').on('keyup', function(event) {
			var searchTerm = $(this).val();

			/*if (searchTerm.length > 1) {*/
				console.log('Query: ' + searchTerm);
				$.ajaxSetup({
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
				});

				$.getJSON('/livesearch/products', {
					productQuery: searchTerm
				}, function(json, textStatus){
					console.log(json);
					var liveSearchHtml = '';

					$.each(json, function(index, product) {
						console.log(product);
						liveSearchHtml += '<tr><td>'+product.id+'</td><td>'+product.name+'</td><td><a href="#" class="btn btn-primary add-prod-rec" data-product-id="'+product.id+'" data-product-name="'+product.name+'"><i class="fa fa-plus"></i></a></td></tr>';
					});

					$('#livesearch-products-table-results tbody').html(liveSearchHtml);
				});
			/*}*/
		});
	});
	function addComboElement(){
		$("#combo-list tbody tr").last().clone().appendTo("#combo-list tbody");
		$("#combo-list tbody tr").last().find("td").last().html('<button type="button" class="btn btn-danger" style="width:100%;" onclick="$(this).parent().parent().remove()">Borrar</button>');
	}
	function addBrandIcon(){
		html = '';
		html += '<div class="row brand-icon-row">';
		html += '	<input type="hidden" name="brand_icon_id[]" value="0" />';
		html += '	<div class="col-sm-3">';
		html += '		<div class="form-group">';
		html += '			<label for="" class="control-label">Ícono</label>';
		html += '			<input type="file" name="brand_icon[]">';
		html += '		</div>';
		html += '	</div>';
		html += '	<div class="col-sm-9">';
		html += '		<div class="form-group">';
		html += '			<label for="" class="control-label">Descripción</label>';
		html += '			<textarea name="brand_icon_description[]" class="brand_icon_description" class="form-control" rows="3"></textarea>';
		html += '		</div>';
		html += '	</div>';
		html += '</div>';
		$(html).insertAfter($(".brand-icon-row").last()); 
		$(".brand-icon-row").last().find(".brand_icon_description").summernote({height: 150});
	}
</script>

@if(!isset($UsingBusinessUnit->name))
<?php //dd($product->gallery); ?>
<script>
	$(document).ready(function() {
	Dropzone.autoDiscover = false;
	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	});

	// $('div#galleryDropzone').dropzone({
	// 	url: '/products/gallery'
	// });

	Dropzone.options.galleryDropzone = {
		addRemoveLinks: true,
		dictRemoveFile: 'Eliminar',
		@if($product->gallery)
		init: function(){
			@foreach($product->gallery->images as $image)
				var mockFile = { name: "{{$image->name}}", size: {{$image->size}}, type: '{{$image->mimetype}}' };
                this.addFile.call(this, mockFile);
                this.options.thumbnail.call(this, mockFile, "{{url($image->filepath .'/'. $image->filename)}}");
			@endforeach
		}
		@endif
	}

	var galleryDropzone = new Dropzone('#galleryDropzone');

	galleryDropzone.on('sending',function(file,xhr,formData){
		console.log(formData);

		formData.append('_token',$('meta[name="csrf-token"]').attr('content'));
		formData.append('productId',{{ $product->id }});
	});

	galleryDropzone.on('error',function(file,message,xhr){
		$('body').html(message);
	});

	galleryDropzone.on('success',function(file,response){
		console.log(response);
	})

	galleryDropzone.on('removedfile',function(file,xhr,formData){
		var formData = new FormData()

		formData.append('photoName',file.name);
		formData.append('_token',$('meta[name="csrf-token"]').attr('content'));

		$.ajax({
			url: '/products/photos/delete',
			type: 'POST',
			data: formData,
			processData: false,
			contentType: false
		})
		.done(function(response) {
			console.log("success");
			console.log(response);
		})
		.fail(function(response) {
			console.log("error");
			$('body').html(response.responseText);
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$('#specifications-editable').Tabledit({
		url: '/products/specifications/quickedit',
		editButton: false,
		restoreButton: false,
		columns: {
			identifier: [0,'id'],
			editable: [
				[1,'name'],
				[2,'value']
			]
		},
		buttons: {
			edit: {
				class: 'btn btn-sm btn-default',
				html: '<i class="fa fa-pencil"></i>',
				action: 'edit'
			},
			delete: {
				class: 'btn btn-sm btn-default',
				html: '<i class="fa fa-trash"></i>',
				action: 'delete'
			},
			save: {
				class: 'btn btn-sm btn-success',
				html: 'Save'
			},
			confirm: {
				class: 'btn btn-sm btn-danger',
				html: 'Confirm'
			}
		},
		onSuccess: function(response)
		{
			console.log(response);
		},
		onAjax: function(action,serialized)
		{
			console.log(action);
			console.log(serialized);
		},
		onFail: function(xhr,textStatus,error)
		{
			console.log('Failed');
			console.log(xhr.responseText)
			$('body').html(xhr.responseText);
		}

	});
});
</script>
@endif

<div class="modal fade" id="add-spec-icon">
	<div class="modal-dialog">
		<div class="modal-content">
			{!! Form::open(['action'=>['Admin\ProductsController@update',$product->id],'method' => 'PATCH','files' => true]) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Agregar Especificación con Ícono</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="" class="control-label">Título</label>
					<input type="text" name="specIcon_name" id="inputName" class="form-control" value="" required="required">
				</div>	
				<div class="form-group">
					<label for="" class="control-label">Texto</label>
					<input type="text" name="specIcon_description" id="inputDescription" class="form-control" value="">
				</div>
				<div class="form-group">
					<label for="" class="control-label">Ícono</label>
					<input type="file" name="specIcon_icon">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection