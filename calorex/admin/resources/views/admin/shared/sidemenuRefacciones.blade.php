<nav id="mainnav-container">
	<div id="mainnav">

		<!--Menu-->
		<!--================================-->
		<div id="mainnav-menu-wrap">
			<div class="nano">
				<div class="nano-content">
					<ul id="mainnav-menu" class="list-group">

						@can('Orders_Read')
						<li>
							<a href="{{ action('Admin\OrdersController@ordersRefacciones') }}">
								<i class="fa fa-th-list"></i>
								<span class="menu-title"><strong>Pedidos</strong></span>
							</a>
						</li>
						@endcan
			
						@can('Products_Read')
						<li>
							<a href="#">
								<i class="fa fa-briefcase"></i>
								<span class="menu-title"><strong>Productos</strong></span>
								<i class="arrow"></i>
							</a>
			
							
							<ul class="collapse">
								<li><a href="{{ action('Admin\ProductsController@index') }}">Listado</a></li>
								@can('Products_Publish')
								<li><a href="{{ action('Admin\ProductsController@import') }}">Importar Excel</a></li>
								<li><a href="{{ action('Admin\ProductsController@importShipping') }}">Importar Metodos de Envío</a></li>
								@endcan
								<li class="list-divider"></li>
								@can('Brands_Read')
								<li><a href="{{action('Admin\BrandsController@index')}}">Marcas</a></li>
								<li><a href="{{ action('Admin\FamiliesController@index') }}">Categorías</a></li>
								<li><a href="{{ action('Admin\LinesController@index') }}">Subcategorías</a></li>
								@endcan

								{{-- <li><a href="{{ action('Admin\LinesController@index') }}">Listas de Precios</a></li> --}}

								<!-- <li><a href="#">Grupos</a></li> -->
							</ul>
						</li>
			
						<li class="list-divider"></li>

						@endcan
						
						<li class="list-divider"></li>

						@can('Admin_Interface_Companies')
						<li>
							<a href="{{ action('Admin\CompaniesController@index') }}">
								<i class="fa fa-building"></i>
								<span class="menu-title"><strong>Empresas</strong></span>
								<i class="arrow"></i>
							</a>

							<ul class="collapse">
								<li><a href="{{ action('Admin\CompaniesController@index') }}">Empresas</a></li>
								@can('Companies_Publish')
								<li><a href="{{ action('Admin\CompaniesController@create') }}">Nueva Empresa</a></li>
								@endcan
							</ul>
						</li>
						@endcan

						@can('Admin_Interface_Users')
						<li>
							<a href="#">
								<i class="fa fa-user"></i>
								<span class="menu-title"><strong>Usuarios</strong></span>
								<i class="arrow"></i>
							</a>

							<ul class="collapse">
								<li><a href="{{ action('Admin\UsersController@indexRefacciones') }}">Usuarios Refacciones</a></li>
								@can('Users_Write')
								<li><a href="{{ action('Admin\UsersController@create') }}">Nuevo Usuario</a></li>
								@endcan
								
								{{-- <li><a href="{{ action('Admin\UsersController@create') }}">Carga Masiva</a></li> --}}
								<li class="list-divider"></li>
								<li>
									<a href="{{ action('Admin\UserGroupsController@index') }}">Grupos de Usuarios</a>
								</li>
							</ul>
						</li>
						@endcan

						<li class="list-divider"></li>

						@can('Admin_Interface_Settings')
						<li>
							<a href="{{action('Admin\ConfigurationController@index')}}">
								<i class="fa fa-gear"></i>
								<span class="menu-title">
									<strong>Configuración</strong>
								</span>
								<i class="arrow"></i>
							</a>
							<ul>
								<li>
									<a href="{{ action('Admin\WebServicesController@index') }}">
										Web Services
										<i class="arrow"></i>
									</a>
									<ul class="collapse">
										<li>
											<a href="{{ action('Admin\PricelistsController@index') }}">Listas de Precios</a>
										</li>
										<li>
											<a href="{{ action('Admin\StockController@index') }}">Inventario</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="{{ action('Admin\ConfigurationController@notifications') }}">Notificaciones</a>
								</li>
							</ul>
						</li>
						@endcan
						
						@can('Admin_Interface_Refacciones')
						<li>
							<a href="#">
								<i class="fa fa-truck"></i>
								<span class="menu-title">
									<span class="menu-title"><strong>Refacciones</strong></span>
								</span>											
								<i class="arrow"></i>
							</a>					
							<ul class="collapse">
								<li><a href="{{ action('Admin\CMSBannersController@refacciones') }}">Página Inicio</a></li>
								{{-- <li><a href="{{ action('Admin\CMSBannersController@menuRefacciones') }}">Menu Lateral (Refacciones)</a></li> --}}
								<li><a href="{{ action('Admin\CMSBannersController@refaccionesDistribuidores') }}">Página Inicio Distribuidores</a></li>
							</ul>
						</li>
						@endcan

						@can('Orders_Publish')
						<li>
							<a href="{{action('Admin\OrdersController@create')}}">
								<i class="fa fa-gear"></i>
								<span class="menu-title">
									<strong>P.O.S</strong>
								</span>
							</a>
						</li>
						@endcan
		
					</ul>


				</div>
			</div>
		</div>
		<!--================================-->
		<!--End menu-->

	</div>
</nav>