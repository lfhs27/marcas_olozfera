<nav id="mainnav-container">
	<div id="mainnav">

		<!--Menu-->
		<!--================================-->
		<div id="mainnav-menu-wrap">
			<div class="nano">
				<div class="nano-content">
					<ul id="mainnav-menu" class="list-group">
			
						<!--Category name-->
						{{-- <li class="list-header">Navigation</li> --}}
			
						<!--Menu list item-->
						<li>
							<a href="{{action('Admin\DashboardController@index')}}">
								<i class="fa fa-dashboard"></i>
								<span class="menu-title">
									<strong>Dashboard</strong>
									<!-- <span class="label label-success pull-right">Top</span> -->
								</span>
							</a>
						</li>

						@can('Business_Unit_Read')
						<li>
							<a href="{{ action('Admin\BusinessUnitsController@index') }}">
								<i class="fa fa-building"></i>
								<span class="menu-title"><strong>Unidades de Negocio</strong></span>
								<i class="arrow"></i>
							</a>

							<ul class="collapse">
								<li><a href="{{action('Admin\BusinessUnitsController@index')}}">Administrar Unidades</a></li>
								{{-- @can('admin-super-admin') --}}
								<li><a href="{{action('Admin\BusinessUnitsController@create')}}">Nueva Unidad</a></li>
								{{-- @endcan --}}
								<li>
									<a href="{{ action('Admin\SlidersController@index') }}">Sliders P/ Unidades <i class="arrow"></i></a>
									<ul class="collapse">
										<li><a href="{{ action('Admin\SlidersController@index') }}">Sliders</a></li>
										<li><a href="{{ action('Admin\SlidersController@create') }}">Nuevo slider</a></li>
									</ul>
								</li>
							</ul>
						</li>
						@endcan

						@can('Orders_Read')
						<li>
							<a href="{{ action('Admin\OrdersController@index') }}">
								<i class="fa fa-th-list"></i>
								<span class="menu-title"><strong>Pedidos</strong></span>
							</a>
							<ul class="collapse">
								@foreach(Auth::user()->roles as $role)
								@foreach($role->businessUnits as $rBunit)
								<li>
									<a href="{{ action('Admin\OrdersController@ordersByBusinessUnit',$rBunit->id) }}">Pedidos en {{$rBunit->name}}</a>
								</li>
								@endforeach
								@endforeach
								<li>
									<a href="{{ action('Admin\OrdersController@ordersRefacciones') }}">Pedidos en Refacciones</a>
								</li>
							</ul>
						</li>
						@endcan
			
						@can('Products_Read')
						<li>
							<a href="#">
								<i class="fa fa-briefcase"></i>
								<span class="menu-title"><strong>Productos</strong></span>
								<i class="arrow"></i>
							</a>
			
							
							<ul class="collapse">
								<li><a href="{{ action('Admin\ProductsController@index') }}">Listado</a></li>
								<li><a href="{{ action('Admin\RatingsController@index') }}">Ratings</a></li>
								@can('Products_Publish')
								<li><a href="{{ action('Admin\ProductsController@import') }}">Importar Excel</a></li>
								<li><a href="{{ action('Admin\ProductsController@importShipping') }}">Importar Metodos de Envío</a></li>
								@endcan
								<li class="list-divider"></li>
								@can('Brands_Read')
								<li><a href="{{action('Admin\BrandsController@index')}}">Marcas</a></li>
								<li><a href="{{ action('Admin\FamiliesController@index') }}">Categorías</a></li>
								<li><a href="{{ action('Admin\LinesController@index') }}">Subcategorías</a></li>
								@endcan

								<li><a href="{{ action('Admin\AdditionalItemsController@index') }}">Items Adicionales</a></li>
								{{-- <li><a href="{{ action('Admin\LinesController@index') }}">Listas de Precios</a></li> --}}

								<!-- <li><a href="#">Grupos</a></li> -->
							</ul>
						</li>
			
						<li class="list-divider"></li>

						@endcan
						@can('Admin_Interface_Pages')
						<li>
							<a href="#">
								<i class="fa fa-newspaper-o"></i>
								<span class="menu-title"><strong>CMS</strong></span>
								<i class="arrow"></i>
							</a>

							<ul class="collapse">
								@can('Master_Page_Settings')
								<li>
									<a href="{{ action('Admin\MasterPageController@index') }}">Página Principal <i class="arrow"></i></a>
									<ul class="collapse">
										<li><a href="{{ action('Admin\MasterPageController@editLogo') }}">Logo</a></li>
										<li><a href="{{ action('Admin\MasterPageController@editMenu') }}">Menú Superior</a></li>
										<li><a href="{{ action('Admin\MasterPageController@editSideBar') }}">Menú Lateral</a></li>
										<li><a href="{{ action('Admin\MasterPageController@editSlider') }}">Slider Principal</a></li>
										<li><a href="{{ action('Admin\MasterPageController@editBanners') }}">Banners Media Página</a></li>
										<li><a href="{{ action('Admin\MasterPageController@editBannerWide') }}">Banner Inferior</a></li>
										<li><a href="{{ action('Admin\MasterPageController@editBrands') }}">Marcas</a></li>
										<li><a href="{{ action('Admin\CMSBannersController@promociones') }}">Promociones</a></li>
									</ul>
								</li>
								@endcan
								@can('Admin_Interface_Pages')
								<li>
									<a href="{{ action('Admin\PagesController@index') }}">Páginas Informativas <i class="arrow"></i></a>
									<ul class="collapse">
										<li><a href="{{ action('Admin\PagesController@index') }}">Listado</a></li>
										@can('Pages_Publish')
										<li><a href="{{ action('Admin\PagesController@create') }}">Nueva Página</a></li>
										@endcan
									</ul>
								</li>
								@endcan
								@can('Master_Page_Settings')
								<li><a href="{{ action('Admin\MasterPageController@editLoginBackground') }}">Login</a></li>
								<li><a href="{{ action('Admin\ZipcodesController@index') }}">Códigos Postales</a></li>
								@endcan
								{{-- 
								<li>
									<a href="{{ action('Admin\FormatsController@index') }}">Formatos <i class="arrow"></i></a>
									<ul class="collape">
										<li><a href="{{ action('Admin\FormatsController@index') }}">Formatos</a></li>
										<li><a href="{{ action('Admin\FormatsController@create') }}">Crear Formato</a></li>
									</ul>
								</li>
								--}}
							</ul>
						</li>
						@endcan

						<li class="list-divider"></li>

						@can('Admin_Interface_Companies')
						<li>
							<a href="{{ action('Admin\CompaniesController@index') }}">
								<i class="fa fa-building"></i>
								<span class="menu-title"><strong>Empresas</strong></span>
								<i class="arrow"></i>
							</a>

							<ul class="collapse">
								<li><a href="{{ action('Admin\CompaniesController@index') }}">Empresas</a></li>
								@can('Companies_Publish')
								<li><a href="{{ action('Admin\CompaniesController@create') }}">Nueva Empresa</a></li>
								@endcan
							</ul>
						</li>
						@endcan

						@can('Admin_Interface_Users')
						<li>
							<a href="#">
								<i class="fa fa-user"></i>
								<span class="menu-title"><strong>Usuarios</strong></span>
								<i class="arrow"></i>
							</a>

							<ul class="collapse">
								<li><a href="{{ action('Admin\UsersController@index') }}">Usuarios</a></li>
								<li><a href="{{ action('Admin\UsersController@indexRefacciones') }}">Usuarios Refacciones</a></li>
								@can('Users_Write')
								<li><a href="{{ action('Admin\UsersController@create') }}">Nuevo Usuario</a></li>
								@endcan
								
								{{-- <li><a href="{{ action('Admin\UsersController@create') }}">Carga Masiva</a></li> --}}
								<li class="list-divider"></li>
								<li>
									<a href="{{ action('Admin\UserGroupsController@index') }}">Grupos de Usuarios</a>
								</li>
								<li>
									<a href="{{ action('Admin\BillingGroupsController@index') }}">Grupos de Facturación</a>
								</li>
								<li>
									<a href="{{ action('Admin\UsersController@createExcel') }}">Exportar Usuarios</a>
								</li>
								<li>
									<a href="{{ action('Admin\UsersController@pointsReport') }}">Reporte de Puntos</a>
								</li>
							</ul>
						</li>
						@endcan

						<li class="list-divider"></li>

						@can('Admin_Interface_Settings')
						<li>
							<a href="{{action('Admin\ConfigurationController@index')}}">
								<i class="fa fa-gear"></i>
								<span class="menu-title">
									<strong>Configuración</strong>
								</span>
								<i class="arrow"></i>
							</a>
							<ul>
								<li>
									<a href="{{ action('Admin\PricelistsController@index') }}">Listas de Precios</a>
								</li>
								<li>
									<a href="{{ action('Admin\StockController@index') }}">Inventario</a>
								</li>
								<li>
									{{-- <a href="{{ action('Admin\StockController@editStatesIndex') }}">CEDIS / Estados</a> --}}
								</li>
								<li>
									<a href="{{ action('Admin\ConfigurationController@notifications') }}">Notificaciones</a>
								</li>
							</ul>
						</li>
						@endcan
						
						@can('Admin_Interface_Refacciones')
						<li>
							<a href="#">
								<i class="fa fa-truck"></i>
								<span class="menu-title">
									<span class="menu-title"><strong>Refacciones</strong></span>
								</span>											
								<i class="arrow"></i>
							</a>					
							<ul class="collapse">
								<li><a href="{{ action('Admin\CMSBannersController@refacciones') }}">Página Inicio</a></li>
								{{-- <li><a href="{{ action('Admin\CMSBannersController@menuRefacciones') }}">Menu Lateral (Refacciones)</a></li> --}}
								<li><a href="{{ action('Admin\CMSBannersController@refaccionesDistribuidores') }}">Página Inicio Distribuidores</a></li>
							</ul>
						</li>
						@endcan

						@can('Orders_Publish')
						<li>
							<a href="{{action('Admin\OrdersController@create')}}">
								<i class="fa fa-gear"></i>
								<span class="menu-title">
									<strong>P.O.S</strong>
								</span>
							</a>
						</li>
						@endcan
			
						<!--Category name-->
						<!-- <li class="list-header">Components</li> -->
			
						<!--Menu list item-->
						<!-- <li>
							<a href="#">
								<i class="fa fa-briefcase"></i>
								<span class="menu-title">UI Elements</span>
								<i class="arrow"></i>
							</a>
			
							
							<ul class="collapse">
								<li><a href="ui-buttons.html">Buttons</a></li>
								<li><a href="ui-checkboxes-radio.html">Checkboxes &amp; Radio</a></li>
								<li><a href="ui-panels.html">Panels</a></li>
								<li><a href="ui-modals.html">Modals</a></li>
								<li><a href="ui-progress-bars.html">Progress bars</a></li>
								<li><a href="ui-components.html">Components</a></li>
								<li><a href="ui-typography.html">Typography</a></li>
								<li><a href="ui-list-group.html">List Group</a></li>
								<li><a href="ui-tabs-accordions.html">Tabs &amp; Accordions</a></li>
								<li><a href="ui-alerts-tooltips.html">Alerts &amp; Tooltips</a></li>
								<li><a href="ui-helper-classes.html">Helper Classes</a></li>
								
							</ul>
						</li> -->
			
						<!--Menu list item-->
						<!-- <li>
							<a href="#">
								<i class="fa fa-edit"></i>
								<span class="menu-title">Forms</span>
								<i class="arrow"></i>
							</a>
			
							<ul class="collapse">
								<li><a href="forms-general.html">General</a></li>
								<li><a href="forms-components.html">Components</a></li>
								<li><a href="forms-validation.html">Validation</a></li>
								<li><a href="forms-wizard.html">Wizard</a></li>
								
							</ul>
						</li> -->
		
					</ul>


				</div>
			</div>
		</div>
		<!--================================-->
		<!--End menu-->

	</div>
</nav>