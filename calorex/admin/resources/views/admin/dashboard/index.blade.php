@extends('layouts.admin')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('success'))
	<div class="alert alert-success" role="alert">{!! Session::get('success') !!}</div>
@endif

<div class="row">
	<div class="eq-height">
		@foreach($slides as $slide)
		<div class="col-sm-12">
			<div class="panel">
				<form method="POST" action="banners/upload" accept-charset="UTF-8" enctype="multipart/form-data">
					<input name="_token" type="hidden" value="{{ csrf_token() }}">
					<div class="panel-heading">
						<h3 class="panel-title">
							<a href="#" class="deleteBanner" data-id="{{ $slide->id }}" style="float:right;"><i class="fa fa-times"></i></a>
						</h3>
					</div>
					<div class="panel-body">
						<img src="{{ $slide->src }}" alt="" style="width:100%;" />
					</div>
					<div class="panel-footer">
						<input type="text" class="form-control" name="liga" placeholder="Liga" value="{{ $slide->link }}" style="margin-bottom:15px;" />
						<div class="col-xs-6" style="padding:0px;">
							<input type="hidden" name="id" value="{{ $slide->id }}" />
							<input type="hidden" name="type" value="slide" />
							<input type="hidden" name="title" value="" />
							<input type="hidden" name="viewCallback" value="/" />
							<input name="image" type="file">
						</div>
						<div class="col-xs-6" style="padding:0px;">
							<input type="submit" class="btn btn-success" value="Guardar" style="float:right; position:relative; bottom:5px;" />
						</div>
						<div style="clear:both;"></div>
					</div>
				</form>
			</div>
		</div>
		@endforeach
		<div class="col-sm-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						Agregar Slide
					</h3>
				</div>
				<div class="panel-body">
					<form method="POST" action="banners/upload" accept-charset="UTF-8" enctype="multipart/form-data">
						<input name="_token" type="hidden" value="{{ csrf_token() }}">
						<div class="control-group">
							<div class="controls">
								<input name="image" type="file">
								<input type="text" name="liga" placeholder="Liga" style="margin-bottom:10px;" class="form-control" />
								<input type="hidden" name="type" value="slide" />
								<input type="hidden" name="title" value="" />
								<input type="hidden" name="viewCallback" value="/" />
							</div>
						</div>
						<div id="success"> </div>
						<input class="btn btn-primary send-btn" type="submit" value="Guardar">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$(".deleteBanner").click(function(e){
		e.preventDefault();
		elm = $(this);
		
		$.ajax({
			url : "banners/delete/"+elm.attr("data-id"),
			method : "GET",
			success : function(data){
				location.reload();
			}
		});
		
		return false;
	});
});
</script>



{{-- @include('admin.modals.company') --}}

@endsection