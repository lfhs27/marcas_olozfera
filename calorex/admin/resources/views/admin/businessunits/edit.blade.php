@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				@include('admin.shared.page-title')		

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					@include('admin.shared.page-section-title')

					<div class="row">
						<div class="col-sm-3">
							<a target="_blank" href="{{ action('StoreController@getBusinessUnit',$businessUnit->slug) }}" class="btn btn-primary">Ver Página de Unidad</a>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							{!! Form::open(['action' => ['Admin\BusinessUnitsController@update',$businessUnit->id],'method' => 'PATCH','files' => true]) !!}
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Configuración General</h3>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-6">
											<div class="panel-group">
												<label class="control-label">Nombre</label>
												<input type="text" name="name" id="inputName" class="form-control" value="{{ $businessUnit->name }}">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												@if($businessUnit->logo)
												<img class="img-responsive" src="{{asset($businessUnit->logo->image->filepath.'/'.$businessUnit->logo->image->filename)}}">
												@endif
												<label class="control-label">Logo</label>
												<input type="file" name="logo">
											</div>
										</div>
									</div>
									{{-- <div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="">Lista de Precios</label>
												<select name="pricelist" id="inputPricelist" class="form-control" required="required">
													<option value="0">-- SELECCIONE UNA LISTA DE PRECIOS --</option>
													@forelse($pricelists as $pricelist)
													<option value="{{ $pricelist->id }}">{{ $pricelist->name }}</option>
													@empty
														<option value="0">-- NO EXISTEN LISTAS DE PRECIOS --</option>
													@endforelse
												</select>
											</div>
										</div>
									</div> --}}
									<div class="row">
										{{-- <div class="col-sm-6">
											<label for="" class="control-label">Menú Principal</label>
											<select name="mainmenu" id="inputMainmenu" class="form-control">
												<option value="0">-- Seleccione un menú --</option>
												@forelse($businessUnit->menus as $menu)
												<option {{ $businessUnit->menu_id === $menu->id ? 'selected="selected"' : '' }} value="{{ $menu->id }}">{{ $menu->name }}</option>
												@empty
												<option value="0">-- NO EXISTEN MENÚS --</option>
												@endforelse
											</select>
										</div> --}}
										<div class="col-sm-6">
											<div class="form-group">
												<label for="" class="control-label">Slider Principal</label>
												<select name="mainSlider" id="inputMainSlider" class="form-control">
													<option value="0">-- Seleccione un slider --</option>
													@foreach($sliders as $slider)
													<option value="{{ $slider->id }}">{{ $slider->name }}</option>
													@endforeach
												</select>
											</div>
											<a class="btn btn-sm btn-info" href="{{ action('Admin\SlidersController@create') }}">Nuevo Slider</a>
										</div>
									</div>
								</div>
								<div class="panel-footer">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Configuración de Banners Laterales</h3>
								</div>
								<div class="panel-body">
									{!! Form::open(['action' => ['Admin\BusinessUnitsController@update',$businessUnit->id],'method' => 'PATCH','id' => 'form-delete-banner']) !!}
									<input type="hidden" name="deleteBanner" id="inputDeleteBanner" value="">
									<div class="banner-group">
										<?php $banners = json_decode($businessUnit->banners_json); ?>
										@if(count($banners) > 0)
											@foreach($banners as $key => $banner)
											<div class="row" data-index="{{$key}}">
												<div class="col-sm-6">
													<img src="{{ asset('image/' . $banner->image_id) }}" alt="">
												</div>
												<div class="col-sm-4">
													<input type="text" name="bannerUrl{{ $key }}" id="inputBannerUrl{{ $key }}" class="form-control" value="{{$banner->url}}" placeholder="http://">
												</div>
												<div class="col-sm-2">
													<a href="#inputBannerUrl{{ $key }}" data-banner-position="normal" data-banner-index="{{ $key }}" class="pull-left btn btn-primary btn-save-banner-url"><i class="fa fa-save"></i></a>
													<a href="#" data-banner-position="normal" data-banner-index="{{ $key }}" class="btn btn-danger btn-delete-banner pull-right"><i class="fa fa-trash"></i></a>
												</div>
											</div>
											@endforeach
										@endif
									</div>
									{!! Form::close() !!}

									<a class="btn btn-add-banner btn-default" data-toggle="modal" href='#modal-add-banner'>Agregar Banner</a>
								</div>
								<div class="panel-footer">
									<!-- <button type="submit" class="btn btn-primary">Guardar</button> -->
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Configuración de Banners Inferiores</h3>
								</div>
								<div class="panel-body">
									{!! Form::open(['action' => ['Admin\BusinessUnitsController@update',$businessUnit->id],'method' => 'PATCH','id' => 'form-delete-banner-bottom']) !!}
									<input type="hidden" name="deleteBannerBottom" id="inputDeleteBannerBottom" value="">
									<div class="banner-group">
										<?php $bannersBottom = json_decode($businessUnit->banners_bottom_json); ?>
										@if($bannersBottom)
											@foreach($bannersBottom as $key => $banner)
											<div class="row" data-index="{{$key}}">
												<div class="col-sm-6">
													<img src="{{ asset('image/' . $banner->image_id) }}" alt="" class="img-responsive">
												</div>
												<div class="col-sm-4">
													<input type="text" name="bannerUrl{{ $key }}" id="inputBannerBottomUrl{{ $key }}" class="form-control" value="{{$banner->url}}" placeholder="http://">
												</div>
												<div class="col-sm-2">
													<a href="#inputBannerBottomUrl{{ $key }}" data-banner-position="bottom" data-banner-index="{{ $key }}" class="pull-left btn btn-primary btn-save-banner-url"><i class="fa fa-save"></i></a>

													<a href="#" data-banner-position="bottom" data-banner-index="{{ $key }}" class="btn btn-danger btn-delete-banner"><i class="fa fa-trash"></i></a>
												</div>
											</div>
											@endforeach
										@endif
									</div>
									{!! Form::close() !!}

									<a class="btn btn-default" data-toggle="modal" href='#modal-add-banner-bottom'>Agregar Banner Inferior</a>
								</div>
								<div class="panel-footer">
									<!-- <button type="submit" class="btn btn-primary">Guardar</button> -->
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Marcas</h3>
								</div>
								<div class="panel-body">
									{!! Form::open(['action' => ['Admin\BusinessUnitsController@update',$businessUnit->id],'method' => 'PATCH','id' => 'form-delete-brand']) !!}
									<input type="hidden" name="deleteBrand" id="inputDeleteBrand" value="">
									<div class="brand-group">
										<?php $brandsBottom = json_decode($businessUnit->brands_json); ?>
										@if($brandsBottom)
											@foreach($brandsBottom as $key => $brand)
											<div class="row" data-index="{{$key}}">
												<div class="col-sm-4">
													<img src="{{ asset('image/' . $brand->image_id_on) }}" alt="" class="img-responsive">
												</div>
												<div class="col-sm-4">
													<img src="{{ asset('image/' . $brand->image_id_off) }}" alt="" class="img-responsive">
												</div>
												<div class="col-sm-4">
													{{$brand->url}}
												</div>
												<div class="col-sm-1">
													<a href="#" data-brand-index="{{ $key }}" class="btn btn-danger btn-delete-brand"><i class="fa fa-trash"></i></a>
												</div>
											</div>
											@endforeach
										@endif
									</div>
									{!! Form::close() !!}

									<a class="btn btn-default" data-toggle="modal" href='#modal-add-brand'>Agregar logo de marca</a>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Menú Superior</h3>
								</div>
								<div class="panel-body">
									<div id="menuSuccessAlert" class="alert alert-success" role="alert" style="display:none;">El menú ha sido guardado</div>
									<div class="table-responsive">
										<input type="hidden" id="businessUnitId" value="{{ $businessUnit->id }}" />
										<input type="hidden" id="token" value="{{ csrf_token() }}" />
										<table id="header-menu" class="table table-hover">
											<thead>
												<tr>
													<th>#</th>
													<th>Nombre</th>
													<th>Liga</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php $menu = json_decode($businessUnit->header_menu);?>
												@if($menu)
												@foreach($menu as $i => $element)
												<tr>
													<td>{{ $i+1 }}</td>
													<td><input type="text" name="name" data-i="{{ $i+1 }}" class="form-control" value="{{ $element->name }}" placeholder="Nombre de la Liga" /></td>
													<td><input type="text" name="url" data-i="{{ $i+1 }}" class="form-control" value="{{ $element->url }}" placeholder="URL del elemento" /></td>
													<td><span class="btn btn-danger" onclick="deleteMenuElement(this)">Borrar</span></td>
												</tr>
												@endforeach
												@endif
												<tr>
													<td>{{ COUNT($menu)+1 }}</td>
													<td><input type="text" name="name" data-i="{{ COUNT($menu)+1 }}" class="form-control" value="" placeholder="Nombre de la Liga" /></td>
													<td><input type="text" name="url" data-i="{{ COUNT($menu)+1 }}" class="form-control" value="" placeholder="URL del elemento" /></td>
													<td><span class="btn btn-danger" onclick="deleteMenuElement(this)">Borrar</span></td>
												</tr>
											</tbody>
										</table>
									</div>

									
								</div>
								<div class="panel-footer">
									<span class="btn btn-default" onclick="addMenuElement()">Agregar Elemento</span>
									<span class="btn btn-primary" onclick="guardarMenu()">Guardar</span>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">CEDIS</h3>
								</div>
								<div class="panel-body">
									<table class="table table-condensed table-hover">
										<thead>
											<tr>
												<th>Nombre</th>
												<th>Org. Code</th>
												<th>Sub. Code</th>
												<th>&nbsp;</th>
											</tr>
										</thead>
										<tbody>
											@foreach($businessUnit->centers as $cedis)
											<tr>
												<td>{{ $cedis->name }}</td>
												<td>{{ $cedis->organization_code }}</td>
												<td>{{ $cedis->subinventory_code }}</td>
												<td>{{ $cedis->ship_from_org }}</td>
												<td>
													{!! Form::open(['action' => ['Admin\BusinessUnitsController@update',$businessUnit->id],'method' => 'PATCH']) !!}
													<input type="hidden" name="removeCedis" value="{{ $cedis->id }}">
													<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
													{!! Form::close() !!}
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
									
									<a class="btn btn-primary" data-toggle="modal" href='#modal-add-cedis'>Agregar CEDIS</a>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Datos para Colocación de Pedidos</h3>
								</div>
								<div class="panel-body">
									{!! Form::open(['action' => ['Admin\BusinessUnitsController@update',$businessUnit->id],'method' => 'PATCH']) !!}
									<div class="row">
										<div class="col-sm-2">
											<div class="form-group">
												<label for="" class="control-label"><strong>ORG_ID</strong></label>
												<input type="text" name="org_id" id="inputOrg_id" class="form-control" value="{{ $businessUnit->org_id }}">
											</div>
										</div>

										{{-- <div class="col-sm-2">
											<div class="form-group">
												<label for="" class="control-label"><strong>SHIP_FROM_ORG</strong></label>
												<input type="text" name="ship_from_org" id="inputShip_from_org" class="form-control" value="{{ $businessUnit->ship_from_org }}">
											</div>
										</div>

										<div class="col-sm-3">
											<div class="form-group">
												<label for="" class="control-label"><strong>SHIP_FROM_ORG SALTILLO</strong></label>
												<input type="text" name="ship_from_org_saltillo" id="inputShip_from_org_saltillo" class="form-control" value="{{ $businessUnit->ship_from_org_saltillo }}">
											</div>
										</div>

										<div class="col-sm-3">
											<div class="form-group">
												<label for="" class="control-label"><strong>SHIP_FROM_ORG MEXICO</strong></label>
												<input type="text" name="ship_from_org_mexico" id="inputShip_from_org_mexico" class="form-control" value="{{ $businessUnit->ship_from_org_mexico }}">
											</div>
										</div>

										<div class="col-sm-2">
											<div class="form-group">
												<label for="" class="control-label"><strong>CONTEXT</strong></label>
												<input type="text" name="context" id="inputContext" class="form-control" value="{{ $businessUnit->context }}">
											</div>
										</div> --}}
									</div>	
{{-- 
									<div class="row">
										<div class="col-sm-12">
											<label for="" class="control-label"><strong>ORDER_TYPE</strong></label>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-3">
											<p>
												<label for="" class="control-label">POTVTAS PEDIDO OTRAS VENTAS DIFERENTES A CRÉDITO o FILIALES CALENTADORES SALTILLO</label>
												<input type="text" name="order_type_venta_saltillo" id="inputOrder_type_1" class="form-control" value="{{ $businessUnit->order_type_venta_saltillo }}">
											</p>
										</div>
										<div class="col-sm-3">
											<p>
												<label for="" class="control-label">POTVTAS PEDIDO OTRAS VENTAS DIFERENTES A CRÉDITO o FILIALES CALENTADORES MÉXICO</label>
												<input type="text" name="order_type_venta_mexico" id="inputOrder_type_2" class="form-control" value="{{ $businessUnit->order_type_credito_mexico }}">
											</p>
										</div>
										<div class="col-sm-3">
											<p>
												<label for="" class="control-label">PVFILNAL PEDIDO VTA FILIAL NACIONAL CALENTADORES SALTILLO</label>
												<input type="text" name="order_type_credito_saltillo" id="inputOrder_type_3" class="form-control" value="{{ $businessUnit->order_type_credito_saltillo }}">
											</p>
										</div>
										<div class="col-sm-3">
											<p>
												<label for="" class="control-label">POTVTAS PEDIDO OTRAS VENTAS DIFERENTES A CRÉDITO o FILIALES CALENTADORES MÉXICO</label>
												<input type="text" name="order_type_credito_mexico" id="inputOrder_type_4" class="form-control" value="{{ $businessUnit->order_type_credito_mexico }}">
											</p>
										</div>
									</div>
 --}}
									<div class="row">
										<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
											<button class="btn btn-primary" type="submit">Guardar</button>
										</div>
									</div>

									{!! Form::close() !!}
								</div>
							</div>
						</div>
					</div>

				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

			<div class="modal fade" id="modal-add-banner">
				<div class="modal-dialog">
					<div class="modal-content">
						{!! Form::open(['action' => ['Admin\BusinessUnitsController@update',$businessUnit->id],'method' => 'PATCH','files' => true]) !!}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Agregar Banner</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<p>
									<label for="" class="control-label">URL</label>
									<input type="text" name="url" id="inputUrl" class="form-control" value="">
								</p>
							</div>

							<div class="form-group">
								<p>
									<label for="" class="control-label">Archivo</label>
									<input type="file" name="bannerImage">
								</p>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-primary">Guardar</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>

			<div class="modal fade" id="modal-add-banner-bottom">
				<div class="modal-dialog">
					<div class="modal-content">
						{!! Form::open(['action' => ['Admin\BusinessUnitsController@update',$businessUnit->id],'method' => 'PATCH','files' => true]) !!}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Agregar Banner</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<p>
									<label for="" class="control-label">URL</label>
									<input type="text" name="url" id="inputUrl" class="form-control" value="">
								</p>
							</div>

							<div class="form-group">
								<p>
									<label for="" class="control-label">Archivo</label>
									<input type="file" name="bannerBottomImage">
								</p>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-primary">Guardar</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>

			<div class="modal fade" id="modal-add-brand">
				<div class="modal-dialog">
					<div class="modal-content">
						{!! Form::open(['action' => ['Admin\BusinessUnitsController@update',$businessUnit->id],'method' => 'PATCH','files' => true]) !!}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Agregar Logos de Marcas</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<p>
									<label for="" class="control-label">URL</label>
									<input type="text" name="url" id="inputUrl" class="form-control" value="">
								</p>
							</div>

							<div class="form-group">
								<p>
									<label for="" class="control-label">Archivo estado normal</label>
									<input type="file" name="brandImage">
								</p>

								<p>
									<label for="" class="control-label">Archivo estado mouseover</label>
									<input type="file" name="brandImageMouseover">
								</p>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-primary">Guardar</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>

			<div class="modal fade" id="modal-add-cedis">
				<div class="modal-dialog">
					<div class="modal-content">
						{!! Form::open(['action' => ['Admin\BusinessUnitsController@update',$businessUnit->id],'method' => 'PATCH','files' => true]) !!}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Agregar CEDIS</h4>
						</div>
						<div class="modal-body">
							{{-- <div class="form-group">
								<label for="" class="control-label">Nombre</label>
								<input type="text" name="cedis_name" id="inputCedisName" class="form-control">
							</div> --}}
							<div class="form-group">
								<select name="cedis_id" id="inputCedis_id" class="form-control" required="required">
									<option value="">Seleccione un Centro de Distribución</option>
									@foreach($cedisList as $cedis)
										<option value="{{ $cedis->id }}">{{ $cedis->name . ' ' . $cedis->organization_code . ' ' . $cedis->subinventory_code }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-primary">Guardar</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>


	<script>
		$(document).ready(function() {
			$.ajaxSetup({
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
			});

			$('.btn-delete-banner').on('click', function(event) {
				event.preventDefault();
				var bannerPosition = $(this).data('banner-position');
				var bannerIndex = $(this).data('banner-index');

				if (bannerPosition == 'normal') {
					$('#inputDeleteBanner').val(bannerIndex);
					$('#form-delete-banner').submit();
				} else {
					$('#inputDeleteBannerBottom').val(bannerIndex);
					$('#form-delete-banner-bottom').submit();
				}
			});

			$('.btn-save-banner-url').on('click',function(event){
				event.preventDefault();

				var bannerPosition = $(this).data('banner-position');
				var bannerIndex = $(this).data('banner-index');
				var bannerInputId = $(this).attr('href');
				var bannerUrl = $(bannerInputId).val();
				var formData = new FormData();

				formData.append('_method','PATCH');
				formData.append('bannerPosition',bannerPosition);
				formData.append('bannerIndex',bannerIndex);
				formData.append('bannerUrl',bannerUrl);

				$.ajax({
					url: '/admin/businessunits/{{$businessUnit->id}}',
					type: 'POST',
					processData: false,
					contentType: false,
					data: formData
				})
				.done(function(response) {
					console.log("success");
					console.log(response);
					alert(response.message);
				})
				.fail(function(xhr) {
					console.log("error");
					$('body').html(response.responseText);
				})
				.always(function() {
					console.log("complete");
				});
				
			});

			$('.btn-delete-brand').on('click', function(event){
				event.preventDefault();

				var brandIndex = $(this).data('brand-index');
				
				$('#inputDeleteBrand').val(brandIndex);

				$('#form-delete-brand').submit();
			});
		});
		
		function addMenuElement(){
			i = $("#header-menu tbody tr").length + 1;
			html = '<tr>';
			html += '	<td>'+i+'</td>';
			html += '	<td><input type="text" name="name" data-i="'+i+'" class="form-control" value="" placeholder="Nombre de la Liga" /></td>';
			html += '	<td><input type="text" name="url" data-i="'+i+'" class="form-control" value="" placeholder="URL del elemento" /></td>';
			html += '	<td><span class="btn btn-danger" onclick="deleteMenuElement(this)">Borrar</span></td>';
			html += '</tr>';
			$("#header-menu tbody").append(html);
		}
		
		function deleteMenuElement(elm){
			$(elm).parent().parent().remove();
			$.each($("#header-menu tbody tr"), function(i, val){
				$(val).find("td").first().html(i+1);
			});
		}
		
		function guardarMenu(){
			elements = $.map($("#header-menu tbody tr"), function(val, i){
							name = ($(val).find("[name=name]").val()).trim();
							url = ($(val).find("[name=url]").val()).trim();
							
							if(name!="" && url!=""){
								return {
										"name" : name,
										"url" : url
										};
							}else{
								return null;
							}
						});
						
			if(elements.length > 0){		
				$.ajax({
					url: '/admin/businessunits/saveHeaderMenu',
					type: 'POST',
					dataType: 'json',
					data: {
						"id" : $("#businessUnitId").val(),
						"menu" : elements
					},
					headers: {
						'X-CSRF-TOKEN': $("#token").val()
					}
				})
				.done(function(response) {
					console.log("success");
					$("#menuSuccessAlert").show();
				})
				.fail(function(response) {
					console.log("error");
					$("#menuSuccessAlert").hide();
				});
			}
		}
	</script>

@endsection