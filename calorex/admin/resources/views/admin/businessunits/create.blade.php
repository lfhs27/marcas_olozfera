@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				@include('admin.shared.page-title')		

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					@include('admin.shared.page-section-title')
					
					{!! Form::open(['action'=>'Admin\BusinessUnitsController@store']) !!}

					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="inputName" class="control-label">Nombre</label>
										<input type="text" name="name" id="inputName" class="form-control">
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label">Descripción</label>
										<input type="text" name="description" id="inputDescription" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
							</div>
						</div>
					</div>

					{!! Form::close() !!}
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

@endsection