	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Grupo Industrial Saltillo</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<script>
	var baseUrl = '<?php echo URL::to('/'); ?>';
	</script>


	<!--STYLESHEET-->
	<!--=================================================-->

	<!--Open Sans Font [ OPTIONAL ] -->
 	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">


	<!--Bootstrap Stylesheet [ REQUIRED ]-->
	<link href="{{ asset('assets/nifty/css/bootstrap.min.css') }}" rel="stylesheet">


	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="{{ asset('assets/nifty/css/nifty.min.css') }}" rel="stylesheet">

	
	<!--Font Awesome [ OPTIONAL ]-->
	<link href="{{ asset('assets/nifty/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">


	<!--Switchery [ OPTIONAL ]-->
	<link href="{{ asset('assets/nifty/plugins/switchery/switchery.min.css') }}" rel="stylesheet">


	<!--Bootstrap Select [ OPTIONAL ]-->
	<link href="{{ asset('assets/nifty/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet">

	<link rel="stylesheet" href="{{ asset('assets/nifty/plugins/dropzone/dropzone.css') }}">

	<link href="{{ asset('assets/nifty/plugins/summernote/summernote.min.css') }}" rel="stylesheet">

	<link href="{{ asset('assets/nifty/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/nifty/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}" rel="stylesheet">

	<!--Demo [ DEMONSTRATION ]-->
	<link href="{{ asset('assets/nifty/css/demo/nifty-demo.min.css') }}" rel="stylesheet">


	<link rel="stylesheet" href="{{ asset('assets/lib/bootstrap-colorpicker-master/dist/css/bootstrap-colorpicker.min.css') }}">

	<link rel="stylesheet" href="{{ asset('assets/admin/css/jquery.tagit.css')}}">
	<link rel="stylesheet" href="{{ asset('assets/admin/css/tagit.ui-zendesk.css')}}">
	<link rel="stylesheet" href="{{ asset('assets/admin/css/gis.css')}}">
	
	<link rel="stylesheet" type="text/css" href="{{ asset('vendors/jquery-ui-1.11.4.custom/jquery-ui.min.css') }}">


	<!--SCRIPT-->

	<!--jQuery [ REQUIRED ]-->
	<script src="{{ asset('assets/nifty/js/jquery-2.1.1.min.js') }}"></script>
	
    <!-- jQuery and jQuery UI are required dependencies. -->
    <!-- Although we use jQuery 1.4 here, it's tested with the latest too (1.8.3 as of writing this.) -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
	
	<script src="{{ asset('assets/admin/js/tag-it.min.js') }}"></script>
	<!--=================================================-->

	<!--Page Load Progress Bar [ OPTIONAL ]-->
	<!-- <link href="{{ asset('assets/nifty/plugins/pace/pace.min.css') }}" rel="stylesheet"> -->
	<!-- <script src="{{ asset('assets/nifty/plugins/pace/pace.min.js') }}"></script> -->


	
	<!--

	REQUIRED
	You must include this in your project.

	RECOMMENDED
	This category must be included but you may modify which plugins or components which should be included in your project.

	OPTIONAL
	Optional plugins. You may choose whether to include it in your project or not.

	DEMONSTRATION
	This is to be removed, used for demonstration purposes only. This category must not be included in your project.

	SAMPLE
	Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


	Detailed information and more samples can be found in the document.

	-->
		