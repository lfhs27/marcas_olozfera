@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
<!--===================================================-->
<!--div id="content-container"-->
	
	@include('admin.shared.page-title')		

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		
		@include('admin.shared.page-section-title')
		
		<div class="row">
			<div class="col-md-6">
				{!! Form::open(['action' => ['Admin\FamiliesController@update',$family->id],'method' => 'PATCH']) !!}
				<div class="panel panel-default">
					<div class="panel-body">
						{{-- <div class="form-group">
							<label for="" class="control-label">Nombre</label>
							<input type="text" name="name" id="inputName" class="form-control" value="{{$family->name}}" required="required">
						</div> --}}

						<div class="form-group">
							<label for="" class="control-label">Nombre</label>
							<input type="text" name="front_name" id="inputFrontName" class="form-control" value="{{$family->front_name}}" required="required">
						</div>

						<div class="form-group">
							<label for="" class="control-label">Texto Mouseover</label>
							<input type="text" name="hovertext" id="inputHovertext" class="form-control" value="{{ $family->hovertext }}" required="required">
						</div>

						<div class="form-group">
							<label for="" class="control-label">Descripción</label>
							<textarea name="description" id="inputDescription" class="form-control" rows="3">{{ $family->description }}</textarea>
						</div>

						<div class="form-group">
							<label for="" class="control-label">Marca</label>
							<select name="brand_id" id="inputBrand_id" class="form-control" required="required">
								@foreach($brands as $brand)
								<option {{ $family->brand_id == $brand->id ? 'selected="selected"' : '' }} value="{{ $brand->id }}">{{$brand->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="panel-footer">
						<button class="btn btn-primary">Guardar</button>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<!--===================================================-->
	<!--End page content-->


<!--/div-->
<!--===================================================-->
<!--END CONTENT CONTAINER-->

@endsection