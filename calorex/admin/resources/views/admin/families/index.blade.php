@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
<!--===================================================-->
<!--div id="content-container"-->
	
	@include('admin.shared.page-title')		

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		
		@include('admin.shared.page-section-title')

		<a class="btn btn-primary" data-toggle="modal" href='#add-family'>Agregar Categoría</a>
		{!! Form::open(['action'=>['Admin\FamiliesController@updateOrder'],'method' => 'POST','id' => 'fOrder']) !!}
		
		<input type="hidden" name="families_order" value="" />
		<input type="submit" class="btn btn-success" value="Guardar Orden" />
		{!! Form::close() !!}

		<div class="panel panel-default">
			<div class="panel-body">

				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nombre</th>
								<th><abbr title="Identificador para URL (SEO)">Slug</abbr></th>
								<th>Marca</th>
								<th></th>
							</tr>
						</thead>
						<tbody class="boxes">
							@forelse($families as $family)
							<tr data-model-id="{{ $family->id }}">
								<td>{{$family->id}}</td>
								<td>{{$family->front_name}}</td>
								<td>{{$family->slug}}</td>
								<td>{{($family->brand?$family->brand->name:"")}}</td>
								<td>
									{!! Form::open(['action' => ['Admin\FamiliesController@destroy',$family->id],'method' => 'DELETE']) !!}
									<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
									<a href="{{ action('Admin\FamiliesController@edit',$family->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
									{!! Form::close() !!}
								</td>
							</tr>
							@empty

							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
	<!--===================================================-->
	<!--End page content-->


<!--/div-->
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<div class="modal fade" id="add-family">
	<div class="modal-dialog">
		<div class="modal-content">
			{!! Form::open(['action' => 'Admin\FamiliesController@store','method' => 'post']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Agregar Categoría</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<span class="control-label">Nombre Original</span>
					<input type="text" name="name" id="inputName" class="form-control" value="" required="required">
				</div>

				<div class="form-group">
					<lable class="control-label">Nombre Tienda</lable>
					<input type="text" name="front_name" id="inputFrontName" class="form-control" value="" required="required">
				</div>

				<div class="form-group">
					<label for="" class="control-label">Texto Mouseover</label>
					<input type="text" name="hovertext" id="inputHovertext" class="form-control" value="" required="required">
				</div>

				<div class="form-group">
					<label for="" class="control-label">Marca</label>
					<select class="form-control" name="brand_id" id="inputBrandId">
						<option value="0">Seleccione una marca</option>
						@foreach($brands as $brand)
						<option value="{{ $brand->id }}">{{$brand->name}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$('.boxes').sortable({
		opacity: 0.6,
		tolerance: "pointer"
	});
	
	$("#fOrder").submit(function(e){
		sorted = $('.boxes').sortable('toArray',{attribute: 'data-model-id'});
		sortedObj = $.map(sorted, function(val, i){
			return {
					'id' : val,
					'order' : i
				};
		});
		$(this).find("[name=families_order]").val(JSON.stringify(sortedObj));
		
	});
});
</script>
@endsection