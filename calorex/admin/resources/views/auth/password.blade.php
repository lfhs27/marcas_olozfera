@extends('layouts.mail')

@section('content')

<p>Hola, no te preocupes m&aacute;s, da click <a href="{{ url('password/reset/'.$token) }}">aqu&iacute;</a> para restablecer tu contrase&ntilde;a.</p>
<p>Recuerda que una vez restablecida, puedes cambiarla nuevamente cuando gustes dentro de la secci&oacute;n de Mi Cuenta. Tu contrase&ntilde;a debe de ser privada y recomendamos que consista de caracteres, may&uacute;sculas y n&uacute;meros para hacerla m&aacute;s segura. </p>
<p>Atentamente,<br />
Equipo Calorex</p>
			
@endsection