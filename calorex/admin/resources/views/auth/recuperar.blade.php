<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tienda Calorex</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{ asset('assets/front/css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/ventadirecta.css') }}">

	<script src="{{ asset('assets/front/js/jquery-1.11.3.min.js') }}"></script>
	<script src="{{ asset('assets/front/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/front/js/jquery.cycle2.min.js') }}"></script>
	<script src="{{ asset('assets/front/js/ventadirecta.js') }}"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="template-login" style="background-image: url({{ asset('assets/front/img/ventadirecta/bgVD02.png') }}); background-repeat: no-repeat;">
	<header>
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('assets/front/img/ventadirecta/logob-tiendaCalorex.png') }}" alt="Grupo Industrial Saltillo"></a>
				</div>
			</div>
		</nav>
	</header>

	<section id="login">
		<div class="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
			<div class="login-content">
				{!! Form::open(array('url' => '/password/email')) !!}
					<div class="form-group">
						<div class="col-sm-12">
							@if (count($errors) > 0)
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label>Correo Electrónico</label>
						<input type="text" name="email" class="form-control" value="{{ old('email') }}">
					</div>
				
					<div class="controls text-center">
						<button type="submit" class="btn btn-primary btn-block">Recuperar Contrase&ntilde;a</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</section>
</body>
</html>