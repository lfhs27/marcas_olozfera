<html>
	<head>
		<style>
		body{
			font-family:Arial, Helvetica, sans-serif;
			background-color:#F6F8FA;
		}
		.title{
			color:#546578;
			font-weight:bold;
		}	
		.bg-header{
			background-color:#546578; 
			height:150px;
		}
		.content{
			box-shadow:1px 1px 5px #546578; 
			margin:-110px auto 0 auto; 
			background-color:#FFF; 
			width:80%;
			padding:35px 55px;
		}
		.link{
			color:#FFB22B;
			text-decoration:underline;
			cursor:pointer;
		}
		.footer{
			background-color:#546578;
			box-shadow:1px 1px 5px #546578; 
			padding:35px 55px;
			width:80%;
			margin:-1px auto 0px auto;
			color:#FFF;
		}
		.address{
			float:right;
			margin: 10px 50px;
		}
		.clear{
			clear:both;
		}
		.dudas{
			text-align:center; 
			padding:20px;
			color:#C0C0C0;
		}
		.dudas .link{
			color:#546578;
			text-decoration:none;
		}
		table{
			width:100%;
			border-collapse:collapse;
		}
		table thead tr td{
			font-weight:bold;
			background-color:#E9E9E9;
			text-align:left;
			padding:20px 30px;
		}
		table tbody tr td{
			padding:20px 30px;
		}
		table tfoot{
			border-top:2px solid #E9E9E9;
		}
		table tfoot tr td{
			padding:5px 30px;
		}
		table tfoot tr td:nth-child(1){
			text-align:right;
			font-weight:bold;
		}
		</style>
	</head>
	<body>
		<div style="font-family:Arial, Helvetica, sans-serif;background-color:#F6F8FA;">
			<table width="100%" style="border-collapse:collapse;">
				<tr>
					<td colspan="4" style="background-color:#546578;	height:50px;"></td>
				</tr>
				<tr>
					<td style="background-color:#546578; width:10%;"></td>
					<td colspan="2" style="background-color:#FFF; width:80%;"><img src="http://tiendacalorex.com/images/mail/calorex-logo.png" alt="logo" style="margin-left:25px; width:300px;" /></td>
					<td style="background-color:#546578; width:10%;"></td>
				</tr>
				<tr>
					<td style="background-color:#F6F8FA; width:10%;"></td>
					<td colspan="2" style="background-color:#FFF; width:80%;padding:35px 55px;">@yield('content')</td>
					<td style="background-color:#F6F8FA; width:10%;"></td>
				</tr>
				<tr>
					<td style="background-color:#F6F8FA; width:10%;"></td>
					<td colspan="2" style="background-color:#F6F8FA; width:80%;padding:35px 55px;">@yield('datosPedido')</td>
					<td style="background-color:#F6F8FA; width:10%;"></td>
				</tr>
				<tr>
					<td style="background-color:#F6F8FA; width:10%;"></td>
					<td colspan="2" style="background-color:#FFF; width:80%;padding:35px 55px;">@yield('datosPago')</td>
					<td style="background-color:#F6F8FA; width:10%;"></td>
				</tr>
				<tr>
					<td style="background-color:#F6F8FA; width:10%;"></td>
					<td style="background-color:#EDF0F1; width:60%;padding:35px 55px; color:#BBBDBF;">
						<div class="address" style="float:right;margin: 10px 50px;">
							Cont&aacute;ctanos: <span class="link" style="color:#546578 !important;text-decoration:none !important;">tiendacalorex@gis.com.mx</span><br />
							O v&iacute;a telef&oacute;nica en el <span class="link" style="color:#546578 !important;text-decoration:none !important;">018000098300</span> o en DF <span class="link" style="color:#546578 !important;text-decoration:none !important;">56400601.</span>
						</div>
					</td>
					<td style="background-color:#EDF0F1; width:20%;padding:35px 55px; color:#FFF; text-align:right;">
						<img src="http://tiendacalorex.com/images/mail/logo_footer.png" alt="logo" style="" />
					</td>
					<td style="background-color:#F6F8FA; width:10%;"></td>
				</tr>
			</table>
			<div class="dudas" style="text-align:center; padding:20px;	color:#C0C0C0;">
				Derechos Reservados Tienda Calorex 2015
			</div>
		</div>
	</body>
</html>