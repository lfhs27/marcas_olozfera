<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tienda Calorex</title>
	@include('ventadirecta.parts.head')
</head>
<body class="template-product">
	
	<header>
		@include('ventadirecta.parts.header')
	</header>

	<section id="breadcrumb" class="hidden-xs">
		<div class="container">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<ol class="breadcrumb">
				</ol>	
			</div>
		</div>
	</section>


	@yield('content')
	
	<footer>
		@include('ventadirecta.parts.footer')
	</footer>
</body>
</html>