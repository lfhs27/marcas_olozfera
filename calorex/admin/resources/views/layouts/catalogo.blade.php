<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tienda Calorex</title>
	@include('ventadirecta.parts.head')
</head>
<body style="background-image: url({{ asset('assets/front/img/ventadirecta/bannerVD-catalogo.png') }}); background-repeat: no-repeat;">
	
	<header class="opacity">
		@include('ventadirecta.parts.header')
	</header>

	

	<section id="page-header" class="hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1>{{ isset($catalogTitle) ? $catalogTitle : 'Tienda Calorex' }}</h1>
				</div>
			</div>
		</div>
	</section>

	<section id="page-content">
		<div class="container">
			<div class="row">
				<div class="hidden-xs col-sm-3" id="sidebar">
					{!! $SidebarNav; !!}
					
					<div class="banners">
					</div>
				</div><!-- #sidebar -->

				<div class="col-xs-12 col-sm-9" id="main-content">
					<div class="row" id="breadcrumbs">
						<div class="col-xs-12 col-sm-4">
							<h1>
								<span class="part-one"><a href="{{ $breadcrumbs['partOne']['link'] }}">{{$breadcrumbs['partOne']['title']}}</a></span>
								@if(isset($breadcrumbs['partTwo']))
								<span class="sep">/</span>
								<span class="part-two"><a href="{{ $breadcrumbs['partTwo']['link'] }}">{{$breadcrumbs['partTwo']['title']}}</a></span>
								@endif
								@if(isset($breadcrumbs['partThree']))
								<span class="sep">/</span>
								<span class="part-three"><a href="{{ $breadcrumbs['partThree']['link'] }}">{{$breadcrumbs['partThree']['title']}}</a></span>
								@endif
							</h1>
						</div>
						<!-- <div class="col-xs-12 col-sm-4">
							<div class="clearfix">
								<label>Ordenar Por</label>
								<select name="order">
									<option value="1">Popularidad</option>
									<option value="2">Precio (Bajo)</option>
									<option value="3">Precio (Alto)</option>
									option
								</select>
							</div>
						</div>
						<div class="hidden-xs col-sm-4">
							<a href="#" class="btn-calentador-ideal pull-right">Mi Calentador Ideal</a>
						</div>
						-->
					</div><!-- .row #breadcrumbs -->

					<div class="row row-sep hidden-xs">
						<div class="col-xs-12"><hr></div>
					</div>

					<div class="row" id="products">
						@yield('content')
					</div><!-- #products -->
				
				</div><!-- #main-content -->
			</div>
		</div>
	</section>
	
	<footer>
		@include('ventadirecta.parts.footer')
	</footer>
</body>
</html>