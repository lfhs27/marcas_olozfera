<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Grupo Industrial Saltillo</title>
	<script>
	var baseUrl = '<?php echo URL::to('/'); ?>';
	</script>


	<!--STYLESHEET-->
	<!--=================================================-->

	<!--Bootstrap Stylesheet [ REQUIRED ]-->
	<link href="{{ asset('assets/nifty/css/bootstrap.min.css') }}" rel="stylesheet">


	<!--Nifty Stylesheet [ REQUIRED ]-->
	{{-- <link href="{{ asset('assets/nifty/css/nifty.min.css') }}" rel="stylesheet"> --}}

	<style>
		.facturainfocont strong {
			display: block;
		}
	</style>

	<script>
	window.print();
	</script>
</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
	<div id="container" class="effect">
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container" class="container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title" class="hidden-print">
					<h1 class="page-header text-overflow">GIS Admin</h1>

					<!--Searchbox-->
					{{-- <div class="searchbox">
						<div class="input-group custom-search-form">
							<input type="text" class="form-control" placeholder="Search..">
							<span class="input-group-btn">
								<button class="text-muted" type="button"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</div> --}}
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->


				<!--Breadcrumb-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!-- <ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol> -->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End breadcrumb-->


		

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					@yield('content')
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->
		</div>

		

	

	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->

	
	{{-- @include('admin.footer') --}}
		

</body>
</html>

