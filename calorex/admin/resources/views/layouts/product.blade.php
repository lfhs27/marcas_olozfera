<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tienda Calorex</title>
	@include('ventadirecta.parts.head')
</head>
<body class="template-product">
	
	<header>
		@include('ventadirecta.parts.header')
	</header>

	<section id="breadcrumb" class="hidden-xs">
		<div class="container">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<ol class="breadcrumb">
					@if(isset($breadcrumbs['partOne']))
					<li>
						<a href="{{ $breadcrumbs['partOne']['link'] }}">{{$breadcrumbs['partOne']['title']}}</a>
					</li>
					@endif
					@if($breadcrumbs['partTwo'])
					<li>
						<a href="{{ $breadcrumbs['partTwo']['link'] }}">{{$breadcrumbs['partTwo']['title']}}</a>
					</li>
					@endif
					@if($breadcrumbs['partThree'])
					<li>
						<a href="{{ $breadcrumbs['partThree']['link'] }}">{{$breadcrumbs['partThree']['title']}}</a>
					</li>
					@endif
				</ol>	
			</div>
		</div>
	</section>


	@yield('content')
	
	<footer>
		@include('ventadirecta.parts.footer')
	</footer>
</body>
</html>