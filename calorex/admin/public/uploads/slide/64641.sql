-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2015 at 05:59 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gislocal`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `calle` varchar(255) NOT NULL,
  `num_ext` varchar(255) NOT NULL,
  `num_int` varchar(255) NOT NULL,
  `colonia` varchar(255) NOT NULL,
  `zip_code_id` int(11) NOT NULL,
  `receptor` varchar(255) NOT NULL,
  `instrucciones` varchar(255) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `zip_code_id` (`zip_code_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `calle`, `num_ext`, `num_int`, `colonia`, `zip_code_id`, `receptor`, `instrucciones`, `default`, `updated_at`, `created_at`) VALUES
(2, 3, 'Prueba D', '1865', '', 'Colinas del Testeo', 1, '', 'Casa Negra', 0, '2015-11-04 21:50:03', '2015-11-04 13:35:22'),
(3, 3, 'Test2', '120', '33', 'Test5', 1, 'Fernando', 'Hernandez', 1, '2015-11-04 16:59:10', '2015-11-04 13:41:33'),
(4, 3, 'test4', '12', '', 'test5', 9216, 'equis', 'hernande', 0, '2015-11-04 16:59:10', '2015-11-04 13:44:21');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`zip_code_id`) REFERENCES `zip_codes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
