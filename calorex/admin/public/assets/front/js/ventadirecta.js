$(document).ready(function() {
	var allPanels = $('#sidebar nav .submenu').hide();

	$('#sidebar nav li a.toggle-panel').click(function(){
		var itemSiblings = $(this).parent().siblings();
		$('.submenu',itemSiblings).slideUp()
		$('.active',itemSiblings).removeClass('active');

		$(this).addClass('active');
		$(this).next('.submenu').slideDown()

		return false;
	});

	$('.product').on('mouseenter',function(){
		$('.subtitle-overlay',this).fadeIn();
	}).on('mouseleave',function(){
		$('.subtitle-overlay',this).fadeOut();
	})
});

$(document).ready(function() {
	$('#payment-options-collapsible').on('shown.bs.collapse',function(){
		var expandedEl = $('[aria-expanded=true]',this);

		$('.glyphicon',expandedEl).removeClass('glyphicon-menu-right').addClass('glyphicon-menu-down');
	});

	$('#payment-options-collapsible').on('hide.bs.collapse',function(){
		var expandedEl = $('[aria-expanded=true]',this);		
		$('.glyphicon',expandedEl).removeClass('glyphicon-menu-down').addClass('glyphicon-menu-right');
	});


	$(window).on('scroll',function(){
		if ($(window).width() > 768) {
			var st = $(window).scrollTop();
			var opacity = st / 80;
			if (opacity < 0.7) {
				$('header.opacity').css({
					opacity: 0.7
				})
			}
			if (opacity > 0.7) {
				$('header.opacity').css({
					opacity: opacity
				})
			};
		}
	});

	if ($(window).width() < 767 ) {
		$('.mobile-cycle').cycle({
			slides: '>.col-xs-12',
			fx: 'scrollHorz',
			swipe: true
		});
	};
});

// $(window).load(function(){
// 	if ($('body').hasClass('template-login')) {
// 		var winH = $(window).height();
// 		var blockH = $('.login-content').height();
// 		var marginTop = ((winH - blockH) / 2) - 170;
// 		console.log(winH);
// 		console.log(blockH);

// 		$('.login-content').css({
// 			marginTop: marginTop
// 		});
// 	}
// });

$(document).on('click', '.btn-solicitud-pago-nomina', function(event) {
	event.preventDefault();

	var laCantidad = $(this).data('cantidad');
	var elPlazo = $('select[name="selPlazo"]').val();
	var elPlazoTipoVal = $('input[name="plazoTipo"]').val();

	if (elPlazoTipoVal === "WEEKLY") {
		plazoTipoTxt = "semanas"
	} else {
		plazoTipoTxt = "quincenas"
	}

	bootbox.dialog({
		message: "Al dar click estás de acuerdo en que se te haga el descuento vía nómina de "+laCantidad+" en el plazo de  "+elPlazo+" "+ plazoTipoTxt +". Al utilizar el método de pago descuento por nómina, los productos no podrán ser facturados a nombre del empleado, solo a nombre de la empresa.",
		title: "Confirmar",
		buttons: {
			success: {
				label: "ACEPTAR",
				className: 'btn-success',
				callback: function(e){
					$('.btn-success',this).hide();
					$(this).hide();
					if ($('.input_ReqFactura:checked').val() == 1) {
						$('#form-pago-plazos').append('<input type="hidden" name="RequiereFactura" value="1">');

						var formFacturaInputs = $('#form-factura').serializeArray();

						$.each(formFacturaInputs, function(index, val) {
							$('#form-pago-plazos').append('<input type="hidden" name="'+val.name+'" value="'+ val.value +'">');
						});
						$('#form-pago-plazos').submit();
					} else {
						$('#form-pago-plazos').submit();
					}
				}
			},
			danger: {
				label: "Cancelar",
				className: "btn-danger",
				callback: function(){
					this.hide()
				}
			}
		}

	});

	// bootbox.confirm("Al dar click estás de acuerdo en que se te haga el descuento vía nómina de "+laCantidad+" en el plazo de  "+elPlazo+" "+ plazoTipoTxt +" y al utilizar el método de pago DESCUENTO POR NÓMINA, los productos no podrán ser facturados.",function(result){
	// 	if (result) {
	// 		if ($('.input_ReqFactura').val() == 1) {
	// 			$('#form-pago-plazos').append('<input type="hidden" name="RequiereFactura" value="1">');

	// 			var formFacturaInputs = $('#form-factura').serializeArray();

	// 			$.each(formFacturaInputs, function(index, val) {
	// 				$('#form-pago-plazos').append('<input type="hidden" name="'+val.name+'" value="'+ val.value +'">');
	// 			});
	// 			$('#form-pago-plazos').submit();
	// 		} else {
	// 			$('#form-pago-plazos').submit();
	// 		}
	// 	}
	// });

});

$(document).on('click','.btn-add-to-cart',function(event){
	event.preventDefault();
	var modalTipo = $(this).data('modalid');

	if (modalTipo == "combustible") {
		$('#modal-version-combustible').modal('show');
	}

	if (modalTipo == "energiaelectrica") {
		$('#modal-version-energiaelectrica').modal('show');
	}
});

$(document).on('click','.btn-combustible',function(event){
	var tipoCombustible = $(this).data('combustible');

	$('#input_CombustibleId').val(tipoCombustible);

	$('#form-add-product-to-cart')

	submitFormProductToCart('gas');
});

$(document).on('click','.btn-energia',function(event){
	var tipoEnergia = $(this).data('energia');

	$('#input_EnergiaId').val(tipoEnergia);

	submitFormProductToCart('electrico');
});

function submitFormProductToCart (tipoProducto) {
	var modelId = $('#form-add-product-to-cart input[name="model_id"]').val();
	var token = $('input[name="_token"]').val();
	var formData = new FormData();
	var safetyFactor = $('#form-add-product-to-cart input[name="safetyFactor"]').val();

	formData.append('_token',token);
	formData.append('modelId',modelId);
	formData.append('tipoProducto',tipoProducto);

	$.ajax({
		url: '/producto/existenciamodalt/' + modelId,
		type: 'POST',
		contentType: false,
		processData: false,
		data: formData,
	})
	.done(function(response) {
		console.log(response);

		if (response.inv - safetyFactor > 0) {
			$('#form-add-product-to-cart').submit();
		} else {
			bootbox.alert('Lo sentimos, no existen productos disponibles en este modelo. Intente con otro tipo de combustible o energía');
		}
	})
	.fail(function(response) {
		console.log("error");
		console.log(response);
		$('body').html(response.responseText);
	})
	.always(function() {
		console.log("complete");
	});
}

$(document).on('click','.input_ReqFactura',function(event){
	if ($(this).val()==1) {
		$('#form-factura').removeClass('hidden');
	} else {
		$('#form-factura').addClass('hidden');
	}
});

$(document).on('submit','#form-paypal-payment',function(event){
	var self = this;

	$('input[name="submit"]',self).hide();

	// Antes de enviar el pago, registro la orden
	event.preventDefault();

	var token = $('input[name="_token"]').val();
	var formData = new FormData();

	formData.append('_token',token);
	formData.append('shipping_type',$('input[name="shipping_type"]').val())
	formData.append('shipping_address_id',$('input[name="shipping_address_id"]').val())
	formData.append('shipping_selected_pickup',$('input[name="shipping_selected_pickup"]').val())

	if ($('.input_ReqFactura').val() == 1) {
		formData.append('RequiereFactura',1);

		var formFacturaInputs = $('#form-factura').serializeArray();

		$.each(formFacturaInputs, function(index, val) {
			formData.append(val.name,val.value);
		});
	}

	$.ajax({
		url: '/cart/paypalorder',
		type: 'POST',
		contentType: false,
		processData: false,
		data: formData,
	})
	.done(function(orderData) {
		console.log("success");
		console.log(orderData);
		var returnUrl = $('input[name=return]').val() + '/' + orderData.id;

		$('input[name="return"]').attr('value',returnUrl);

		self.submit();
	})
	.fail(function(response) {
		console.log("error");
		console.log(response);
		bootbox.alert("Hubo un error al guardar la órden. Recargue esta página haciendo click en MI CARRITO y vuelva a intentarlo.");
	})
	.always(function() {
		console.log("complete");
	});

	return false;
})