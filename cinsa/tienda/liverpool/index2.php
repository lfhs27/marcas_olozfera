﻿<!DOCTYPE html>
<html>
	<head>
    	<title>Liverpool</title>
        <meta charset="utf-8">
        <meta name="author" content="SIOrbita">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
        <link rel="stylesheet" href="css/style.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:300&subset=latin,greek' rel='stylesheet' type='text/css'>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <script src="js/modernizr.custom.js"></script>
	    <script src="js/respond.min.js"></script>
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="js/sio.js"></script>
		<script>
			var token='clailvoerrepx';
			var id=0;
			var resultado;
			var calentadorID=3;
			var calentadoreslength=0;
			var flag=true;
			var stop=function(){
				flag=!flag;
			}
			var decrece=function(){
				w=window.innerWidth;
				h=screen.availHeight;
				var wind=window.open("index.php","","top=0,left=0,width="+w+",height="+h);
				setInterval(function(){
						if(w>0){
							w-=10;
						}else{
							w=window.innerWidth;
							w-=10;
						}
						if(flag){
							wind.resizeTo(w,h);
						}
				},500);
			}
			window.addEventListener("click", function(ev){
				console.log(ev);
			});
			window.addEventListener("resize", function(ev){
				var w = window.innerWidth;
				var h = window.innerHeight;
				render();
				console.log('w:'+w+' h:'+h);
			});
			var resp_obtenerInfo=function(resp){
				resultado=JSON.parse(resp);
				codigo=document.getElementById('lsPreguntas').innerHTML;
				texto=generarHTML(resultado,codigo);
				document.getElementById('preguntas').innerHTML=texto;
			}
			var obtenerInfo=function(){
				texto="<ul><h6>Cargando mi calentador ideal... </h6></ul>";
				document.getElementById('preguntas').innerHTML=texto;
				paramSend('&accion=obtenerPreguntas&id='+id,resp_obtenerInfo);
			}
			var generar=function(variable,valor){
				localStorage.setItem(variable,valor);
				id++;
				render();
			}
			var return1=function(){
				id--;
				render();
			}
			var render=function(){
				switch(id){
					case 0:
						id=1;
					case 1:
						if(window.innerWidth>600){
							document.getElementById('imgLateralIzq').style.display='block';
							document.getElementById('cuerpoDer').style.marginLeft="510px";
						}
						document.getElementById('titDerecho').style.display="block";
						document.getElementById('divBackCalentadores').style.display="none";
						document.getElementById('anteriorCal').style.display="none";
						document.getElementById('siguienteCal').style.display="none";
						document.getElementById('regresar').style.display="none";
						document.getElementById('btns').style.display="none";
						obtenerInfo();
					break;	
					case 2:
						document.getElementById('gat').style.display="none";
						if(localStorage.getItem('tipo')=='solar'){
							id=6;
							document.getElementById('siguiente').style.display="block";
							document.getElementById('titDerecho').style.display="none";
							document.getElementById('imgLateralDer').src="img/divCuestDerecha.png";
							obtenerResultados();
						}else{
							obtenerInfo();
							document.getElementById('regresar').style.display="block";
							document.getElementById('siguiente').style.display="none";
							document.getElementById('titDerecho').style.display="block";
							document.getElementById('imgLateralDer').src="img/divDerecha.png";
						}
					break;
					case 3:
					case 4:
						obtenerInfo();
						document.getElementById('regresar').style.display="block";
						document.getElementById('siguiente').style.display="none";
						document.getElementById('titDerecho').style.display="block";
						document.getElementById('imgLateralDer').src="img/divDerecha.png";
					break;	
					case 5:
						if(window.innerWidth>600){
							document.getElementById('imgLateralIzq').style.display='block';
							document.getElementById('cuerpoDer').style.marginLeft="510px";
						}
						texto=document.getElementById('cuestionario').innerHTML;
						document.getElementById('preguntas').innerHTML=texto;
						document.getElementById('siguiente').style.display="block";
						document.getElementById('titDerecho').style.display="none"
						document.getElementById('btns').style.display="none";
						document.getElementById('imgLateralDer').src="img/divCuestDerecha.png";
						calentadorID=3;
					break;
					case 6:
						calentadoreslength=resultado["rows"].length;
						codigo=document.getElementById('calentadores1').innerHTML;
						texto=generarHTML(resultado,codigo,calentadorID-3,calentadorID-2);
						if(window.innerWidth<345){
							document.getElementById('imgBackCalentadores').style.height="300px";
						}
						if(calentadoreslength>=2){
							codigo=document.getElementById('calentadores2').innerHTML;
							texto+=generarHTML(resultado,codigo,calentadorID-2,calentadorID-1);
							if(window.innerWidth<345){
								document.getElementById('imgBackCalentadores').style.height="600px";
							}
						}
						if(calentadoreslength>=3){
							codigo=document.getElementById('calentadores3').innerHTML;
							texto+=generarHTML(resultado,codigo,calentadorID-1,calentadorID);
							if(window.innerWidth<345){
								document.getElementById('imgBackCalentadores').style.height="900px";
							}
						}
						if(calentadoreslength>=4){
							codigo=document.getElementById('calentadores4').innerHTML;
							texto+=generarHTML(resultado,codigo,calentadorID,calentadorID+1);
							if(window.innerWidth<345){
								document.getElementById('imgBackCalentadores').style.height="1014px";
							}
						}
						document.getElementById('preguntas').innerHTML=texto;
						//Borra 4
						if(calentadoreslength>3){
							var cid=resultado["rows"][calentadorID]['calentador_id'];
							if(resultado["rows"][calentadorID]['liverpool_LP']=='0'){
								document.getElementById('detGasLp4_'+cid).style.opacity="0";
								document.getElementById('detGasLp4_'+cid).style.visibility="hidden";
							}
							if(resultado["rows"][calentadorID]['liverpool_Nat']=='0'){
								document.getElementById('detGasNt4_'+cid).style.opacity="0";
								document.getElementById('detGasNt4_'+cid).style.visibility="hidden";
							}
						}
						//Borra 3
						if(calentadoreslength>2){
							cid=resultado["rows"][calentadorID-1]['calentador_id'];
							if(resultado["rows"][calentadorID-1]['liverpool_LP']=='0'){
								document.getElementById('detGasLp3_'+cid).style.opacity="0";
								document.getElementById('detGasLp3_'+cid).style.visibility="hidden";
							}
							if(resultado["rows"][calentadorID-1]['liverpool_Nat']=='0'){
								document.getElementById('detGasNt3_'+cid).style.opacity="0";
								document.getElementById('detGasNt3_'+cid).style.visibility="hidden";
							}
						}
						//Borra 2
						if(calentadoreslength>1){
							cid=resultado["rows"][calentadorID-2]['calentador_id'];
							if(resultado["rows"][calentadorID-2]['liverpool_LP']=='0'){
								document.getElementById('detGasLp2_'+cid).style.opacity="0";
								document.getElementById('detGasLp2_'+cid).style.visibility="hidden";
							}
							if(resultado["rows"][calentadorID-2]['liverpool_Nat']=='0'){
								document.getElementById('detGasNt2_'+cid).style.opacity="0";
								document.getElementById('detGasNt2_'+cid).style.visibility="hidden";
							}
						}
						//Borra 1
						cid=resultado["rows"][calentadorID-3]['calentador_id'];
						if(resultado["rows"][calentadorID-3]['liverpool_LP']=='0'){
							document.getElementById('detGasLp1_'+cid).style.opacity="0";
							document.getElementById('detGasLp1_'+cid).style.visibility="hidden";
						}
						if(resultado["rows"][calentadorID-3]['liverpool_Nat']=='0'){
							document.getElementById('detGasNt1_'+cid).style.opacity="0";
							document.getElementById('detGasNt1_'+cid).style.visibility="hidden";
						}
						if(window.innerWidth>600){
							document.getElementById('imgLateralIzq').style.display='none';
							document.getElementById('cuerpoDer').style.marginLeft="232px";
						}
						document.getElementById('siguiente').style.display="none";
						document.getElementById('regresar').style.display="none";
						document.getElementById('imgDer').style.display="none";
						document.getElementById('imgIzq').style.display="none";
						document.getElementById('btns').style.display="block";
						document.getElementById('divBackCalentadores').style.display="block";
						if (calentadoreslength>3 && calentadorID<calentadoreslength){
							document.getElementById('siguienteCal').style.display="block";
						}
					break;
					case 7:
						texto=document.getElementById('formulario').innerHTML;
						document.getElementById('preguntas').innerHTML=texto;
						document.getElementById('siguiente').style.display="none";
						document.getElementById('regresar').style.display="none";
					break;
				}
			}
			var resp_obtenerResultados=function(resp){
				resultado=JSON.parse(resp);
				if(resultado["status"]==false){
					id=7;
				}else{
					id=6;
				}
				render();
			}
			var obtenerResultados=function(){
				var param='&accion=obtenerResultados';
				param+="&tipo="+localStorage.getItem('tipo');
				param+="&presion="+localStorage.getItem('presion');
				param+="&regadera="+localStorage.getItem('regadera');
				param+="&distancia="+localStorage.getItem('distancia');
				param+="&regaderas="+document.getElementById('banos').value;
				param+="&lavabos="+document.getElementById('mbanos').value;
				param+="&tina="+document.getElementById('tinas').value;
				param+="&lavadoras="+document.getElementById('lavadoras').value;
				paramSend(param,resp_obtenerResultados);
				texto="<ul><h6>Cargando mi calentador ideal...</h6></ul>";
				document.getElementById('preguntas').innerHTML=texto;
			}
			var siguienteCalentador=function(){
				if(calentadorID>=calentadoreslength){
					return false;
				}
				document.getElementById('anteriorCal').style.display="block";
				calentadorID++;
				revision=calentadorID+1;
				console.log(revision+'-'+calentadoreslength);
				render();
				if(revision==calentadoreslength){
					document.getElementById('siguienteCal').style.display="none";
				}
			}
			var anteriorCalentador=function(){
				if(calentadorID<=3){
					return false;
				}
				document.getElementById('siguienteCal').style.display="block";
				calentadorID--;
				if(calentadorID<=3){
					document.getElementById('anteriorCal').style.display="none";	
				}
				render();
			}
			var regresarInicio=function(){
				window.location="index.php";
			}
			var resp_enviarCorreo=function(resp){
				window.location="index.php";
			}
			var enviarCorreo=function(){
				nombre=document.getElementById('c_nombre').value;
				correo=document.getElementById('c_correo').value;
				telefono=document.getElementById('c_telefono').value;
				celular=document.getElementById('c_celular').value;
				estado=document.getElementById('c_estado').value;
				ciudad=document.getElementById('c_ciudad').value;
				mensaje=document.getElementById('c_mensaje').value;
				if (nombre==''){
					alert('Campo nombre obligatorio');
					return false;
				}
				if (correo==''){
					alert('Campo Correo obligatorio');
					return false;
				}
				if(telefono=='' || celular=='' || estado=='' || ciudad==''|| mensaje==''){
					alert('Llena los campos obligatorios');
					return false;
				}
				param="&nombre="+nombre;
				param+="&correo="+correo;
				param+="&telefono="+telefono;
				param+="&celular="+celular;
				param+="&estado="+estado;
				param+="&ciudad="+ciudad;
				param+="&mensaje="+mensaje;
				paramSend('&accion=mandarCorreo'+param,resp_enviarCorreo);
			}
			
			var anterior;
			function ocultar(object){
				object.style.visibility='hidden';
				document.getElementById('contenedor').innerHTML=anterior;
				render();
			}
			function visible(id){
				valor=id.getAttribute("data-tipo");
				control=document.getElementById('esp_'+valor);
				anterior=document.getElementById('contenedor').innerHTML;
				document.getElementById('contenedor').innerHTML=control.innerHTML;
				control.style.visibility='visible';	
			}
		</script>
    </head>
    <body ondragstart="return false;" ondrop="return false;" onLoad="generar()" class="ns">
    	<div class="contenedor" id="contenedor">
            <div class="encabezado">
            	<div class="logoCalore">
            		<a href="http://www.calorex.com.mx/tienda/liverpool"><img src="img/logocalorex.png" id="logoCalorex"></a>
                </div>
                <img src="img/header.png">
                <ul id="gat" class="rhidden hidden">
                	<li class="rhidden hidden">Sección 1</li>
                    <li class="rhidden hidden">Sección 2</li>
                    <li class="rhidden hidden">Sección 3</li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div id="divBackCalentadores" class="hidden">
            	<img src="img/imgBackCalentadores.png" class="imgBackCalentadores" id="imgBackCalentadores">
                <div id="btns" class="hidden contenedor">
					<img src="img/verCatCalorex.png" onClick="window.open('../calorex.pdf')">
					<img src="img/finalizar.png" id="imgFinalizar" class="clickable" onClick="regresarInicio();">
				</div>
    		</div>
            <div class="cuerpoIzq">
                <div id="imgIzq">
                	<img src="img/imgLateral.png" id="imgLateralIzq">
                </div>
            </div>
			
            <div class="cuerpoDer" id="cuerpoDer">
            	<div>
                	<p id="titDerecho">HAY UN CALOREX ESPECIALMENTE HECHO PARA TI</p>
                </div>
            	<div id="imgDer">
                	<img src="img/divDerecha.png" id="imgLateralDer">  
                </div>
				<div class="listDer" id="preguntas">
					<ul><h6>Cargando mi calentador ideal...</h6></ul>
				</div>
                <div id="divBtnRegresar" style="z-index:100;">
                	<div id="divBtnRegresar1">
                    	<img src="img/btnRegresar.png" onClick="return1()" class="regresar clickable hidden" id="regresar" id="btnRegresar">
                    </div>
                    <div id="divBtnContinuar">
                    	<img src="img/btnSiguiente.png" onClick="obtenerResultados()" class="siguiente clickable hidden" id="siguiente" id="btnSiguiente">
                    </div>
				</div>
                <div class="btnsAvanzar"> 
					<div id="anteriorCal" class="regresar hidden clickable">
                    	<img src="img/left.png" onClick="anteriorCalentador()">
                    </div>
					<div id="siguienteCal" class="siguiente hidden clickable">
                    	<img src="img/right.png" onClick="siguienteCalentador()">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="pie">
            	<img src="img/footer.png">
                <p id="titFoot">Copyright 2015 calorex | GIS | <span class="clickable" onClick="window.open('http://www.calorex.com.mx/preguntas-frecuentes.html')">Preguntas Frecuentes</span> |</p>
            </div>       
        </div>
    </body> 
    <hidden class="espec" id="esp_deposito">
			<div class="ocult clickeable" id="ocult" style="text-align:center;" onClick="ocultar(this);">
           		<img src="img/btnRegresar.png" class="clickable">
            </div>
            <div class="espec1">
				<h2>Especificaciones</h2>
				<p>- Funciona con cualquier tipo de llave de agua (incluyendo monomando)</p>
				<p>- Funciona hasta con 10 regaderas simultáneas***</p>
				<p>- Ideal para climas fríos</p>
				<p>- No requiere presión de agua</p>
				<p>- Funcionamiento óptimo sin importar la salinidad del agua</p>
				<p>- Instalación sin complicaciones<br><br></p>
				<h3>Hay un Calorex especialmente hecho para ti. Elígelo.</h3>
                <div class="imgEspec">
                	<img src="../../images/misc/espTec02.png" alt="Especificaciones" title="Especificaciones" class="imgEspec1">
                </div>
		
                <!--<p class="nota">*En comparación con un  calentador de depósito con 10 años de uso, acumulación de sales, eficiencia inferior al 75% y hábitos de uso similares.</p>
                <p class="nota">**Válida en la República Mexicana, 1 año para otros países.</p>
                <p class="nota">***Sólo en modelo 100-83CX</p>-->
			</div>
    </hidden>
	<hidden class="espec" id="esp_paso">
			<div class="ocult clickeable" id="ocult" style="text-align:center;" onClick="ocultar(this);">
           		<img src="img/btnRegresar.png" class="clickable">
            </div>
            <div class="espec1">
				<h2>Especificaciones</h2>
				<p>- Funciona con cualquier tipo de llave de agua (incluyendo monomando)</p>
				<p>- Funciona hasta con 4 regaderas simultáneas***</p>
				<p>- No requiere presión de agua</p>
				<p>- Funcionamiento óptimo sin importar la salinidad del agua</p>
				<p>- Instalación sin complicaciones<br><br></p>
				<h2>Hay un Calorex especialmente hecho para ti. Elígelo.</h2>
				<div class="imgEspec">
	                <img src="../../images/misc/espTec01.png" alt="Especificaciones" title="Especificaciones" class="imgEspec1">
                </div>
                <!--<p class="nota">*45% más ahorro de gas en comparación con un  calentador de depósito con 10 años de uso, acumulación de sales, eficiencia inferior al 75% y hábitos de uso similares.</p>
                <p class="nota">**Válida en la República Mexicana, 1 año para otros países.</p>
                <p class="nota">***Sólo en modelo COXDP-20</p>-->
           </div>
	</hidden>
	<hidden class="espec" id="esp_electrico">
            <div class="ocult clickeable" id="ocult" style="text-align:center;"  onClick="ocultar(this);">
                <img src="img/btnRegresar.png" class="clickable">
            </div>
            <div class="espec1">
                <h2>Especificaciones</h2>
                <p>- Funciona con cualquier tipo de llave de agua (incluyendo monomando)</p>
                <p>- Funciona hasta con 7 regaderas simultáneas**</p>
                <p>- Ideal para climas fríos</p>
                <p>- No requiere presión de agua</p>
                <p>- Funcionamiento óptimo sin importar la salinidad del agua</p>
                <p>- Instalación sin complicaciones<br><br></p>
                <h3>Hay un Calorex especialmente hecho para ti. Elígelo.</h3>
                <div class="imgEspec">
	                <img src="../../images/misc/espTec03.png"  alt="Especificaciones" title="Especificaciones" class="imgEspec1">
                </div>
                <p class="nota">*Válida en la República Mexicana, 1 año para otros países.</p>
                <p class="nota">**Sólo en modelo E-75</p>
            </div>
	</hidden>
	<hidden class="espec" id="esp_instantaneo">
		<div class="ocult clickeable" id="ocult" style="text-align:center;"  onClick="ocultar(this);">
                <img src="img/btnRegresar.png" class="clickable">
        </div>
        <div class="espec1">
            <h2>Especificaciones</h2>
            <p>- Funciona hasta con 2 regaderas simultáneas***</p>
            <p>- Requiere mínima presión de agua</p>
            <p>- Fácil instalación </p>
            <div class="imgEspec">
            	<img src="../../images/misc/espTec04.png" alt="Especificaciones" title="Especificaciones" class="imgEspec1">
            </div>
            <p class="nota">*Dependiendo de los hábitos de uso y calentador actual.</p>
            <p class="nota">**Requiere al menos 1m de altura entre el tinaco y la regadera (120gr/cm<sup>2</sup> de presión).</p>
            <p class="nota">***Sólo en modelo COXDPI-13</p>
        </div>
	</hidden>
	<hidden class="espec" id="esp_solar">
		<div class="ocult clickeable" id="ocult" style="text-align:center;"  onClick="ocultar(this);">
                <img src="img/btnRegresar.png" class="clickable">
        </div>
        <div class="espec1">
            <h2>Especificaciones</h2>
            <p>- Ahorra dinero usando un sistema solar***</p>
            <p>- Cuida el ambiente</p>
            <p>- Siente que es un calentador como al que estas acostumbrado </p>
            <!--<img src="../../images/misc/espTec04.png" width="580" alt="Especificaciones" title="Especificaciones">
            <p class="nota">*Dependiendo de los hábitos de uso y calentador actual.</p>
            <p class="nota">**Requiere al menos 1m de altura entre el tinaco y la regadera (120gr/cm<sup>2</sup> de presión).</p>
            <p class="nota">***Sólo en modelo COXDPI-13</p>-->
        </div>
	</hidden>
		
	<hidden id="lsPreguntas">
		<ul>
			<h6>-pregunta_texto-</h6>
			<li onClick="generar('-pregunta_var-','-pregunta_val1-')" title="-pregunta_alt1-" class="clickable"><sioimg src="-pregunta_img1-" class="iconos"><br>-pregunta_texto1-</li>
			<li onClick="generar('-pregunta_var-','-pregunta_val2-')" id="imgMedia" title="-pregunta_alt2-" class="clickable"><sioimg src="-pregunta_img2-" class="iconos"><br>-pregunta_texto2-</li>
			<li onClick="generar('-pregunta_var-','-pregunta_val3-')" title="-pregunta_alt3-" class="clickable"><sioimg src="-pregunta_img3-" class="iconos"><br>-pregunta_texto3-</li>
		</ul>
        
	</hidden>
	<hidden id="cuestionario" class="listDer">
		<form>
			<label for="banos">¿Cuántos baños completos tienes?</label>
			<input type="number" id="banos" class="centrado" value="0" min="0">
				<img src="img/regadera.png"class="iconImg">
				<img src="img/lavabo.png" class="iconImg1">
			<label for="mbanos">¿Cuántos medios baños tienes?</label>
			<input type="number" id="mbanos" class="centrado" value="0" min="0">
				<img src="img/lavabo.png" class="iconImg">
			<label for="tinas">¿Cuántas tinas tienes?</label>
			<input type="number" id="tinas" class="centrado" value="0" min="0">
			<img src="img/tina.png" class="iconImg"><br>
			<label for="lavadoras">¿Cuántas lavadoras tienes?</label>
			<input type="number" id="lavadoras" class="centrado" value="0" min="0">
			<img src="img/lavadora.png" class="iconImg">
		</form>
	</hidden>
    <div class="calentadoresGral">
        <hidden id="calentadores1">
        
        <div class="cal1">
            <!--BRO, AQUI VA TODO EL CODIGO PARA LA PARTE DEL CALENTADOR 1>-->
            <div id="desCalentador1">
                -calentador_descripcion-
            </div>
            <div id="imgCalentador1">
                <sioimg src="../-calentador_imagen-">
            </div>
            <h6 class="clickable" data-tipo="-calentador_tipo-" onClick="visible(this)">Especificaciones</h6>
            <div id="detGasLp1_-calentador_id-" class="clickable detGasLp1">
                <img src="img/verDetalleLp.png" onClick="window.open('-liverpool_LP-')">
            </div>
            <div id="detGasNt1_-calentador_id-" class="clickable detGasNt1">
                <img src="img/verDetalleGn.png" onClick="window.open('-liverpool_Nat-')">
            </div>
        </div>
         
        </hidden>
        <hidden id="calentadores2">
        <div class="cal2">
            <div id="desCalentador2">
                -calentador_descripcion-
            </div>
            <div id="imgCalentador2">
                <sioimg src="../-calentador_imagen-">
            </div>
            <h6 class="clickable" data-tipo="-calentador_tipo-" onClick="visible(this)">Especificaciones</h6>
            <div id="detGasLp2_-calentador_id-" class="clickable detGasLp2">
                <img src="img/verDetalleLp.png" onClick="window.open('-liverpool_LP-')">
            </div>
            <div id="detGasNt2_-calentador_id-" class="clickable detGasNt2">
                <img src="img/verDetalleGn.png" onClick="window.open('-liverpool_Nat-')">
            </div>
        </div>
        </hidden>
        <hidden id="calentadores3">
        <div class="cal3">
            <!--BRO, AQUI VA TODO EL CODIGO PARA LA PARTE DEL CALENTADOR 1>-->
            <div id="desCalentador3">
                -calentador_descripcion-
            </div>
            <div id="imgCalentador3">
                <sioimg src="../-calentador_imagen-">
            </div>
            <h6 class="clickable" data-tipo="-calentador_tipo-" onClick="visible(this)">Especificaciones</h6>
            <div id="detGasLp3_-calentador_id-" class="clickable detGasLp3">
                <img src="img/verDetalleLp.png" onClick="window.open('-liverpool_LP-')">
            </div>
            <div id="detGasNt3_-calentador_id-" class="clickable detGasNt3">
                <img src="img/verDetalleGn.png" onClick="window.open('-liverpool_Nat-')">
            </div>
        </div>
        </hidden>
        <hidden id="calentadores4">
			<div class="cal4">
				<div id="desCalentador4">
					-calentador_descripcion-
				</div>
				<div id="imgCalentador4">
					<sioimg src="../-calentador_imagen-">     
				</div>
				<h6 class="clickable" data-tipo="-calentador_tipo-" onClick="visible(this)">Especificaciones</h6>
				<div id="detGasLp4_-calentador_id-" class="clickable detGasLp4">
					<img src="img/verDetalleLp.png" onClick="window.open('-liverpool_LP-')">
				</div>
				<div id="detGasNt4_-calentador_id-" class="clickable detGasNt4">
					<img src="img/verDetalleGn.png" id="imgNT4" onClick="window.open('-liverpool_Nat-')">
				</div>
			</div>
           
			
            <!--iGUAL QUE EL DE ARRIBA, PERO CON ESTILOS DE CALENTADOR 2-->
        </hidden>
      
	</div>
     <!--<div id="btns" class="hidden">
					<img src="img/verCatCalorex.png" onClick="window.open('../calorex.pdf')" id="imgVerCat">
					<img src="img/finalizar.png" id="imgFinalizar" class="clickable" onClick="regresarInicio();">
		</div>-->
	<div>
        <hidden id="formulario">
            <div id="formContacto">
				<form>
					<h6>Para ofrecerte una solución de acuerdo a tus necesidades, déjanos tus datos</h6>
					<label ><span class="red">*</span> Nombre completo</label>
					<input type="text" id="c_nombre" name="nombre" required>
					<label ><span class="red">*</span>  Teléfono</label>
					<input type="text" id="c_telefono" name="telefono" required>
					<label ><span class="red">*</span>  Estado</label>
					<input type="text" id="c_estado" name="estado" required>
					<div class="form3">
						<label class="form2"><span class="red">*</span>  Correo electrónico</label>
						<input type="email" id="c_correo" name="correo" class="form2" required>
						<label class="form2"><span class="red">*</span>  Celular</label>
						<input type="text" id="c_celular" name="celular" class="form2" required>
						<label class="form2"><span class="red">*</span>  Ciudad</label>
						<input type="text" id="c_ciudad" name="ciudad" class="form2" required>
					</div>
					<label >&nbsp;&nbsp;Mensaje</label>
					<textarea id="c_mensaje" name="mensaje"></textarea>
					<div id="btnEnviar">
						<p><span class="red">(*) Campos Obligatorios</span></p>
						<img src="img/enviar.png" onClick="enviarCorreo();" class="clickable" style="z-index:50">
					</div>
				</form>
            </div>
        </hidden>
	</div>
    
</html>