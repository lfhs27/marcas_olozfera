// var LHCChatOptions = {};
// LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500};
// (function() {
// var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
// var refferer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
// var location = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
// po.src = '//itconcepts.me/clientes/atencionenlinea/index.php/esp/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true?r='+refferer+'&l='+location;
// var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
// })();

$(window).load(function() {
	$('#products-carousel-container').owlCarousel({
		loop: true,
		margin: 15,
		items: 4,
		pagination : true
	});
});

$(document).ready(function(){
	//var allPanels = $('#sidebar nav .submenu').hide();

	$('#sidebar nav li a.toggle-panel').click(function(){
		var itemSiblings = $(this).parent().siblings();
		$('.submenu',itemSiblings).slideUp()
		$('.active',itemSiblings).removeClass('active');

		$(this).addClass('active');
		$(this).next('.submenu').slideDown()

		return false;
	});

	$('.product').on('mouseenter',function(){
		$('.subtitle-overlay',this).fadeIn();
	}).on('mouseleave',function(){
		$('.subtitle-overlay',this).fadeOut();
	});

	$('#products-carousel .product').on('mouseenter',function(){
		$('.product-overlay',this).fadeIn();
	}).on('mouseleave',function(){
		$('.product-overlay',this).fadeOut();
	})

	$('#products-carousel .product .product-overlay').on('click',function(){
		var nextUrl = $('a',this).attr('href');

		top.location = nextUrl;

		return false;
	})

	if ($(window).width() < 768) {
		$('#related-products .products-cycle').cycle({
			slides: '>.col-xs-12'
		});
	};

});