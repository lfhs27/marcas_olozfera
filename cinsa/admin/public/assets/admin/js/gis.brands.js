$(document).on('click', '.edit-sector-details', function(event) {
	event.preventDefault();

	var sectorId = $(this).data('sector-id');
	var token = $('input[name="_token"]').val();

	var formData = new FormData();

	formData.append('objectname','sector');
	formData.append('sector_id',sectorId);
	formData.append('_token',token);

	$.ajax({
		url: '/admin/sectors/' + sectorId + '/json',
		type: 'POST',
		contentType: false,
		processData: false,
		data: formData,
		headers: {
			'X-CVL-Auth': token
		}
	})
	.done(function(response) {
		console.log("success");
		showEditSectorForm(response);
	})
	.fail(function(response) {
		console.log("error");
		$('body').html(response.responseText);
	})
	.always(function() {
		console.log("complete");
	});
});

$(document).on('click', '.edit-family-details', function(event) {
	event.preventDefault();
	
	var familyId = $(this).data('family-id');
	var token = $('input[name="_token"]').val();

	var formData = new FormData();
	formData.append('objectname','family');
	formData.append('family_id',familyId);
	formData.append('_token',token);

	$.ajax({
		url: '/admin/families/' + familyId + '/json',
		type: 'POST',
		contentType: false,
		processData: false,
		data: formData,
		headers: {
			'X-CVL-Auth': token
		}
	})
	.done(function(response) {
		console.log("success");
		console.log(response);

		showEditFamilyForm(response);
	})
	.fail(function(response) {
		console.log("error");
		$('body').html(response.responseText);
	})
	.always(function() {
		console.log("complete");
	});
	
});

function showEditFamilyForm (familyData) {
	var $formEditFamilyDetails = $('#form-edit-family-details');
	$('input#inputFamilyName').val(familyData.name);
	$('input#inputFamilyDescription').val(familyData.description);
	$('input#inputFamilyHoverText').val(familyData.hovertext);
	

	if (familyData.image) {
		$('#listing-img-placeholder').html('<img src="/'+familyData.image.path + familyData.image.filename+'">');
	};
	
	$formEditFamilyDetails.attr('action','/admin/families/' + familyData.id);
	$formEditFamilyDetails.removeClass('hidden');
}

function showEditSectorForm (sectorData) {
	var $formEditSectorDetails = $('#form-edit-sector-details');
	$('input#inputSectorName').val(sectorData.name);
	$('input#inputSectorDescription').val(sectorData.description);
	$('input#inputSectorHoverText').val(sectorData.hovertext);
	if (sectorData.image) {
		$('#sector-img-placeholder').html('<img src="/'+sectorData.image.path + sectorData.image.filename +'">');
	};

	$formEditSectorDetails.attr('action','/admin/sectors/' + sectorData.id)
	$formEditSectorDetails.removeClass('hidden');
}