$(document).ready(function() {
	$('.bs-selectbox').selectpicker();

	$('.btn-save').on('click', function(event) {
		event.preventDefault();
		
		var formId = $(this).attr('href');

		$(formId).submit();
	});

	$('.fieldColorpicker').colorpicker();

	$('.help-tooltip-parent i').popover({
		trigger: 'hover',
		container: 'body'
	});

	$('.global-specs-editor').summernote({
		height: 99,
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
		]
	});
});

$(document).on('change','#product-filter select',function(event){
	event.preventDefault();

	var which = $('#product-filter').data('which');
	console.log(which);

	if ($(this).prop('name') == 'products') {
		return false;
	} else {
		updateSelects(which);
	}
});

$(document).on('click','.btn-add-models',function(event){
	event.preventDefault();

	var selProds = $('select[name="products"]').val();

	$.each(selProds, function(index, val) {
		addProductModel(val);
	});
});

function addProductModel (INVENTORY_ITEM_ID) {
	$.ajax({
		url: '/admin/webservice/productInfo',
		type: 'get',
		dataType: false,
		data: {productId: INVENTORY_ITEM_ID}
	})
	.done(function(response) {
		console.log("success");
		console.log(response);
		addModelFormInputs(response);
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		// console.log("complete");
	});
}

$(document).on('click','.btn-remove-prod',function(event){
	event.preventDefault();

	var INVENTORY_ITEM_ID = $(this).data('item-id');

	$('input[name="productModels[]"][value="'+ INVENTORY_ITEM_ID +'"]').remove();
	$(this).parent('li.list-group-item').remove();
});

function addModelFormInputs (modelInfo) {
	$('#product-filter').append('<input type="hidden" name="productModels[]" value="'+modelInfo[0]['INVENTORY_ITEM_ID']+'">')
	$('#model-list-group').append('<li class="list-group-item">'+modelInfo[0]['DESCRIPTION_ITEM']+' <a class="btn-remove-prod" href="#" data-item-id="'+modelInfo[0]['INVENTORY_ITEM_ID']+'"><i class="fa fa-close pull-right"></i></a></li>');
}

function updateSelects (which) {
	var formData = $('#product-filter').serialize();

	// console.log(formData);

	requestData(formData,which);
}

function requestData (postData,which) {
	$.ajax({
		url: '/admin/webservice/objects',
		type: 'get',
		dataType: false,
		processData: false,
		data: postData
	})
	.done(function(response) {
		// console.log("success");
		console.log(response);
		pushOptions(response,which);
	})
	.fail(function(response) {
		console.log("error");
		console.log(response)
		$('#errordiv').html(response.responseText)
	})
	.always(function() {
		// console.log("complete");
	});
	
}

function pushOptions ( objects , which ) {
	// console.log(objects);
	if (objects.sectors.length > 0 && objects.families.length == 0) {
		fillSelect('sectors',objects.sectors,which);
		clearSelect('families');
	}

	if (objects.families.length > 0 && objects.products.length == 0) {
		fillSelect('families',objects.families,which);
		clearSelect('products');
	}

	if (objects.products.length > 0) {
		fillSelect('products',objects.products,which);
	}
}

function fillSelect (selectName,objects,which) {
	var newOps = "<option>SELECCIONA UNA OPCIÓN</option>";
	$.each(objects, function(index, val) {
		if (selectName == "sectors") {
			newOps += '<option value="'+val.LINEA_MERCADO+'">'+val.LINEA_MERCADO+'</option>'	
		}

		if (selectName == "families") {
			newOps += '<option value="'+val.LINEA+'">'+val.LINEA+'</option>'	
		}

		if (selectName == "products") {
			// if (which == "normal") {
			// 	if (val.COMBUSTIBLE != "NATURAL") {
			// 		newOps += '<option value="'+val.INVENTORY_ITEM_ID+'">'+val.DESCRIPTION_ITEM+'</option>';
			// 	}
			// } else {
			// 	newOps += '<option value="'+val.INVENTORY_ITEM_ID+'">'+val.DESCRIPTION_ITEM+'</option>';
			// }
			newOps += '<option value="'+val.INVENTORY_ITEM_ID+'">'+val.DESCRIPTION_ITEM+'</option>';
		};
	});

	// console.log(newOps);

	$('select[name="'+selectName+'"]').html(newOps);
}

function clearSelect (selectName) {
	$('select[name="'+selectName+'"]').empty();
}

$(document).on('click','.add-specs-item',function(event){
	event.preventDefault();

	var specListNum = $('#specs-list-group .row').size();

	var $inputSpecContent = $('<div class="row mar-btm"/>');
	var $specNombre = $('<div class="col-sm-5"><input placeholder="Nombre de Especificación" class="form-control" type="text" name="globalSpecs['+specListNum+'][nombre]" value=""></div>');
	var $specValor = $('<div class="col-sm-6"><input placeholder="Valor" class="form-control" type="text" name="globalSpecs['+specListNum+'][valor]" value=""></div>');
	var $specDelete = $('<div class="col-sm-1"><a class="del-spec-item" href="#"><i class="fa fa-close"></i></a></div>')

	$inputSpecContent.append($specNombre);
	$inputSpecContent.append($specValor);
	$inputSpecContent.append($specDelete);

	$('#specs-list-group').append($inputSpecContent);
});

$(document).on('click', '.del-spec-item', function(event) {
	event.preventDefault();
	
	$(this).parent().parent().remove();
});

$(document).on('click','.add-specs-graphic-item',function(event){
	var itemId = $(this).data('item-id');
	var specListContentId = '#specs-graphics-' + itemId;
	var specListNum = $('.row',$(specListContentId)).size();

	var $inputSpecContent = $('<div class="row mar-btm"/>');
	var $specIco = $('<div class="col-sm-4"><input type="file" name="modelo['+itemId+'][specGraphs]['+specListNum+'][icono]"></div>');
	var $specNombre = $('<div class="col-sm-4"><input placeholder="Ttitulo de Especificación" class="form-control" type="text" name="modelo['+itemId+'][specGraphs]['+specListNum+'][titulo]" value=""></div>');
	var $specValor = $('<div class="col-sm-4"><input placeholder="Texto de la especificación" class="form-control" type="text" name="modelo['+itemId+'][specGraphs]['+specListNum+'][texto]" value=""></div>');

	$inputSpecContent.append($specIco);
	$inputSpecContent.append($specNombre);
	$inputSpecContent.append($specValor);

	$(specListContentId).append($inputSpecContent);

	event.preventDefault();
});

$(document).on('click', '.add-img-prod', function(event) {
	event.preventDefault();
	
	var imgListNum = $('#prod-imgs .row').size();

	var $inputImgContent = $('<div class="row mar-btm"/>');
	var $imgInput  = $('<div class="col-sm-12"><input type="file" name="fotos['+imgListNum+']"></div>');

	$inputImgContent.append($imgInput);

	$('#prod-imgs').append($inputImgContent);
});	

$(document).on('click', '.delete-model-from-product', function(event) {
	event.preventDefault();
	var productModelId = $(this).data('model-id');
	var productId = $(this).data('product-id');
	var formData = new FormData();
	var token = $('input[name="_token"]').val();

	formData.append('productModelId',productModelId);
	formData.append('productId',productId)
	formData.append('_token',token);
	formData.append('_method','POST');

	$.ajax({
		url: '/admin/products/deletemodel/' + productModelId,
		type: 'POST',
		contentType: false,
		processData: false,
		data: formData,
		headers: {
			'X-CVL-Auth': token
		}
	})
	.done(function(response) {
		console.log("DELETE MODEL success");
		console.log(response);

		top.location.reload();
	})
	.fail(function(response) {
		console.log("DELETE MODEL error");
		console.log(response)
		$('body').html(response.responseText);
	})
	.always(function() {
		console.log("DELETE MODEL complete");
	});
});

$(document).on('click','.btn-modal-alternate',function(event){
	event.preventDefault();

	var inputName = $(this).data('input-name');
	var inventoryItemId = $(this).data('inventory-item-id');

	$('#modal-add-version-natural .btn-add-altern-version').data('input-name',inputName);
	$('#modal-add-version-natural .btn-add-altern-version').data('inventory-item-id',inventoryItemId);
});

$(document).on('click', '.btn-add-altern-version', function(event) {
	event.preventDefault();

	var inputName = $(this).data('input-name');
	var modelInventoryItemId = $(this).data('inventory-item-id');
	var versionItemId = $('select[name=products]').val();
	var versionItemName = $('select[name=products] option:selected').text()
	var vgnatContainer = $('#vgnat-' + modelInventoryItemId).html(versionItemName);

	var $nuevoInput = $('<input/>').attr({
		name: inputName,
		value: versionItemId,
		type: 'hidden'
	});

	$('#form-product-detail').append($nuevoInput);

	$('#modal-add-version-natural').modal('hide');
});

$(document).on('click', '.btn-delete-prop', function(event) {
	event.preventDefault();
	
	var propId = $(this).data('prop-id');

	var formData = new FormData();
	var token = $('input[name="_token"]').val();

	formData.append('id',propId);
	formData.append('_token',token);
	formData.append('_method','DELETE');
	console.log(formData);

	$.ajax({
		url: '/admin/properties/' + propId,
		type: 'POST',
		contentType: false,
		processData: false,
		data: formData,
		headers: {
			'X-CVL-Auth': token
		}
	})
	.done(function(response) {
		console.log("success");
		console.log(response);
		top.location.reload()
	})
	.fail(function(response) {
		console.log("error");
		console.log(response);
		$('body').html(response.responseText);
	})
	.always(function() {
		console.log("complete");
	});
	
});

$(document).on('click','.delete-image',function(event){
	event.preventDefault();
	var $imgContainer = $(this).parent();
	var imageId = $(this).data('image-id');
	var formData = new FormData();
	var token = $('input[name="_token"]').val();

	formData.append('id',imageId);
	formData.append('_token',token);
	formData.append('_method','DELETE');

	$.ajax({
		url: '/admin/images/' + imageId,
		type: 'POST',
		contentType: false,
		processData: false,
		data: formData,
		headers: {
			'X-CVL-Auth': token
		}
	})
	.done(function(response) {
		console.log("DELETE IMG success");
		console.log(response);

		$imgContainer.remove();
	})
	.fail(function(response) {
		console.log("DELETE IMG error");
		console.log(response)
		$('body').html(response.responseText);
	})
	.always(function() {
		console.log("DELETE IMG complete");
	});
});