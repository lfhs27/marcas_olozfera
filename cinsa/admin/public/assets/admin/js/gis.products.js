$(document).on('click','.btn-add-version220v',function(event){
	event.preventDefault();
	var modelId = $(this).data('model-id');

	$('.btn-guardar-version-220v').data('model-id',modelId);
});

$(document).on('click','.btn-guardar-version-220v',function(event){
	event.preventDefault();
	var token = $('input[name="_token"]').val();
	var inputSKU = $('#input_modelo220V').val();
	var modelId = $(this).data('model-id');
	var formData = new FormData();

	// console.log(inputSKU);
	// console.log(modelId);	

	formData.append('_token',token);
	formData.append('sku',inputSKU);
	formData.append('modelId',modelId);

	$.ajax({
		url: '/admin/products/addalternateversion',
		type: 'POST',
		contentType: false,
		processData: false,
		data: formData,
		headers: {
			'X-CVL-Auth': token
		}
	})
	.done(function(response) {
		console.log("success");
		console.log(response);
		$('#modal-version-elect220').modal('hide');
	})
	.fail(function(response) {
		console.log("error");
		console.log(response);
		$('body').html(response.responseText);
	})
	.always(function() {
		console.log("complete");
	});
	

	// $('#modal-version-elect220').modal('hide');
});

$(document).on('click', '.remove-spec-from-model', function(event) {
	event.preventDefault();

	var formData = new FormData();
	var token = $('input[name="_token"]').val();
	var modelId = $(this).data('model-id');
	var productId = $(this).data('product-id');
	var propertyId = $(this).data('prop-id');
	var $parentElement = $(this).parents('.row');

	formData.append('_token',token);
	formData.append('modelId',modelId);
	formData.append('productId',productId);
	formData.append('propertyId',propertyId);
	
	$.ajax({
		url: '/admin/products/deletespec',
		type: 'POST',
		data: formData,
		contentType: false,
		processData: false
	})
	.done(function(response) {
		console.log("success");
		console.log(response);
		if (response.status == 1) {
			top.location.reload()
		} else {
			alert('Por favor, recargue la página e intente nuévamente');
		}
	})
	.fail(function(response) {
		console.log("error");
		// $('body').html(response.responseText);
		alert('Hubo un error. Recargue la página e intente nuévamente');
	})
	.always(function() {
		console.log("complete");
	});
	
});	

// $(document).on('change','.input_selStatus',function(){
// 	var fieldname = $(this).data('fieldname');
// 	var selectedOp = $(this).val();
// 	var token = $('input[name="_token"]').val();	
// 	var orderId = $(this).data('order-id');
// 	var formData = new FormData();

// 	formData.append('orderId',orderId);
// 	formData.append('status',selectedOp);
// 	formData.append('_token',token);
// 	formData.append('fieldname',fieldname);

// 	$.ajax({
// 		url: '/admin/orders/changestatus',
// 		type: 'POST',
// 		contentType: false,
// 		processData: false,
// 		data: formData,
// 		headers: {
// 			'X-CVL-Auth': token
// 		}
// 	})
// 	.done(function(response) {
// 		console.log("success");
// 		dataNum = nifty.randomInt(0,8);
// 		contentHTML = 'El status ha cambiado correctamente.';

// 		$.niftyNoty({
// 			type: 'info',
// 			container : 'floating',
// 			html : contentHTML,
// 			timer : 3000
// 		});
// 	})
// 	.fail(function(response) {
// 		console.log("error");
// 		$('body').html(response.responseText);
// 	})
// 	.always(function() {
// 		console.log("complete");
// 	});
// })
