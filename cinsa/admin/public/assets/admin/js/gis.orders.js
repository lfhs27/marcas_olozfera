$(document).on('change','.input_selStatus',function(){
	var $loadingImg = $(this).next('img.loadingimg');
	var fieldname = $(this).data('fieldname');
	var selectedOp = $(this).val();
	var token = $('input[name="_token"]').val();	
	var orderId = $(this).data('order-id');
	var formData = new FormData();

	$loadingImg.removeClass('hidden');

	formData.append('orderId',orderId);
	formData.append('status',selectedOp);
	formData.append('_token',token);
	formData.append('fieldname',fieldname);

	$.ajax({
		url: '/admin/orders/changestatus',
		type: 'POST',
		contentType: false,
		processData: false,
		data: formData,
		headers: {
			'X-CVL-Auth': token
		}
	})
	.done(function(response) {
		console.log("success");
		dataNum = nifty.randomInt(0,8);
		contentHTML = 'El status ha cambiado correctamente.';

		$.niftyNoty({
			type: 'info',
			container : 'floating',
			html : contentHTML,
			timer : 3000
		});

		$loadingImg.addClass('hidden');
	})
	.fail(function(response) {
		console.log("error");
		$('body').html(response.responseText);
	})
	.always(function() {
		console.log("complete");
	});
})
