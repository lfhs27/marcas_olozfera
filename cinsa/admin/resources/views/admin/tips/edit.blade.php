@extends('layouts.admin')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
	<div class="eq-height">
		<div class="col-sm-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						Tip
					</h3>
				</div>
				<div class="panel-body">
					<iframe id="form_target" name="form_target" style="display:none"></iframe>
					<form id="my_form" action="/upload" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input name="image" type="file" onchange="SubmitWithCallback($('#my_form'), $('#form_target'), fillFile);this.value='';" />
					</form>
					<form id="tform" action="/tips/save" method="post" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" value="{{ $tip->id }}" />
						<div class="row form-group">
							<div class="col-xs-6">
								<label style="width:100%;">T&iacute;tulo
									<input type="text" class="form-control" name="title" value="{{ $tip->title }}" placeholder="T&iacute;tulo" />
								</label>
							</div>
							<div class="col-xs-6">
								<label style="width:100%;">Tags
									<?php
									$tags = array();
									foreach($tip->tags->lists('tag') as $t){
										array_push($tags, $t);
									}
									$tags = IMPLODE(',', $tags);
									?>
									<input name="tags" id="tags" value="{{ $tags }}" /> 
								</label>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-xs-12">
								<label style="width:100%;">Metadata
									<?php
									$tags = array();
									foreach($tip->metadata->lists('data') as $t){
										array_push($tags, $t);
									}
									$tags = IMPLODE(',', $tags);
									?>
									<input name="metadata" id="metadata" value="{{ $tags }}" /> 
								</label>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-xs-6">
								<label style="width:100%;">Introducci&oacute;n:
									<textarea id="intro" name="intro" class="form-control">{{ $tip->intro }}</textarea>
								</label>
							</div>
							<div class="col-xs-6">
								<label>Im&aacute;gen
									<img src="/{{ $tip->image }}" alt="" style="width:100%" />
									<input name="image" type="file" />
								</label>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-xs-12">
								<label style="width:100%;">Mensaje:
									<textarea id="text" name="text">{{ $tip->text }}</textarea>
								</label>
							</div>
						</div>
						<input type="submit" class="btn btn-primary" value="Guardar" />
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="tipModal" tabindex="-1" role="dialog" aria-labelledby="Direcci&oacute;n">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Direcci&oacute;n</h4>
			</div>
			<div class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#tform').submit();">Guardar</button>
			</div>
		</div>
	</div>
</div>
<script>
var table;
$(document).ready(function(){
	tinymce.init({
	    selector: "textarea",
	    resize: "both",
	    relative_urls: false,
		remove_script_host: false,
	    plugins: ["autoresize", "image", "code", "lists", "code","example", "link", 'textcolor', "print", "preview", "media", "emoticons", "hr", "anchor", "pagebreak", "table", "insertdatetime", "nonbreaking", "wordcount", "searchreplace", "visualblocks", "visualchars", "fullscreen"],
	    indentation : '20pt',
		width: '100%',
	    file_browser_callback: function(field_name, url, type, win) {
	        if (type == 'image') $('#my_form input').click();
	    },
	    image_list: "/uploads/tips",
		toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | forecolor backcolor | emoticons',
		toolbar2: 'print preview'
	});
	$('#tags, #metadata').tagit({
		allowSpaces: true
	});
});
function fillFile(){
	fileName = $('#form_target').contents().find("input[id=fileName]").val();
	
	$('.mce-btn.mce-open').parent().find('.mce-textbox').val('/uploads/tips/'+fileName).closest('.mce-window').find('.mce-primary').click();
}
function SubmitWithCallback(form, frame, successFunction) {
   var callback = function () {
       if(successFunction)
           successFunction();
       frame.unbind('load', callback);
   };
   
   frame.bind('load', callback);
   form.submit();
}
</script>

@endsection