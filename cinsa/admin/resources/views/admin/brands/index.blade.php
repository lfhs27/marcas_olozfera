@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
<!--===================================================-->
<!--div id="content-container"-->
	
	@include('admin.shared.page-title')		

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		
		@include('admin.shared.page-section-title')

		<a class="btn btn-primary" data-toggle="modal" href='#modal-add-brand'>Agregar Marca</a>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nombre</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							@forelse($brands as $brand)
							<tr>
								<td>{{ $brand->id }}</td>
								<td>{{ $brand->front_name }}</td>
								<td>
									{!! Form::open(['action' => ['Admin\BrandsController@destroy',$brand->id],'method' => 'DELETE']) !!}

									<a href="{{ action('Admin\BrandsController@edit',$brand->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>

									<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>

									{!! Form::close() !!}
								</td>
							</tr>
							@empty
							<tr>
								<td>
									<div class="text-center pad-all">
										No existen marcas.
									</div>
								</td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>	
		
	</div>
	<!--===================================================-->
	<!--End page content-->


<!--/div-->
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<div class="modal fade" id="modal-add-brand">
	<div class="modal-dialog">
		<div class="modal-content">
			{!! Form::open(['action' => 'Admin\BrandsController@store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Agregar Marca</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="" class="control-label">Nombre GIS</label>
					<input type="text" name="name" id="inputName" class="form-control" value="" required="required">
				</div>
				<div class="form-group">
					<label for="" class="control-label">Nombre Tienda</label>
					<input type="text" name="front_name" id="inputFront_name" class="form-control" value="" required="required">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


@endsection