@extends('layouts.admin')

@section('content')

<h1>Pedidos</h1>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if($order->shipping_type == 'PICKUP')
	<div class="alert alert-success">
		<h3 style="margin-top: 0">
			Usuario recoge en:
			@if($order->selected_pickup == 1)
				Lago de Guadalupe
			@elseif($order->selected_pickup == 2)
				Ramos Arizpe
			@endif
		</h3>
	</div>
@endif

<div class="panel">
	<div class="panel-body">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Nº de Órden</th>
					<th>Nombre de Persona</th>
					<th>Email</th>
					<th>Teléfono</th>
					<th>Empresa</th>
					<th>Grupo de Facturación</th>
					<th>Monto</th>
					<th>Tipo de Pago</th>
					<th>Autorización</th>
					<th>Fecha y Hora</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $order->id }}</td>
					<td>
						@if($orderUserDetail)
							{{ $orderUserDetail->name . ' ' . $orderUserDetail->lastname }}
						@else
							{{ $order->user->name . ' ' . $order->user->lastname }}
						@endif
					</td>
					<td>
						@if($orderUserDetail)
							{{ $orderUserDetail->email }}
						@else
							{{ $order->user->email }}
						@endif
					</td>
					<td>
						@if($orderUserDetail)
							{{ $orderUserDetail->phone_mobile }}
						@else
							{{ $order->user->phone_mobile }}
						@endif
					</td>
					<td>
						@if($orderUserDetail)
							{{ $orderUserDetail->company->name }}
						@else
							{{ $order->user->company->name }}
						@endif
					</td>
					<td>
						@if($orderUserDetail)
							{{ $orderUserDetail->billing_group }}
						@else
							{{ $order->user->billing_group }}
						@endif
					</td>
					<td>{{money_format('%(#10n',$order->amount)}}</td>
					<td>{{ $order->payment_type }}</td>
					<td>
						@if($user->hasRole('Orders_Authorize_Write'))
							@if(($order->getOriginal('payment_type') === 'PAYMENT_METHOD_PAYPAL') && $order->getOriginal('authorization_status') == 1)
								{{$order->authorization_status}}
							@else
							<?php $autStatus = $order->getOriginal('authorization_status'); ?>
								{!! Form::open(['admin/orders/changestatus']) !!}
								<select name="selAutStatus" id="input_selAutStatus" data-order-id="{{ $order->id }}" data-fieldname="authorization_status" class="input_selStatus">
									<option value="-1" {{ $autStatus == -1 ? 'selected="selected"' : '' }}>No Aprobado</option>
									<option value="1" {{ $autStatus == 1 ? 'selected="selected"' : '' }}>Aprobado</option>
									<option value="0" {{ $autStatus == 0 ? 'selected="selected"' : '' }}>En espera</option>
								</select>
								<img src="{{asset('assets/admin/img/ajax-loader.gif')}}" alt="loading" class="loadingimg hidden">
								{!! Form::close() !!}
							@endif
						@else
							{{$order->authorization_status}}
						@endif
					</td>
					<td>{{ $order->created_at }}</td>
					<td>
						@if($user->hasRole('Orders_Invoice_Write'))
							<?php $delivStatus = $order->getOriginal('delivery_status'); ?>
							{!! Form::open(['admin/orders/changestatus']) !!}	
							<select name="selDelivStatus" id="input_selDelivStatus" data-order-id="{{ $order->id }}" data-fieldname="delivery_status" class="input_selStatus">
								<option value="-1" {{$delivStatus == -1 ? 'selected="selected"' : ''}}>NO Facturar</option>
								<option value="0" {{$delivStatus == 0 ? 'selected="selected"' : ''}}>Por Facturar</option>
								<option value="1" {{$delivStatus == 1 ? 'selected="selected"' : ''}}>Por Entregar</option>
								<option value="2" {{$delivStatus == 2 ? 'selected="selected"' : ''}}>Entregado</option>
							</select>
							<img src="{{asset('assets/admin/img/ajax-loader.gif')}}" alt="loading" class="loadingimg hidden">
							{!! Form::close() !!}
						@else
							{{ $order->delivery_status }}
						@endif
					</td>
				</tr>
			</tbody>
		</table>

		@if(isset($order->address_id))
		<h3>Dirección de Envío</h3>

		<table class="table table-hover">
			<thead>
				<tr>
					<th>Alias</th>
					<th>Calle</th>
					<th>Núm. Ext.</th>
					<th>Núm. Int.</th>
					<th>Colonia</th>
					<th>Código Postal</th>
					<th>Ciudad</th>
					<th>Estado</th>
					<th>Persona que recibe</th>
					<th>Instrucciones</th>
				</tr>
			</thead>
			<tbody>
				@if(!empty($order->address_detail))
				<?php $addressDetails = json_decode($order->address_detail); ?>
					<tr>
						<td>{{ $addressDetails->alias }}</td>
						<td>{{ $addressDetails->calle }}</td>
						<td>{{ $addressDetails->num_ext }}</td>
						<td>{{ $addressDetails->num_int }}</td>
						<td>{{ $addressDetails->colonia }}</td>
						<td>{{ $addressDetails->zip_code->code }}</td>
						<td>{{ $addressDetails->zip_code->city->name }}</td>
						<td>{{ $addressDetails->zip_code->city->state->name }}</td>
						<td>{{ $addressDetails->receptor }}</td>
						<td>{{ $addressDetails->instrucciones }}</td>
					</tr>
				@else
					<tr>
						<td>{{ $order->address->alias }}</td>
						<td>{{ $order->address->calle }}</td>
						<td>{{ $order->address->num_ext }}</td>
						<td>{{ $order->address->num_int }}</td>
						<td>{{ $order->address->colonia }}</td>
						<td>{{ $order->address->zip_code->code }}</td>
						<td>{{ $order->address->zip_code->city->name }}</td>
						<td>{{ $order->address->zip_code->city->state->name }}</td>
						<td>{{ $order->address->receptor }}</td>
						<td>{{ $order->address->instrucciones }}</td>
					</tr>
				@endif
			</tbody>
		</table>
		@endif

		<h3>Productos en el Pedido</h3>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>SKU</th>
					<th>Marca</th>
					<th>Cantidad</th>
					<th>Precio Unitario</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($order->items as $item): ?>
					<tr>
						<td>{{ $item->product_name }}</td>
						<td>{{ $item->sku }}</td>
						<td>{{ $item->marca }}</td>
						<td>{{ $item->quantity }}</td>
						<td>{{money_format('%(#10n',$item->price_per_product)}}</td>
						<td>{{money_format('%(#10n',$item->price_per_set)}}</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>

		@if($order->getOriginal('payment_type') != 'PAYMENT_METHOD_PAYROLL' && $order->reqfactura == 1)
		<?php $facturaInfo = json_decode($order->facturainfo); ?>
		<h3>Información de Facturación</h3>
		<div class="row">
			<div class="col-sm-4">
				<ul>
					<li><strong>Razón Social</strong>: {{ $facturaInfo->razonsocial }}</li>
					<li><strong>RFC</strong>:  {{ $facturaInfo->factura_rfc }}</li>
					<li><strong>Lugar de Expedición</strong>:  {{ $facturaInfo->lugar_expedicion }}</li>
					<li><strong>Correo de Facturación</strong>: {{ $facturaInfo->correo }}</li>
				</ul>
			</div>

			<div class="col-sm-4">
				<ul>
					<li><strong>Calle</strong>: {{ $facturaInfo->calle }}</li>
					<li><strong>Número Exterior</strong>: {{ $facturaInfo->numext }}</li>
					<li><strong>Número Interior</strong>: {{ $facturaInfo->numint }}</li>
					<li><strong>Colonia</strong>: {{ $facturaInfo->colonia }}</li>
					<li><strong>Código Postal</strong>: {{ $facturaInfo->zipcode }}</li>
					<li><strong>Estado</strong>: {{ $facturaInfo->estado }}</li>
					<li><strong>Municipio/Delegación</strong>: {{ $facturaInfo->municipio }}</li>
					<li><strong>Estado</strong>: {{ $facturaInfo->ciudad }}</li>
				</ul>
			</div>
		</div>
		@endif

		<div class="row">
			@if($order->user->company->id == 2 && $order->getOriginal('payment_type') === 'PAYMENT_METHOD_PAYROLL')
			<div class="col-sm-3">
				<a target="_blank" href="{{ url('cart/successpdf/' . $order->id) }}" class="btn btn-block btn-lg btn-info btn-print">Ver Solicitud de Compra</a>
			</div>
			@endif
			<div class="col-sm-3">
				<a target="_blank" href="{{ url('/admin/orders/' . $order->id . '/print') }}" class="btn btn-block btn-lg btn-info btn-print">Imprimir</a>
			</div>
		</div>
	</div>
</div>

@endsection