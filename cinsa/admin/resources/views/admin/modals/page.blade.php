<div class="modal fade" id="modal-add-page">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Agregar Página</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(array('action' => 'Admin\PagesController@store','id'=>'form-add-page-step-1')) !!}

				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label">Nombre</label>
							<input type="text" name="pgNombre" id="inputPgNombre" class="form-control" required="required">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label">Empresa</label><br>
							<?php echo Form::select('company_id',$companies,null,array('class' => 'bs-selectbox')); ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label">Plantilla</label><br>
							<?php echo Form::select('template_id',$templates,null,array('class' => 'bs-selectbox')); ?>
						</div>
					</div>
				</div>

				{!! Form::close() !!}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<a href="#form-add-page-step-1" class="btn btn-save btn-primary">Continuar</a>
			</div>
		</div>
	</div>
</div>