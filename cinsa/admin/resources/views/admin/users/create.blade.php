@extends('layouts.admin')

@section('content')

<h1>Usuarios</h1>

@if (count($errors) > 0)
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h3 class="panel-title">Información</h3>
		</div>
		<div class="panel-body">
			<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
			</ul>
		</div>
	</div>
@endif

<div class="panel panel-default">
	<div class="panel-body">
		{!! Form::open(array('action' => 'Admin\UsersController@store','class'=>'form form-horizontal')) !!}	
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="control-label col-sm-5">Empresa</label>
						<div class="col-sm-7">
							{!! Form::select('company_id',$companyList,null,['class'=>'bs-selectbox']) !!}
						</div>
				</div>
				<div class="form-group bg-info pad-all">
					<label class="control-label col-sm-5">
						Tipo de Usuario
					</label>
					<div class="col-sm-7">
						<select name="user_role" class="bs-selectbox">
							<option value="CLIENTE">Cliente</option>
							<option value="VISTA">Vista</option>
							<option value="RRHH">RRHH</option>
							<option value="FACTURACION">Facturación</option>
							<option value="BRAND_MANAGER">Brand Manager</option>
							<option value="ADMIN">Admin</option>
						</select>

						<div class="mar-top">
							<strong>"RRHH", "BRAND MANAGER" Y "ADMIN"
							tienen acceso al área de administración.</strong>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-5">Grupo de Facturaci&oacute;n</label>
					<div class="col-sm-7">
						<select name="billing_group" class="bs-selectbox">
							<option value="ASGIS">ASGIS</option>
							<option value="AZENTI">AZENTI</option>
							<option value="AXIMUS">AXIMUS</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-5">Nombre</label>
					<div class="col-sm-7">
						<input type="text" name="name" id="inputUserFirstName" class="form-control" required="required" value="{{ old('name') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-5">Apellidos</label>
					<div class="col-sm-7">
						<input type="text" name="lastname" id="inputUserLastName" class="form-control" required="required" value="{{ old('lastname') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-5">Número de Empleado</label>
					<div class="col-sm-7">
						<input type="text" name="employee_number" class="form-control" required="required" value="{{ old('employee_number') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-5">Celular</label>
					<div class="col-sm-7">
						<input type="text" name="phone_mobile" class="form-control" required="required" value="{{ old('phone_mobile') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-5">Email</label>
					<div class="col-sm-7">
						<input type="text" name="email" class="form-control" value="{{ old('email') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-5">Contraseña</label>
					<div class="col-sm-7">
						<input type="password" name="password" class="form-control" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-5">Confirmar Contraseña</label>
					<div class="col-sm-7">
						<input type="password" name="password_confirmation" class="form-control" required="required">
					</div>
				</div>

				{{-- <div class="form-group">
					<label class="control-label col-sm-5">Pregunta Secreta</label>
					<div class="col-sm-7">
						<input type="text" name="secret_question" class="form-control" required="required" value="{{ old('secret_question') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-5">Respuesta</label>
					<div class="col-sm-7">
						<input type="text" name="secret_question_answer" class="form-control" required="required" value="{{ old('secret_question_answer') }}">
					</div>
				</div> --}}

				{{-- <div class="form-group">
					<label class="control-label col-sm-5">Código de Registro</label>
					<div class="col-sm-7">
						<input type="text" name="signup_code" class="form-control" required="required" value="{{ old('signup_code') }}">
					</div>
				</div> --}}
				<div class="form-group">
					<div class="col-sm-12">
						<input type="submit" name="Guardar" value="Guardar" class="btn btn-lg btn-success pull-right">
					</div>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>	

@endsection