@extends('layouts.admin')

@section('content')

@if (count($errors) > 0)
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">Usuario</h3>
	</div>
	<div class="panel-body">
		{!! Form::open(array('action' => array('Admin\UsersController@update',$user_data->id), 'method'=>'put','id'=>'form-update-user','class'=>'form-horizontal')) !!}
			<div class="form-group">
				<label class="control-label col-sm-4">Empresa</label>
				<div class="col-sm-7">
					<select class="bs-selectbox" name="company_id">
						@foreach ($companies as $company)
							<option value="{{ $company->id }}" {{ ($user_data->company_id == $company->id ? 'selected' : '') }}>{{ $company->name }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Tipo de Usuario</label>
				<div class="col-sm-8">
					<select name="user_type" class="bs-selectbox">
						<option value="USER_TYPE_CLIENTE"{{$user_data->type == "USER_TYPE_CLIENTE" ? ' selected="selected"' : ""}}>Cliente</option>
						<option value="USER_TYPE_VISTA"{{$user_data->type == "USER_TYPE_VISTA" ? ' selected="selected"' : ""}}>Vista</option>
						<option value="USER_TYPE_RRHH"{{$user_data->type == "USER_TYPE_RRHH" ? ' selected="selected"' : ""}}>RRHH</option>
						<option value="USER_TYPE_FACTURACION"{{$user_data->type == "USER_TYPE_FACTURACION" ? ' selected="selected"' : ""}}>Facturación</option>
						<option value="USER_TYPE_BRAND_MANAGER"{{$user_data->type == "USER_TYPE_BRAND_MANAGER" ? ' selected="selected"' : ""}}>Brand Manager</option>
						<option value="USER_TYPE_ADMIN"{{$user_data->type == "USER_TYPE_ADMIN" || $user_data->type == "USER_TYPE_SUPER_ADMIN" ? ' selected="selected"' : ""}}>Administrador</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Grupo de Facturaci&oacute;n</label>
				<div class="col-sm-7">
					<select name="billing_group" class="bs-selectbox">
						<option value="ASGIS"{{$user_data->billing_group == "ASGIS" ? ' selected="selected"' : ""}}>ASGIS</option>
						<option value="AZENTI"{{$user_data->billing_group == "AZENTI" ? ' selected="selected"' : ""}}>AZENTI</option>
						<option value="AXIMUS"{{$user_data->billing_group == "AXIMUS" ? ' selected="selected"' : ""}}>AXIMUS</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-sm-4">N&uacute;mero de Empleado</label>
				<div class="col-sm-7">
					<input name="employee_number" type="text" class="form-control" value="{{ $user_data->employee_number }}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-sm-4">Nombre</label>
				<div class="col-sm-7">
					<input name="name" type="text" class="form-control" value="{{ $user_data->name }}">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Apellido</label>
				<div class="col-sm-7">
					<input name="lastname" type="text" class="form-control" value="{{ $user_data->lastname }}">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Correo Electr&oacute;nico</label>
				<div class="col-sm-7">
					<input name="email" type="text" class="form-control" value="{{ $user_data->email }}">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Contrase&ntilde;a</label>
				<div class="col-sm-7">
					<input name="password" type="password" class="form-control" value="">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Tel&eacute;fono Celular</label>
				<div class="col-sm-7">
					<input name="phone_mobile" type="text" class="form-control" value="{{ $user_data->phone_mobile }}">
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Guardar</button>
		{!! Form::close() !!}
	</div>
</div>
<!-- 
<div class="well">
	<?php $cnt = 1; ?>
	@foreach($user_data->roles as $role)
	{{$cnt}} {{$role->name}}<br>
	<?php $cnt++; ?>
	@endforeach
</div>
-->
<!--===================================================-->
<!-- End Striped Table -->

@endsection