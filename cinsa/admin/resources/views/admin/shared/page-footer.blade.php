<div class="modal fade" id="modal-select-business-unit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Seleccione Unidad de Negocio</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<p>Seleccione una unidad de negocio para administrar.</p>
					</div>
				</div>
				<div class="row">
					@forelse($BusinessUnits as $bunit)
					{!! Form::open(['action'=>['Admin\BusinessUnitsController@set']]) !!}
					<input type="hidden" name="businessUnitId" value="{{$bunit->id}}">
					<div class="col-md-3">
						<button class="btn btn-primary" type="submit" name="Unidad de Negocio" value="{{ $bunit->name }}">{{ $bunit->name }}</button>
					</div>
					{!! Form::close() !!}
					@empty
					No existen unidades de negocios.
					@endforelse
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				@can('admin-super-admin')
				<a href="{{ action('Admin\BusinessUnitsController@create') }}" class="btn btn-primary">Agregar Unidad de Negocio</a>
				@endcan
			</div>
		</div>
	</div>
</div>

@if(!$UsingBusinessUnit)
<script>
	$(document).ready(function() {
		$('#modal-select-business-unit').modal('show');
	});
</script>
@endif