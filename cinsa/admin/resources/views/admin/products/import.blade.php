@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	
	@include('admin.shared.page-title')		

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		
		@include('admin.shared.page-section-title')

		<div class="panel panel-default">
			<div class="panel-body">
				<p><strong>Seleccione el archivo que desea importar.</strong></p>

				{!! Form::open(['action'=>'Admin\ProductsController@postImport','files' => true]) !!}
				<p>
				<input type="file" name="excelproducts">
				</p>
				<p>
					<button type="submit" name="Importar" value="Importar" class="btn btn-primary">Importar</button>
				</p>
				{!! Form::close() !!}
			</div>
		</div>
		
	</div>
	<!--===================================================-->
	<!--End page content-->


</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<div class="modal fade" id="modal-warning-business-unit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Recuerde seleccionar la unidad de negocio</h4>
			</div>
			<div class="modal-body">
				<p>
				Al importar productos, éstos serán asociados a la unidad de negocio seleccionada actualmente
				@if(isset($UsingBusinessUnit))
				(<strong>{{ $UsingBusinessUnit->name }}</strong>)
				@endif.
				</p>
				<p>
					Si desea seleccionar otra unidad de negocio, haga click en el botón en la parte superior y elija una nueva unidad de negocio.
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#modal-warning-business-unit').modal('show');
	});
</script>

@endsection