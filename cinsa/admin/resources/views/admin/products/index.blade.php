@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
<!--===================================================-->
<!--div id="content-container"-->
	
	@include('admin.shared.page-title')		

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		
		@include('admin.shared.page-section-title')

		<a class="btn btn-primary" data-toggle="modal" href='#modal-add-product'>Agregar Producto</a>
		<div class="panel panel-primary">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover" id="produts-editable-main">
						<thead>
							<tr>
								<th>#</th>
								<th>Nombre</th>
								<th>Nombre Corto</th>
								<th>Segment1</th>
								<th>Marca</th>
								<th>Categoría</th>
								<th>Subcategoría</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
								@forelse($products as $product)
								<tr>
									<td>{{ isset($product->id) ? $product->id : '' }}</td>
									<td>{{ isset($product->name) ? $product->name : '' }}</td>
									<td>{{ isset($product->short_name) ? $product->short_name : '' }}</td>
									<td>{{ isset($product->segment1) ? $product->segment1 : '' }}</td>
									<td>{{ isset($product->brand->name) ? $product->brand->name : '' }}</td>
									<td>{{ isset($product->family->name) ? ($product->family->brand ? $product->family->brand->name : '') . ' > ' . $product->family->front_name : '' }}</td>
									<td>{{ isset($product->line->name) ? ($product->line->brand?$product->line->brand->name:'') . ' > ' . $product->line->front_name : '' }}</td>
									<td><a class="btn btn-default" href="{{ action('Admin\ProductsController@edit',$product->id) }}"><i class="fa fa-pencil"></i></a></td>
								</tr>
								@empty
								<tr>
									<td colspan="5">
										<div class="pad-all">No existen productos en el inventario. Puede <a href="{{ action('Admin\ProductsController@create') }}">Agregar un producto</a> ó <a href="{{ action('Admin\ProductsController@import') }}">Importar de una lista de Excel</a>.</div>
									</td>
								</tr>
								@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
	<!--===================================================-->
	<!--End page content-->


<!--/div-->
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<div class="modal fade" id="modal-add-product">
	<div class="modal-dialog">
		<div class="modal-content">
			{!! Form::open(['action' => 'Admin\ProductsController@store']) !!}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Agregar Producto</h4>
				</div>
				<div class="modal-body">
					<p>
						<label for="" class="control-label">Nombre</label>
						<input type="text" name="name" id="inputName" class="form-control" value="" required="required">
					</p>
					
					<p>
						<label for="" class="control-label">Es Combo</label>
						<select name="isCombo" id="isCombo" class="form-control">
							<option value="0">No</option>
							<option value="1">Si</option>
						</select>
					</p>

					<p>
						<label for="" class="control-label">SKU / SEGMENT1</label>
						<input type="text" name="segment1" id="inputSegment1" class="form-control" value="" required="required">
					</p>

					<p>
						<select name="brand" id="inputBrand" class="form-control" required="required">
							<option value="0">--Elige una marca--</option>
							@foreach($brands as $brand)
							<option value="{{ $brand->id }}">{{ $brand->name }}</option>
							@endforeach
						</select>
					</p>
					<p>
						<select name="family" id="inputFamily" class="form-control" required="required">
							<option value="0">--Elige una categoría--</option>
						</select>
					</p>
					<p>
						<select name="line" id="inputLine" class="form-control">
							<option value="">-- Elige una sub categoría --</option>
						</select>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		});
	});

	$(document).on('change', '#inputBrand', function(event) {
		event.preventDefault();
		
		selectFilter($('#inputBrand'),$('#inputFamily'),'/search/brand_families');
	});

	$(document).on('change', '#inputFamily', function(event) {
		event.preventDefault();
		selectFilter($('#inputFamily'),$('#inputLine'),'/search/family_lines');
	});

	$(document).ready(function() {
		$('#main-search').on('keyup', function(event) {
			event.preventDefault();

			var searchTerm = $(this).val();

			if ( searchTerm.length > 3) {
				$.getJSON('/livesearch/products', {
					productQuery: searchTerm
				}, function(json, textStatus){
					/*console.log(json);*/
					var liveSearchHtml = '';

					$.each(json, function(index, product) {
						/*console.log(product);*/
						liveSearchHtml += '<tr>';
							liveSearchHtml += '<td>';
								liveSearchHtml += product.id;
							liveSearchHtml += '</td>';
							liveSearchHtml += '<td>';
								liveSearchHtml += product.name;
							liveSearchHtml += '</td>';
							liveSearchHtml += '<td>';
								if ( product.short_name != null ) { liveSearchHtml += product.short_name; }
							liveSearchHtml += '</td>';
							liveSearchHtml += '<td>';
								liveSearchHtml += product.segment1;
							liveSearchHtml += '</td>';
							liveSearchHtml += '<td>';
								liveSearchHtml += product.brand.name;
							liveSearchHtml += '</td>';
							liveSearchHtml += '<td>';
								liveSearchHtml += product.family.name;
							liveSearchHtml += '</td>';
							liveSearchHtml += '<td>';
								liveSearchHtml += product.line.name;
							liveSearchHtml += '</td>';
							liveSearchHtml += '<td>';
								liveSearchHtml += '<a class="btn btn-default" href="/products/'+product.id+'/edit"><i class="fa fa-pencil"></i></a>';
							liveSearchHtml += '</td>';
						liveSearchHtml += '</tr>';
					});

					$('#produts-editable-main tbody').html(liveSearchHtml);

					$('#produts-editable-main').Tabledit({
						url: '/products/quickedit',
						editButton: false,
						columns: {
							identifier: [0,'id'],
							editable: [
								[1,'name'],
								[2,'short_name'],
								[3,'segment1'],
								[4,'brand','<?php echo json_encode($brands->pluck('name','id')) ?>'],
								[5,'family','<?php echo json_encode($families) ?>'],
								[6,'line','<?php echo json_encode($lines) ?>']
							]
						},
						buttons: {
							edit: {
								class: 'btn btn-sm btn-default',
								html: '<i class="fa fa-pencil"></i>',
								action: 'edit'
							},
							delete: {
								class: 'btn btn-sm btn-default',
								html: '<i class="fa fa-trash"></i>',
								action: 'delete'
							},
							save: {
								class: 'btn btn-sm btn-success',
								html: 'Save'
							},
							restore: {
								class: 'btn btn-sm btn-warning',
								html: 'Restore',
								action: 'restore'
							},
							confirm: {
								class: 'btn btn-sm btn-danger',
								html: 'Confirm'
							}
						},
						onSuccess: function(response)
						{
							console.log(response);
						},
						// onAjax: function(action,serialized)
						// {
						// 	console.log(action);
						// 	console.log(serialized);
						// },
						onFail: function(xhr,textStatus,error)
						{
							console.log('Failed');
							console.log(xhr.responseText)
							$('body').html(xhr.responseText);
						}

					});
				});
			}
		});	
	});

	function selectFilter($source,$target,route)
	{
		var objectId = $source.val();
		var formData = new FormData();

		$target.empty();

		fetcher(objectId,$target,route);
	}

	function fetcher(objectId,$target,route) {

		$.ajax({
			url: route,
			type: 'GET',
			data: {objectId: objectId},
		})
		.done(function(response) {
			console.log("success");
			console.log(response)

			$target.append('<option value="0">-- Elija una opción --</option>');

			$.each(response, function(index, object) {
				$target.append('<option value="'+object.id+'">'+object.name+'</option>')
			});
		})
		.fail(function(response) {
			console.log("error");
			$('body').html(response.responseText);
		})
		.always(function() {
			console.log("complete");
		});
		
	}
</script>

@can('Products_Write')
@if(isset($brands))
<script>
	$(document).ready(function() {
	// $.ajaxSetup({
	// 	headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	// });

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('#produts-editable-main').Tabledit({
		url: '/products/quickedit',
		editButton: false,
		columns: {
			identifier: [0,'id'],
			editable: [
				[1,'name'],
				[2,'short_name'],
				[3,'segment1'],
				[4,'brand','<?php echo json_encode($brands->brands->pluck('name','id')) ?>'],
				[5,'family','<?php echo json_encode($families) ?>'],
				[6,'line','<?php echo json_encode($lines) ?>']
			]
		},
		buttons: {
			edit: {
				class: 'btn btn-sm btn-default',
				html: '<i class="fa fa-pencil"></i>',
				action: 'edit'
			},
			delete: {
				class: 'btn btn-sm btn-default',
				html: '<i class="fa fa-trash"></i>',
				action: 'delete'
			},
			save: {
				class: 'btn btn-sm btn-success',
				html: 'Save'
			},
			restore: {
				class: 'btn btn-sm btn-warning',
				html: 'Restore',
				action: 'restore'
			},
			confirm: {
				class: 'btn btn-sm btn-danger',
				html: 'Confirm'
			}
		},
		onSuccess: function(response)
		{
			console.log(response);
		},
		// onAjax: function(action,serialized)
		// {
		// 	console.log(action);
		// 	console.log(serialized);
		// },
		onFail: function(xhr,textStatus,error)
		{
			console.log('Failed');
			console.log(xhr.responseText)
			$('body').html(xhr.responseText);
		}

	});
});
</script>
@endif
@endcan

@endsection