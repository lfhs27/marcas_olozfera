@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	
	@include('admin.shared.page-title')		

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		
		@include('admin.shared.page-section-title')
		
		<div class="row">
			<div class="col-sm-4">
			{!! Form::open(['action'=>'Admin\ProductsController@store','method' => 'POST','files' => true]) !!}
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Nombre</label>
								<input type="text" name="name" id="inputName" class="form-control" value="" required="required">
							</div>
						
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="" class="control-label">Nombre Corto</label>
										<input type="text" name="short_name" id="inputShort_name" class="form-control" value="">
									</div>
									<div class="col-md-6">
										<label for="" class="control-label">Precio al público</label>
										<input type="text" name="fake_price" id="inputFake_price" class="form-control" value="">
									</div>
								</div>
							</div>
						
							<div class="form-group">
								<label for="" class="control-label">Subtítulo</label>
								<input type="text" name="description" id="inputDescription" class="form-control" value="">
							</div>

							<div class="form-group">
								<label for="" class="control-label">Marca</label>
								<select name="brand_id" id="inputBrand_id" class="form-control">
									<option value="0">SELECCIONE UNA MARCA</option>
									@foreach($UsingBusinessUnit->brands as $brand)
									<option value="{{$brand->id}}">{{$brand->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="" class="control-label">Categoría</label>
										<select name="family_id" id="inputFamily_id" class="form-control">
											<option value="0">Selecciones una categoría</option>
											@foreach($UsingBusinessUnit->families as $family)
											<option value="{{ $family->id }}">{{ $family->brand->name }} - {{ $family->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="" class="control-label">Sub-Categoría</label>
										<select name="line_id" id="inputLine_id" class="form-control">
											<option value="">Seleccione una Sub-Categoría</option>
											@foreach($UsingBusinessUnit->lines as $line)
											<option value="{{$line->id}}">{{ $line->brand->name }} {{ $line->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</div>
			{!! Form::close() !!}
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<p>
								<label for="" class="control-label">Foto Principal:</label>
								<a class="btn btn-primary" data-toggle="modal" href='#modal-add-featured-image'>Seleccionar Imagen Principal</a>
							</p>
						</div>
						<div class="form-group">
							<!-- <p>
								<label for="" class="control-label">Galería de Fotos</label>
								<form action="/admin/products/photos" class="dropzone" id="galleryDropzone" method="post" enctype="multipart/form-data">
									Arrastre sus archivos a este recuadro
								</form>
							</p> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--===================================================-->
	<!--End page content-->


</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script>
	$(document).on('click','.featured-selector-link',function(event){
		event.preventDefault();
		var featuredImageId = $(this).data('id');

		$('#inputFeaturedImage').val(featuredImageId);

		$('#form-add-featured-image').submit();

	});

	$(document).ready(function() {
		$('#inputDescriptionHtml').summernote({height: 250});

		$('#inputProductoRecomendado').on('keyup', function(event) {
			var prodNombreQ = $(this).val();

			var formData = new FormData();

			formData.append('productQuery',prodNombreQ);

			if (prodNombreQ.length > 3) {
				$.ajax({
					url: '/livesearch/products',
					type: 'POST',
					contentType: false,
					processData: false,
					data: formData
				})
				.done(function(response) {
					console.log("success");
					// console.log(response);
					var liveSearchHtml = '';

					$.each(response, function(index, product) {
						liveSearchHtml += '<tr><td>'+product.id+'</td><td>'+product.name+'</td><td>'+product.brand.name+'</td><td><a href="#" class="btn btn-primary add-prod-rec" data-product-id="'+product.id+'" data-product-name="'+product.name+'"><i class="fa fa-plus"></i></a></td></tr>';
					});

					$('#livesearch-products-table-results tbody').html(liveSearchHtml);
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
				
			}
		});
	});

</script>

@if(isset($UsingBusinessUnit->name))
<script>
	Dropzone.autoDiscover = false;
	$(document).ready(function() {
	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	});

	// $('div#galleryDropzone').dropzone({
	// 	url: '/admin/products/gallery'
	// });

	// Dropzone.options.galleryDropzone = {
	// 	@if(isset($product) && $product->gallery)
	// 	init: function(){
	// 		@foreach($product->gallery->images as $image)
	// 			var mockFile = { name: "{{$image->name}}", size: {{$image->size}}, type: '{{$image->mimetype}}' };
 //                this.addFile.call(this, mockFile);
 //                this.options.thumbnail.call(this, mockFile, "{{url($image->filepath .'/'. $image->filename)}}");
	// 		@endforeach
	// 	}
	// 	@endif
	// }

	// var galleryDropzone = new Dropzone('#galleryDropzone');

	// galleryDropzone.on('sending',function(file,xhr,formData){
	// 	console.log(formData);

	// 	formData.append('_token',$('meta[name="csrf-token"]').attr('content'));
	// 	formData.append('productId',);
	// });

	// galleryDropzone.on('error',function(file,message,xhr){
	// 	$('body').html(message);
	// });

	// galleryDropzone.on('success',function(file,response){
	// 	console.log(response);
	// })

	$('#specifications-editable').Tabledit({
		url: '/admin/products/specifications/quickedit',
		editButton: false,
		restoreButton: false,
		columns: {
			identifier: [0,'id'],
			editable: [
				[1,'name'],
				[2,'value']
			]
		},
		buttons: {
			edit: {
				class: 'btn btn-sm btn-default',
				html: '<i class="fa fa-pencil"></i>',
				action: 'edit'
			},
			delete: {
				class: 'btn btn-sm btn-default',
				html: '<i class="fa fa-trash"></i>',
				action: 'delete'
			},
			save: {
				class: 'btn btn-sm btn-success',
				html: 'Save'
			},
			confirm: {
				class: 'btn btn-sm btn-danger',
				html: 'Confirm'
			}
		},
		onSuccess: function(response)
		{
			console.log(response);
		},
		onAjax: function(action,serialized)
		{
			console.log(action);
			console.log(serialized);
		},
		onFail: function(xhr,textStatus,error)
		{
			console.log('Failed');
			console.log(xhr.responseText)
			$('body').html(xhr.responseText);
		}

	});
});
</script>
@endif

@endsection