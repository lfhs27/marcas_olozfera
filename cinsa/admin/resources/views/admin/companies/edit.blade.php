@extends('layouts.admin')

@section('content')

<h1>EMPRESAS</h1>

@if (count($errors) > 0)
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

<a class="btn btn-warning" id="btn-add-company" data-toggle="modal" href='#modal-add-company'>Agregar Empresa</a>
<a class="btn btn-default" id="btn-import-employees" data-toggle="modal" href='#modal-import-employees'>Importar Empleados</a>
<br><br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">{{ $company->name }}</h3>
	</div>
	<div class="panel-body">
		{!! Form::open(array('action' => array('Admin\CompaniesController@update',$company->id), 'method'=>'put','id'=>'form-update-company','class'=>'form-horizontal')) !!}
			<div class="form-group">
				<label class="control-label col-sm-4">Nombre Comercial</label>
				<div class="col-sm-7">
					<input name="companyName" type="text" class="form-control" value="{{ $company->name }}">
					<div class="checkbox">
						<label>
							<input name="belongsGISVerify" type="checkbox" value="1" <?php echo $company->belongs_gis ? 'checked' : ''; ?>>
							Pertenece a GIS
						</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Código</label>
				<div class="col-sm-7">
					<input name="companyCode" type="text" class="form-control" value="{{ $company->code }}">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Dominio</label>
				<div class="col-sm-7">
					<input name="companyDomain" type="text" class="form-control" value="{{ $company->domain }}">
					<div class="checkbox">
						<label>
							<input name="companyDomainVerify" type="checkbox" value="1" <?php echo $company->domain_verify ? 'checked' : ''; ?>>
							Verificar Dominio
						</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Tipo de Pago</label>
				<div class="col-sm-7">
					<div class="checkbox">
						<label>
							<input name="allowed_payment_methods[]" type="checkbox" value="PAYMENT_METHOD_PAYROLL"<?php echo $allowedPaymentMethods ? (in_array('PAYMENT_METHOD_PAYROLL',$allowedPaymentMethods) ? ' checked' : '') : '' ?>>
							Nómina
						</label>
					</div>

					<div class="checkbox">
						<label>
							<input name="allowed_payment_methods[]" type="checkbox" value="PAYMENT_METHOD_PAYU"<?php echo $allowedPaymentMethods ? (in_array('PAYMENT_METHOD_PAYU',$allowedPaymentMethods) ? ' checked' : '') : '' ?>>
							PAYU
						</label>
					</div>

					<div class="checkbox">
						<label>
							<input name="allowed_payment_methods[]" type="checkbox" value="PAYMENT_METHOD_PAYPAL"<?php echo $allowedPaymentMethods ? (in_array('PAYMENT_METHOD_PAYPAL',$allowedPaymentMethods) ? ' checked' : '') : '' ?>>
							PAYPAL
						</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Envío</label>
				<div class="col-sm-7">
					<div class="checkbox">
						<label>
							<input name="allowed_shipping_methods[]" type="checkbox" value="SHIPPING_METHOD_IMMEDIATE_DELIVERY"<?php echo $allowedShippingMethods ? (in_array('SHIPPING_METHOD_IMMEDIATE_DELIVERY',$allowedShippingMethods) ? ' checked' : '') : '' ?>>
							Entrega Inmediata
						</label>
					</div>

					<div class="checkbox">
						<label>
							<input name="allowed_shipping_methods[]" type="checkbox" value="SHIPPING_METHOD_CEDIS"<?php echo $allowedShippingMethods ? (in_array('SHIPPING_METHOD_CEDIS',$allowedShippingMethods) ? ' checked' : '') : '' ?>>
							CEDIS
						</label>
					</div>

					<div class="checkbox">
						<label>
							<input name="allowed_shipping_methods[]" type="checkbox" value="SHIPPING_METHOD_HOME_DELIVERY"<?php echo $allowedShippingMethods ? (in_array('SHIPPING_METHOD_HOME_DELIVERY',$allowedShippingMethods) ? ' checked' : '') : '' ?>>
							Domicilio
						</label>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-sm-4">Cuota Mensual por Empleado</label>
				<div class="col-sm-7">
					<input name="employeeMonthlyFee" type="number" class="form-control" value="{{ $company->employee_monthly_fee }}" step="0.01" min="0">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Número de Invitados</label>
				<div class="col-sm-7">
					<input name="allows_guests" type="number" class="form-control" value="{{ $company->allows_guests }}" />
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Guardar</button>
		{!! Form::close() !!}
	</div>
</div>
<!--===================================================-->
<!-- End Striped Table -->

@include('admin.modals.company')

@endsection