@extends('layouts.admin')

@section('content')

<h1>EMPRESAS</h1>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

	<a class="btn btn-warning" id="btn-add-company" data-toggle="modal" href='#modal-add-company'>Agregar Empresa</a><br><br>

	<!-- Basic Data Tables -->
	<!--===================================================-->
	<div class="panel">
		<div class="panel-heading">
			<h3 class="panel-title">Información Básica</h3>
		</div>
		<div class="panel-body">
			<table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Código</th>
						<th>Dominio</th>
						<th>Métodos de Pago</th>
						<th>Métodos de Envío</th>
						<th>Permite invitados</th>
						<th>Cuota mensual</th>
					</tr>
				</thead>
				<tbody>
					
						<tr>
							<td width="10%">{{ $company->name }}</td>
							<td width="10%">{{ $company->code }}</td>
							<td width="10%">{{ $company->domain }}</td>
							<td>{{ $company->allowed_payment_methods }}</td>
							<td>{{ $company->allowed_shipping_methods }}</td>
							<td width="5%">{{ $company->allows_guests }}</td>
							<td width="10%">{{ number_format($company->employee_monthly_fee, 0) }}</td>
						</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!--===================================================-->
	<!-- End Striped Table -->

@include('admin.modals.company')

@endsection