@extends('layouts.admin')

@section('content')

<h1>Empresas</h1>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if($user->hasRole('Companies_Write'))
<a class="btn btn-warning" id="btn-add-company" data-toggle="modal" href='#modal-add-company'>Agregar Empresa</a><br><br>
@endif

	<!-- Basic Data Tables -->
	<!--===================================================-->
	<div class="panel">
		<div class="panel-heading">
			<h3 class="panel-title">Listado de Empresas</h3>
		</div>
		<div class="panel-body">
			<table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre</th>
						<th>Código</th>
						<th>Dominio</th>
						<th>Métodos de Pago</th>
						<th>Métodos de Envío</th>
						<th>Permite invitados</th>
						<th>Cuota mensual</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php $companyCnt = 1; ?>
					<?php foreach ($companiesList as $company): ?>
						<tr>
							<td width="5%">{{ $companyCnt }}</td>
							<td width="10%">{{ $company->name }}</td>
							<td width="10%">{{ $company->code }}</td>
							<td width="10%">{{ $company->domain }}</td>
							<td width="15%">{{ $company->allowed_payment_methods }}</td>
							<td width="15%">{{ $company->allowed_shipping_methods }}</td>
							<td width="5%">{{ $company->allows_guests }}</td>
							<td width="10%">{{ $company->employee_monthly_fee }}</td>
							<td width="20%">
								<a href="{{ action('Admin\CompaniesController@show',['id' => $company->id]) }}" class="btn btn-info pull-left btn-sm" style="margin-right: 5px;">Admin</a> 
								@if($user->hasRole('Companies_Write'))
								<a href="{{ action('Admin\CompaniesController@edit',['id' => $company->id]) }}" class="btn btn-purple pull-left btn-sm" style="margin-right: 5px;"><i class="fa fa-pencil"></i></a> 
								 {!! Form::open(array('route' => array('admin.companies.destroy', $company->id), 'method' => 'delete')) !!}
							        <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></button>
							    {!! Form::close() !!}
							    @endif

							</td>
						</tr>
						<?php $companyCnt++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
	<!--===================================================-->
	<!-- End Striped Table -->

@include('admin.modals.company')

@endsection