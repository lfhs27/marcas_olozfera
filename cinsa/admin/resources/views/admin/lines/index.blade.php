@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
<!--===================================================-->
<!--div id="content-container"-->
	
	@include('admin.shared.page-title')		

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		
		@include('admin.shared.page-section-title')

		<a class="btn btn-primary" data-toggle="modal" href='#modal-add-line'>Crear Subcategoría de Productos</a>
		{!! Form::open(['action'=>['Admin\LinesController@updateOrder'],'method' => 'POST','id' => 'fOrder']) !!}
		
		<input type="hidden" name="lines_order" value="" />
		<input type="submit" class="btn btn-success" value="Guardar Orden" />
		{!! Form::close() !!}

		<div class="panel panel-default">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nombre</th>
								<th>Categoría Padre</th>
								<th>Marca</th>
								<th width="100px">&nbsp;</th>
							</tr>
						</thead>
						<tbody class="boxes">
							@forelse($lines as $line)
							<tr data-model-id="{{ $line->id }}">
								<td>{{ $line->id }}</td>
								<td>{{ $line->front_name }}</td>
								<td>{{ isset($line->family) ? $line->family->name : '' }}</td>
								<td>{{ isset($line->brand) ? $line->brand->name : '' }}</td>
								<td style="witdh: 30px;">
									{!! Form::open(['action' => ['Admin\LinesController@destroy',$line->id],'method' => 'DELETE']) !!}
									<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
									<a data-description="{{ $line->description }}" data-name="{{ $line->front_name }}" data-id="{{ $line->id }}" class="btn btn-primary btn-edit-line" data-brand="{{ $line->brand_id }}" data-family="{{ $line->family_id }}" data-toggle="modal" href='#modal-edit-line'><i class="fa fa-pencil"></i></a>

									{!! Form::close() !!}
								</td>
							</tr>
							@empty
							<tr>
								<td colspan="4">No existen líneas de productos.</td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>	
			</div>
		</div>
		
	</div>
	<!--===================================================-->
	<!--End page content-->


<!--/div-->
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<div class="modal fade" id="modal-edit-line">
	<div class="modal-dialog">
		<div class="modal-content">
			{!! Form::open(['action' => ['Admin\LinesController@update',1],'id' => 'form-edit-line','method' => 'PATCH']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Editar Línea</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<p>
						<label for="" class="control-label">Nombre</label>
						<input type="text" name="name" id="inputNameEdit" class="form-control" value="" required="required">
					</p>
					<p>
						<label class="control-label">Descripción</label>
						<input type="text" name="description" id="inputDescriptionEdit" class="form-control" value="">
					</p>

					<p>
						<label for="" class="control-label">Familia</label>
						<select name="family_id" id="inputFamily_idEdit" class="form-control">							
							@foreach($families as $family)
								<option value="{{ $family->id }}">{{ ($family->brand ? $family->brand->name : "") }} - {{$family->name}}</option>
							@endforeach
						</select>
					</p>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-add-line">
	<div class="modal-dialog">
		<div class="modal-content">
			{!! Form::open(['action' => 'Admin\LinesController@store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Crear Línea de Producto</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<p>
						<label for="" class="control-label">Nombre</label>
						<input type="text" name="name" id="inputName" class="form-control" value="" required="required">
					</p>
					<p>
						<label class="control-label">Descripción</label>
						<input type="text" name="description" id="inputDescription" class="form-control" value="">
					</p>
					<p>
						<label for="" class="control-label">Marca</label>
						<select name="brand_id" id="inputBrand_id" class="form-control">
							<option value="">-- Selecciona una --</option>
							@foreach($brands as $brand)
								<option value="{{ $brand->id }}">{{ $brand->name }}</option>
							@endforeach
						</select>
					</p>
					<p>
						<label for="" class="control-label">Familias</label>
						<select name="family_id" id="inputFamily_id" class="form-control" required="required">
							<option value="">--Selecciona una--</option>
							@foreach($families as $family)
								<option value="{{ $family->id }}">{{ ($family->brand?$family->brand->name:"") }} - {{$family->name}}</option>
							@endforeach
						</select>
					</p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	$('.boxes').sortable({
		opacity: 0.6,
		tolerance: "pointer"
	});
	
	$("#fOrder").submit(function(e){
		sorted = $('.boxes').sortable('toArray',{attribute: 'data-model-id'});
		sortedObj = $.map(sorted, function(val, i){
			return {
					'id' : val,
					'order' : i
				};
		});
		$(this).find("[name=lines_order]").val(JSON.stringify(sortedObj));
		
	});
});
	$(document).on('click','.btn-edit-line',function(event){
		var $form = $('#form-edit-line');
		var lineId = $(this).data('id');
		var name = $(this).data('name');
		var description = $(this).data('description');
		var newAction = '/lines/' + lineId;
		var brandId = $(this).data('brand');
		var familyId = $(this).data('family');

		$('#inputNameEdit').val(name);
		$('#inputDescriptionEdit').val(description);

		$.each($('#inputFamily_idEdit option'), function(index, family) {
			if (family.value == familyId) {
				$(this).prop('selected','selected');
			}
		});

		$form.attr('action', newAction);
	});
</script>

@endsection