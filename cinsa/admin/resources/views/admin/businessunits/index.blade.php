@extends('layouts.admin')

@section('content')

<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				@include('admin.shared.page-title')		

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					@include('admin.shared.page-section-title')
					
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>Nombre</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										@forelse($businessUnits as $businessUnit)
										<tr>
											<td>{{ $businessUnit->id }}</td>
											<td>{{ $businessUnit->name }}</td>
											<td>
												<a class="btn btn-primary" href="{{ action('Admin\BusinessUnitsController@edit',$businessUnit->id) }}"><i class="fa fa-pencil"></i></a>
											</td>
										</tr>
										@empty
										<tr>
											<td><div class="pad-all text-center">No existen Unidades de Negocio en el sistema.</div></td>
										</tr>
										@endforelse
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

@endsection