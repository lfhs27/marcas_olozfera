<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tienda Calorex</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{ asset('assets/front/css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/ventadirecta.css') }}">

	<script src="{{ asset('assets/front/js/jquery-1.11.3.min.js') }}"></script>
	<script src="{{ asset('assets/front/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/front/js/jquery.cycle2.min.js') }}"></script>
	<script src="{{ asset('assets/front/js/bootbox.min.js')}}"></script>
	<script src="{{ asset('assets/front/js/ventadirecta.js') }}"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="template-login" style="background-image: url({{ asset('assets/front/img/ventadirecta/bgVD02.png') }}); background-repeat: no-repeat;">
	<header>
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('assets/front/img/ventadirecta/logob-tiendaCalorex.png') }}" alt="Grupo Industrial Saltillo"></a>
				</div>
			</div>
		</nav>
	</header>

	<section id="login">
		<div class="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
			<div class="login-content">
				{!! Form::open(array('url' => 'auth/register','class' => 'form form-horizontal')) !!}
					<legend>CREA TU CUENTA</legend>
					<div class="form-group">
						<div class="col-sm-12">
							@if (count($errors) > 0)
							<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
							</ul>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Nombre</label>
						<div class="col-sm-8">
							<input name="name" type="text" class="form-control" placeholder="Escribe tu(s) nombre(s)" value="{{ old('name') }}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Apellidos</label>
						<div class="col-sm-8">
							<input name="lastname" type="text" class="form-control" placeholder="Escribe tus apellidos" value="{{ old('lastname') }}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Número de Empleado</label>
						<div class="col-sm-8">
							<input name="employee_number" type="text" class="form-control" id="" placeholder="#########" value="{{ old('employee_number') }}">
						</div>
					</div>

					<hr>

					<div class="form-group">
						<label class="col-sm-4 control-label">Celular</label>
						<div class="col-sm-8">
							<input name="phone_mobile" type="text" class="form-control" id="" placeholder="Escribe los 10 dígitos" value="{{ old('phone_mobile') }}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Correo Electrónico</label>
						<div class="col-sm-8">
							<input name="email" type="text" class="form-control" id="" placeholder="Escribe tu correo electrónico" value="{{ old('email') }}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Contraseña</label>
						<div class="col-sm-8">
							<input name="password" type="password" class="form-control" id="" placeholder="Escribe tu contraseña">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Confirmar Contraseña</label>
						<div class="col-sm-8">
							<input name="password_confirmation" type="password" class="form-control" id="" placeholder="Confirme la contraseña">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Código de Registro</label>
						<div class="col-sm-8">
							<input name="signup_code" type="text" class="form-control" id="" placeholder="#####" value="{{old('signup_code')}}">
							<div class="alert alert-danger alert-codigo" role="alert" style="margin:20px 0px 0px 0px; display:none;">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Error:</span>
								C&oacute;digo Inv&aacute;lido
							</div>
						</div>
					</div>
				
					<div class="controls text-center">
						<button type="submit" class="btn btn-primary btn-block">REGISTRARME</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</section>

<script>
$(document).ready(function(){
	$(".nEmpleado").hide();
	$("[name=signup_code]").change(function(e){
		elm = $(this);
		$.ajax({
			url: "/company/verify_code/"+elm.val(),
			dataType : 'json',
			success: function(data){
				if(data.company.length > 0){
					elm.parent().removeClass("has-error");
					$(".alert-codigo").hide();
					if(data.company[0].belongs_gis == 1){
						$(".nEmpleado").show(500);
					}else{
						$(".nEmpleado").hide();
					}
				}else{
					elm.parent().addClass("has-error");
					$(".alert-codigo").show();
				}
			}
		});
	});
	$(".form").submit(function(e){
		if($(".alert-codigo").is(":visible")){
			e.preventDefault();
			return false;
		}
		return true;		
	});
});
</script>
</body>
</html>