<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tienda Calorex</title>
	@include('ventadirecta.parts.head')
</head>
<body class="{{ isset($bodyclass) ? $bodyclass : '' }}" style="background-image: url({{ asset('assets/front/img/ventadirecta/bgVDinicio.png') }}); background-repeat: no-repeat;">
	
	<header>
		@include('ventadirecta.parts.header')
	</header>

	@yield('content')
	
	<footer>
		@include('ventadirecta.parts.footer')
	</footer>
</body>
</html>