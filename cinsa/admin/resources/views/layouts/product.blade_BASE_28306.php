<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tienda Calorex</title>
	@include('ventadirecta.parts.head')
</head>
<body class="template-product">
	
	<header>
		@include('ventadirecta.parts.header')
	</header>

	<section id="breadcrumb" class="hidden-xs">
		<div class="container">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<ol class="breadcrumb">
					{{-- <li>
						<a href="#">CALOREX</a>
					</li>
					<li>
						<a href="#">Línea residencial</a>
					</li>
					<li class="active">Calentador de Paso</li> --}}
				</ol>	
			</div>
		</div>
	</section>


	@yield('content')

	{{-- <section id="related-products">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3>Productos Relacionados</h3>
					<div class="row mobile-cycle">
						<div class="col-xs-12 col-sm-4">
							<div class="product">
								<img src="{{ asset('assets/front/img/cinsa/cinstantaneo.png') }}" height="351" width="302" alt="Instantáneo">
								<h4>Calentador Instantáneo</h4>
							</div>
						</div>

						<div class="col-xs-12 col-sm-4">
							<div class="product">
								<img src="{{ asset('assets/front/img/cinsa/cdepaso.png') }}" height="351" width="302" alt="De paso">
								<h4>Calentador de Paso</h4>
							</div>
						</div>

						<div class="col-xs-12 col-sm-4">
							<div class="product">
								<img src="{{ asset('assets/front/img/cinsa/cinstantaneo.png') }}" height="351" width="302" alt="Instantáneo">
								<h4>Calentador Eléctrico</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> --}}
	
	<footer>
		@include('ventadirecta.parts.footer')
	</footer>
</body>
</html>