<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tienda Calorex</title>
	@include('ventadirecta.parts.head')
</head>
<body class="template-cart">
	
	<header>
		@include('ventadirecta.parts.header')
	</header>

	@yield('content')
	
	<footer>
		@include('ventadirecta.parts.footer')
	</footer>
</body>
</html>