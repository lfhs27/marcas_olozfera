<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;

class Blueprint extends Model
{
	protected $fillable = ['product_id'];

    public function product()
    {
    	return $this->belongsTo('CMS\Product');
    }
}
