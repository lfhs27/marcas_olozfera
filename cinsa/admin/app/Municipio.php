<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipio extends Model
{
	
	public $table = "municipios";

    public function estado()
    {
    	return $this->belongsTo('CMS\Estado');
    }

    /*public function direcciones()
    {
    	return $this->hasMany('CMS\Address');
    }*/
}
