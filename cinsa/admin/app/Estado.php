<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estado extends Model
{
	
	public $table = "estados";

    public function municipios()
    {
    	return $this->hasMany('CMS\Municipio');
    }
}
