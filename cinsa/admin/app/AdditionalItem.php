<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;

class AdditionalItem extends Model
{
	public function image()
	{
		return $this->morphOne('CMS\Image','imageable');
	}

	public function products()
	{
		return $this->belongsToMany('CMS\Product');
	}

	public function prices()
	{
		return $this->hasMany('CMS\Price','InventoryItemId','inventory_item_id');
	}

	public function userPrice($user)
	{	
		$pricelistCode = "";

		if ( $user && $user->company && $user->company->pricelists ) {
			$pricelistCode = $user->company->pricelists()->where('business_unit_id',$this->business_unit_id)->first()->code;
		} else {
			$pricelistCode = "NCAL_EMPLEADOS_ONLINE";
		}

		$pricelistCode = "NCAL_EMPLEADOS_ONLINE";

		foreach ($this->prices as $price) {
			if ($price->priceListName == $pricelistCode) {
				return $price;
			}
		}
	}
}
