<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeaturedImage extends Model
{
	use SoftDeletes;

	protected $fillable = ['product_id'];

    public function product()
    {
    	return $this->belongsTo('CMS\Product');
    }

    public function image()
    {
    	return $this->morphOne('CMS\Image','imageable')->orderBy('created_at','DESC');
    }
}
