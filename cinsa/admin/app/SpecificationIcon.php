<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;

class SpecificationIcon extends Model
{
    public function product()
    {
    	return $this->belongsTo('CMS\Product');
    }

    public function image()
    {
    	return $this->morphOne('CMS\Image','imageable');
    }
}
