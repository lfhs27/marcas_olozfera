<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tip_tag extends Model
{
	
	public $table = "tips_tag";

    public function tip()
    {
    	return $this->belongsTo('CMS\Tip');
    }
}
