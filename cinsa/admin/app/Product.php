<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use CMS\Stock;

class Product extends Model
{
	use SoftDeletes;

	public $fillable = ['segment1','business_unit_id'];

	public function businessUnit()
	{
		return $this->belongsTo('CMS\BusinessUnit');
	}

	public function brand()
	{
		return $this->belongsTo('CMS\Brand');
	}

	public function family()
	{
		return $this->belongsTo('CMS\Family');
	}

	public function line()
	{
		return $this->belongsTo('CMS\Line');
	}

	public function products()
	{
		return $this->hasMany('CMS\Product');
	}

	public function author()
	{
		return $this->belongsTo('CMS\User','created_by');
	}

	public function children()
	{
		return $this->hasMany('CMS\Product','parent_id');
	}

	public function brand_icons()
	{
		return $this->hasMany('CMS\BrandIcon');
	}

	public function combos()
	{
		return $this->belongsToMany('CMS\Product', 'product_product', 'product_id', 'combo_id');
	}

	public function combo_products()
	{
		return $this->belongsToMany('CMS\Product', 'product_product', 'combo_id', 'product_id')->withPivot("cantidad");
	}

	public function parent()
	{
		return $this->belongsTo('CMS\Product','parent_id');
	}

	public function specifications()
	{
		return $this->hasMany('CMS\Specification');
	}

	public function downloads()
	{
		return $this->morphMany('CMS\Download','downloadable');
	}

	public function blueprint()
	{
		return $this->hasOne('CMS\Blueprint');
	}

	public function gallery()
	{
		return $this->hasOne('CMS\Gallery');
	}

	// public function price()
	// {
	// 	return $this->hasOne('CMS\Price','productAttrValDisp','segment1');
	// }

	public function prices()
	{
		return $this->hasMany('CMS\Price','productAttrValDisp','segment1');
	}

	public function userPrice($user)
	{	
		$pricelistCode = "";
		
		if(is_string($user) && strcmp($user, "refacciones") == 0){
			$pricelistCode = "NCAL_LP_CLIENTE FINAL_ONLINE";
		} else if(is_string($user) && strcmp($user, "distribuidor") == 0){
			$pricelistCode = "NCAL_LP_DISTRIBUIDORES_CSA_ONLINE";
		} else if ( $user && $user->company && $user->company->pricelists ) {
			$pricelistCode = $user->company->pricelists()->where('business_unit_id',$this->business_unit_id)->first();
			if ( $pricelistCode ) {
				$pricelistCode = $pricelistCode->code;
			} else {
				return false;
			}
		} else {
			$pricelistCode = "NCAL_EMPLEADOS_ONLINE";
		}

		foreach ($this->prices as $price) {
			if ($price->priceListName == $pricelistCode) {
				return $price;
			}
		}
	}

	public function pricelistPrice($pricelistId)
	{
		
	}

	public function getSiblingProductsAttribute($value)
	{
		if (!empty($value)) {
			$prodsArray = json_decode($value);
			$newValue = [];
			foreach ($prodsArray as $prodId) {
				$currentProd = Product::find($prodId);

				if ($currentProd && $currentProd->is_visible == 1) {
					$newValue[] = Product::find($prodId);
				}
			}
			return $newValue;
		} else {
			return $value;
		}
	}

	public function pOption()
	{
		if ($this->product_option_id > 0) {
			$productOption = Product::find($this->product_option_id);

			return $productOption;	
		}
	}

	// public function getOpcionesGasEnergiaAttribute($value)
	// {
		
	// }

	public function featuredImage()
	{
		return $this->hasOne('CMS\FeaturedImage');
	}

	public function stock()
	{
		return $this->hasMany('CMS\Stock','segment1','segment1');
	}

	public function cedisStock($cedisId)
	{
		$stock = Stock::where(['segment1' => $this->segment1, 'center_id' => $cedisId])->first();

		if ($stock) {
			return $stock->disponible;	
		} else {
			return false;
		}
	}
	public function ratings()
	{
		return $this->hasMany('CMS\Rating');
	}

	public function additionalItems()
	{
		return $this->belongsToMany('CMS\AdditionalItem');
	}

	public function specificationIcons()
	{
		return $this->hasMany('CMS\SpecificationIcon');
	}
}
