<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
	
	public $table = "Addresses";

    public function municipio()
    {
    	return $this->hasMany('CMS\Municipio');
    }
}
