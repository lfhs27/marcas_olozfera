<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
	use SoftDeletes;

	public $fillable = ['name','business_unit_id'];

	public function businessUnit()
	{
		return $this->belongsTo('CMS\BusinessUnit');
	}

	public function products()
	{
		return $this->hasMany('CMS\Product')->where(['is_visible' => 1]);
	}

	public function author()
	{
		return $this->belongsTo('CMS\User','created_by');
	}

	public function lines()
	{
		return $this->hasMany('CMS\Line')->orderBy("order", "asc");
	}

	public function families()
	{
		//return $this->hasMany('CMS\Family')->where('name','!=','REFACCIONES')->orderBy("order", "asc");
		return $this->hasMany('CMS\Family')->orderBy("order", "asc");
	}
}
