<?php

namespace CMS\Http\Controllers\Admin;

use Gate;

use Illuminate\Http\Request;

use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;
use CMS\Specification;

class SpecificationsController extends Controller
{
    public function postQuickEdit(Request $request)
    {

    	$specification = Specification::find($request->get('id'));

    	if ($request->has('action') && $request->get('action') == 'delete') {
    		$specification->delete();
    		return response()->json(['status' => 200,'message'=>'Especificación eliminada correctamente']);
    	}

    	if ($specification) {
	    	if ($request->has('name')) {
	    		$specification->name = $request->get('name');	
	    	}

	    	if ($request->has('value')) {
				$specification->value = $request->get('value');    		
	    	}

	    	$specification->save();

	    	return response()->json(['status' => 200, 'message' => 'Actualizado correctamente.']);
	    } else {
	    	return response()->json(['status' => 200, 'message' => 'No se realizó ninguna acción.']);
	    }
    }
}
