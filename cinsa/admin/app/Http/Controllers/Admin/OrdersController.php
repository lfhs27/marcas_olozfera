<?php

namespace GIS\Http\Controllers\Admin;

use Illuminate\Http\Request;

use GIS\Http\Requests;
use GIS\Http\Controllers\Controller;
use GIS\Order;
use Mail;
use GIS\LoyaltyPoints;

class OrdersController extends Controller
{
	protected $user;

	public function __construct()
	{
		$this->user = \Auth::user();

		setlocale(LC_MONETARY, 'en_US');
		
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$orders = Order::all();



		return view('admin.orders.index')
			->with('orders',$orders)
			->with('user',$this->user);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$order = Order::find($id);

		$order->load(['items']);

		if(!empty($order->user_detail)) {
			$orderUserDetail = json_decode($order->user_detail);
		} else {
			$orderUserDetail = false;
		}

		return view('admin.orders.show')
			->with('order',$order)
			->with('orderUserDetail',$orderUserDetail)
			->with('user',$this->user);
	}

	public function showPrint($id)
	{
		$order = Order::find($id);

		$order->load(['items']);

		return view('admin.orders.print')
			->with('order',$order);
	}


	public function postChangeStatus(Request $request)
	{
		$orderId = $request->input('orderId');
		$newStatus = $request->input('status');
		$fieldname = $request->input('fieldname');

		$order = Order::find($orderId);

		$sendmail = false;

		if ($fieldname == 'authorization_status') {
			$order->authorization_status = $newStatus;
			$sendmail = true;
		}

		if ($fieldname == 'delivery_status') {
			$order->delivery_status = $newStatus;
			$sendmail = false;
		}

		if ($newStatus == 1 && $sendmail == true) {
			$this->sendMailConfirm($order);
		}

		if ($newStatus == 1) {
			$LoyaltyPoints = new LoyaltyPoints;
			$LoyaltyPoints->user_id = $order->user_id;
			$LoyaltyPoints->points = floor($order->amount * .05);
			$LoyaltyPoints->save();
		}

		$order->created_at = date("Y-m-d H:i:s");

		$order->save();

		return '1';
	}

	public function sendMailConfirm($order)
	{
		if (!empty($order->user->email)) {
			$mailTo = $order->user->email;
		} else {
			$mailTo = 'salvador.recio@gis.com.mx';
		}

		Mail::send(array('html' => 'mails.confirmacion'), ['order'=>$order], function($message) use ($order,$mailTo) {
			$message->to($mailTo, $order->user->name)
						->subject('Confirmación de Compra')
						->bcc(array(
							'daniela.fuentes@gis.com.mx',
							'norma.valdes@gis.com.mx',
							'ruben.valdez@gis.com.mx',
							'luis.marente@gis.com.mx',
							'luis.soriano@gis.com.mx',
							'yessica.sandoval@gis.com.mx',
							'erika.garcia@gis.com.mx',
							'uben.valdez@gis.com.mx',
							'alejandra.rodriguez@gis.com.mx',
							'miguel.labastida@gis.com.mx',
							'jose.lopezl@gis.com.mx',
							'erick.perez@gis.com.mx',
							'alejandra.rodriguez@gis.com.mx',
							'miguel.labastida@gis.com.mx',
							'diego.chapa@gis.com.mx',
							'salvador.recio@gis.com.mx',
							'irene.torres@gis.com.mx',
							'steven@softdepot.mx',
							));
		});
	}

}
