<?php

namespace CMS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use Input;
use Redirect;
use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;
use CMS\Estado;
use CMS\Municipio;
use CMS\Address;

class AddressesController extends Controller
{
	protected $user;

	public function __construct()
	{
		$this->user = \Auth::user();
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$estados = Estado::all();
		
		return view('admin.addresses.index')
				->with('estados',$estados)
				->with('user',$this->user);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$address = null;
		if($request->get("id") == 0)
			$address = new Address;
		else
			$address = Address::find($request->get("id"));
		$address->nombre_comercial = $request->get("nombre_comercial");
		$address->razon_social = $request->get("razon_social");
		$address->sucursal = $request->get("sucursal");
		$address->direccion = $request->get("direccion").", ".$request->get("colonia");
		$address->municipio_id = $request->get("municipio");
		$address->telefono = $request->get("telefono");
		$address->codigo_postal = $request->get("codigo_postal");
		$address->calorex = 1;
		$address->save();
		
		return response()->json(["status" => 200]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$address = Address::find($id);
		$address->delete();
		
		return response()->json("200");
	}
	
	public function csv(Request $request){
		if (Input::hasFile('file')){
			$file = Input::file('file');
			$name = time() . '-' . $file->getClientOriginalName();

			$storage = ''; 
			$path = 'uploads/CSV';

			// Moves file to folder on server
			$file->move($path, $name);
			$file_handle = fopen($path."/".$name, "r");

			while (!feof($file_handle) ) {
				$line = fgetcsv($file_handle, 1024, ";");
				if($line){
					$address = new Address;
					$address->nombre_comercial = utf8_encode($line[0]);
					$address->razon_social = utf8_encode($line[1]);
					$address->sucursal = utf8_encode($line[2]);
					$address->direccion = utf8_encode($line[3].", ".$line[4]);
					$address->telefono = isset($line[8]) ? utf8_encode($line[8]) : "";
					$address->codigo_postal = utf8_encode($line[5]);
					$address->calorex = 1;
					
					$municipio = DB::table('municipios')
							->join('estados', 'estados.id', '=', 'municipios.estado_id')
							->select('municipios.*')
							->where('estados.name', 'like', '%'.ucwords(strtolower(trim(utf8_encode($line[7])))).'%')
							->where('municipios.municipio', 'like', '%'.ucwords(strtolower(trim(utf8_encode($line[6])))).'%')
							->first();  
					/*echo ucwords(strtolower(trim(utf8_encode($line[7]))))."  --  ".ucwords(strtolower(trim(utf8_encode($line[6]))));
					var_dump($municipio);
					echo "<br />";*/
					if($municipio){
						$address->municipio_id = $municipio->id;
						$address->save();
					}
				}
			}
 
			fclose($file_handle);
		}
		
		$estados = Estado::all();
		
		return Redirect::to('/addresses');
	}
	
	public function getMunicipios($id){
		$municipios = Municipio::where(["estado_id" => $id])->get();
		
		return response()->json(['municipios' => $municipios]);
	}
	
	public function getAllAddresses(){
		$addresses = Address::orderBy("order")->get();
		
		$arr = array();
		foreach($addresses as $address){
			$address->municipio = Municipio::find($address->municipio_id);
			$address->municipio->estado = Estado::find($address->municipio->estado_id);
			array_push($arr, $address);
		}
		
		return response()->json($arr);
	}
	
	public function getAddress($id){
		$address = Address::find($id);
		$address->municipio = Municipio::find($address->municipio_id);
		
		return response()->json($address);
	}
	
	public function getStatesWS(){
		$states = DB::table("Addresses")
					->select("estados.*")
					->join("municipios", "Addresses.municipio_id", "=", "municipios.id")
					->join("estados", "municipios.estado_id", "=", "estados.id")
					->groupBy('estado_id')
					->get();
					
		echo json_encode($states);
	}
	
	public function getMunicipiosWS($estado){
		$municipios = DB::table("Addresses")
					->select("municipios.*")
					->join("municipios", "Addresses.municipio_id", "=", "municipios.id")
					->join("estados", "municipios.estado_id", "=", "estados.id")
					->where("estados.id", $estado)
					->groupBy('municipio_id')
					->get();
					
		echo json_encode($municipios);
	}
	
	public function getAllAddressesEstado($estado){
		$addresses = Address::orderBy("order")->get();
		
		$arr = array();
		foreach($addresses as $address){
			$address->municipio = Municipio::find($address->municipio_id);
			$address->municipio->estado = Estado::find($address->municipio->estado_id);
			if($address->municipio->estado_id == $estado){
				array_push($arr, $address);
			}
		}
		
		return response()->json($arr);
	}
	
	public function getAllAddressesMunicipio($municipio){
		$addresses = Address::orderBy("order")->get();
		
		$arr = array();
		foreach($addresses as $address){
			$address->municipio = Municipio::find($address->municipio_id);
			$address->municipio->estado = Estado::find($address->municipio->estado_id);
			if($address->municipio_id == $municipio){
				array_push($arr, $address);
			}
		}
		
		return response()->json($arr);
	}
	
    public function updateOrder(Request $request)
    {
        if ($request->has('addresses_order')) {
            $lines = json_decode($request->input("addresses_order"));
			
			if(COUNT($lines)){
				foreach($lines as $idLine){
					$line = Address::find($idLine->id);
					$line->order = $idLine->order;
					$line->save();
				}
			}
        }

        return redirect()->back();
    }
}
