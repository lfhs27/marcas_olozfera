<?php

namespace CMS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use File;
use Input;
use Validator;
use Redirect;
use Session;
use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;
use CMS\Tip;
use CMS\Tip_tag;
use CMS\Tip_metadata;

class TipsController extends Controller
{
	protected $user;

	public function __construct()
	{
		$this->user = \Auth::user();
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{		
		return view('admin.tips.index')
				->with('user',$this->user);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (Input::hasFile('image')){
			// getting all of the post data
			$file = array('image' => Input::file('image'));
			// setting up rules
			$rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
			// doing the validation, passing post data, rules and the messages
			$validator = Validator::make($file, $rules);
			if ($validator->fails()) {
				// send back to the page with the input data and errors
				return Redirect::to('tips')->withInput()->withErrors($validator);
			} else {
				// checking file is valid.
				if (Input::file('image')->isValid()) {
					$destinationPath = 'uploads/tips'; // upload path
					$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
					$fileName = rand(11111,99999).'.'.$extension; // renameing image
					Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
					// sending back with message
					Session::flash('success', 'El Banner ha sido guardado'); 
				}  else {
					// sending back with error message.
					Session::flash('error', 'uploaded file is not valid');
					return Redirect::to('tips');
				}
			}
		}
		$tip = null;
		if($request->get("id") == 0)
			$tip = new Tip;
		else
			$tip = Tip::find($request->get("id"));
		$tip->title = $request->get("title");		$tip->slug = str_slug($request->get("title"));
		if(isset($fileName))
			$tip->image = $destinationPath."/".$fileName;
		$tip->intro = $request->get("intro");
		$tip->text = $request->get("text");
		$tip->save();
		
		//TAGS
		foreach($tip->tags as $tag){
			$tag->delete();
		}
		$tags = explode(",", $request->get("tags"));
		foreach($tags as $tag){
			$tip_tag = new Tip_tag;
			$tip_tag->tip_id = $tip->id;
			$tip_tag->tag = $tag;
			$tip_tag->save();
		}
		
		//METADATA
		foreach($tip->metadata as $md){
			$md->delete();
		}
		$metadata = explode(",", $request->get("metadata"));
		foreach($metadata as $md){
			$tip_metadata = new Tip_metadata;
			$tip_metadata->tip_id = $tip->id;
			$tip_metadata->data = $md;
			$tip_metadata->save();
		}
		
		return Redirect::to('tips');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$tip = Tip::find($id);
		
		return view('admin.tips.edit')
				->with('tip',$tip)
				->with('user',$this->user);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$tip = Tip::find($id);
		$tip->delete();
		
		return response()->json("200");
	}
	
	public function imageList() {
        $imgList = array();
        $files = File::allfiles(public_path().'/uploads/tips');
        foreach ($files as $file) {
            array_push($imgList, array("title"=>str_replace( './', '',$file), "value"=>'img/'.str_replace( './', '',$file)));
        }
        return json_encode($imgList);
    } 

	public function upload(){
		$valid_exts = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
		$max_size = 2000 * 1024; // max file size (200kb)
		$path = public_path() . '/uploads/tips/'; // upload directory
		$fileName = NULL;
		$file = Input::file('image');
		// get uploaded file extension
		//$ext = $file['extension'];
		$ext = $file->guessClientExtension();
		// get size
		$size = $file->getClientSize();
		// looking for format and size validity
		$name = $file->getClientOriginalName();
		if (in_array($ext, $valid_exts) AND $size < $max_size)
		{
			// move uploaded file from temp to uploads directory
			if ($file->move($path,$name))
			{
				$status = 'Image successfully uploaded!';
				$fileName = $name;
			}
			else {
				$status = 'Upload Fail: Unknown error occurred!';
			}
		}
		else {

			$status = 'Upload Fail: Unsupported file format or It is too large to upload!';
		}

		//echo out script that TinyMCE can handle and update the image in the editor
		/*
		return ("
		// <![CDATA[
		$('.mce-btn.mce-open').parent().find('.mce-textbox').val('/img/".$fileName."').closest('.mce-window').find('.mce-primary').click();
		// ]]>
		"); 
		*/
		return ("<input type='hidden' id='fileName' value='".$fileName."' />");
	}
	
	function getAllTips(){
		$tips = Tip::orderBy('created_at', 'DESC')->get();
		
		echo json_encode($tips);
	}
	
	function searchTips($key){
		$key = urldecode($key); 
		$tips_arr = array();
		if(strcmp($key, '0') == 0){
			$tips = Tip::orderBy('created_at', 'DESC')->get();
		}else{
			$tags = explode(" ", $key);
			//DB::enableQueryLog();
			$tips = DB::table('tips')
					->select('tips.*')
					->leftJoin('tips_tag', 'tips.id', '=', 'tips_tag.tip_id')
					->where('title', 'like', '%'.$key.'%')
					->orwhere('intro', 'like', '%'.$key.'%')
					->orwhere('text', 'like', '%'.$key.'%')
					->whereIn('tips_tag.tag', $tags, 'or')
					->groupBy('tips.id')
					->get();
			//print_r(DB::getQueryLog());
		}
		
		foreach($tips as $tip){
			$t = Tip::find($tip->id);
			//$t->load(["tip.tags"]);
			array_push($tips_arr, $t); 
		}
		
		echo json_encode($tips_arr);
	}
	
	function getTip($id){
		$id = urldecode($id);
		$tips = Tip::where(["slug" => $id])->first();
		$tips->load("tags");
		$tips->load("metadata");
		echo json_encode($tips);
	}
	
	function getAllTags(){
		$tags = Tip_tag::distinct()->select('tag')->limit(20)->get();
		
		echo json_encode($tags);
	}
}
