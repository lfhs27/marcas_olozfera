<?php

namespace CMS\Http\Controllers\Admin;

use Session;
use Gate;

use Illuminate\Http\Request;

use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;
use CMS\Brand;

class BrandsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$brands = Brand::all();
		return view('admin.brands.index')
			->with('brands',$brands);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if ($request->exists('name')) {
			$brand = new Brand;
			$brand->name = $request->input('name');
			$brand->front_name = $request->exists('front_name') ? $request->input('front_name') : $request->input('name');
			$brand->slug = str_slug($request->input('name'));
			$brand->business_unit_id = Session::get('UsingBusinessUnit');
			$brand->save();
		}

		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$brand = Brand::find($id);
		return view('admin.brands.edit')
			->with('brand',$brand);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$brand = Brand::find($id);

		if ($brand) {
			if ($request->exists('name')) {
				$brand->front_name = $request->input('name');
				$brand->save();
			}
		}
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$brand = Brand::find($id);

		if ($brand) {
			$brand->delete();
		}

		return redirect()->back();
	}
}
