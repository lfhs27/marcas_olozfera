<?php

namespace CMS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;
Use CMS\User;
Use CMS\Banner;

class BackgroundsController extends Controller
{
	protected $user;

	public function __construct()
	{
		$this->user = \Auth::user();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$backgrounds = array(
			"1" => array(
							"type" => "bg-deposito",
							"src" => Banner::where(["type" => "bg-deposito"])->first(),
							"titulo" => "Deposito"
						),
			"2" => array(
							"type" => "bg-calefactores",
							"src" => Banner::where(["type" => "bg-calefactores"])->first(),
							"titulo" => "Calefactores"
						),
			"3" => array(
							"type" => "bg-paso",
							"src" => Banner::where(["type" => "bg-paso"])->first(),
							"titulo" => "De Paso"
						),
			"4" => array(
							"type" => "bg-paso-evo",
							"src" => Banner::where(["type" => "bg-paso-evo"])->first(),
							"titulo" => "De Paso Evolution"
						)
		);
		return view('admin.backgrounds.index')
				->with('user',$this->user)
				->with('backgrounds',$backgrounds);
	}
	
	public function index_backgrounds(){
		$backgrounds = Banner::where(["type" => "index-bg"])->get();
		
		return view('admin.dashboard.backgrounds')
				->with('user',$this->user)
				->with('backgrounds',$backgrounds);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function getBackground($type){
		$bg = Banner::where(["type" => $type])->first();
		
		echo json_encode($bg);
	}
	
	public function getAllIndexBackgrounds(){
		$bg = Banner::where(["type" => "index-bg"])->get();
		
		echo json_encode($bg);
	}
}