<?php

namespace CMS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;
Use CMS\User;
Use CMS\Banner;

class DashboardController extends Controller
{
	protected $user;

	public function __construct()
	{
		$this->user = \Auth::user();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$slides = Banner::where(["type" => "slide"])->get();
		return view('admin.dashboard.index')
				->with('user',$this->user)
				->with('slides',$slides);
	}
	public function productos()
	{
		$productos = Banner::where(["type" => "producto"])->get();
		return view('admin.dashboard.productos')
				->with('user',$this->user)
				->with('productos',$productos);
	}
	public function indexBanners()
	{
		$banners = Banner::where(["type" => "index-banner"])->get();
		return view('admin.dashboard.banners')
				->with('user',$this->user)
				->with('banners',$banners);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function getAllSlides(){
		$slides = Banner::where(["type" => "slide"])->get();
		
		echo json_encode($slides);
	}
	
	public function getAllProducts(){
		$products = Banner::where(["type" => "producto"])->get();
		
		echo json_encode($products);
	}
	
	public function getAllIndexBanners(){
		$banners = Banner::where(["type" => "index-banner"])->get();
		
		echo json_encode($banners);
	}
}