<?php

namespace CMS\Http\Controllers\Admin;

use Session;
use Storage;
use File;
use Excel;
use View;
use Gate;

use Illuminate\Http\Request;

use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;
use CMS\BusinessUnit;
use CMS\Product;
use CMS\BrandIcon;
use CMS\Brand;
use CMS\Line;
use CMS\Family;
use CMS\Specification;
use CMS\Blueprint;
use CMS\Gallery;
use CMS\Image;
use CMS\FeaturedImage;
use CMS\SpecificationIcon;
use CMS\AdditionalItem;

class ProductsController extends Controller
{
	public function __construct()
	{
		View::share('viewScripts',['tiendagis.products.js']);

		$this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$bUnit = BusinessUnit::find(Session::get('UsingBusinessUnit'));
		//$familiesQ = Family::where(['business_unit_id' => Session::get('UsingBusinessUnit')])->get();
		//$linesQ = Line::where(['business_unit_id' => Session::get('UsingBusinessUnit')])->get();
		$familiesQ = Family::all();
		$linesQ = Line::all();
		$brandsQ = Brand::all();
		$productsQ = Product::all();

		$families = array();
		$lines = array();

		foreach ($familiesQ as $family) {
			$families[$family->id] = $family->brand ? $family->brand->name . ' > ' . $family->front_name : $family->front_name;
		}

		foreach ($linesQ as $line) {
			$lines[$line->id] = $line->brand ? $line->brand->name . ' > ' . $line->front_name : $line->front_name;
		}

		asort($families);
		asort($lines);

		return view('admin.products.index')
			->with('adminSectionTitle','Productos en Inventario')
			->with('families',$families)
			->with('lines',$lines)
			->with('brands',$brandsQ)
			->with('products',$productsQ);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$additionalItems = AdditionalItem::all();

		return view('admin.products.create')
			->with('adminSectionTitle','Crear Producto')
			->with('additionalItems',$additionalItems);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$product = new Product;
		$product->name = $request->input('name');
		$product->isCombo = $request->input('isCombo');
		$product->brand_id = $request->input('brand');
		$product->family_id = $request->input('family');
		$product->line_id = $request->input('line');
		$product->business_unit_id = Session::get('UsingBusinessUnit');
		$product->slug = str_slug($request->input('name'));
		$product->segment1 = $request->input('segment1');
		$product->save();
		return redirect('/admin/products/');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$familiesQ = Family::all();
		$linesQ = Line::all();
		$brandsQ = Brand::all();
		
		$product = Product::find($id);
		$products = Product::all();
		$productSiblingOptions = Product::where('line_id',$product->line_id)->get();
		
		$siblings = array();
		$productSiblingProducts = json_decode($product->getOriginal('sibling_products')); 
		if(COUNT($productSiblingProducts)){
			foreach($productSiblingProducts as $sib){
				array_push($siblings, Product::find($sib));
			}
		}

		$additionalItems = AdditionalItem::all();

		return view('admin.products.edit')
			->with('adminSectionTitle','Editando ' . $product->name)
			->with('product',$product)
			->with('productSiblingOptions',$productSiblingOptions)
			->with('siblings',$siblings)
			->with('all_products',$products)
			->with('families',$familiesQ)
			->with('lines',$linesQ)
			->with('brands',$brandsQ)
			->with('additionalItems',$additionalItems);
	}

	public function addAdditionalItem(Request $request)
	{
		$productId = $request->input('product');
		$itemId = $request->input('item');

		$product = Product::find($productId);
		$additionalItem = AdditionalItem::find($itemId);

		$product->additionalItems()->attach($itemId);

		return response()->json([
			'status'	=> 'Item agregado correctamente.',
			'code'		=> 200,
			'item'		=> $additionalItem
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$product = Product::find($id);

		if ($product) {
			if ($request->has('name')) {
				$product->name = $request->get('name');

				if (empty($product->slug)) {
					$product->slug = str_slug($product->name);
				}
			}

			if ($request->hasFile('specIcon_icon')) {
				$destPath = 'uploads/products/specicons/' + $product->id;

				$prodSpecIcon = new SpecificationIcon;
				$prodSpecIcon->name = $request->input('specIcon_name');
				$prodSpecIcon->description = $request->input('specIcon_description');
				$prodSpecIcon->product_id = $product->id;
				$prodSpecIcon->save();

				$uploadedFile = $request->file('specIcon_icon');
				$filename = $uploadedFile->getClientOriginalName();

				$fileName = $uploadedFile->getClientOriginalName();
				$fileExt = $uploadedFile->getClientOriginalExtension();
				$destinationName = time() . $fileName;
				
				$uploadedFile->move($destPath,$destinationName);

				list($width, $height) = getimagesize($destPath . '/' . $destinationName);

				$specIconImage = new Image;
				$specIconImage->name = $destinationName;
				$specIconImage->filename = $destinationName;
				$specIconImage->filepath = $destPath;
				$specIconImage->description = $destinationName;
				$specIconImage->alt = $destinationName;
				$specIconImage->width = $width;
				$specIconImage->height = $height;
				$specIconImage->extension = $fileExt;
				$specIconImage->created_by = \Auth::user()->id;
				$specIconImage->imageable_type = 'CMS\SpecificationIcon';
				$specIconImage->imageable_id = $prodSpecIcon->id;
				$specIconImage->mimetype = $uploadedFile->getClientMimeType();
				$specIconImage->size = $uploadedFile->getClientSize();

				$specIconImage->save();
			}

			if ($request->exists('deleteSpecIcon')) {
				$specIconDel = SpecificationIcon::find($request->input('deleteSpecIcon'));
				if ($specIconDel) {
					$specIconDel->delete();
				}
			}

			if ($request->exists('safetyFactor')) {
				$product->safety_factor = $request->input('safetyFactor');
			}

			if ($request->exists('removeAdditionalItem')) {
				$product->additionalItems()->detach($request->input('removeAdditionalItem'));
			}

			if ($request->exists('isVisible')) {
				$product->is_visible = $request->input('isVisible');
			}

			if ($request->exists('isCombo')) {
				$product->isCombo = $request->input('isCombo');
			}
			//UPDATE COMBO PRODUCTS
			if ($request->exists('combo_update')) {
				$product->combo_products()->detach();
				$cantidades = $request->input('cantidad_sku_combo');
				foreach($request->input('sku_combo') as $i => $comboElement){
					if($comboElement > 0){
						$comboProd = Product::find($comboElement);
						$comboProd->combos()->attach($product->id, ['cantidad' => $cantidades[$i]]);
					}
				}
			}
			
			if ( $request->has('video') ) {
				$url = $request->input('video');
				$parts = parse_url($url);
				parse_str($parts['query'], $query);
				
				$product->video = $query["v"];
			}

			if ($request->exists('segment1')) {
				$product->segment1 = $request->input('segment1');
			}

			if ($request->exists('inventory_item_id')) {
				$product->inventory_item_id = $request->input('inventory_item_id');
			}

			if ($request->exists('featuredImage')) {
				$featuredImage = FeaturedImage::firstOrCreate(['product_id' => $product->id]);
				$selectedImageId = $request->input('featuredImage');

				$selectedImage = Image::find($selectedImageId);

				$newImageRecord = $selectedImage->replicate();
				$newImageRecord->imageable_type = 'CMS\FeaturedImage';
				$newImageRecord->imageable_id = $featuredImage->id;
				$newImageRecord->save();
			}

			if ($request->exists('family_id')) {
				$product->family_id = $request->input('family_id');
			}

			if ($request->exists('line_id')) {
				$product->line_id = $request->input('line_id');
			}

			if ($request->exists('allowed_shipping_methods')) {
				$product->allowed_shipping_methods = json_encode($request->input('allowed_shipping_methods'));
			}
			
			if ($request->exists('servicios_lavabos')) {
				$product->num_lavabos = $request->input('servicios_lavabos');
			}

			if ($request->exists('servicios_regaderas')) {
				$product->num_regaderas = $request->input('servicios_regaderas');
			}

			if ($request->exists('servicios_tinaestandar')) {
				$product->num_tina = $request->input('servicios_tinaestandar');
			}

			if ($request->has('description')) {
				$product->description = $request->get('description');
			}

			if ($request->has('short_name')) {
				$product->short_name = $request->get('short_name');
			}

			if ($request->exists('fake_price')) {
				$product->fake_price = $request->input('fake_price');
			}

			if ($request->exists('siblings')) {
				/*$siblings = $request->input('siblings');
				$siblingsArr = array();
				if(COUNT($siblings)){
					foreach($siblings as $sib){
						$siblingObj = new \stdClass();
						$siblingObj->id = $sib;
						$siblingObj->order = 1;
						array_push($siblingsArr, $siblingObj);
					}
				}*/
				$product->sibling_products = json_encode($request->input('siblings'));
			}

			if ($request->has('sibling_products')) {
				$product->sibling_products = $request->input('sibling_products');
			}

			if ($request->has('disallowed_zipcodes')) {
				$product->disallowed_zipcodes = $request->input('disallowed_zipcodes');
			}
			
			//WIZARD
			if ($request->has('tipo_energia_w')) {
				$product->tipo_energia_w = $request->get('tipo_energia_w');
			}

			if ($request->has('n_regaderas_min') && $request->has('n_regaderas')) {
				$n = array();
				for($i = $request->get('n_regaderas_min'); $i <= $request->get('n_regaderas'); $i++){
					array_push($n, $i);
				}
				$product->n_regaderas = IMPLODE('_', $n);
			}

			if ($request->has('tipo_ducha')) {
				$product->tipo_ducha = $request->get('tipo_ducha');
			}

			if ($request->has('n_personas_min') && $request->has('n_personas')) {
				$n = array();
				for($i = $request->get('n_personas_min'); $i <= $request->get('n_personas'); $i++){
					array_push($n, $i);
				}
				$product->n_personas = IMPLODE('_', $n);
			}

			if ($request->has('tipo_domicilio')) {
				$product->tipo_domicilio = $request->get('tipo_domicilio');
			}

			if ($request->has('tipo_domicilio_a')) {
				$product->tipo_domicilio_a = $request->get('tipo_domicilio_a');
			}

			if ($request->has('flujo_agua')) {
				$product->flujo_agua = $request->get('flujo_agua');
			}

			if ($request->has('n_servicios')) {
				$product->n_servicios = $request->get('n_servicios');
			}

			if ($request->exists('seleccionador_label')) {
				$product->seleccionador_label = $request->get('seleccionador_label');
			}

			if ($request->exists('liverpool_url')) {
				$product->liverpool_url = $request->get('liverpool_url');
			}

			if ($request->exists('liverpool_url_alt')) {
				$product->liverpool_url_alt = $request->get('liverpool_url_alt');
			}
			
			if ($request->has('parent_id')) {
				$product->parent_id = $request->get('parent_id');
			}

			if ($request->has('brand_id')) {
				$product->brand_id = $request->get('brand_id');
			}

			if ($request->has('descriptionHtml')) {
				$product->description_html = $request->get('descriptionHtml');
			}

			if ($request->exists('addRecProd')) {
				$prodsRec = json_decode($product->prods_rec,TRUE);

				$prodsRec[] = [
					'id'	=> $request->input('prodId'),
					'name'	=> $request->input('prodName')
				];

				$product->prods_rec = json_encode($prodsRec);
			}

			if ($request->hasFile('disallowed_zipcodes_list')) {
				$excelFile = $request->file('disallowed_zipcodes_list');
				$filePath = 'import/products/';
				$timestamp = time();
				$filename = $timestamp . '-' . $excelFile->getClientOriginalName();
				$extension = $excelFile->getClientOriginalExtension();

				$fileFullPath = $filePath . $timestamp . '-' . $filename . '.' . $extension;

				Storage::put($fileFullPath,File::get($excelFile));

				Excel::load('storage/app/'.$fileFullPath,function($reader) use ($product){
					$rows = $reader->all();
					$zips = array();
					foreach ($rows as $row) {
						if(isset($row->codigos_postales)) {
							array_push($zips, TRIM($row->codigos_postales));						
						}						
					}
					$zips = array_slice($zips, 0, 8000);
					$product->disallowed_zipcodes = json_encode($zips);
					$product->save();
				});
			}
			
			if ($request->hasFile('specsFileExcel')) {
				$excelFile = $request->file('specsFileExcel');
				$filePath = 'import/specifications/';
				$timestamp = time();
				$filename = $timestamp . '-' . $excelFile->getClientOriginalName();
				$extension = $excelFile->getClientOriginalExtension();

				$fileFullPath = $filePath . $timestamp . '-' . $filename . '.' . $extension;

				Storage::put($fileFullPath,File::get($excelFile));

				Excel::load('storage/app/'.$fileFullPath,function($reader) use($product) {
				   $rows = $reader->all();

				    foreach ($rows as $row) {
				    	$spec = new Specification;
				    	$spec->product_id = $product->id;
				    	$spec->name = $row->nombre;
				    	$spec->value = $row->valor;
				    	$spec->save();
				    }
				});
			}

			if ($request->hasFile('blueprint')) {
				$blueprintFile = $request->file('blueprint');
				$bpFilename = $blueprintFile->getClientOriginalName();
				$bpPath = 'uploads/products/blueprints';
				$bpFullPath = $bpPath . '/' . $bpFilename;

				$blueprintFile->move($bpPath,$bpFilename);

				$blueprint = Blueprint::firstOrNew(['product_id' => $product->id]);
				$blueprint->name = $bpFilename;
				$blueprint->filename = $bpFilename;
				$blueprint->filePath = $bpPath;
				$blueprint->product_id = $product->id;
				$blueprint->created_by = \Auth::user()->id;
				$blueprint->save();
			}
			
			if ($request->exists('brand_icon_id')) {
				$ics = $request->input('brand_icon_id');
				$descriptions = $request->input('brand_icon_description');
				$files = $request->file('brand_icon');
				foreach($ics as $i => $ic){
					$brand_icon = null;
					if($ic == 0)
						$brand_icon = new BrandIcon;
					else
						$brand_icon = BrandIcon::find($ic);
						
					if($request->hasFile('brand_icon.'.$i)){
						$iconFile = $files[$i];
						$icFilename = $iconFile->getClientOriginalName();
						$icPath = 'uploads/products/brand_icons';
						$icFullPath = $icPath . '/' . $icFilename;

						$iconFile->move($icPath,$icFilename);

						$brand_icon->icon = $icFullPath;
					}
					$brand_icon->description = $descriptions[$i];
					$brand_icon->product_id = $product->id;
					$brand_icon->save();
				}
			}

			/*if ($request->hasFile('brand_icon')) {
				$iconFile = $request->file('brand_icon');
				$icFilename = $iconFile->getClientOriginalName();
				$icPath = 'uploads/products/brand_icons';
				$icFullPath = $icPath . '/' . $icFilename;

				$iconFile->move($icPath,$icFilename);

				$product->brand_icon = $icFullPath;
			}

			if ($request->has('brand_icon_description')) {
				$product->brand_icon_description = $request->get('brand_icon_description');
			}*/

			if ($request->exists('product_option_id')) {
				$productOptions = $request->input('product_option_id');

				$product->product_option_id = $productOptions;
			}

			if ($request->exists('product_option_title')) {
				$productOptionsTitle = $request->input('product_option_title');
				$product->product_option_title = $productOptionsTitle;
			}

			if ($request->hasFile('explosionado_image')) {
				$explosionadoFile = $request->file('explosionado_image');
				$exFilename = $explosionadoFile->getClientOriginalName();
				$exPath = 'uploads/products/explosionados';
				$exFullPath = $exPath . '/' . $exFilename;

				$explosionadoFile->move($exPath,$exFilename);

				$product->explosionado_image = $exFullPath;
			}

			if ($request->exists('explosionado_code')) {
				$product->explosionado_code = $request->input('explosionado_code');
			}

			$product->save();

			$request->session()->flash('notification', 'Información actualizada correctamente.');
			$request->session()->flash('type', 'success');

			return redirect()->back();
		} else {
			return redirect()->action('Admin\ProductsController@index');
		}
	}

	public function postDeletePhoto(Request $request)
	{
		$photoName = $request->input('photoName');

		$photo = Image::where('name',$photoName)->first();

		if ($photo) {
			$photo->delete();

			return response()->json(['status' => 200,'message' => 'Imagen eliminada']);
		} else {
			return response()->json(['status' => 200,'message' => 'No se encontró imagen a eliminar']);
		}
	}

	public function postPhoto(Request $request)
	{
		if ($request->hasFile('file') && $request->exists('productId')) {
			$product = Product::find($request->input('productId'));
			$gallery = Gallery::firstOrNew(['product_id' => $product->id]);
			$gallery->name = 'Galería de fotos del producto ' . $product->name;
			$gallery->created_by = \Auth::user()->id;
			$gallery->save();

			$uploadedFile = $request->file('file');
			$filename = $uploadedFile->getClientOriginalName();

			$fileName = $uploadedFile->getClientOriginalName();
			$fileExt = $uploadedFile->getClientOriginalExtension();
			$destinationName = time() . $fileName;
			$destPath = 'uploads/products/galleries/' . $product->id;

			$uploadedFile->move($destPath,$destinationName);

			list($width, $height) = getimagesize($destPath . '/' . $destinationName);
			
			$image = new Image;
			$image->name = $destinationName;
			$image->filename = $destinationName;
			$image->filepath = $destPath;
			$image->description = $destinationName;
			$image->alt = $destinationName;
			$image->width = $width;
			$image->height = $height;
			$image->extension = $fileExt;
			$image->created_by = \Auth::user()->id;
			$image->imageable_type = 'CMS\Gallery';
			$image->imageable_id = $gallery->id;
			$image->mimetype = $uploadedFile->getClientMimeType();
			$image->size = $uploadedFile->getClientSize();

			$image->save();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function import()
	{
		return view('admin.products.import')
			->with('adminSectionTitle','Importar Excel');
	}

	public function importShipping()
	{
		return view('admin.products.importShipping')
			->with('adminSectionTitle','Importar Excel');
	}

	public function postImport(Request $request)
	{
		if ($request->hasFile('excelproducts')) {
			$excelFile = $request->file('excelproducts');
			$filePath = 'import/products/';
			$timestamp = time();
			$filename = $timestamp . '-' . $excelFile->getClientOriginalName();
			$extension = $excelFile->getClientOriginalExtension();

			$fileFullPath = $filePath . $timestamp . '-' . $filename . '.' . $extension;

			Storage::put($fileFullPath,File::get($excelFile));

			Excel::load('storage/app/'.$fileFullPath,function($reader){
				$businessUnitId = Session::get('UsingBusinessUnit');
				$usingBusinessUnit = BusinessUnit::find($businessUnitId);


			   $rows = $reader->all();

			    foreach ($rows as $row) {
			    	if (isset($row->segment1)) {
						$product = Product::firstOrNew(['segment1' => $row->segment1,'business_unit_id' => $usingBusinessUnit->id]);

						// Propiedades generales de los productos
						$product->name = $row->description_item;
						$product->original_name = $row->description_item;
						$product->description = $row->description_item;
						$product->slug = str_slug($row->description_item);
						$product->inventory_item_id = (int)$row->inventory_item_id;
						$product->organization_id = $row->organization_id;
						$product->OI = $row->oi;
						$product->UO = $row->uo;
						$product->enabled_flag = $row->enabled_flag;
						$product->segment1 = $row->segment1;
						$product->primary_uom_code = $row->primary_uom_code;
						$product->inventory_item_status = $row->inventory_item_status_code;
						$product->category_set_id = $row->category_set_id;
						$product->category_id = $row->category_id;
						$product->category_set_name = $row->category_set_name;
						$product->description_categoria = $row->description_categoria;
						$product->unidad_negocio = $row->unidad_negocio;
						$product->origen = $row->origen;
						$product->linea_mercado = $row->linea_mercado;
						$product->calidad = $row->calidad;
						$product->color = $row->color;
						$product->formato = $row->formato;
						$product->orientacion = $row->orientacion;
						$product->tipo_fregadero = $row->tipo_fregadero;
						$product->carac_esp = $row->carac_esp;
						$product->encendido = $row->encendido;
						$product->combustible = $row->combustible;
						$product->capacidad = $row->capacidad;
						$product->created_by = \Auth::user()->id;
						$product->business_unit_id = $usingBusinessUnit->id;

						// Relationships
						if (isset($row->marca) && !empty($row->marca)) {
							$brand = Brand::firstOrNew(['name' => $row->marca,'business_unit_id' => $usingBusinessUnit->id]);   
							$brand->slug = str_slug($row->marca);
							$brand->front_name = $row->marca;
							$brand->save();
							$product->brand_id = $brand->id;
						}                    

						if (isset($row->familia) && !empty($row->familia)) {
							$family = Family::firstOrNew([
								'name' 				=> $row->familia,
								'business_unit_id' 	=> $usingBusinessUnit->id,
								'brand_id'			=> $brand->id
								]);

							$family->front_name = $row->familia;

							$family->slug = str_slug($row->familia);
							$family->save();

							$product->family_id = $family->id;   
						}

						if (isset($row->linea) && !empty($row->linea)) {
							$line = Line::firstOrNew([
								'name' 					=> $row->linea,
								'business_unit_id' 		=> $usingBusinessUnit->id,
								'brand_id'				=> $brand->id,
								'family_id'				=> $family->id
								]);

							$line->front_name = $row->linea;

							$line->slug = str_slug($row->linea);
							$line->save();
							
							$product->line_id = $line->id;
						}

						$product->save();
					}
			   }
			});

			$request->session()->flash('notification', 'Productos importados satisfactoriamente.');
			$request->session()->flash('type', 'success');
			return redirect()->back();
		} else {
			$request->session()->flash('notification', 'Archivo no encontrado. Favor de seleccionar un archivo.');
			$request->session()->flash('type', 'warning');
			return redirect()->back();
		}
	}

	public function postImportShipping(Request $request)
	{
		if ($request->hasFile('excelproducts')) {
			$excelFile = $request->file('excelproducts');
			$filePath = 'import/products/';
			$timestamp = time();
			$filename = $timestamp . '-' . $excelFile->getClientOriginalName();
			$extension = $excelFile->getClientOriginalExtension();

			$fileFullPath = $filePath . $timestamp . '-' . $filename . '.' . $extension;

			Storage::put($fileFullPath,File::get($excelFile));

			Excel::load('storage/app/'.$fileFullPath,function($reader){
				$businessUnitId = Session::get('UsingBusinessUnit');
				$usingBusinessUnit = BusinessUnit::find($businessUnitId);


			   $rows = $reader->all();

			    foreach ($rows as $row) {
			    	if (isset($row->segment1)) {
						$product = Product::where(['segment1' => $row->segment1])->first();

						if($product){
							if(isset($row->allowed_shipping_methods)) {
								$allowed = EXPLODE(",", $row->allowed_shipping_methods);
								if(COUNT($allowed))
									foreach($allowed as $i => $method)
										$allowed[$i] = STRTOUPPER(TRIM($method));
								else
									$allowed = array();
									
								$product->allowed_shipping_methods = json_encode($allowed);
							}
							
							if(isset($row->disallowed_zipcodes)) {
								$disallowed = EXPLODE(",", $row->disallowed_zipcodes);
								if(COUNT($disallowed))
									foreach($disallowed as $i => $method)
										$disallowed[$i] = TRIM($method);
								else
									$disallowed = array();
									
								$product->disallowed_zipcodes = json_encode($disallowed);
							}
							
							$product->save();
						}
					}
			   }
			});

			$request->session()->flash('notification', 'Productos actualizados satisfactoriamente.');
			$request->session()->flash('type', 'success');
			return redirect()->back();
		} else {
			$request->session()->flash('notification', 'Archivo no encontrado. Favor de seleccionar un archivo.');
			$request->session()->flash('type', 'warning');
			return redirect()->back();
		}
	}

	public function postQuickEdit(Request $request)
	{
		$product = Product::find($request->get('id'));

		if ($request->has('action') && $request->get('action') == 'delete') {
			$product->delete();

			return response()->json(['status' => 200,'message'=>'Producto eliminado correctamente']);
		}

		if ($request->has('action') && $request->get('action') == 'restore') {
			Product::withTrashed($request->get('id'))->restore();

			return response()->json(['status' => 200,'message'=>'Producto restablecido correctamente.']);
		}

		if ($product) {
			
			if ($request->has('name')) {
				$product->name = $request->get('name');
			}

			if ($request->has('short_name')) {
				$product->short_name = $request->get('short_name');
			}

			if ($request->has('brand')) {
				$product->brand_id = $request->get('brand');
			}

			if ($request->has('line')) {
				$product->line_id = $request->get('line');
			}

			if ($request->has('family')) {
				$product->family_id = $request->get('family');
			}

			if ($request->has('is_parent')) {
				$product->is_parent = $request->get('is_parent');
			}

			if ($request->has('parent')) {
				$product->parent_id = $request->get('parent');
			}

			if ($request->input('segment1')) {
				$product->segment1 = $request->input('segment1');
			}

			$product->save();

			return response()->json(['status' => 200, 'message' => 'Actualizado correctamente.']);

		} else {
			return response()->json(['status' => 201, 'message' => 'No existe el producto que quiere editar.']);
		}
	}
	
	public function deleteBrandIcon($id){
		$brand_icon = BrandIcon::find($id);
		if($brand_icon)
			$brand_icon->delete();
		
		return redirect()->back();
	}
}
