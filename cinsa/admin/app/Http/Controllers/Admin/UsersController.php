<?php

namespace GIS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use GIS\Http\Requests;
use GIS\Http\Controllers\Controller;
use GIS\User;
use GIS\Comapny;
use Validator;

class UsersController extends Controller
{
	protected $user;

	public function __construct()
	{
		$this->user = \Auth::user();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$usersList = User::all();
		// $usersList = User::paginate(100);

		return view('admin.users.index')
			->with('user',$this->user)
			->with('usersList',$usersList);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$companyList = \GIS\Company::lists('name','id');

		return view('admin.users.create')
			->with('user',$this->user)
			->with('companyList',$companyList);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,[
			'name' => 'required|max:255',
			// 'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
			]);

		$user = new User;
		$user->name = $request->input('name');
		$user->lastname = $request->input('lastname');
		$user->employee_number = $request->input('employee_number');
		$user->phone_mobile = $request->input('phone_mobile');
		// $user->signup_code = $request->input('signup_code');
		// $user->secret_question = $request->input('secret_question');
		// $user->secret_question_answer = $request->input('secret_question_answer');
		$user->email = $request->input('email');
		$user->billing_group = $request->input('billing_group');
		$user->password = bcrypt($request->input('password'));
		$user->company_id = $request->input('company_id');
		$user->status = 1;

		$user->save();

		$tipoUsuario = $request->input('user_role');

		$user->makeEmployee($tipoUsuario);

		return back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return view('admin.users.show')
			->with('user',$this->user);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		$companiesList = \GIS\Company::all();
		
		return view('admin.users.edit')
			->with('user_data',$user)
			->with('companies',$companiesList)
			->with('user',$this->user);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$user = User::find($id);
		
		$user->name = $request->input('name');
		$user->lastname = $request->input('lastname');
		$user->employee_number = $request->input('employee_number');
		$user->phone_mobile = $request->input('phone_mobile');
		$user->company_id = $request->input('company_id');
		$user->billing_group = $request->input('billing_group');
		$user->email = strcmp($request->input('email'), "") != 0 ? $request->input('email') : null;
		if(strcmp($request->input('password'), "") != 0)
			$user->password = bcrypt($request->input('password'));
		$user->type = $request->input('user_type');

		$user->resetRoles($user->type);

		$user->save();

		return redirect('admin/users');	//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);

		$user->delete();

		return redirect('admin/users');
	}
}
