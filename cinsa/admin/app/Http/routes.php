<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
	return view('welcome');
});*/

/* PROTEGER UNA RUTA */
Route::get('/',[
	'middleware'=>'auth',
	'uses' => 'Admin\DashboardController@index' 
	]);

/* Proteger una ruta con controlador */
// Route::get('profile', [
//     'middleware' => 'auth',
//     'uses' => 'ProfileController@show'
// ]);


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('banners', 'Admin\BannersController@index');
Route::get('dashboard/productos', 'Admin\DashboardController@productos');
Route::get('dashboard/banners', 'Admin\DashboardController@indexBanners');

Route::post('banners/upload', 'Admin\BannersController@store');
Route::get('banners/delete/{id}', 'Admin\BannersController@destroy');
Route::get('banners/getAllBanners', 'Admin\BannersController@getAllBanners');

Route::get('dashboard/getAllSlides', 'Admin\DashboardController@getAllSlides');
Route::get('dashboard/getAllProducts', 'Admin\DashboardController@getAllProducts');
Route::get('dashboard/getAllIndexBanners', 'Admin\DashboardController@getAllIndexBanners');

Route::get('backgrounds', 'Admin\BackgroundsController@index');
Route::get('backgrounds/getBackground/{type}', 'Admin\BackgroundsController@getBackground');
Route::get('backgrounds/index', 'Admin\BackgroundsController@index_backgrounds');

Route::get('addresses', 'Admin\AddressesController@index');
Route::get('addresses/getMunicipios/{id}', 'Admin\AddressesController@getMunicipios');
Route::get('addresses/saveAddress', 'Admin\AddressesController@store');
Route::get('addresses/getAllAddresses', 'Admin\AddressesController@getAllAddresses');
Route::get('addresses/getAddress/{id}', 'Admin\AddressesController@getAddress');
Route::get('addresses/delete/{id}', 'Admin\AddressesController@destroy');
Route::post('addresses/csv', 'Admin\AddressesController@csv');
Route::post('addresses/update_order', 'Admin\AddressesController@updateOrder');

Route::get('tips', 'Admin\TipsController@index');
Route::post('tips/save', 'Admin\TipsController@store');
Route::get('tips/delete/{id}', 'Admin\TipsController@destroy');
Route::get('tips/edit/{id}', 'Admin\TipsController@edit');

/*Webservices*/
Route::get('tips/getTips', 'Admin\TipsController@getAllTips');
Route::get('tips/searchTips/{key}', 'Admin\TipsController@searchTips');
Route::get('tips/getTip/{id}', 'Admin\TipsController@getTip');
Route::get('tips/getAllTags', 'Admin\TipsController@getAllTags');
Route::get('addresses/getStates', 'Admin\AddressesController@getStatesWS');
Route::get('addresses/getMunicipiosWS/{id}', 'Admin\AddressesController@getMunicipiosWS');
Route::get('addresses/getAllAddressesEstado/{id}', 'Admin\AddressesController@getAllAddressesEstado');
Route::get('addresses/getAllAddressesMunicipio/{id}', 'Admin\AddressesController@getAllAddressesMunicipio');
Route::get('dashboard/getIndexBackgrounds', 'Admin\BackgroundsController@getAllIndexBackgrounds');

/*TinyMCE*/
Route::any('/imglist', ['as'=>'imglist', 'middleware'=>'auth', 'uses'=>'Admin\TipsController@imageList']);
Route::any('/upload', ['middleware' => 'auth', 'uses' =>'Admin\TipsController@upload']);


Route::resource('businessunits', 'Admin\BusinessUnitsController');
Route::resource('companies', 'Admin\CompaniesController');
Route::resource('users', 'Admin\UsersController');
Route::resource('usergroups', 'Admin\UserGroupsController',['except' => ['create']]);
Route::resource('billinggroups', 'Admin\BillingGroupsController',['except' => ['create']]);
Route::resource('menus', 'Admin\MenusController');
Route::resource('pages', 'Admin\PagesController');
Route::resource('banners', 'Admin\BannersController');
Route::resource('sliders', 'Admin\SlidersController');
Route::resource('products', 'Admin\ProductsController');
Route::resource('parts', 'Admin\PartsController');
Route::resource('lines', 'Admin\LinesController');
Route::resource('families', 'Admin\FamiliesController');
Route::resource('brands', 'Admin\BrandsController');
Route::resource('additionalitems','Admin\AdditionalItemsController');
Route::resource('downloads','Admin\DownloadsController');

Route::get('/search/brand_families','SearchController@brandFamilies');
Route::get('/search/family_lines','SearchController@familyLines');
Route::post('products/addadditionalitem','Admin\ProductsController@addAdditionalItem');
Route::post('products/photos/delete','Admin\ProductsController@postDeletePhoto');
Route::post('products/photos','Admin\ProductsController@postPhoto');
Route::post('products/quickedit','Admin\ProductsController@postQuickEdit');
Route::post('products/specifications/quickedit','Admin\SpecificationsController@postQuickEdit');
Route::get('products/import','Admin\ProductsController@import');
Route::post('products/import','Admin\ProductsController@postImport');
Route::get('products/import-shipping','Admin\ProductsController@importShipping');
Route::post('products/import-shipping','Admin\ProductsController@postImportShipping');
Route::get('products/brand-icons/delete/{id}','Admin\ProductsController@deleteBrandIcon');

//LINES
Route::post('lines/update_order','Admin\LinesController@updateOrder');
Route::post('families/update_order','Admin\FamiliesController@updateOrder');

//Searches
Route::get('/livesearch/users','SearchController@getUserSearch');
Route::get('/livesearch/products','SearchController@getProductSearch');
Route::post('/livesearch','SearchController@postLiveSearch');
Route::post('/livesearch/products','SearchController@postLiveSearchProducts');
Route::post('/livesearch/product_siblings','SearchController@postLiveSearchSiblings');
Route::get('/buscar/{query?}','SearchController@buscar');