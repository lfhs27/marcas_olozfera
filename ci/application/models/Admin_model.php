<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends CI_Model
{
	protected $brand_db = null;
	protected $brand = "calorex";
    function __construct($param1)
    {
        parent::__construct();
		$brand = array_pop($param1);
		if(strcmp($brand, "calorex") == 0)
			$this->brand_db = $this->load->database('calorex', TRUE);
		else
			$this->brand_db = $this->load->database('cinsa', TRUE);
    }
	
	function getNavigation($brandSlug){
		$b = $this->brand_db->get_where("brands", ["slug" => $brandSlug]);
		$brands = array();
		if($b->num_rows() > 0){
			foreach($b->result() as $brand){
				$f = $this->brand_db->order_by("order", "asc")->get_where("families", ["brand_id" => $brand->id, "deleted_at IS NULL" => null]);
				$families = array();
				if($f->num_rows() > 0){
					foreach($f->result() as $family){
						$l = $this->brand_db->order_by("order", "asc")->get_where("lines", ["family_id" => $family->id, "deleted_at IS NULL" => null]);
						$lines = array();
						if($l->num_rows() > 0){
							foreach($l->result() as $line){						
								array_push($lines, $line);
							}
						}
						$family->lines = $lines;
						
						array_push($families, $family);
					}
				}
				$brand->families = $families;
				
				array_push($brands, $brand);
			}
		}
		//$b = $b->result();
		/*print_r("<pre>");
		print_r($brands);
		print_r("</pre>");***/
		
		$response = new stdClass;
		$response->brand = array_pop($brands);
		
		return $response;
	}
	
	function getLines($buSlug, $bSlug, $fSlug){
		$b = $this->brand_db->get_where("brands", ["slug" => $bSlug]);
		$brands = array();
		$lines = array();
		if($b->num_rows() > 0){
			foreach($b->result() as $brand){
				$f = $this->brand_db->order_by("order", "asc")->get_where("families", ["brand_id" => $brand->id, "slug" => $fSlug]);
				$families = array();
				if($f->num_rows() > 0){
					foreach($f->result() as $family){
						$l = $this->brand_db->order_by("order", "asc")->get_where("lines", ["family_id" => $family->id]);
						if($l->num_rows() > 0){
							foreach($l->result() as $line){		
								$line->brand = $brand;
								$line->family = $family;
								
								$p = $this->brand_db->get_where("products", ["line_id" => $line->id]);
								$products = array();
								if($p->num_rows() > 0){
									foreach($p->result() as $product){
										$product->gallery = $this->getGallery($product->id);
										
										array_push($products, $product);
									}
								}
								$line->products = $products;
								array_push($lines, $line);
							}
						}
						$family->lines = $lines;
						
						array_push($families, $family);
					}
				}
				$brand->families = $families;
				
				array_push($brands, $brand);
			}
		}
		
		return $lines;
	}
	
	function getProducts($buSlug, $bSlug, $fSlug, $lSlug){
		$b = $this->brand_db->get_where("brands", ["slug" => $bSlug]);
		$brands = array();
		$products = array();
		if($b->num_rows() > 0){
			foreach($b->result() as $brand){
				$f = $this->brand_db->order_by("order", "asc")->get_where("families", ["brand_id" => $brand->id, "slug" => $fSlug]);
				$families = array();
				if($f->num_rows() > 0){
					foreach($f->result() as $family){
						$l = $this->brand_db->order_by("order", "asc")->get_where("lines", ["family_id" => $family->id, "slug" => $lSlug]);
						$lines = array();
						if($l->num_rows() > 0){
							foreach($l->result() as $line){		
								$line->brand = $brand;
								$line->family = $family;
								
								$p = $this->brand_db->get_where("products", ["line_id" => $line->id]);
								//$products = array();
								if($p->num_rows() > 0){
									foreach($p->result() as $product){
										$product->brand = $brand;
										$product->family = $family;
										$product->line = $line;
										$product->gallery = $this->getGallery($product->id);
										
										array_push($products, $product);
									}
								}
								$line->products = $products;
								array_push($lines, $line);
							}
						}
						$family->lines = $lines;
						
						array_push($families, $family);
					}
				}
				$brand->families = $families;
				
				array_push($brands, $brand);
			}
		}
		
		return $products;
	}
	
	function getProductBySlug($buSlug, $bSlug, $fSlug, $lSlug, $pSlug){
		$b = $this->brand_db->get_where("brands", ["slug" => $bSlug]);
		$brands = array();
		
		if($b->num_rows() > 0){
			foreach($b->result() as $brand){
				$f = $this->brand_db->order_by("order", "asc")->get_where("families", ["brand_id" => $brand->id, "slug" => $fSlug]);
				$families = array();
				if($f->num_rows() > 0){
					foreach($f->result() as $family){
						$l = $this->brand_db->order_by("order", "asc")->get_where("lines", ["family_id" => $family->id, "slug" => $lSlug]);
						$lines = array();
						if($l->num_rows() > 0){
							foreach($l->result() as $line){		
								$line->brand = $brand;
								$line->family = $family;
								
								$p = $this->brand_db->get_where("products", ["line_id" => $line->id, "slug" => $pSlug]);
								$products = array();
								if($p->num_rows() > 0){
									foreach($p->result() as $product){
										$product->brand = $brand;
										$product->family = $family;
										$product->line = $line;
										$product->gallery = $this->getGallery($product->id);
										$product->brand_icons = $this->getBrancIcons($product->id);
										$product->specifications = $this->getSpecifications($product->id);
										$product->downloads = $this->getDownloads($product->id);
										$product->blueprint = $this->getBlueprint($product->id);
										
										//SIBLINGS
										$siblings_arr = json_decode($product->sibling_products);
										$siblings = array();
										if(is_array($siblings_arr) && COUNT($siblings_arr) > 0){
											foreach($siblings_arr as $sib)
												array_push($siblings, $this->getProductById($sib));
										}
										$product->siblings = $siblings;
										
										return $product;
										array_push($products, $product);
									}
								}
								$line->products = $products;
								array_push($lines, $line);
							}
						}
						$family->lines = $lines;
						
						array_push($families, $family);
					}
				}
				$brand->families = $families;
				
				array_push($brands, $brand);
			}
		}
		
		return $products;
	}
	
	function getProductById($pid){
		$p = $this->brand_db->get_where("products", ["id" => $pid]);
		$products = array();
		if($p->num_rows() > 0){
			foreach($p->result() as $product){
				$product->brand = $this->getBrandById($product->brand_id);
				$product->family = $this->getFamilyById($product->family_id);
				$product->line =  $this->getLineById($product->line_id);
				$product->gallery = $this->getGallery($product->id);
				$product->blueprint = $this->getBlueprint($product->id);
				
				return $product;
				array_push($products, $product);
			}
		}
	}
	
	function getBrandById($bid){
		$q = $this->brand_db->get_where("brands", ["id" => $bid]);
		if($q->num_rows() > 0)
			foreach($q->result() as $brand)
				return $brand;
		else
			return null;
	}
	
	function getFamilyById($fid){
		$q = $this->brand_db->order_by("order", "asc")->get_where("families", ["id" => $fid]);
		if($q->num_rows() > 0)
			foreach($q->result() as $family)
				return $family;
		else
			return null;
	}
	
	function getLineById($lid){
		$q = $this->brand_db->order_by("order", "asc")->get_where("lines", ["id" => $lid]);
		if($q->num_rows() > 0)
			foreach($q->result() as $line)
				return $line;
		else
			return null;
	}
	
	function getBrancIcons($pid){
		$q = $this->brand_db->get_where("brand_icons", ["product_id" => $pid]);
		if($q->num_rows() > 0)
			return $q->result();
		else
			return null;
	}
	
	function getSpecifications($pid){
		$q = $this->brand_db->get_where("specifications", ["product_id" => $pid]);
		if($q->num_rows() > 0)
			return $q->result();
		else
			return null;
	}
	
	function getDownloads($pid){
		$q = $this->brand_db->get_where("downloads", ["downloadable_id" => $pid, "downloadable_type" => "CMS\Product"]);
		if($q->num_rows() > 0)
			return $q->result();
		else
			return null;
	}
	
	function getBlueprint($pid){
		$q = $this->brand_db->get_where("blueprints", ["product_id" => $pid]);
		if($q->num_rows() > 0)
			foreach($q->result() as $blueprint)
				return $blueprint;
		else
			return null;
	}
	
	function getGallery($pid){
		$g = $this->brand_db->get_where("galleries", ["product_id" => $pid]);
		if($g->num_rows() > 0){
			foreach($g->result() as $gallery){
				$i = $this->brand_db->get_where("images", ["imageable_id" => $gallery->id, "imageable_type" => "CMS\\Gallery"]);
				//$images = array();
				if($i->num_rows() > 0)
					$gallery->images = $i->result();
				else
					$gallery->images = null;
					
				return $gallery;
			}
		}else{
			return null;
		}
	}
}

?>