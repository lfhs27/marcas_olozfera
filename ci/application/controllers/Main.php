<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public $domain;

	public $pageProperties;
	public $brandId;
	public $brand;
	public $navigation;

	public function __construct()
	{
		
		parent::__construct();
        $this->load->helper('custom');

		$this->domain = $_SERVER['SERVER_NAME'];
		$this->setPageProperties();
        $this->load->model('Admin_model', 'admin', false, [$this->pageProperties['skin']]);
		//$this->navigation = json_decode($this->fileGetContentsCurl('https://www.tiendagis.com/gis_api/brand_catalog/' . $this->brandId));
		$this->navigation = $this->admin->getNavigation("calorex");	
	}
	
	public function test(){
		$obj = json_decode($this->fileGetContentsCurl('http://srgisazr1-0002.cloudapp.net/gis_api/brand_catalog/1'));
		print_r("<pre>");
		print_r($obj);
		print_r("</pre>");
	}

	// Load Brand Index
	public function index()
	{
		$data = [
			'pageProperties' 	=> $this->pageProperties,
		];
		$data['navigation'] = $this->navigation;
		$data["slides"] = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'dashboard/getAllSlides'));
		$data["products"] = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'dashboard/getAllProducts'));
		$data["banners"] = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'dashboard/getAllIndexBanners'));
		$data["bgs"] = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'dashboard/getIndexBackgrounds'));
		
		$this->load->view('brandIndex',$data);
	}

	public function page($id)
	{
		
	}

	public function brand($brandId)
	{
		$data = [
			'pageProperties'		=> $this->pageProperties,
			'navigation'			=> $this->navigation
		];

		$this->load->view('brandFamilies', $data);
	}

	public function tips()
	{
		$key = isset($_GET["search"]) ? $_GET["search"] : null;
		$data = [
			'pageProperties'		=> $this->pageProperties,
			'navigation'			=> $this->navigation
		];
		/*
		switch ($this->pageProperties["skin"]){
			case 'calorex':
				$data["tags"] = json_decode($this->fileGetContentsCurl('http://admin.calorex.com.mx/tips/getAllTags'));
				if($key != null && strcmp($key, "") != 0)
					$data["tips"] = json_decode($this->fileGetContentsCurl('http://admin.calorex.com.mx/tips/searchTips/'.urlencode($key)));
				else
					$data["tips"] = json_decode($this->fileGetContentsCurl('http://admin.calorex.com.mx/tips/searchTips/0'));
				break;
			case 'cinsa':
				$data["tags"] = json_decode($this->fileGetContentsCurl('http://admin.calorex.com.mx/tips/getAllTags'));
				if($key != null && strcmp($key, "") != 0)
					$data["tips"] = json_decode($this->fileGetContentsCurl('http://admin.calorex.com.mx/tips/searchTips/'.urlencode($key)));
				else
					$data["tips"] = json_decode($this->fileGetContentsCurl('http://admin.calorex.com.mx/tips/searchTips/0'));
				break;
		}*/
		$data["tags"] = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'tips/getAllTags'));
		if($key != null && strcmp($key, "") != 0)
			$data["tips"] = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'tips/searchTips/'.urlencode($key)));
		else
			$data["tips"] = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'tips/searchTips/0'));
		
		$this->load->view('brandTips', $data);
	}

	public function tip($tipName)
	{
		/*$tipName = str_replace('-', '%20', $tipName);
		$tipName = str_replace('_', '-', $tipName);
		$tipName = str_replace('|', '/', $tipName);
		$tipName = urlencode($tipName);*/
		$data = [
			'pageProperties'		=> $this->pageProperties,
			'navigation'			=> $this->navigation
		];
		/*
		switch ($this->pageProperties["skin"]){
			case 'calorex':
			case 'cinsa':
				$data["tip"] = json_decode($this->fileGetContentsCurl('http://admin.calorex.com.mx/tips/getTip/'.$tipName));
				break;
		}*/
		
		$data["tip"] = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'tips/getTip/'.$tipName));
		$this->load->view('brandTip', $data);
	}

	public function calentadorIdeal()
	{
		$data = [
			'pageProperties'		=> $this->pageProperties,
			'navigation'			=> $this->navigation
		];
		$this->load->view('brandCalentadorIdeal', $data);
	}
	
	public function WScalentador(){
		$modelos = $this->fileGetContentsCurl("http://srgisazr1-0002.cloudapp.net/calentador-ideal/WSgetModels?".$_SERVER['QUERY_STRING']);
		
		echo $modelos;
	}

	public function family($brandId,$familyId)
	{
		$productList = json_decode($this->fileGetContentsCurl('http://srgisazr1-0002.cloudapp.net/gis_api/brand_catalog/'. $brandId .'/family_products/' . $familyId));
		$bannersList = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'banners/getAllBanners'));
		$bg = "";
		switch($productList->name){
			case "DEPOSITO":
				$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-deposito'));
				break;
			case "INSTANTANEO":
				$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-calefactores'));
				break;
			case "DE PASO":
				$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-paso'));
				break;
			case "SOLAR":
				$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-paso-evo'));
				break;
		}
		$data = [
			'pageProperties'		=> $this->pageProperties,
			'catalogTitle'			=> $productList->name,
			'catalogSubtitle'		=> $productList->description,
			'navigation'			=> $this->navigation,
			'products'				=> $productList->products,
			'banners'				=> $bannersList,
			'bg'					=> $bg
		];
		
		$this->load->view('familyProducts',$data);
	}

	public function familyByName($bu,$brand,$family)
	{ /*echo "entraellll";
		print_r("<pre>");
		print_r($this->navigation);
		print_r("</pre>");
		$family = str_replace('-', '%20', $family);*/

		//$linesList = json_decode($this->fileGetContentsCurl('https://www.tiendagis.com/gis_api/business_unit/'.$bu.'/brand/'. $brand .'/family/' . $family));
		$linesList = $this->admin->getLines($bu, $brand, $family);
		$bannersList = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'banners/getAllBanners'));
		$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-deposito'));

		$data = [
			'pageProperties'		=> $this->pageProperties,
			'catalogTitle'			=> "Family",
			'catalogSubtitle'		=> "Family",
			'navigation'			=> $this->navigation,
			'lines'					=> $linesList,
			'banners'				=> $bannersList,
			'bg'					=> $bg
		];
		$this->admin->getNavigation("calorex");
		//var_dump($linesList);
		$this->load->view('familyLines',$data);
		/*if(COUNT($data["products"]) == 1){
			$first_model = $data["products"][0];
			header("Location: ".base_url($first_model->business_unit->slug. '/' . $first_model->brand->slug. '/' . $first_model->family->slug. '/' . $first_model->line->slug. '/' . $first_model->slug));
		}else{
			$this->load->view('familyProducts',$data);
		}*/
	}

	public function lineByName($bu,$brand,$family,$line)
	{ /*echo "entraellll";
		print_r("<pre>");
		print_r($this->navigation);
		print_r("</pre>");
		$family = str_replace('-', '%20', $family);*/
		//echo 'https://www.tiendagis.com/gis_api/business_unit/'.$bu.'/brand/'. $brand .'/family/' . $family.'/line/' . $line;
		//$productList = json_decode($this->fileGetContentsCurl('https://www.tiendagis.com/gis_api/business_unit/'.$bu.'/brand/'. $brand .'/family/' . $family.'/line/' . $line));
		$productList = $this->admin->getProducts($bu, $brand, $family, $line);
		$bannersList = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'banners/getAllBanners'));
		$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-deposito'));
		/*$bg = "";
		switch($productList->name){
			case "DEPOSITO": 
				$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-deposito'));
				break;
			case "INSTANTANEO":
				$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-calefactores'));
				break;
			case "DE PASO":
				$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-paso'));
				break;
			case "SOLAR":
				$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-paso-evo'));
				break;
		}*/
		$data = [
			'pageProperties'		=> $this->pageProperties,
			'catalogTitle'			=> "Family",
			'catalogSubtitle'		=> "Family",
			'navigation'			=> $this->navigation,
			'products'				=> $productList,
			'banners'				=> $bannersList,
			'bg'					=> $bg
		];
		$first_model = $data["products"][0];
		$siblings = json_decode($first_model->sibling_products);
		if(COUNT($siblings) > 0){
			$first_model = $this->admin->getProductById($siblings[0]);
		}
		/*print_r("<pre>");
		print_r($first_model);
		print_r("</pre>");*/
		header("Location: ".base_url('calentadores/' . $first_model->brand->slug. '/' . $first_model->family->slug. '/' . $first_model->line->slug. '/' . $first_model->slug));
		/*
		if(COUNT($data["products"]) == 1){
			$first_model = $data["products"][0];
			header("Location: ".base_url($first_model->business_unit->slug. '/' . $first_model->brand->slug. '/' . $first_model->family->slug. '/' . $first_model->line->slug. '/' . $first_model->slug));
		}else{
			$this->load->view('familyProducts',$data);
		}*/
	}

	public function buscarLinea($query = null)
	{ 
		$data = [
			'pageProperties'		=> $this->pageProperties,
			'navigation'			=> $this->navigation
		];
		$bg = "";
		if($query){
			/* EN CINSA EL PRODUCTO NO LLEVA SOLAR EN EL NOMBRE, SE LLAMA SOLEI. */
			if ($query == 'solar' && $this->pageProperties["skin"] == 'cinsa') {
				$query = 'solei';
			}
			$query = str_replace('-', '%20', $query);
			$query = urlencode($query);
			$title = urldecode($query);
			$title = str_replace('%C3%B3', '&oacute;', $title);
			$title = str_replace('%20', ' ', urldecode($title));
			
			switch($title){
				case 'deposito':
					$title = "Depósito";
					$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-deposito'));
					break;
				case 'solar':
					$title = "solares";
					$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-paso-evo'));
					break;
				case 'instantaneo':
					$title = "instantáneos";
					$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-calefactores'));
					break;
				case 'de-paso':
					$title = "De Paso";
					$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-paso'));
					break;
			}
			$data["catalogTitle"] = ucwords($title);
		}else{
			$query = urlencode($this->input->get("q"));
			$data["catalogTitle"] = "Búsqueda";
		}
		$linesList = json_decode($this->fileGetContentsCurl('https://www.tiendagis.com/gis_api/buscarLinea/'.$this->pageProperties["skin"].'/' . $query));
		$bannersList = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'banners/getAllBanners'));
		
		$data["lines"] = $linesList;
		$data["banners"] = $bannersList;
		$data["bg"] = $bg;
		/*
		$nProd = 0;
		if(count($data["products"])){
			foreach($data["products"] as $product){
				if($product->brand_id == $this->pageProperties["brand_id"]){
					$nProd++;
				}
			}
		}*/
		$this->load->view('familyLines',$data);
		/*if($nProd == 1){
			$data['url_base'] = 'calentadores/'.strtolower($data["products"][0]->sector->name)."/". strtolower(str_replace('/', '|', str_replace(' ', '-', str_replace('-', '_', remove_accents($data["products"][0]->family->name)))));
			$first_model = $data["products"][0];
			header("Location: ".base_url($first_model->business_unit->slug. '/' . $first_model->brand->slug. '/' . $first_model->family->slug. '/' . $first_model->line->slug. '/' . $first_model->slug));
		}else{
			$this->load->view('familyLines',$data);
		}*/
	}

	public function buscar($query = null)
	{ 
		$data = [
			'pageProperties'		=> $this->pageProperties,
			'navigation'			=> $this->navigation
		];
		$bg = "";
		if($query){
			/* EN CINSA EL PRODUCTO NO LLEVA SOLAR EN EL NOMBRE, SE LLAMA SOLEI. */
			if ($query == 'solar' && $this->pageProperties["skin"] == 'cinsa') {
				$query = 'solei';
			}
			$query = str_replace('-', '%20', $query);
			$query = urlencode($query);
			$title = urldecode($query);
			$title = str_replace('%C3%B3', '&oacute;', $title);
			$title = str_replace('%20', ' ', urldecode($title));
			
			switch($title){
				case 'deposito':
					$title = "Depósito";
					$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-deposito'));
					break;
				case 'solar':
					$title = "solares";
					$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-paso-evo'));
					break;
				case 'instantaneo':
					$title = "instantáneos";
					$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-calefactores'));
					break;
				case 'de-paso':
					$title = "De Paso";
					$bg = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'backgrounds/getBackground/bg-paso'));
					break;
			}
			$data["catalogTitle"] = ucwords($title);
		}else{
			$query = urlencode($this->input->get("q"));
			$data["catalogTitle"] = "Búsqueda";
		}
		$productList = json_decode($this->fileGetContentsCurl('https://www.tiendagis.com/gis_api/buscar/'.$this->pageProperties["skin"].'/' . $query));
		$bannersList = json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'banners/getAllBanners'));
		//echo 'https://www.tiendagis.com/gis_api/buscar/'.$this->pageProperties["skin"].'/' . $query;
		$data["products"] = $productList;
		$data["banners"] = $bannersList;
		$data["bg"] = $bg;
		
		$nProd = 0;
		if(count($data["products"])){
			foreach($data["products"] as $product){
				if($product->brand_id == $this->pageProperties["brand_id"]){
					$nProd++;
				}
			}
		}
		if($nProd == 1){
			$data['url_base'] = 'calentadores/'.strtolower($data["products"][0]->sector->name)."/". strtolower(str_replace('/', '|', str_replace(' ', '-', str_replace('-', '_', remove_accents($data["products"][0]->family->name)))));
			$first_model = $data["products"][0];
			header("Location: ".base_url($first_model->business_unit->slug. '/' . $first_model->brand->slug. '/' . $first_model->family->slug. '/' . $first_model->line->slug. '/' . $first_model->slug));
		}else{
			$this->load->view('searchProducts',$data);
		}
	}

	public function product($productId,$modelId = null)
	{
		if ($modelId) {
			$productInfo = json_decode($this->fileGetContentsCurl('http://srgisazr1-0002.cloudapp.net/gis_api/brand_catalog/product/' . $productId . '/' .$modelId));	
		} else {
			$productInfo = json_decode($this->fileGetContentsCurl('http://srgisazr1-0002.cloudapp.net/gis_api/brand_catalog/product/' . $productId));
		}
		

		$data = [
			'pageProperties'		=> $this->pageProperties,
			'navigation'			=> $this->navigation,
			'product'				=> $productInfo->product,
			'model'					=> $productInfo->model,
			'modelProperties'		=> $productInfo->modelProperties,
			'graphProps'			=> $productInfo->graphProps
		];

		$this->load->view('brandProduct',$data);
	}

	public function productByName($business_unit,$brand,$family,$line,$pr)
	{ 
		//$jsonProd = $this->fileGetContentsCurl('https://www.tiendagis.com/gis_api/getProduct/'.$business_unit.'/'.$brand.'/'.$family.'/'.$line.'/'.$pr);
		//$product = json_decode($jsonProd);
		$product = $this->admin->getProductBySlug($business_unit, $brand, $family, $line, $pr);
		$data = [
			//'url_base'				=> 'calentadores/'.$sector.'/'.str_replace('%20', '-', $family),
			'pageProperties'		=> $this->pageProperties,
			'navigation'			=> $this->navigation,
			/*'product'				=> $productInfo->product,
			'model'					=> $productInfo->model,
			'modelProperties'		=> $productInfo->modelProperties,
			'graphProps'			=> $productInfo->graphProps,*/
			'product'				=> $product,
			'siblings'				=> $product->siblings,
			//'brands'				=> $brands
		];

		$this->load->view('brandProduct',$data);
	}
	
	public function distribuidores(){
		$data = [
			'pageProperties'		=> $this->pageProperties,
			'navigation'			=> $this->navigation,
			'states'				=> json_decode($this->fileGetContentsCurl($this->pageProperties['domain'].'addresses/getStates'))
		];
		
		$this->load->view('brandDistribuidores',$data);
	}
	
	public function nosotros(){
		$data = [
			'pageProperties' 	=> $this->pageProperties,
		];
		$data['navigation'] = $this->navigation;
		
		switch ($this->pageProperties["skin"]){
			case 'calorex':
				$this->load->view("calorex/nosotros", $data);
				break;
			case 'cinsa':
				$this->load->view("cinsa/nosotros", $data);
				break;
		}
	}
	
	public function aviso_privacidad(){
		$data = [
			'pageProperties' 	=> $this->pageProperties,
		];
		$data['navigation'] = $this->navigation;
		
		switch ($this->pageProperties["skin"]){
			case 'calorex':
				$this->load->view("calorex/aviso-privacidad", $data);
				break;
			case 'cinsa':
				$this->load->view("cinsa/aviso-privacidad", $data);
				break;
		}
	}
	
	public function solicita_ayuda(){
		$data = [
			'pageProperties' 	=> $this->pageProperties,
		];
		$data['navigation'] = $this->navigation;
		
		switch ($this->pageProperties["skin"]){
			case 'calorex':
				$this->load->view("calorex/info-solicita", $data);
				break;
			case 'cinsa':
				$this->load->view("cinsa/info-solicita", $data);
				break;
		}
	}
	
	public function extiende_garantia(){
		$data = [
			'pageProperties' 	=> $this->pageProperties,
		];
		$data['navigation'] = $this->navigation;
		
		switch ($this->pageProperties["skin"]){
			case 'calorex':
				$this->load->view("calorex/extiende_garantia", $data);
				break;
			case 'cinsa':
				$this->load->view("cinsa/extiende_garantia", $data);
				break;
		}
	}
	
	public function solicita_dudas(){
		$data = [
			'pageProperties' 	=> $this->pageProperties,
		];
		$data['navigation'] = $this->navigation;
		
		switch ($this->pageProperties["skin"]){
			case 'calorex':
				$this->load->view("calorex/info-dudas", $data);
				break;
			case 'cinsa':
				$this->load->view("cinsa/info-dudas", $data);
				break;
		}
	}
	
	public function solicita_garantia(){
		$data = [
			'pageProperties' 	=> $this->pageProperties,
		];
		$data['navigation'] = $this->navigation;
		
		switch ($this->pageProperties["skin"]){
			case 'calorex':
				$this->load->view("calorex/info-garantia", $data);
				break;
			case 'cinsa':
				$this->load->view("cinsa/info-garantia", $data);
				break;
		}
	}
	
	public function solicita_distribuidor(){
		$data = [
			'pageProperties' 	=> $this->pageProperties,
		];
		$data['navigation'] = $this->navigation;
		
		switch ($this->pageProperties["skin"]){
			case 'calorex':
				$this->load->view("calorex/info-distribuidor", $data);
				break;
			case 'cinsa':
				$this->load->view("cinsa/info-distribuidor", $data);
				break;
		}
	}
	
	public function solicita_asesoria(){
		$data = [
			'pageProperties' 	=> $this->pageProperties,
		];
		$data['navigation'] = $this->navigation;
		
		switch ($this->pageProperties["skin"]){
			case 'calorex':
				$this->load->view("calorex/info-asesoria", $data);
				break;
			case 'cinsa':
				$this->load->view("cinsa/info-asesoria", $data);
				break;
		}
	}

	protected function setPageProperties()
	{
		switch ($this->domain) {
			case 'calorex.com.mx':
			case 'calorex.mx':
			case 'www.calorex.com.mx':
			case 'www.calorex.mx':
			case 'www.calorex.softdepotserver2.com':
			case 'www.calorex2.softdepotserver2.com':
			case 'calorex.softdepotserver2.com':
			case 'calorex2.softdepotserver2.com':
			case 'www.calorex.localhost':
				$this->brandId = 8;
				$this->pageProperties['skin'] = 'calorex';
				$this->pageProperties['brand_id'] = 8;
				$this->pageProperties['domain'] = 'http://admin.calorex.com.mx/';
				$this->pageProperties['logo'] = base_url() . '/img/calorex/logoCALOREX.png';
				$this->pageProperties['slider'] = [
					[
						'desktop' => base_url() . '/img/calorex/bannerCX-calentadorIdeal.png',
						'mobile' => base_url() . '/img/calorex/bannerCX-calentadorIdeal-movil.png'
					]
				];
				$this->pageProperties['bodyBackground'] = base_url() . '/img/calorex/fondo001.jpg';

				break;

			case 'www.cinsa.softdepotserver2.com':
			case 'cinsa.softdepotserver2.com':
			case 'cinsaboilers.com.mx':
			case 'cinsaboilers.mx':
			case 'www.cinsaboilers.com.mx':
			case 'www.cinsaboilers.mx':
			case 'cinsa2.softdepotserver2.com':
			case 'www.cinsa.localhost':

				$this->brandId = 2;
				$this->pageProperties['skin'] = 'cinsa';
				$this->pageProperties['brand_id'] = 2;
				$this->pageProperties['domain'] = 'http://admin.cinsaboilers.com.mx/';
				$this->pageProperties['logo'] = base_url() . '/img/cinsa/logoCINSA.png';
				$this->pageProperties['slider'] = [
					[
						'desktop' => base_url() . '/img/cinsa/bannerCN-calentadorIdeal.png',
						'mobile' => base_url() . '/img/cinsa/bannerCN-calentadorIdeal.png'
					]
				];
				$this->pageProperties['bodyBackground'] = base_url() . '/img/cinsa/bgCN-01.png';
				break;
			
			default:
				$this->cssSkin = 'calorex';
				break;
		}
	}

	private function fileGetContentsCurl($url) {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

	private function getProductListingImage($imagesArray)
	{
		foreach ($imagesArray as $image) {
			if ($image->location === 'listing') {
				return $image;
			}
		}
	}
	
	public function getCalorexStates(){
		$states = $this->fileGetContentsCurl($this->pageProperties['domain'].'addresses/getStates');
		
		echo $states;
	}
	
	public function getCalorexMunicipios($estado){
		//$estado = $this->uri->segment(3);
		$municipios = $this->fileGetContentsCurl($this->pageProperties['domain'].'addresses/getMunicipiosWS/'.$estado);
		
		echo $municipios;
	}
	
	public function getCalorexAddresses(){
		$addresses = $this->fileGetContentsCurl($this->pageProperties['domain'].'addresses/getAllAddresses');
		
		echo $addresses;
	}
	
	public function getCalorexAddressesEstado($estado){
		//$estado = $this->uri->segment(3);
		$addresses = $this->fileGetContentsCurl($this->pageProperties['domain'].'addresses/getAllAddressesEstado/'.$estado);
		
		echo $addresses;
	}
	
	public function getCalorexAddressesMunicipio($municipio){
		//$municipio = $this->uri->segment(3);
		$addresses = $this->fileGetContentsCurl($this->pageProperties['domain'].'addresses/getAllAddressesMunicipio/'.$municipio);
		
		echo $addresses;
	}
	
	public function getIndexBackgrounds(){
		$bgs = $this->fileGetContentsCurl($this->pageProperties['domain'].'dashboard/getIndexBackgrounds');
		
		echo $bgs;
	}
	
	public function getComparadorSectors(){
		$brand = $this->uri->segment(3);
		$sectors = $this->fileGetContentsCurl('http://srgisazr1-0002.cloudapp.net/gis_api/comparador/getSectors/'.$brand);
		
		echo $sectors;
	}
	
	public function getComparadorProducts(){
		$sector = $this->uri->segment(3);
		$typeId = $this->uri->segment(4);
		$products = $this->fileGetContentsCurl('http://srgisazr1-0002.cloudapp.net/gis_api/comparador/getProducts/'.$sector . '/' .$typeId);
		
		echo $products;
	}
	
	public function getComparadorModels(){
		$product = $this->uri->segment(3);
		$models = $this->fileGetContentsCurl('http://srgisazr1-0002.cloudapp.net/gis_api/comparador/getModels/'.$product);
		
		echo $models;
	}
	
	public function getComparadorSpecs(){
		$model = $this->uri->segment(3);
		$specs = $this->fileGetContentsCurl('http://srgisazr1-0002.cloudapp.net/gis_api/comparador/getSpecs/'.$model);
		$specs = json_decode($specs);
		/*$specs = json_decode($specs);
		echo gettype($specs)."<br />";
		foreach($specs as $spec){
			var_dump($spec);
			echo "<br />";
		}*/
		echo $specs;
	}
	
	public function sendEmail(){
		$nombre = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("nombre"));

		$telefono = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("telefono"));
		
		$celular = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("celular"));

		 $lineaModelo = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("lineaModelo"));

		 $estado = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("estado"));
		 $ciudad = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("ciudad"));

		$email=$this->load->library('email'); // load email library
	
		$tipo = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("tipo"));


		$mensaje=htmlspecialchars($this->input->post("mensaje"), ENT_QUOTES);
		$msg = '';
		$msg .= '<strong>P&aacute;gina</strong>: '.ucfirst($this->pageProperties["skin"]).'<br />';
		$msg .= '<strong>Nombre</strong>: '.$nombre.'<br />';
		$msg .= '<strong>Correo Electr&oacute;nico</strong>: '.$this->input->post("email").'<br />';
		$msg .= '<strong>Tel&eacute;fono</strong>: '.$telefono.'<br />';
		$msg .= '<strong>Celular</strong>: '.$celular.'<br />';
		$msg .= '<strong>L&iacute;nea/Modelo</strong>: '.$lineaModelo.'<br />';
		$msg .= '<strong>Estado</strong>: '.$estado.'<br />';
		$msg .= '<strong>Ciudad</strong>: '.$ciudad.'<br />';
		$msg .= '<strong>Mensaje</strong>: '.$mensaje;
		
		
		
		$this->email->from('steven@softdepot.mx', 'Calorex');
		$this->email->to('socialmedia@gis.com.mx');
		//$this->email->to('steven@softdepot.mx');

		$this->email->subject("Solicitud de Atención - ".ucfirst($this->pageProperties["skin"]));
		$this->email->message($msg);
		$this->email->set_mailtype("html");
		$this->email->send();
		
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}
	

	
	public function sendEmail_dos(){
		$nombre = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("nombre"));

		$telefono = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("telefono"));
		
		$celular = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("celular"));

		 $lineaModelo = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("lineaModelo"));

		 $estado = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("estado"));
		 $ciudad = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("ciudad"));

		$email=$this->load->library('email'); // load email library
		
		$tipo = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("tipo"));
		
		
		$vende = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("vende_calentadores"));
		$segmento = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("segmento"));
		$tipo_distribucion = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("distri"));
		$cobertura = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("cob"));

		$punto_venta = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("punto"));

		
		
		$mensaje=htmlspecialchars($this->input->post("mensaje"), ENT_QUOTES);
		$msg = '';
		$msg .= '<strong>Nombre</strong>: '.$nombre.'<br />';
		$msg .= '<strong>Correo Electr&oacute;nico</strong>: '.$this->input->post("email").'<br />';
		$msg .= '<strong>Tel&eacute;fono</strong>: '.$telefono.'<br />';
		$msg .= '<strong>Celular</strong>: '.$celular.'<br />';
		$msg .= '<strong>Vende Calentadores: </strong>: '.$vende.'<br />';
		$msg .= '<strong>Segmento al que pertenece: </strong>'.$segmento.'<br />';
		$msg .= '<strong>Tipo de distribucion: </strong> '.$tipo_distribucion.'<br />';
		$msg .= '<strong>Cobertura territorial: </strong> '.$cobertura.'<br />';
		$msg .= '<strong>No punto de venta: </strong> '.$punto_venta.'<br />';

		$this->email->from('steven@softdepot.mx', 'Calorex');
		$this->email->to('socialmedia@gis.com.mx');
		//$this->email->to('steven@softdepot.mx');
		$this->email->subject($tipo); 
		$this->email->message($msg);
		$this->email->set_mailtype("html");
	    $this->email->send();

	}
	
	public function sendEmailGarantia(){
		$nombre = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("nombre"));

		$telefono = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("telefono"));
		
		$celular = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("celular"));

		 $lineaModelo = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("lineaModelo"));

		 $estado = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("estado"));
		 $ciudad = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("ciudad"));

		$email=$this->load->library('email'); // load email library
	
		$domicilio = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("domicilio"));
		$marca = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("marca"));
		$n_serie = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("n_serie"));
		$t_gas = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("t_gas"));
		$tienda = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("tienda"));
		$n_ticket = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("n_ticket"));
		$f_compra = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $this->input->post("f_compra"));
		$correo = $this->input->post("email");


		$mensaje=htmlspecialchars($this->input->post("mensaje"), ENT_QUOTES);
		$msg = '';
		$msg .= '<strong>P&aacute;gina</strong>: '.ucfirst($this->pageProperties["skin"]).'<br />';
		$msg .= '<strong>Nombre</strong>: '.$nombre.'<br />';
		$msg .= '<strong>Email</strong>: '.$correo.'<br />';
		$msg .= '<strong>Tel&eacute;fono</strong>: '.$telefono.'<br />';
		$msg .= '<strong>Celular</strong>: '.$celular.'<br />';
		$msg .= '<strong>Domicilio</strong>: '.$domicilio.'<br />';
		$msg .= '<strong>Estado</strong>: '.$estado.'<br />';
		$msg .= '<strong>Ciudad</strong>: '.$ciudad.'<br />';
		$msg .= '<strong>Marca</strong>: '.$marca.'<br />';
		$msg .= '<strong>Modelo</strong>: '.$lineaModelo.'<br />';
		$msg .= '<strong>N&uacute;mero de Serie</strong>: '.$n_serie.'<br />';
		$msg .= '<strong>Tipo de Gas</strong>: '.$t_gas.'<br />';
		$msg .= '<strong>Tienda</strong>: '.$tienda.'<br />';
		$msg .= '<strong>N&uacute;mero de Ticket</strong>: '.$n_ticket.'<br />';
		$msg .= '<strong>Fecha de compra</strong>: '.$f_compra.'<br />';
		
		
		
		$this->email->from('steven@softdepot.mx', 'Calorex');
		$this->email->to('socialmedia@gis.com.mx');
		$this->email->to('l.hernandez@softdepot.mx');

		$this->email->subject("Solicitud de Garantía - ".ucfirst($this->pageProperties["skin"]));
		$this->email->message($msg);
		$this->email->set_mailtype("html");
		$this->email->send();
		
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}
	
	
}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */