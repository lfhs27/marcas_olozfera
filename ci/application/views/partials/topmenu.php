<header>
	<nav id="top-links" class="hidden-xs horizontal clearfix">
		<div class="container">
			<ul class="pull-right"> 
				<li><a href="<?=base_url()?>distribuidores" title="&iquest;D&oacute;nde comprar?" style="<?=(strcmp($pageProperties["skin"], "calorex") == 0) ? "" : "text-shadow:none;"?>">&iquest;D&oacute;nde comprar?</a></li>
				<li><a href="<?=base_url()?>nosotros" style="<?=(strcmp($pageProperties["skin"], "calorex") == 0) ? "" : "text-shadow:none;"?>">Nosotros</a></li>
				<li><a href="<?=base_url()?>blog" title="Blog" style="<?=(strcmp($pageProperties["skin"], "calorex") == 0) ? "" : "text-shadow:none;"?>">Blog</a></li>
				<li><a href="<?=base_url()?>solicita_ayuda" style="<?=(strcmp($pageProperties["skin"], "calorex") == 0) ? "" : "text-shadow:none;"?>">Solicita Ayuda</a></li>
				<li><a href="<?=base_url()?>extiende_garantia" style="<?=(strcmp($pageProperties["skin"], "calorex") == 0) ? "" : "text-shadow:none;"?>">&iexcl;Extiende tu Garantía!</a></li>
				<li>
					<form action="/buscar" method="GET">
					<?php
					$x_query="";
					 if (isset($_GET["q"])){ 
							$query=$this->input->get("q");
						    $x_query = (string) preg_replace('/[^A-Z0-9_áéíóúÁÉÍÓÚÑñ\s]/i', '', $query);

					 }
					 ?>
						<input type="search" placeholder="&#57347;" class="form-control glyphicon" name="q" placeholder="Buscar..." value="<?php echo$x_query;?>" style="position:relative;bottom:4px;height:25px;top: -4px;" />
					</form>

					
				</li>
				<!-- <li><i class="fa fa-phone"></i> 01 800 55 CINSA</li> -->
			</ul>
		</div>
	</nav><!-- /#top-links -->

	<nav class="navbar navbar-default" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo site_url(); ?>"><img style="margin-top:3px;" src="<?php echo $pageProperties['logo']; ?>"></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<?php $this->load->view('partials/navigation.php',['navigation' => $navigation]); ?>
			</div><!--/.nav-collapse -->
		</div>
	</nav>
</header>