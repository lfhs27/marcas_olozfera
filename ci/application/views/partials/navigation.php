<link rel="icon" href="http://www.calorex.com.mx/img/calorex/favicon.ico">
<?php 
$itemCnt = 1;
 ?><ul class="nav navbar-nav navbar-right">
	<li class="visible-xs responsibe_clase" style="padding-top:20px;">
		<a href="#" class="toggle-panel <?=strcmp($pageProperties["skin"], "calorex") == 0 ? 'calorex-txt' : 'cinsa-txt'?>" title="Línea Residencial" style="">Línea Residencial</a>
		<?php foreach($navigation->brand->families as $family): ?>
		<?php
		 $family->name=strtolower($family->name);
	     $nombre = str_replace("deposito", "DEP&oacuteSITO",$family->name);
		 $nombre = str_replace("instantaneo", "INSTANT&aacuteNEO",$nombre);
          ?>
		<li class="visible-xs responsibe_clase"><a href="<?php echo base_url('calentadores/' . strtolower(remove_accents($navigation->brand->name)) . '/' . strtolower(remove_accents($family->name))); ?>"><?php echo $nombre; ?></a></li>
		<?php endforeach; ?>
	</li>
	<script>
	$(document).ready(function() {
    var $window = $(window);

    function checkWidth() {
        var windowsize = $window.width();
        if (windowsize < 1198) {
			  $(".responsibe_clase").removeClass('visible-xs');
              $(".responsibe_clase").css('display','block');
        }else{
              $(".responsibe_clase").css('display','none');
		}
    } 
    checkWidth();
    $(window).resize(checkWidth);
});
</script>
 <?php 
 // shuffle($navigation->brand->families);
  ?>
	<li class="hidden-xs">
		<li><a href="/calentadores/<?=$pageProperties["skin"]?>/deposito">DEP&Oacute;SITO</a></li>
		<li><a href="/calentadores/<?=$pageProperties["skin"]?>/de-paso">De Paso</a></li>
		<li><a href="/calentadores/<?=$pageProperties["skin"]?>/instantaneo">INSTANT&aacuteNEOS</a></li>
		<li><a href="/calentadores/<?=$pageProperties["skin"]?>/solar">Solares</a></li>
		<?php if(strcmp($pageProperties["skin"], "calorex") == 0): ?>
		<!--li><a href="/calentadores/calorex/calefactores">Calefactores</a></li-->
		<?php endif; ?>
	<?php if($navigation): ?>
		<?php foreach($navigation->brand->families as $family): ?>
			<?php if(strcmp($family->name, "DEPOSITO" ) == 0 || 
					strcmp($family->name, "DE PASO" ) == 0 || 
					strcmp($family->name, "INSTANTANEO" ) == 0 || 
					strcmp($family->name, "CALEFACTORES" ) == 0 || 
					strcmp($family->name, "SOLAR" ) == 0 ): ?>
			<!--li><a href="<?php echo base_url('calentadores/'.strtolower($navigation->brand->name).'/' . strtolower($family->name)) ?>"><?php echo $family->name ?></a></li-->
			<?php endif; ?> 
		<?php endforeach; ?>
	<?php endif; ?> 
	</li>
	<li class="boiler-finder hidden-xs"><a href="<?=base_url()?>nosotros" class="porque_cl">&iquest;POR QU&Eacute; <?=$pageProperties["skin"]?>?</a></li>
	<li class="boiler-finder-xs visible-xs"><a href="<?=base_url()?>nosotros">&iquest;POR QU&Eacute; <?=$pageProperties["skin"]?>?</a></li>
	<li class="visible-xs"><a href="<?=base_url()?>distribuidores" title="&iquest;D&oacute;nde comprar?">&iquest;D&oacute;nde comprar?</a></li>
	<li class="visible-xs"><a href="<?=base_url()?>nosotros">Nosotros</a></li>
	<li class="visible-xs"><a href="<?=base_url()?>blog">Blog</a></li>
	<li class="visible-xs"><a href="<?=base_url()?>solicita_ayuda">Ayuda</a></li>
	<li class="visible-xs">
		<form action="/buscar" method="GET">
			<input type="text" class="form-control glyphicon" name="q" placeholder="&#57347;" value="<?=isset($_GET["q"]) ? $this->input->get("q") : ""?>" style="position:relative;bottom:4px;height:25px;" />
		</form>
	</li>
   <li class="visible-xs">

   </li>

	<!-- <li class="hidden-xs search">
		<a href="#"><i class="fa fa-search"></i></a>
	</li> -->
</ul>					