<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<nav class="horizontal clearfix">
					<ul>
						<li><a href="http://www.gis.com.mx/" target="_blank">GIS</a></li>
						<li><a href="<?=base_url()?>aviso_privacidad">Aviso de Privacidad</a></li>
					</ul>
				</nav>
			</div>
			
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<nav class="horizontal clearfix" style="text-align:center;">
					<?php if(strcmp($pageProperties["skin"], "calorex") == 0): ?>
					CALOREX&reg; - UN BUEN BA&Ntilde;O TE CAMBIA EL D&Iacute;A
					<?php else: ?>
					<?=strtoupper("Cinsa – Confiable como tÚ")?>
					<?php endif; ?>
				</nav>
			</div>

			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<nav class="horizontal social clearfix">
					<ul class="pull-right socialv2">
						<li style="padding-right:0px;">
							<?php if(strcmp($pageProperties["skin"], "calorex") == 0): ?>
							<a href="https://www.facebook.com/calorex" target="_blank"><img src="<?=base_url()?>img/ic-facebook.png" alt="Facebook" style="position:relative;bottom:8px;margin:10px; margin-right: 0px;" /></i></a>
							<a href="https://twitter.com/calorexmx" target="_blank"><img src="<?=base_url()?>img/ic-twitter.png" alt="Twitter" style="position:relative;bottom:8px;margin:10px;" /></a>
							<?php else: ?>
							<a href="https://www.facebook.com/CinsaBoilers/" target="_blank"><img src="<?=base_url()?>img/ic-facebook.png" alt="Facebook" style="position:relative;bottom:8px; margin-right: 0px;" /></i></a>
							<?php endif; ?>
						</li>
					</ul>
					<label class="hidden-xs pull-right">S&iacute;guenos</label>
				</nav>
			</div>
		</div>
	</div>
</footer>