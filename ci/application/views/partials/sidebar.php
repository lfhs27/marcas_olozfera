				<div class="hidden-xs col-sm-3 col-md-3 col-lg-3" id="sidebar">
					<?php
					/*print_r("<pre>");
					print_r($navigation);
					print_r("</pre>");*/
					?>
					<nav>
						<ul>
							<?php foreach($navigation->brand->families as $family): ?>
							<?php if (count($family->lines) > 0 && strcmp($family->slug, "calefactores") != 0 && strpos($family->slug, "titanio") === false && strpos($family->slug, "refacciones") === false): ?>
							<li>
								<!--a href="<?php echo base_url('calentadores/' . $navigation->brand->slug . '/' . $family->slug); ?>"><?php echo $family->front_name; ?></a-->
								<a href="#" class="toggle-panel"><?php echo $family->front_name; ?></a>
								
								<?php if(COUNT($family->lines) > 0): ?>
								<div class="submenu" style="display: block;">
									<ul>
										<?php foreach($family->lines as $line): ?>
										<?php if(!strpos($line->name, "GEN 2")): ?>
										<li><a href="<?php echo base_url('calentadores/' . $navigation->brand->slug . '/' . $family->slug . '/' . $line->slug); ?>"><?php echo $line->name; ?></a></li>
										<?php endif; ?>
										<?php endforeach; ?>
									</ul>
								</div>
								<?php endif; ?>
							</li>
							<?php endif ?>
							<?php endforeach; ?>
							<!--li><a href="https://www.tiendacalorex.com/calentador-ideal" class="calentador-ideal" target="_blank">&iquest;Quieres saber si es ideal para ti?</a></li-->
							<li>
								<div class="banners">
									<?php 
									if(count($banners) > 0):
										foreach($banners as $banner): 
									?>
									<a href="<?=$banner->link?>"><img src="<?=$pageProperties["domain"].$banner->src?>" alt="GIS Banner" style="width:100%;border-radius:15px;padding-top:10px;" /></a>
									<?php 
										endforeach; 
									endif;
									?>
								</div>
							</li>
						</ul>
						<!--
						<ul>
							<li>
								<a href="#" class="toggle-panel" title="Línea Residencial">Línea Residencial</a>
								<div class="submenu" style="display:block;">
									<ul>
										<?php foreach($navigation->brand->families as $family): ?>
										<?php //if (count($family->products) > 0): ?>
										<li>
											<a href="#" class="toggle-panel"><?php echo $family->front_name; ?><i class="fa fa-angle-down"></i></a>
											
											<?php if(COUNT($family->lines) > 0): ?>
											<div class="submenu" style="display: block;">
												<ul>
													<?php foreach($family->lines as $line): ?>
													<li><a href="<?php echo base_url('calentadores/' . $navigation->brand->slug . '/' . $family->slug . '/' . $line->slug); ?>"><?php echo $line->name; ?></a></li>
													<?php endforeach; ?>
												</ul>
											</div>
											<?php endif; ?>
										</li>
										<?php //endif ?>
										<?php endforeach; ?>
									</ul>
								</div>
								<div class="banners">
									<?php 
									if(count($banners) > 0):
										foreach($banners as $banner): 
									?>
									<a href="<?=$banner->link?>"><img src="<?=$pageProperties["domain"].$banner->src?>" alt="GIS Banner" style="width:100%;border-radius:15px;padding-top:10px;" /></a>
									<?php 
										endforeach; 
									endif;
									?>
								</div>
							</li>
						</ul> -->
					</nav>

					<!-- <div class="banners">
						<div class="banner">
							<a href="#"><img src="http://softdepotserver2.com/base/img/banner1.png" class="img-responsive" alt="Banner 1"></a>
						</div>

						<div class="banner">
							<a href="#"><img src="http://softdepotserver2.com/base/img/banner2.png" class="img-responsive" alt="Banner 2"></a>
						</div>
					</div> -->
				</div>