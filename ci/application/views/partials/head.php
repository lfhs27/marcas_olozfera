  <?php if($pageProperties["skin"] == "calorex"): ?>
	<meta name="description" content="Calorex 70 años dando buenos baños que cambian el día de toda tu familia, con agua a la temperatura ideal, de forma inmediata y contínua.">
  <?php else: ?>
  	<meta name="description" content="Descubre el confort de un buen baño con agua caliente. Bríndale a tu familia comodidad y bienestar. Entra ahora y encuentra el calentador ideal para ti.">
  <?php endif; ?>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta property="og:title" content="<?=ucfirst($pageProperties["skin"])?>" />
  <meta property="og:url" content="<?=base_url()?>" />
  <meta property="og:image" content="http://cinsa.softdepotserver2.com/img/<?=$pageProperties["skin"]?>/logo<?=strtoupper($pageProperties["skin"])?>.png" />
  <meta property="og:image:url" content="http://cinsa.softdepotserver2.com/img/<?=$pageProperties["skin"]?>/logo<?=strtoupper($pageProperties["skin"])?>.png" />
  <meta property="og:image:type" content="image/png" />
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo base_url('css/font-awesome.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('css/bootstrap.css'); ?>">
	<!-- <link rel="stylesheet" href="http://owlgraphic.com/owlcarousel/owl-carousel/owl.carousel.css"> -->
	<link rel="stylesheet" href="<?php echo base_url('owl.carousel.2.0.0-beta.2.4/assets/owl.carousel.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('css/brand.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('css/skins/' . $skin . '.css'); ?>">
	<!-- <link rel="stylesheet" href="http://owlgraphic.com/owlcarousel/owl-carousel/owl.theme.css"> -->
    <link rel="stylesheet" href="<?=base_url()?>css/ammap.css" type="text/css">

	<script src="<?php echo base_url('js/jquery-1.11.3.min.js'); ?>"></script>
	<script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('js/jquery.cycle2.min.js'); ?>"></script>
	<script src="<?php echo base_url('js/jquery.cycle2.video.js'); ?>"></script>
	<!-- <script src="http://owlgraphic.com/owlcarousel/owl-carousel/owl.carousel.min.js"></script> -->
	<script src="<?php echo base_url('owl.carousel.2.0.0-beta.2.4/owl.carousel.min.js'); ?>"></script>
	<script src="<?php echo base_url('js/brand.js'); ?>"></script>
	<script src="<?=base_url()?>js/ammap.js" type="text/javascript"></script>
	<script src="<?=base_url()?>js/maps/js/mexicoLow.js" type="text/javascript"></script>
	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.js" type="text/javascript"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<?php if($pageProperties["skin"] == "calorex"): ?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	 
	  ga('create', 'UA-43931538-2', 'auto');
	  ga('send', 'pageview');
	 
	</script>
	<?php endif; ?>

	<?php if($pageProperties["skin"] == "cinsa"): ?>
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			  ga('create', 'UA-43931538-3', 'auto');
			  ga('send', 'pageview');
			</script>
	<?php endif; ?>
<style type="text/css">
#s1-chat-status-container * {direction:ltr;text-align:left;;font-family:arial;font-size:12px;box-sizing: content-box;zoom:1;margin:0;padding:0}
#s1-chat-status-container .s1-status-icon{text-decoration:none;font-size:12px;font-weight:bold;color:#000000;display:block;padding:10px 10px 10px 35px;background:url('//contactocc.s1gateway.com/integrations/chats/chat_calorex/images/user_green_chat.png') no-repeat left center}
#s1-chat-status-container{box-sizing: content-box;-webkit-border-top-left-radius: 20px;-moz-border-radius-topleft: 20px;border-top-left-radius: 20px;-webkit-box-shadow: -1px -1px 5px rgba(50, 50, 50, 0.17);border:1px solid #e3e3e3;border-right:0;border-bottom:0;;-moz-box-shadow:-1px -1px 5px rgba(50, 50, 50, 0.17);box-shadow: -1px -1px 5px rgba(50, 50, 50, 0.17);padding:5px 0px 0px 5px;width:190px;font-family:arial;font-size:12px;transition: 1s;position:fixed;bottom:0;right:0;;background-color:#f6f6f6;z-index:9989;}
@media only screen and (max-width : 640px) {#s1-chat-status-container{position:relative;top:0;right:0;bottom:0;left:0;width:auto;border-radius:2px;box-shadow:none;border:1px solid #e3e3e3;margin-bottom:5px;}}
</style>

<div id="s1-chat-status-container"><a class="s1-status-icon" href="#" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 && window.event.preventDefault) window.event.preventDefault(); this.chat = window.open('https://contactocc.s1gateway.com/integrations/chats/chat_calorex/index.html', 'S1Gateway', 'toolbar=0,scrollbars=0,location=0,status=0,menubar=0,width=402,height=590,resizable=0'); this.chat.focus(); var windowwidth = 402; var windowheight = 590; var screenwidth = screen.availWidth; var screenheight = screen.availHeight; this.chat.moveTo(screenwidth - windowwidth,screenheight - windowheight);this.chat.opener=window;return false;">Ayuda en línea</a></div>
