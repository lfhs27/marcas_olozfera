<?php
/*print_r("<pre>");
print_r($product->brand_icons);
print_r("</pre>");*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>	
<style>
#compararModal .comparador tbody td{
	padding: 5px 5px;
}
#compararModal .comparador tbody tr:nth-child(even){
	background-color:#ddd;
}
</style>
</head>
<body id="template-catalogo" class="template-interior template-catalogo-producto template-header-sm">
	<?php $this->load->view("partials/topmenu"); ?>



	<section id="page-content">
		<div class="container">
			<div class="row" id="product-overview">
				<section id="breadcrumb" class="hidden-xs" style="height: 50px;">
					<div class="container">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=base_url()?>"><?php echo ucfirst(strtolower($navigation->brand->front_name)); ?></a>
								</li>
								<li>
									<a href="<?=base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."/".$this->uri->segment(3)?>"><?=ucwords(str_replace('|', '/', str_replace('_', '-', str_replace('-', ' ', $this->uri->segment(3)))))?></a>
								</li>
								<?php if(!$this->uri->segment(5)): ?>
								<!--li class="active"><?php echo $product->name; ?></li-->
								<li class="active"><?=str_replace('|', '/', str_replace('_', '-', str_replace('-', ' ', $this->uri->segment(4))))?></li>
								<?php else: ?>
								<li><a href="<?=base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."/".$this->uri->segment(3)."/".$this->uri->segment(4)?>"><?=ucwords($product->name)?></a></li>
								<li class="active"><?=str_replace('|', '/', str_replace('_', '-', str_replace('-', ' ', $this->uri->segment(5))))?></li>
								<?php endif; ?>
							</ol>	
						</div>
					</div>
				</section>
				<div class="col-xs-12 col-sm-6">
					<div class="product-images clearfix">
						<div class="main-image cycle-slideshow"  data-cycle-center-horz="true" data-cycle-manual-fx="scrollHorz" data-cycle-timeout="0" data-cycle-pager=".thumbnails-nav" data-cycle-youtube="true" data-cycle-youtube-autostart="false" data-cycle-slides=">a,>img" data-cycle-pager-template="">
							
							<?php if(isset($product->gallery->images)): ?>
							<?php foreach($product->gallery->images as $img): ?>
								
								<img src="<?=$pageProperties["domain"]?><?php echo $img->filepath . '/' . $img->filename; ?>" class="img-responsive">
								
							<?php endforeach; ?>
							<?php endif; ?>
							
							<?php if(strcmp($product->video, "") != 0): ?>
							<a href="https://www.youtube.com/embed/<?=$product->video?>?autoplay=0" data-cycle-pager-template="<a href='#'>Sunset</a>"></a>
							<?php endif; ?>
						</div> 
						<div class="thumbnails-nav hidden-xs">
						
						<?php if(isset($product->gallery->images)): ?>
						<?php foreach($product->gallery->images as $img): ?>
							<a href='#'><img class='img-responsive' src='<?=$pageProperties["domain"]?><?php echo $img->filepath . '/' . $img->filename; ?>' width='115'></a>							
						<?php endforeach; ?>
						<?php endif; ?>
						
						<?php if(strcmp($product->video, "") != 0): ?>
							<a href='#'><img class='img-responsive' src='/img/play.png' width='115'></a>	
						<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="overview-content">
						<h2><?php echo $product->name; ?></h2>
						<h3><?php echo $product->description; ?></h3>

						<div class="well">
							<h5>Modelo: <strong><?php echo $product->short_name. " - ".$product->id; ?></strong></h5>
							<ul id="model-selector" class="clearfix">
								<!-- <li><a class="selected" href="#">COXDP-06</a></li> -->
								<?php if(isset($siblings)): ?>
									<?php foreach($siblings as $sibling): ?>
										<li><a data-id="<?php echo $sibling->id; ?>" <?php echo $sibling->id == $product->id ? ' class="selected" ' : ''; ?> href="<?php echo base_url('calentadores/' . $sibling->brand->slug. '/' . $sibling->family->slug. '/' . $sibling->line->slug. '/' . $sibling->slug); ?>"><?php echo $sibling->short_name; ?></a></li>
									<?php endforeach; ?>
								<?php endif; ?>
							</ul>
						</div>

						<div class="add-cart-form">
							<form method="post" action="">
								<a href="<?=base_url()?>distribuidores" class="btn btn-primary btn-lg">¿Dónde Comprar?</a> 
							</form>
						</div>
						<!-- <div class="text-center">
							ó
						</div> -->
						<!--a class="btn btn-info btn-lg btn-white" style="margin-top:10px;" onclick="comparar(<?=$product->brand_id?>, <?=$product->family_id?>, <?=$product->id?>, 'tipo')">Comparar</a-->
					</div><!-- .overview-content -->
				</div>
			</div>
		</div>
	</section>

	<section id="product-detail">
		<div class="container">
			<div role="tabpanel">
			    <!-- Nav tabs -->
			    <ul class="nav nav-tabs" role="tablist">
			        <li role="presentation" class="active">
			            <a href="#tab-description" aria-controls="tab-description" role="tab" data-toggle="tab">Descripción</a>
			        </li>
			        <li role="presentation">
			            <a href="#tab-specs" aria-controls="tab-specs" role="tab" data-toggle="tab">Especificaciones</a>
			        </li>
			    </ul>
			
			    <!-- Tab panes -->
			    <div class="tab-content">
			        <div role="tabpanel" class="tab-pane active" id="tab-description">
			        	<div class="tabpanel-content row" style="padding-top:50px;">
							<div class="row">
								<!--div class="col-xs-12 col-sm-3">
									<?php if($product->blueprint): ?>
									<img src="https://www.tiendagis.com/<?=$product->blueprint->filepath .'/'. $product->blueprint->filename?>" alt="" class="img-responsive">
									<?php endif; ?>
								</div-->
								<div class="col-xs-12 col-sm-12">
									<?php if(COUNT($product->brand_icons) > 0): ?>
									<table>
									<?php foreach($product->brand_icons as $icon): ?>
										<tr>
											<td><img src="https://www.tiendagis.com/<?=$icon->icon?>" alt="icon" style="width:75px;margin-right: 20px;margin-bottom:20px;" /></td>
											<td><?=$icon->description?></td>
										</tr>
									<?php endforeach; ?>
									</table>
									<?php endif; ?>
									<?php if(isset($product->description_html)): ?>
									<?=$product->description_html?>
									<?php endif; ?>
								</div>
			        		</div>
			        	</div>
			        </div>
			        <div role="tabpanel" class="tab-pane" id="tab-specs">
			        	<div class="tabpanel-content" style="padding-top:50px;">
			        		<div class="row">
			        			<div class="col-xs-12 col-sm-6 col-sm-push-6">
			        				<div id="product-specs">
			        					<h4 class="text-center">Tabla de Especificaciones</h4>
			        					<?php $modelSpecifications = $product->specifications; ?>
			        					<table class="table table-striped table-hover">
			        						<tbody>
												<?php 
													if($modelSpecifications):
													foreach($modelSpecifications as $modelSpec): 
													if(strcmp($modelSpec->value, "N/A") != 0):
												?>
													<tr>
														<td><?php echo $modelSpec->name ?></td>
														<td><?php echo $modelSpec->value ?></td>
													</tr>
												<?php 
													endif;
													endforeach; 
													endif;
												?>
											</tbody>
			        					</table>	
			        				</div>
			        			</div>
								<?php if(strcmp($this->uri->segment(3), "calefactores") != 0): ?>
			        			<div class="col-xs-12 col-sm-6 col-sm-pull-6">
			        				<div id="product-applications">
			        					<h4>Servicios Simultáneos</h4>
			        					<!-- <table class="table table-striped label-meaning">
											<tr style="background-color: transparent;">
												<td style="width: 140px; padding-left: 0; color: #f44336" class="off"><i class="fa fa-circle" style="color: #f44336;"></i> Suficiente</td>
												<td class="on" style="color: #f44336;"><i class="fa fa-circle" style="color: #f44336;"></i> Óptimo</td>
											</tr>
										</table> -->

										<div class="row">
											<div class="col-xs-3">
												<ul class="applications-info">
													<li class="on">
														<img src="<?php echo base_url('/img/'.$pageProperties["skin"].'/icS-regadera'.($pageProperties["skin"]=="calorex"?"CX":"CN").'.png'); ?>">
														<span class="balloon"><?php echo $product->num_regaderas; ?></span>
														<span class="label">Regaderas</span>
													</li>
													<!-- <li class="off">
														<img src="<?php echo base_url('/img/calorex/icS-regaderaCX.png'); ?>">
														<span class="balloon"><?php echo $product->num_lavabos; ?></span>
														<span class="label">Lavabos</span>
													</li>
													<li class="off">
														<img src="<?php echo base_url('/img/calorex/icS-lavamanosCX.png'); ?>">
														<span class="balloon"><?php echo $product->num_tina; ?></span>
														<span class="label">Regaderas</span>
													</li> -->
												</ul>
											</div>
											<div class="col-xs-3">
												<ul class="applications-info">
													<li class="on">
														<img src="<?php echo base_url('/img/'.$pageProperties["skin"].'/icS-lavamanos'.($pageProperties["skin"]=="calorex"?"CX":"CN").'.png'); ?>">
														<span class="balloon"><?php echo $product->num_lavabos; ?></span>
														<span class="label">Lavabos</span>
													</li>
												</ul>	
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 servicios-nota">Considera 1 servicio por cada regadera en uso simultáneo y ½ por cada lavabo</div>
										</div>
			        				</div>
									<?php endif; ?>
									<?php if(COUNT($product->downloads) > 0): ?>
									<div class="product-files">
			        					<h4>Descargar</h4>
										<?php foreach($product->downloads as $download): ?>
											<?php if(strcmp($pageProperties["skin"], "cinsa") == 0 && $download->type != 'MANUAL'): ?>
											
											<?php else: ?>
			        						<a data-download-type="<?php echo $download->type; ?>" target="_blank" href="https://www.tiendagis.com/<?php echo $download->filepath . '/' . $download->filename; ?>" class="btn btn-lg btn-primary"><i class="fa fa-arrow-down"></i>
												<?=($download->type == 'MANUAL' ? 'Manual de usuario' : 'Catálogo')?>
											</a>
											<?php endif; ?>
										<?php endforeach; ?>
			        				</div>
									<?php endif; ?>
			        			</div>
			        		</div>
			        	</div>
			        </div>
			    </div>
			</div>
		</div>	
	</section>
	
	<?php $this->load->view("partials/lowermenu"); ?>


<div class="modal fade" id="compararModal" tabindex="-1" role="dialog" aria-labelledby="Comparar">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Comparador</h4>
			</div>
			<div class="modal-body">
				<table class="comparador">
					<thead>
						<tr>
							<td style="width:25%;" >Especificaciones</td>
							<td style="width:25%" >
								<select name="brand" onchange="loadSectors(this)" class="form-control" style="width:49%;display:none;padding:6px;">
									<option value="0">Marca</option>
									<?php foreach($brands as $brand): ?>
										<option value="<?=$brand->id?>" <?=strcmp(strtolower($brand->name), $pageProperties["skin"]) == 0 ? "selected" : ""?>><?=$brand->name?></option>
									<?php endforeach; ?>
								</select>
								<select name="sector" onchange="loadProducts(this,<?php echo $product->type_id; ?>)" class="form-control" style="display:block;padding:6px;">
									<option value="0">Sector</option>
								</select>
								<select name="product" onchange="loadModels(this)" class="form-control" style="display:block;padding:6px;">
									<option value="0">Producto</option>
								</select>
								<select name="model" onchange="loadSpecs(this, 1)" class="form-control" style="display:block;padding:6px;">
									<option value="0">Modelo</option>
								</select>
							</td>
							<td style="width:25%" >
								<select name="brand" onchange="loadSectors(this)" class="form-control" style="width:49%;display:none;padding:6px;">
									<option value="0">Marca</option>
									<?php foreach($brands as $brand): ?>
										<option value="<?=$brand->id?>" <?=strcmp(strtolower($brand->name), $pageProperties["skin"]) == 0 ? "selected" : ""?>><?=$brand->name?></option>
									<?php endforeach; ?>
								</select>
								<select name="sector" onchange="loadProducts(this,<?php echo $product->type_id; ?>)" class="form-control" style="display:block;padding:6px;">
									<option value="0">Sector</option>
								</select>
								<select name="product" onchange="loadModels(this)" class="form-control">
									<option value="0">Producto</option>
								</select>
								<select name="model" onchange="loadSpecs(this, 2)" class="form-control">
									<option value="0">Modelo</option>
								</select>
							</td>
							<td style="width:25%" >
								<select name="brand" onchange="loadSectors(this)" class="form-control" style="width:49%;display:none;padding:6px;">
									<option value="0">Marca</option>
									<?php foreach($brands as $brand): ?>
										<option value="<?=$brand->id?>" <?=strcmp(strtolower($brand->name), $pageProperties["skin"]) == 0 ? "selected" : ""?>><?=$brand->name?></option>
									<?php endforeach; ?>
								</select>
								<select name="sector" onchange="loadProducts(this,<?php echo $product->type_id ?>)" class="form-control" style="display:block;padding:6px;">
									<option value="0">Sector</option>
								</select>
								<select name="product" onchange="loadModels(this)" class="form-control">
									<option value="0">Producto</option>
								</select>
								<select name="model" onchange="loadSpecs(this, 3)" class="form-control">
									<option value="0">Modelo</option>
								</select>
							</td>
						</tr>
					</thead>
					<tbody>
						<tr data-spec="1">
							<td>Tipo de Gas</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="2">
							<td>Recomendación por no. de regaderas simultáneas*</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="3">
							<td>Capacidad (L)</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="4">
							<td>Capacidad a nivel del mar (L/min)</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="5">
							<td>Capacidad a nivel de la Cd. de México (L/min)</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="6">
							<td>Altura total (cm)</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="7">
							<td>Peso (kg) </td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="8">
							<td>Ancho (cm)</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="9">
							<td>Fondo (cm)</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="10">
							<td>Diámetro (cm) </td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="11">
							<td>Presión de gas requerida Gas L.P.</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="12">
							<td>Presión hidráulica máxima de trabajo (kg/cm2 )</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="13">
							<td>Eficiencia promedio</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="14">
							<td>Quemador</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="15">
							<td>Tipo de Encendido</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="16">
							<td>Aislante térmico</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="17">
							<td>Recubrimiento interior del tanque</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="16">
							<td>Aislante</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="18">
							<td>Válvula de alivio (psi)</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>

						<tr data-spec="19">
							<td>Garantía</td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
							<td style="text-align:center;"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
$(document).ready(function(){
	$.each($("select[name=sector]"), function(i, val){
		var elm = $(val);
		brand = elm.siblings("select[name=brand]").val();
		
		$.ajax({
			url: "/main/getComparadorSectors/"+brand,
			method: "get",
			dataType: 'json'
		}).done(function(data) {
			html = '<option value="0">Sector</option>';
			$.each(data, function(i, val){
				html += '<option value="'+val.id+'">'+val.front_name+'</option>';
			});
			elm.html(html);
		});
	});
});
function comparar(brand, sector, product, productTypeId){
	console.log('Brand: ' + brand);
	console.log('Sector: ' + sector);
	console.log('Product: ' + product);
	console.log('productTypeId: ' + productTypeId);

	$.ajax({
		url: "/main/getComparadorSectors/"+brand,
		method: "get",
		dataType: 'json'
	}).done(function(data) {
		html = '<option value="0">Sector</option>';
		$.each(data, function(i, val){
			html += '<option value="'+val.id+'">'+val.front_name+'</option>';
		});
		$("[name=sector]").first().html(html).val(sector);
		
		$.ajax({
			url: "/main/getComparadorProducts/"+sector+"/"+productTypeId,
			method: "get",
			dataType: 'json'
		}).done(function(data) {
			html = '<option value="0">Producto</option>';
			$.each(data, function(i, val){
				html += '<option value="'+val.id+'">'+val.name+'</option>';
			});
			$("[name=product]").first().html(html).val(product);
			$("[name=brand]").first().val(brand);
			
			$.ajax({
				url: "/main/getComparadorModels/"+product,
				method: "get",
				dataType: 'json'
			}).done(function(data) {
				html = '<option value="0">Modelo</option>';
				$.each(data, function(i, val){
					html += '<option value="'+val.id+'">'+val.name+'</option>';
				});
				$("[name=model]").first().html(html);
				$("[name=model]").first().val($("ul#model-selector li a.selected").attr("data-id"));
				loadSpecs($("[name=model]").first().get(0), 1);
			});
			
		});
	});
	$("#compararModal").modal("show");
}
function loadSectors(elm){
	elm = $(elm);
	id = elm.val();
	$.ajax({
		url: "/main/getComparadorSectors/"+id,
		method: "get",
		dataType: 'json'
	}).done(function(data) {
		html = '<option value="0">Sector</option>';
		$.each(data, function(i, val){
			html += '<option value="'+val.id+'">'+val.front_name+'</option>';
		});
		elm.siblings("[name=sector]").html(html);
	});
}
function loadProducts(elm,typeId){
	elm = $(elm);
	id = elm.val();
	$.ajax({
		url: "/main/getComparadorProducts/"+id+"/"+typeId,
		method: "get",
		dataType: 'json'
	}).done(function(data) {
		html = '<option value="0">Producto</option>';
		$.each(data, function(i, val){
			html += '<option value="'+val.id+'">'+val.name+'</option>';
		});
		elm.siblings("[name=product]").html(html);
	});
}
function loadModels(elm){
	elm = $(elm);
	id = elm.val();
	$.ajax({
		url: "/main/getComparadorModels/"+id,
		method: "get",
		dataType: 'json'
	}).done(function(data) {
		html = '<option value="0">Modelo</option>';
		$.each(data, function(i, val){
			html += '<option value="'+val.id+'">'+val.name+'</option>';
		});
		elm.siblings("[name=model]").html(html);
	});
}
function loadSpecs(elm, col){
	elm = $(elm);
	id = elm.val();
	$.ajax({
		url: "/main/getComparadorSpecs/"+id,
		method: "get",
		dataType: 'json'
	}).done(function(data) {
		console.log(data);
		dataSpecs = $("[data-spec]");
		$.each(data, function(i, val){
			specName = val.name.toLowerCase().replace(/ /g, "_");
			
			console.log("tr[data-spec="+val.id+"] td:nth-child("+(col+1)+")");
			$("tr[data-spec="+val.id+"] td:nth-child("+(col+1)+")").html(val.value);
		});
		/*$.each(dataSpecs, function(i, val){
			console.log($(val).attr("data-spec"));
		});*/
	});
}
</script>
</body>
</html>