<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>	
</head>
<body id="template-default" class="template-with-forms" style="background-image: url(<?php echo $pageProperties['bodyBackground']; ?>); background-repeat: no-repeat;">
	
	<?php $this->load->view("partials/topmenu"); ?>

	<section id="info-nav">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 info-nav-mg">
					<nav class="nav-container">
						<a href="<?=base_url()?>solicita_ayuda" class="btn btn-primary">Solicita tu Servicio</a>
						<a href="<?=base_url()?>solicita_asesoria" class="btn btn-primary">Asesor&iacute;a T&eacute;cnica</a>
						<a href="<?=base_url()?>solicita_dudas" class="btn btn-primary">Dudas de Producto</a>
						<a href="<?=base_url()?>solicita_garantia" class="btn btn-primary">Registro de Garant&iacute;a</a>
						<a href="<?=base_url()?>solicita_distribuidor" class="btn btn-primary btn-current">Quiero ser Distribuidor</a>
					</nav>
				</div>
			</div>
		</div>
	</section>

	<section id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div id="info-content">
						<h1>M&aacute;s de 60 a&ntilde;os brindando calor de hogar</h1>

						<p>
							Calorex est&aacute; presente en toda la Rep&uacute;blica Mexicana. ¿Quieres ser parte de nuestra red de distribuidores? &iexcl;Cont&aacute;ctanos!
						</p>

						<div class="info-tel">
							<i class="fa fa-phone rounded"></i>
							<p>
								01 800 CALOREX<br>
								D.F. 56 40 06 01<br>
								01 800 225 6739
							</p>
						</div>

						<p>
							<strong>Lunes a S&aacute;bado</strong> de 8:00 a 20:00 horas. <br>
							<strong>Domingo</strong> de 9:00 a 14:00 horas.
						</p>
					</div><!-- #info-content -->
				</div>

				<div class="col-xs-12 col-sm-6">
					<div id="info-form">
						<form action="" method="POST" id="formulariov1" class="form form-horizontal" role="form">
							<input type="hidden" name="tipo" class="form-control" value="Quiero ser Distribuidor">						
							<div class="form-group">
								<div class="col-xs-12">
									<label>Nombre</label>
									<input type="text" name="nombre" id="nombre" class="form-control" placeholder="Escriba su nombre" required>
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-12">
									<label>E-mail</label>
									<input type="email" name="email" id="email" class="form-control" placeholder="Escribe tu correo electr&oacute;nico" required>
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-12 col-sm-6">
									<label>Tel&eacute;fono</label>
									<input type="text" name="telefono" id="inputTelefono" class="form-control" placeholder="Escribe tu n&uacute;mero telef&oacute;nico" required>
								</div>

								<div class="col-xs-12 col-sm-6">
									<label>Celular</label>
									<input type="text" name="celular" id="inputCelular" class="form-control" placeholder="Escribe tu celular" required>
								</div>
							</div>

							<div class="form-group">
	
	
	<div class="col-xs-12 col-sm-12">
	<label>¿Actualmente vende calentadores?</label>
	<div class="row">							
<div class="col-xs-12 col-sm-6">					
<div class="radio">
  <label>
    <input type="radio" name="vende_calentadores" id="vende_calentadores" value="si" checked>
    Si
  </label>
</div></div>
<div class="col-xs-12 col-sm-6">					
<div class="radio">
  <label>
    <input type="radio" name="vende_calentadores" id="vende_calentadores" value="no">
    No
  </label></div>
  </div></div>
</div>
	</div>
								<div class="form-group">
	
<div class="col-xs-12 col-sm-12">
<label>¿A que segmento de mercado pertenece su negocio?</label>
<select class="form-control" name="segmento" id="segmento">
  <option value="Plomero-Ferretero">Plomero-Ferretero</option>
  <option value="Acabados">Acabados </option>
  <option value="Azulejera">Azulejera</option>
  <option value="Especialidades">Especialidades</option>
  <option value="Home Center">Home Center</option>
  <option value="Mueblero">Mueblero</option>
  <option value="Departamentales">Departamentales</option>
  <option value="Construcción">Construcción</option>
</select>
</div>
	</div>
	
	<div class="form-group">
	<div class="col-xs-12 col-sm-12">
	<label>¿Que tipo de distribucion maneja?</label>
	<div class="row">							
<div class="col-xs-12 col-sm-6">					
<div class="radio">
  <label>
    <input type="radio" name="distri" id="distri" value="Mayorista" checked>
    Mayorista
  </label>
</div></div>
<div class="col-xs-12 col-sm-6">					
<div class="radio">
  <label>
    <input type="radio" name="distri" id="distri" value="Minorista">
    Minorista
  </label></div>
  </div></div>
</div>
</div>
							<div class="form-group">
								<div class="col-xs-12 col-sm-12">
									<label>Cobertura territorial</label>
									<input type="text" name="cob" id="cob" class="form-control" required/>
								</div>
                               </div>
							 <div class="form-group">
								<div class="col-xs-12 col-sm-12">
									<label>No. de PDV (punto de venta)</label>
									<input type="text" name="punto" id="punto" class="form-control" required/>
								</div>
							</div>

					
							<div class="form-group">
								<div class="col-xs-12">
									<input type="submit" class="btn btn-block btn-primary btn-submit" value="ENVIAR" />
								</div>
							</div>
						<div id="respuesta"></div>
						</form>
					</div><!-- #info-form -->
				</div>
			</div>
		</div>
	</section>
<script>
	 $('#formulariov1').submit(function() {
         // Enviamos el formulario usando AJAX
		 $("#respuesta").html("<img src='http://giftcube.com.mx/views/layout/default/img/loader.gif'>");
        $.ajax({
            type: 'POST',
            url: '<?=base_url()?>main/sendEmail_dos',
            data: $(this).serialize(),
            // Mostramos un mensaje con la respuesta de PHP
            success: function(data) {
				$("#nombre").val('');
				$("#email").val('');
                $("#inputTelefono").val('');
                $("#inputCelular").val('');
				$("#punto").val('');
                $("#cob").val('');

                $('#respuesta').html("<p>Muchas gracias por tus comentarios, nos pondremos en contacto contigo cuanto antes.</p>");
            }
        })        
        return false;
    }); 
</script>
	<?php $this->load->view("partials/lowermenu"); ?>

</body>
</html>