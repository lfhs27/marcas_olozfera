<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>	
</head>
<body id="template-default" style="/*background-image: url(<?php echo $pageProperties['bodyBackground']; ?>); background-repeat: no-repeat;*/">
	
	<?php $this->load->view("partials/topmenu"); ?>

	<section id="page-header">
		<div class="container">
			<div class="row top_fix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1 style="color:#000;text-shadow:none;">&iquest;Por qu&eacute; Calorex?</h1>
				</div>
			</div>
		</div>
	</section>

	<section id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-offset-1 col-xs-10" style="margin-bottom: 30px;">
					<img src="../img/calorex/calorex_nosotros.png" alt="Nosotros" class="img-responsive" style="width: 80%;margin-left: auto;margin-right: auto;">
				</div>
				<div class="col-sm-offset-1 col-xs-10">
					<h2>EXPERIENCIA, INNOVACI&Oacute;N Y SERVICIO</h2>
					<p>
						70 a&ntilde;os dando buenos ba&ntilde;os que cambian el d&iacute;a de toda tu familia, con agua a la temperatura ideal, de forma inmediata y cont&iacute;nua.
					</p>

					<p>
						Adem&aacute;s de contar con la gama m&aacute;s amplia de productos para satisfacer todas las necesidades del mercado mexicano; nuestros desarrollos e innovaciones maximizan el confort, garantizan la seguridad y cuidan tu econom&iacute;a.
					</p>

					<p>
						Nuestras patentes y dise&ntilde;os exclusivos nos hacen la &uacute;nica marca en el pa&iacute;s que se preocupa por el bienestar de tu familia y del medio ambiente.
					</p>
				</div>
             <div class="col-sm-offset-1 col-xs-10">
					<img src="../img/image001.png" alt="Nosotros" class="img-responsive" style="width: 80%;margin-left: auto;margin-right: auto;">
				</div>
				
				<div class="col-xs-12 col-sm-6">
				</div>
			</div>
		</div>
	</section>
	<?php $this->load->view("partials/lowermenu"); ?>

</body>
</html>