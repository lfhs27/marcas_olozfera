<?php
$months = array(
		1 => "Enero",
		2 => "Febrero",
		3 => "Marzo",
		4 => "Abril",
		5 => "Mayo",
		6 => "Junio",
		7 => "Julio",
		8 => "Agosto",
		9 => "Septiembre",
		10 => "Octubre",
		11 => "Noviembre",
		12 => "Diciembre"
	);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>
	<style>
	header{
		/*top:0px;***/
	}
	</style>	
</head>
<body id="template-blog" class="" style="">
	
	<?php $this->load->view("partials/topmenu"); ?>

	<!--section id="page-content" style="margin-top:121px;">
		<div class="container">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="info-content">
					<div class="content" id="tip-list">
					</div>
				</div>
			</div>
		</div>
	</section-->
	<section id="page-content" style="height:100%;">
		<div class="container calentador-ideal" style="height:100%;">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="info-content">
					<div class="content">
						<h2>&iquest;QU&Eacute; <strong>TIPO DE ENERG&Iacute;A</strong> EST&Aacute;S BUSCANDO?</h2>
						<label for="gas" class="iconContainer">
							<img src="/img/calentadorIdeal/01gas.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_energia_w" id="gas" value="natural_lp" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Gas</strong>
							</center>
						</label>
						<label for="electrico" class="iconContainer">
							<img src="/img/calentadorIdeal/01electrico.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_energia_w" id="electrico" value="electrico" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">El&eacute;ctrico</strong>
							</center>
						</label>
						<label for="solar" class="iconContainer">
							<img src="/img/calentadorIdeal/01solar.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_energia_w" id="solar" value="solar" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Solar</strong>
							</center>
						</label>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="page-content" style="height:100%;background-color:#F2F2F2;">
		<div class="container calentador-ideal" style="height:100%;">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="info-content">
					<center>
						<span class="ciBack" onclick="back()">Atr&aacute;s</span>
					</center>
					<div class="content">
						<h2>
							&iquest;CUANTAS <strong>REGADERAS</strong> HAY EN TU HOGAR?
						</h2>
						<label for="" class="iconContainer">
							<img src="/img/calentadorIdeal/02uno.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="n_regaderas" id="" value="1" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Una</strong>
							</center>
						</label>
						<label for="" class="iconContainer">
							<img src="/img/calentadorIdeal/02dos.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="n_regaderas" id="" value="2" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Dos</strong>
							</center>
						</label>
						<label for="" class="iconContainer">
							<img src="/img/calentadorIdeal/02tres.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="n_regaderas" id="" value="3" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Tres</strong>
							</center>
						</label>
						<label for="" class="iconContainer">
							<img src="/img/calentadorIdeal/02cuatro.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="n_regaderas" id="" value="4" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Cuatro</strong>
							</center>
						</label>
						<label for="" class="iconContainer">
							<img src="/img/calentadorIdeal/02cinco.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="n_regaderas" id="" value="5" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Cinco o m&aacute;s</strong>
							</center>
						</label>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="page-content" style="height:100%;">
		<div class="container calentador-ideal" style="height:100%;">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="info-content">
					<center>
						<span class="ciBack" onclick="back()">Atr&aacute;s</span>
					</center>
					<div class="content">
						<h2>&iquest;COMO SON LAS <strong>DUCHAS</strong> EN TU HOGAR?</h2>
						<label for="simultanea" class="iconContainer">
							<img src="/img/calentadorIdeal/03simultaneas.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_ducha" id="simultanea" value="simultanea" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Simult&aacute;neas</strong><br />
								<span style="font-weight:normal;">al mismo tiempo</span>
							</center>
						</label>
						<label for="continua" class="iconContainer">
							<img src="/img/calentadorIdeal/03continuas.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_ducha" id="continua" value="continua" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Continuas</strong><br />
								<span style="font-weight:normal;">una tras otra</span>
							</center>
						</label>
						<label for="espaciada" class="iconContainer">
							<img src="/img/calentadorIdeal/03espaciadas.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_ducha" id="espaciada" value="espaciada" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Espaciadas</strong><br />
								<span style="font-weight:normal;">variadas a lo largo del d&iacute;a</span>
							</center>
						</label>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="page-content" style="height:100%;background-color:#F2F2F2;">
		<div class="container calentador-ideal" style="height:100%;">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="info-content">
					<center>
						<span class="ciBack" onclick="back()">Atr&aacute;s</span>
					</center>
					<div class="content">
						<h2>
							&iquest;CUANTAS <strong>PERSONAS</strong> HABITAN EN TU HOGAR?
						</h2>
						<label for="" class="iconContainer">
							<img src="/img/calentadorIdeal/04unapersona.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="n_personas" id="" value="1" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Una</strong>
							</center>
						</label>
						<label for="" class="iconContainer">
							<img src="/img/calentadorIdeal/04dospersonas.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="n_personas" id="" value="2" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Dos</strong>
							</center>
						</label>
						<label for="" class="iconContainer">
							<img src="/img/calentadorIdeal/04trespersonas.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="n_personas" id="" value="3" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Tres</strong>
							</center>
						</label>
						<label for="" class="iconContainer">
							<img src="/img/calentadorIdeal/04cuatropersonas.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="n_personas" id="" value="4" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Cuatro</strong>
							</center>
						</label>
						<label for="" class="iconContainer">
							<img src="/img/calentadorIdeal/04cincopersonas.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="n_personas" id="" value="5" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Cinco o m&aacute;s</strong>
							</center>
						</label>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="page-content" style="height:100%;">
		<div class="container calentador-ideal" style="height:100%;">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="info-content">
					<center>
						<span class="ciBack" onclick="back()">Atr&aacute;s</span>
					</center>
					<div class="content">
						<h2>&iquest;COMO ES TU <strong>DOMICILIO</strong>?</h2>
						<label for="departamento" class="iconContainer">
							<img src="/img/calentadorIdeal/05departamento.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_domicilio" id="departamento" value="departamento" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Departamento</strong>
							</center>
						</label>
						<label for="casa_1" class="iconContainer">
							<img src="/img/calentadorIdeal/05unpiso.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_domicilio" id="casa_1" value="casa_1" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Casa 1 piso</strong>
							</center>
						</label>
						<label for="casa_2" class="iconContainer">
							<img src="/img/calentadorIdeal/05dospisos.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_domicilio" id="casa_2" value="casa_2" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Casa 2 pisos o m&aacute;s</strong>
							</center>
						</label>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="page-content" class="tipo_domicilio_a" style="height:100%;background-color:#F2F2F2;display:none;">
		<div class="container calentador-ideal" style="height:100%;">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="info-content">
					<center>
						<span class="ciBack" onclick="back()">Atr&aacute;s</span>
					</center>
					<div class="content">
						<h2>&iquest;EN QU&Eacute; <strong>TIPO DE PLANTA</strong> VIVES?</h2>
						<label for="tipo_domicilio_a_baja" class="iconContainer">
							<img src="/img/calentadorIdeal/05depBajo.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_domicilio_a" id="tipo_domicilio_a_baja" value="baja" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Baja</strong>
							</center>
						</label>
						<label for="tipo_domicilio_a_media" class="iconContainer">
							<img src="/img/calentadorIdeal/05depMedio.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_domicilio_a" id="tipo_domicilio_a_media" value="media" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Media</strong>
							</center>
						</label>
						<label for="tipo_domicilio_a_alta" class="iconContainer">
							<img src="/img/calentadorIdeal/05depAlto.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="tipo_domicilio_a" id="tipo_domicilio_a_alta" value="alta" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Alta</strong>
							</center>
						</label>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="page-content" style="height:100%;">
		<div class="container calentador-ideal" style="height:100%;">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="info-content">
					<center>
						<span class="ciBack" onclick="back()">Atr&aacute;s</span>
					</center>
					<div class="content">
						<h2>&iquest;COMO ES EL FLUJO DE AGUA DE TU REGADERA?</h2>
						<label class="iconContainer">
							<img src="/img/calentadorIdeal/06bajo.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="flujo_agua" value="bajo" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Bajo</strong>
							</center>
						</label>
						<label class="iconContainer">
							<img src="/img/calentadorIdeal/06alto.svg" class="svg borderIcon" alt="" />
							<input type="radio" name="flujo_agua" value="alto" style="display:none;" />
							<center style="color:#BCBCBC;">
								<strong style="font-size:18px;">Alto</strong>
							</center>
						</label>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="page-content" style="height:100%;">
		<div class="container calentador-ideal" style="height:100%;">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="info-content">
					<center>
						<span class="ciBack" onclick="back()">Atr&aacute;s</span>
					</center>
					<div class="content">
						<h2>&iquest;UTILIZAS ALGUNO DE ESTOS SERVICIOS SIMULTANEAMENTE AL BA&Ntilde;ARTE?</h2>
						<label class="labelContainer">
							Lavabo
							<input type="checkbox" name="n_servicios" value=".5" data-icon="lavabo" style="display:none;" />
						</label>
						<label class="labelContainer">
							Lavadora
							<input type="checkbox" name="n_servicios" value="1" data-icon="lavadora" style="display:none;" />
						</label>
						<label class="labelContainer">
							Tina
							<input type="checkbox" name="n_servicios" value="4" data-icon="tina" style="display:none;" />
						</label>
						<label class="labelContainer">
							Lavavajillas
							<input type="checkbox" name="n_servicios" value=".5" data-icon="lavavajillas" style="display:none;" />
						</label>
						<label class="labelContainer">
							Ninguno
							<input type="checkbox" name="n_servicios" value="0" data-icon="ninguno" style="display:none;" />
						</label><br />
						<label class="labelContainer" onclick="next()">
							Siguiente
						</label>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="page-content" style="height:100%;">
		<div class="container calentador-ideal" style="height:100%;">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="info-content">
					<center>
						<span class="ciBack" onclick="reset()">Volver al inicio</span>
					</center>
					<h2 style="font-weight:normal;text-align:center;">Recomendaciones para t&iacute;:</h2>
					<div class="owl-carousel owl-theme" id="products-carousel-container">
						<!--div class="product">
							<a href="www.google.com"><img src="uploads/products/images/14507384737664.png" class="img-responsive"></a>
							<div class="product-overlay">
								<h3><a href="http://www.calorex.com.mx/residencial/solar">80-100% DE AHORRO DE GAS</a></h3>
							</div>
						</div-->
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 content" id="info-content">
					<h3 style="margin:0px;font-weight:normal;font-size:20px;text-align:center;">Por Categor&iacute;a</h3>
					<div class="col-xs-12" id="info-content" style="padding-top:0px;padding-bottom:25px;">
						<div class="owl-carousel owl-theme" id="categories-carousel-container"></div>
						<div id="marcasHTML">
							<?php foreach($navigation->brand->sectors[0]->families as $family): ?>
							<div class="product labelContainer" style="text-align:center;">
								<label class="">
									<?=$family->front_name?>
									<input type="checkbox" name="filtros_categorias" value="<?=$family->id?>" style="display:none;" />
								</label>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
					<h3 style="margin:0px;font-weight:normal;font-size:20px;text-align:center;">Por Marca</h3>
					<div class="col-xs-12" id="info-content" style="padding-top:4px;">
						<div class="owl-carousel owl-theme" id="brands-carousel-container"></div>
						<div id="categoriasHTML">
							<div class="product">
								<label class="labelContainer marca" style="">
									<img src="https://tiendacalorex.com/assets/front/img/ventadirecta/logoC-calorex.png" class="img-responsive" style="">
									<input type="checkbox" name="filtros_marcas" value="1" style="display:none;" <?=$pageProperties["skin"] == "calorex" ? "checked" : ""?> />
								</label>
							</div>
							<div class="product">
								<label class="labelContainer marca" style="padding:0px;height:80px;">
									<img src="https://tiendacalorex.com/assets/front/img/ventadirecta/logoC-cinsa.png" class="img-responsive" style="">
									<input type="checkbox" name="filtros_marcas" value="2" style="display:none;" <?=$pageProperties["skin"] == "cinsa" ? "checked" : ""?> />
								</label>
							</div>
							<div class="product">
								<label class="labelContainer marca" style="padding:0px;height:80px;">
									<img src="https://tiendacalorex.com/assets/front/img/ventadirecta/logoC-hesa.png" class="img-responsive" style="">
									<input type="checkbox" name="filtros_marcas" value="3" style="display:none;" />
								</label>
							</div>
							<div class="product">
								<label class="labelContainer marca" style="padding:0px;height:80px;">
									<img src="https://tiendacalorex.com/assets/front/img/ventadirecta/logoC-optimus.png" class="img-responsive" style="">
									<input type="checkbox" name="filtros_marcas" value="4" style="display:none;" />
								</label>
							</div>
							<div class="product">
								<label class="labelContainer marca" style="padding:0px;height:80px;">
									<img src="https://tiendacalorex.com/assets/front/img/ventadirecta/logoC-heatmaster.png" class="img-responsive" style="" />
									<input type="checkbox" name="filtros_marcas" value="5" style="display:none;" />
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 smallContainers" id="info-content" style="padding-top:45px;">
					<h3 style="font-weight:normal;font-size:20px;text-align:center;">Tu informaci&oacute;n</h3>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="info-content" style="padding:1px;">
						<div class="col-xs-6">
							<label for="gas" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/01gas.svg" class="svgsmall borderIcon tipo_energia_w" data-icon="natural_lp" alt="" />
							</label>
							<label for="electrico" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/01electrico.svg" class="svgsmall borderIcon tipo_energia_w" data-icon="electrico" alt="" />
							</label>
							<label for="solar" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/01solar.svg" class="svgsmall borderIcon tipo_energia_w" data-icon="solar" alt="" />
							</label>
						</div>
						<div class="col-xs-6">
							<label for="" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/02uno.svg" class="svgsmall borderIcon n_regaderas" data-icon="1" alt="" />
							</label>
							<label for="" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/02dos.svg" class="svgsmall borderIcon n_regaderas" data-icon="2" alt="" />
							</label>
							<label for="" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/02tres.svg" class="svgsmall borderIcon n_regaderas" data-icon="3" alt="" />
							</label>
							<label for="" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/02cuatro.svg" class="svgsmall borderIcon n_regaderas" data-icon="4" alt="" />
							</label>
							<label for="" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/02cinco.svg" class="svgsmall borderIcon n_regaderas" data-icon="5" alt="" />
							</label>
						</div>
						<div class="col-xs-6">
							<label for="simultanea" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/03simultaneas.svg" class="svgsmall borderIcon tipo_ducha" data-icon="simultanea" alt="" />
							</label>
							<label for="continua" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/03continuas.svg" class="svgsmall borderIcon tipo_ducha" data-icon="continua" alt="" />
							</label>
							<label for="espaciada" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/03espaciadas.svg" class="svgsmall borderIcon tipo_ducha" data-icon="espaciada" alt="" />
							</label>
						</div>
						<div class="col-xs-6">
							<label for="" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/04unapersona.svg" class="svgsmall borderIcon n_personas" data-icon="1" alt="" />
							</label>
							<label for="" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/04dospersonas.svg" class="svgsmall borderIcon n_personas" data-icon="2" alt="" />
							</label>
							<label for="" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/04trespersonas.svg" class="svgsmall borderIcon n_personas" data-icon="3" alt="" />
							</label>
							<label for="" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/04cuatropersonas.svg" class="svgsmall borderIcon n_personas" data-icon="4" alt="" />
							</label>
							<label for="" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/04cincopersonas.svg" class="svgsmall borderIcon n_personas" data-icon="5" alt="" />
							</label>
						</div>
						<div class="col-xs-6">
							<label for="departamento" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/05departamento.svg" class="svgsmall borderIcon tipo_domicilio" data-icon="departamento" alt="" />
							</label>
							<label for="casa_1" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/05unpiso.svg" class="svgsmall borderIcon tipo_domicilio" data-icon="casa_1" alt="" />
							</label>
							<label for="casa_2" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/05dospisos.svg" class="svgsmall borderIcon tipo_domicilio" data-icon="casa_2" alt="" />
							</label>
						</div>
						<div class="col-xs-6">
							<label for="baja" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/05depBajo.svg" class="svgsmall borderIcon tipo_domicilio_a" data-icon="baja" alt="" />
							</label>
							<label for="media" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/05depMedio.svg" class="svgsmall borderIcon tipo_domicilio_a" data-icon="media" alt="" />
							</label>
							<label for="alta" class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/05depAlto.svg" class="svgsmall borderIcon tipo_domicilio_a" data-icon="alta" alt="" />
							</label>
						</div>
						<div class="col-xs-6">
							<label class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/06bajo.svg" class="svgsmall borderIcon flujo_agua" data-icon="bajo" alt="" />
							</label>
							<label class="iconContainer" style="display:none;">
								<img src="/img/calentadorIdeal/06alto.svg" class="svgsmall borderIcon flujo_agua" data-icon="alto" alt="" />
							</label>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="info-content" style="padding:1px;">
						<div class="col-xs-6">
							<label class="iconContainer">
								<img src="/img/calentadorIdeal/07lavabo.svg" class="svgsmall borderIcon n_servicios" data-icon="lavabo" alt="" />
							</label>
						</div>
						<div class="col-xs-6">
							<label class="iconContainer">
								<img src="/img/calentadorIdeal/07lavadora.svg" class="svgsmall borderIcon n_servicios" data-icon="lavadora" alt="" />
							</label>
						</div>
						<div class="col-xs-6">
							<label class="iconContainer">
								<img src="/img/calentadorIdeal/07lavavajillas.svg" class="svgsmall borderIcon n_servicios" data-icon="lavavajillas" alt="" />
							</label>
						</div>
						<div class="col-xs-6">
							<label class="iconContainer">
								<img src="/img/calentadorIdeal/07tina.svg" class="svgsmall borderIcon n_servicios" data-icon="tina" alt="" />
							</label>
						</div>
						<div class="col-xs-6">
							<label class="iconContainer">
								<img src="/img/calentadorIdeal/07ninguno.svg" class="svgsmall borderIcon n_servicios" data-icon="ninguno" alt="" />
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<div style="clear:both;"></div>
	<?php $this->load->view("partials/lowermenu"); ?>
<script>
var request = {};
var owl, owlMarcas, owlCategorias;
$(window).load(function() {
	$('#products-carousel-container').owlCarousel({
		loop: true,
		margin: 15,
		items: 6,
		pagination : true
	});
	$('#brands-carousel-container').owlCarousel({
		loop: true,
		margin: 5,
		items: 4,
		pagination : true,
		nav : true,
		dots : true,
		navText : ["<img src='assets/front/img/ventadirecta/right-01.png' alt='Siguiente' />", "<img src='assets/front/img/ventadirecta/left-01.png' alt='Anterior' />"]
	});
	$('#categories-carousel-container').owlCarousel({
		loop: true,
		margin: 15,
		items: 4,
		pagination : true,
		nav : true,
		dots : true,
		navText : ["<img src='assets/front/img/ventadirecta/right-01.png' alt='Siguiente' />", "<img src='assets/front/img/ventadirecta/left-01.png' alt='Anterior' />"]
	});
	owl = $("#products-carousel-container").data('owlCarousel');
	owlMarcas = $("#brands-carousel-container").data('owlCarousel');
	owlCategorias = $("#categories-carousel-container").data('owlCarousel');
});
/*
 * Replace all SVG images with inline SVG
 */
function replaceSVG(){
	jQuery('img.svg').each(function(){
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		jQuery.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = jQuery(data).find('svg');

			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');

			// Check if the viewport is set, if the viewport is not set the SVG wont't scale.
			if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
				$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
			}

			// Replace image with new SVG
			$img.replaceWith($svg);

		}, 'xml');

	});
	jQuery('img.svgsmall').each(function(){ console.log("hola");
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');
		jQuery.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = jQuery(data).find('svg');
			
			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');

			// Check if the viewport is set, if the viewport is not set the SVG wont't scale.
			if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
				$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
			}
			$svg.attr('data-icon', $img.attr('data-icon'));
			$svg.attr('height', "100%");
			$svg.attr('width', "100%");
			
			jQuery(data).find('svg path, svg polygon').attr('fill', '#556578');
			$svg.css("border-color", "#556578");

			// Replace image with new SVG
			$img.replaceWith($svg);

		}, 'xml');

	});
}
$(document).ready(function(){
	replaceSVG();
	$(".svgsmall.n_servicios").parent().parent().hide();
	// hover function
	// hover over an element, and find the SVG that you want to change
	console.log($("svg"));
	bindClicks();
	
	$('.product').on('mouseenter',function(){
		$('.subtitle-overlay',this).fadeIn();
	}).on('mouseleave',function(){
		$('.subtitle-overlay',this).fadeOut();
	});

	$('#products-carousel .product').on('mouseenter',function(){
		$('.product-overlay',this).fadeIn();
	}).on('mouseleave',function(){
		$('.product-overlay',this).fadeOut();
	});

	$('#products-carousel .product .product-overlay').on('click',function(){
		var nextUrl = $('a',this).attr('href');

		top.location = nextUrl;

		return false;
	});

	if ($(window).width() < 768) {
		$('#related-products .products-cycle').cycle({
			slides: '>.col-xs-12'
		});
	};
});
function next(){
	current = $(window).scrollTop();
	scrollTo = current + $(window).height() - ($("header").height() - 121);
	$('html, body').animate({
		scrollTop: scrollTo
	}, 500, "swing", function(){
		
	});
}
function back(){
	current = $(window).scrollTop();
	scrollTo = current - ($(window).height() - ($("header").height() - 121));
	$('html, body').animate({
		scrollTop: scrollTo
	}, 500, "swing", function(){
		
	});
}
function reset(){
	$.each($("input[type=radio], input[type=checkbox]"), function(i, val){
		$(val).prop("checked", false);
		
		$(val).parent().css("border-color", "#BCBCBC");
		$(val).parent().css("color", "#BCBCBC");
		
		siblings = $(val).siblings("svg");		
		siblings.find('path, polygon').attr('fill', '#BCBCBC');
		siblings.parent().find("center").attr("style", "color:#BCBCBC;");
		siblings.css("border-color", "#BCBCBC");
		
		$('html, body').animate({
			scrollTop: 0
		}, 500, "swing", function(){
			
		});
	});
}
function displayServices(){
	$(".svgsmall.n_servicios").parent().parent().hide();
	$.each($("[name=n_servicios]:checked"), function(i, val){ console.log($(".svgsmall."+$(val).attr("name")+"[data-icon="+$(val).attr("data-icon")+"]"));
		$(".svgsmall."+$(val).attr("name")+"[data-icon="+$(val).attr("data-icon")+"]").parent().parent().show();
	});
}
function getCalentadores(){
	brands_arr = [];
	brands_arr = $.map($("[name=filtros_marcas]"),function(val, i){
		if($(val).prop("checked")){
			return $(val).attr("value");
		}
	});
	cats_arr = $.map($("[name=filtros_categorias]"),function(val, i){
		if($(val).is(":checked")){
			return $(val).attr("value");
		}
	});
	$.ajax({
		//url: "https://tiendacalorex.com/calentador-ideal/WSgetModels",
		url: "/main/WScalentador",
		data : {
			request : request,
			brands : brands_arr,
			families : cats_arr
		},
		method : 'GET',
		dataType : 'json',
		success: function(data){
			console.log(data);
			html = '';
			marcasHTML = '';
			categoriasHTML = '';
			avCategorias = [];
			avMarcas = [];
			if(data != null){
				$.each(data, function(i, val){
					if(val.product){
						html += '<div class="product">';
						html += '	<a href="https://tiendacalorex.com//producto/'+val.product_id+'/'+val.id+'">';
						html += '		<img src="https://tiendacalorex.com/'+val.product.gallery[0].path+'/'+val.product.gallery[0].filename+'" class="img-responsive"><center>'+val.front_name+'</center>';
						html += '	</a>';
						html += '</div>';
						if($.inArray(val.product.brand_id, avMarcas) < 0){
							avMarcas.push(val.product.brand_id);
							marcasHTML += $("input[name=filtros_marcas][value="+val.product.brand_id+"]").parent().parent()[0].outerHTML;
						} //console.log(val.product.family_id, avCategorias, $.inArray(val.product.family_id, avCategorias));
						if($.inArray(val.product.family_id, avCategorias) < 0){
							avCategorias.push(val.product.family_id); //console.log("obj", val.product.family_id, $("input[name=filtros_categorias][value="+val.product.family_id+"]").parent().parent()[0].outerHTML);
							if($("input[name=filtros_categorias][value="+val.product.family_id+"]").parent().parent().length > 0)
								categoriasHTML += $("input[name=filtros_categorias][value="+val.product.family_id+"]").parent().parent()[0].outerHTML;
						}
					}
				});
			}
			owl.destroy();
			$("#products-carousel-container").html(html);
			$('#products-carousel-container').owlCarousel({
				loop: true,
				margin: 15,
				items: 6,
				pagination : true
			});
			if(brands_arr.length == 1 && cats_arr.length == 0){
				owlCategorias.destroy();
				$("#categories-carousel-container").html(categoriasHTML);
				$.each(cats_arr, function(i, val){
					$("input[name=filtros_categorias][value="+val+"]").prop("checked", true);
					$("input[name=filtros_categorias][value="+val+"]").parent().css("border-color", "#556578");
					$("input[name=filtros_categorias][value="+val+"]").parent().css("color", "#556578");
				});
				$('#categories-carousel-container').owlCarousel({
					loop: true,
					margin: 15,
					items: 4,
					pagination : true,
					nav : true,
					dots : true,
					navText : ["<img src='https://tiendacalorex.com/assets/front/img/ventadirecta/right-01.png' alt='Siguiente' />", "<img src='https://tiendacalorex.com/assets/front/img/ventadirecta/left-01.png' alt='Anterior' />"]
				});
				owlMarcas.destroy();
				$("#brands-carousel-container").html(marcasHTML);
				$.each(brands_arr, function(i, val){
					$("input[name=filtros_marcas][value="+val+"]").prop("checked", true);
					$("input[name=filtros_marcas][value="+val+"]").parent().css("border-color", "#556578");
					$("input[name=filtros_marcas][value="+val+"]").parent().css("color", "#556578");
				});
				$('#brands-carousel-container').owlCarousel({
					loop: true,
					margin: 5,
					items: 4,
					pagination : true,
					nav : true,
					dots : true,
					navText : ["<img src='https://tiendacalorex.com/assets/front/img/ventadirecta/right-01.png' alt='Siguiente' />", "<img src='https://tiendacalorex.com/assets/front/img/ventadirecta/left-01.png' alt='Anterior' />"]
				});
				owlMarcas = $("#brands-carousel-container").data('owlCarousel');
				owlCategorias = $("#categories-carousel-container").data('owlCarousel');
			}
			owl = $("#products-carousel-container").data('owlCarousel');
			bindClicks();
		}
	});
}
function bindClicks(){
	$('.iconContainer').click(function() {
		el = $(this);
		svg = el.find('svg path, svg polygon');
		
		attr = el.find("input[type=radio]").attr("name");
		siblings = $("input[name="+attr+"]").siblings("svg");
		
		siblings.find('path, polygon').attr('fill', '#BCBCBC');
		siblings.parent().find("center").attr("style", "color:#BCBCBC;");
		siblings.css("border-color", "#BCBCBC");
		
		svg.attr('fill', '#556578');
		el.find("svg").css("border-color", "#556578");
		el.find("center").attr("style", "color:#556578;");
		
		$(".svgsmall."+attr).parent().hide(); 
		$(".svgsmall."+attr+"[data-icon="+el.find("input[type=radio]").attr("value")+"]").parent().show();
		
		next();
	});
	$('.labelContainer').click(function(e) {
		el = $(this);		
		//el.css("border", "3px");
		//alert(rgbToHex(el.css("color")));
		if($(e.target)[0].tagName == "INPUT"){
			e.stopImmediatePropagation();
			if(rgbToHex(el.css("color")) == "#556578"){
				if(el.hasClass("marca")){
					el.css("border-color", "#FFF");
					el.css("color", "#FFF");
				}else{
					el.css("border-color", "#BCBCBC");
					el.css("color", "#BCBCBC");
				}
			}else{
				el.css("border-color", "#556578");
				el.css("color", "#556578");
			}
		}
		
		//next();
	});
	$("input[type=radio], select").change(function(e){
		e.stopImmediatePropagation();
		attr = $(this).attr("name");
		val = $(this).attr("value");
		console.log("deb", attr, val);
		if(attr == "tipo_domicilio"){
			if(val == "departamento")
				$("section.tipo_domicilio_a").show();
			else
				$("section.tipo_domicilio_a").hide();
		}
		if($( event.target ).is("input")){
			request[attr] = $("[name="+attr+"]:checked").val();
			//request[attr] = $("[name="+attr+"]").val();
		}else if($( event.target ).is("select")){
			request[attr] = $("[name="+attr+"]").val();
		}
		getCalentadores();
	});
	$("input[type=checkbox]").change(function(e){
		e.stopImmediatePropagation();
		attr = $(this).attr("name");
		val = $(this).attr("value");
		n_servicios = 0;
		$.each($("[name="+attr+"]:checked"), function(){
			n_servicios += parseFloat($(this).val());
		});
		request[attr] = n_servicios;
		if($(this).attr('data-icon') === undefined){
			$(".svgsmall."+attr).parent().hide();
			$(".svgsmall."+attr+"[data-icon="+val+"]").parent().show();
		}else{
			displayServices();
		}
		getCalentadores();
	});
}
function rgbToHex(total) {
    var total = total.toString().split(',');
    var r = total[0].substring(4);
    var g = total[1].substring(1);
    var b = total[2].substring(1,total[2].length-1);
    return ("#"+checkNumber((r*1).toString(16))+checkNumber((g*1).toString(16))+checkNumber((b*1).toString(16))).toUpperCase();
}
function checkNumber(i){
    i = i.toString();
    if (i.length == 1) return '0'+i;
    else return i;
}
</script>
</body>
</html>