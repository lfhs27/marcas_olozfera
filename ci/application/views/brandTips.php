<?php
$months = array(
		1 => "Enero",
		2 => "Febrero",
		3 => "Marzo",
		4 => "Abril",
		5 => "Mayo",
		6 => "Junio",
		7 => "Julio",
		8 => "Agosto",
		9 => "Septiembre",
		10 => "Octubre",
		11 => "Noviembre",
		12 => "Diciembre"
	);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>
	<style>
	header{
		/*top:0px;*/
	}
	</style>
</head>
<body id="template-blog" class="template-with-posts" style="">
	
	<?php $this->load->view("partials/topmenu"); ?>

	<section id="page-content" style="margin-top:121px;">
		<div class="container">	
			<div class="row">
				<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9" id="info-content">
					<div class="content" id="tip-list">
						<?php foreach($tips as $tip): ?>
						<div class="tip clearfix">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<img src="<?=$pageProperties['domain']?><?=$tip->image?>" style="width:100%;" alt="Tip" class="pull-left">
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="tip-text">
									<p class="date"><?=date("d", strtotime($tip->created_at))." de ".$months[(int)date("m", strtotime($tip->created_at))]." ".date("Y", strtotime($tip->created_at))?></p>
									<h2><a href="<?=base_url()?>blog/<?=$tip->slug?>"><?=$tip->title?></a></h2>
									<p class="excerpt">
										<?=$tip->intro?>
									</p>
									<a href="<?=base_url()?>blog/<?=$tip->slug?>" class="btn btn-white">Leer m&aacute;s</a>
								</div>
							</div>
						</div> 
						<?php endforeach; ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="sidebar">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="content">
								<div class="search">
									<h4>Buscar</h4>
									<form id="fsearch" action="<?=base_url()?>blog" method="get">
										<input type="text" name="search" value="<?=isset($_GET["search"]) ? $_GET["search"] : ""?>">
										<a onclick="$('#fsearch').submit();"><i class="fa fa-search"></i></a>
									</form>
								</div>
								<!--ul>
									<li><a href="#">Preguntas Frecuentes</a></li>
									<li><a href="#">Tendencias</a></li>
									<li class="current"><a href="#">Ideas</a></li>
								</ul-->
							</div>

							<!--div class="content newsletter">
								<h4>Suscr&iacute;bete a nuestro NEWSLETTER</h4>

								<input type="text" name="newsletteremail" value="" placeholder="Correo Elctr&oacute;nico" class="form-control">
								<br>
								<a href="#" class="btn btn-block btn-primary">Suscribir</a>
							</div-->

							<div class="content transparent">
								<h4>Tags</h4>

								<ul class="taglist">
									<?php foreach($tags as $tag): ?>
									<li style="cursor:pointer;"><a onclick="searchTag('<?=$tag->tag?>');"><?=$tag->tag?></a></li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php $this->load->view("partials/lowermenu"); ?>
<script>
function searchTag(tag){
	$("#fsearch").find("[name=search]").val(tag);
	$("#fsearch").submit();
}
</script>
</body>
</html>