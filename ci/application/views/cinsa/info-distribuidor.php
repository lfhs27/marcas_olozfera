<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>	
</head>
<body id="template-default" class="template-with-forms" style="background-image: url(<?php echo $pageProperties['bodyBackground']; ?>); background-repeat: no-repeat;">
	
	<?php $this->load->view("partials/topmenu"); ?>

	<section id="info-nav">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 info-nav-mg">
					<nav class="nav-container">
						<a href="<?=base_url()?>main/solicita_ayuda" class="btn btn-primary ayuda-btn">Solicita tu Servicio</a>
						<a href="<?=base_url()?>main/solicita_asesoria" class="btn btn-primary ayuda-btn">Asesor&iacute;a T&eacute;cnica</a>
						<a href="<?=base_url()?>main/solicita_dudas" class="btn btn-primary ayuda-btn">Dudas de Producto</a>
						<a href="<?=base_url()?>main/solicita_garantia" class="btn btn-primary ayuda-btn">Registro de Garant&iacute;a</a>
						<a href="<?=base_url()?>main/solicita_distribuidor" class="btn btn-primary btn-current ayuda-btn">Quiero ser Distribuidor</a>
					</nav>
				</div>
			</div>
		</div>
	</section>

	<section id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div id="info-content">
						<h1>M&aacute;s de 60 a&ntilde;os brindando calor de hogar</h1>

						<p>
							Cinsa est&aacute; presente en toda la Rep&uacute;blica Mexicana.
							Conoce nuestra amplia red de distribuidores en el pa&iacute;s.
							Elige la zona m&aacute;s cercana a tu hogar y cont&aacute;ctanos 
							hoy mismo.
						</p>

						<div class="info-tel">
							<i class="fa fa-phone rounded"></i>
							<p>
								01 800 55CINSA<br>
								D.F. 56 40 06 01<br>
								01 800 552 4672
							</p>
						</div>

						<p>
							<strong>Lunes a S&aacute;bado</strong> de 8:00 a 20:00 horas. <br>
							<strong>Domingo</strong> de 9:00 a 14:00 horas.
						</p>
					</div><!-- #info-content -->
				</div>

				<div class="col-xs-12 col-sm-6">
					<div id="info-form">
						<form action="<?=base_url()?>main/sendEmail" method="POST" class="form form-horizontal" role="form" id="formulariov1">
							<div class="form-group">
								<div class="col-xs-12">
									<label>Nombre</label>
									<input type="text" class="form-control" name="nombre" placeholder="Escriba su nombre">
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-12">
									<label>E-mail</label>
									<input type="text" class="form-control" name="email" placeholder="Escribe tu correo electr&oacute;nico">
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-12">
									<label>E-mail</label>
									<input type="text" name="email" id="inputEmail" class="form-control" required="required" placeholder="Escribe tu correo electr&oacute;nico">
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-12 col-sm-6">
									<label>Tel&eacute;fono</label>
									<input type="text" name="telefono" id="inputTelefono" class="form-control" placeholder="Escribe tu n&uacute;mero telef&oacute;nico">
								</div>

								<div class="col-xs-12 col-sm-6">
									<label>Celular</label>
									<input type="text" name="celular" id="inputCelular" class="form-control" placeholder="Escribe tu celular">
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-12 col-sm-6">
									<label>Estado</label>
									<input type="text" class="form-control" name="estado" placeholder="">	
								</div>

								<div class="col-xs-12 col-sm-6">
									<label>Ciudad</label>
									<input type="text" class="form-control" name="ciudad" placeholder="">
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-12">
									<label>Escribe aqu&iacute; tu Mensaje</label>
									<textarea name="mensaje" id="inputMensaje" class="form-control" rows="3"></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input type="submit" class="btn btn-block btn-primary btn-submit" value="ENVIAR" />
									<div id="respuesta"></div>
								</div>
							</div>
						</form>
					</div><!-- #info-form -->
				</div>
			</div>
		</div>
	</section>
	<?php $this->load->view("partials/lowermenu"); ?>
	<script>
	 $('#formulariov1').submit(function() {
         // Enviamos el formulario usando AJAX
		 $("#respuesta").html("<img src='http://giftcube.com.mx/views/layout/default/img/loader.gif'>");
        $.ajax({
            type: 'POST',
            url: 'http://cinsa.softdepotserver2.com/main/sendEmail',
            data: $(this).serialize(),
            // Mostramos un mensaje con la respuesta de PHP
            success: function(data) {
				$("#inputNombre").val('');
				$("#inputEmail").val('');
                $("#inputTelefono").val('');
                $("#inputCelular").val('');
				$("#inputLineaModelo").val('');
				$("#inputEstado").val('');
				$("#inputCiudad").val('');
				$("#inputMensaje").val('');				
                $('#respuesta').html("<p>Muchas gracias por tus comentarios, nos pondremos en contacto contigo cuanto antes.</p>");
            },
            error: function(data){
            	console.log('error');
            	console.log(data);
            }
        })        
        return false;
    }); 
	</script>
</body>
</html>