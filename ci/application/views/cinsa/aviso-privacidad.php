<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>	
</head>
<body id="template-default" style="background-image: url(<?php echo $pageProperties['bodyBackground']; ?>); background-repeat: no-repeat;">
	
	<?php $this->load->view("partials/topmenu"); ?>

	<section id="page-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1>&iquest;Por qu&eacute; Calorex?</h1>
				</div>
			</div>
		</div>
	</section>

	<section id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<h2>AVISO DE PRIVACIDAD</h2>
					<p>En Calentadores de Am&eacute;rica, S.A. de C.V. ("CALENTADORES GIS") estamos convencidos que el principal activo son nuestros clientes. Como cliente de CALENTADORES GIS, usted tiene la oportunidad de escoger los productos y servicios que le ofrecemos, sabiendo que sus datos personales estar&aacute;n protegidos.</p>

					<p>Adicionalmente, el pasado 5 de julio de 2010 se public&oacute; en el Diario Oficial de la Federaci&oacute;n la Ley Federal de Protecci&oacute;n de Datos Personales en Posesi&oacute;n de Particulares (en lo sucesivo la "Ley"), cuyas disposiciones claramente coadyuvan con nuestro objetivo de proteger sus datos personales. Usted puede acceder al contenido de la Ley a trav&eacute;s de los portales que el Gobierno Federal, por conducto de la Secretar&iacute;a de Gobernaci&oacute;n, y la C&aacute;mara de Diputados del H. Congreso de la Uni&oacute;n tienen en Internet y cuyas direcciones son: www.ordenjuridico.gob.mx y www.diputados.gob.mx</p>

					<p>Por este medio le informamos que los datos personales que obtengamos en la prestaci&oacute;n de servicios que usted solicite a CALENTADORES GIS ser&aacute;n tratados de manera confidencial.</p>

					<h2>RESPONSABLES DEL TRATAMIENTO DE SUS DATOS PERSONALES:</h2>
					<p>El responsable del tratamiento de sus datos personales ser&aacute; el responsable del Centro de Contacto de CALENTADORES GIS. El mismo podr&aacute; ser contactado a trav&eacute;s de nuestras p&aacute;ginas de Internet (www.calorex.com.mx; www.cinsaboilers.com.mx; www.hesacalentadores.com.mx;www.cimamexico.com.mx), a trav&eacute;s de la cuenta de correo cima@gis.com.mx, o al 01 800 009 8300.</p>

					<h2>DOMICILIO DE CALENTADORES GIS:</h2>
					<p>Para efectos del presente aviso de privacidad, CALENTADORES GIS se&ntilde;ala como su domicilio el ubicado en Blvd. Isidro L&oacute;pez Zertuche # 1839, Colonia Zona Industrial, Saltillo, Coahuila, 25260.</p>

					<h2>DATOS PERSONALES QUE PUEDEN RECABARSE:</h2>
					<p>CALENTADORES GIS recabar&aacute; los datos personales necesarios para la prestaci&oacute;n de los servicios de mantenimiento, instalaciones, atenci&oacute;n de garant&iacute;as que usted solicite o requiera; de manera enunciativa, m&aacute;s no limitativa, CALENTADORES GIS podr&aacute; recabar su nombre; domicilio; n&uacute;meros telef&oacute;nicos; correo electr&oacute;nico; equipo instalado y su n&uacute;mero de serie; informaci&oacute;n sobre su sistema hidr&aacute;ulico, de gas y el&eacute;ctrico; informaci&oacute;n sobre los consumos de agua caliente.</p>

					<h2>FINALIDADES DEL TRATAMIENTO DE SUS DATOS PERSONALES:</h2>
					<p>Los datos personales que CALENTADORES GIS recabe ser&aacute;n usados para la prestaci&oacute;n de los servicios que usted hubiese solicitado, para informarle de cambios en los mismo y evaluar la calidad de los productos y servicios; as&iacute; como para ofrecerle, en su caso, otros productos y servicios de CALENTADORES GIS o de cualquiera de sus afiliadas, subsidiarias, controladoras, asociadas, comisionistas o sociedades integrantes del GRUPO INDUSTRIAL SALTILLO y remitirle promociones de otros bienes o servicios relacionados con los citados productos bancarios o financieros.</p>

					<h2>TRANSFERENCIA DE DATOS PERSONALES:</h2>
					<p>CALENTADORES GIS podr&aacute; transferir sus datos personales a terceros mexicanos o extranjeros que le provean de servicios necesarios para su debida operaci&oacute;n, as&iacute; como a sus afiliadas, subsidiarias, controladoras, asociadas, comisionistas o sociedades integrantes del GRUPO INDUSTRIAL SALTILLO. En dichos supuestos, le informamos que CALENTADORES GIS adoptar&aacute; las medidas necesarias para que las personas que tengan acceso a sus datos personales cumplan con la pol&iacute;tica de privacidad de CALENTADORES GIS, as&iacute; como con los principios de protecci&oacute;n de datos personales establecidos en la Ley.</p>

					<h2>DERECHOS ARCO:</h2>
					<p>Usted o su representante legal debidamente acreditado podr&aacute;n limitar el uso o divulgaci&oacute;n de sus datos personales; asimismo, a partir del 6 de enero de 2012 podr&aacute; ejercer, cuando procedan, los derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n u oposici&oacute;n que la Ley prev&eacute; mediante solicitud presentada en el domicilio arriba se&ntilde;alado; es importante mencionar que el ejercicio de cualquiera de dichos derechos no es requisito previo ni impide el ejercicio de otro derecho.</p>

					<p>As&iacute; tambi&eacute;n, le informamos que usted tiene derecho a iniciar un Procedimiento de Protecci&oacute;n de Datos ante el Instituto Federal de Acceso a la Informaci&oacute;n y Protecci&oacute;n de Datos (www.ifai.gob.mx) dentro de los 15 d&iacute;as siguientes a la fecha en que reciba la respuesta de CALENTADORES GIS o a partir de que concluya el plazo de 20 d&iacute;as contados a partir de la fecha de recepci&oacute;n de su solicitud de ejercicio de derechos.</p>

					<h2>MODIFICACIONES AL AVISO DE PRIVACIDAD</h2>
					<p>Cualquier modificaci&oacute;n al presente aviso le ser&aacute; notificada a trav&eacute;s de cualquiera de los siguientes medios: un mensaje enviado a su correo electr&oacute;nico o a su tel&eacute;fono m&oacute;vil; un mensaje dado a conocer a trav&eacute;s de las siguientes p&aacute;ginas de Internet: www.calorex.com.mx; www.cinsaboilers.com.mx;www.hesacalentadores.com.mx; www.cimamexico.com.mx.</p>
				</div>
			</div>
		</div>
	</section>
	<?php $this->load->view("partials/lowermenu"); ?>

</body>
</html>