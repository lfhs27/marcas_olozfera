<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>	
</head>
<body id="template-default" class="template-with-forms" style="/*background-image: url(<?php echo $pageProperties['bodyBackground']; ?>); background-repeat: no-repeat;*/">
	
	<?php $this->load->view("partials/topmenu"); ?>

	<section id="page-header">
		<div class="container">
			<div class="row top_fix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1 style="color:#000;text-shadow:none;">&iquest;Por qu&eacute; Cinsa?</h1>
				</div>
			</div>
		</div>
	</section>

	<section id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<h2>Razones para tener CINSA en casa</h2>
					<p>
						CINSA significa que nunca faltar&aacute; el agua caliente en tu hogar.
						Todos en casa esperan un ba&ntilde;o muy rico y caliente cada d&iacute;a, por lo que 
						CINSA cubrir&aacute; todas tus necesidades.
					</p>

					<p>
						Somos tu c&oacute;mplice para lograrlo por medio de una compra
						inteligente y que todos en tu hogar agradecer&aacute;n.
					</p>
					<p>
						Tecnolog&iacute;a, calidad de punta, econom&iacute;a, seguridad y variedad,
						en fin, costo - beneficio son nuestras caracter&iacute;sticas.
					</p>
					<p>
						CINSA es el calentador confiable que no falla, el delicioso
						ba&ntilde;o caliente que tu familia espera todos los d&iacute;as y el agua 
						caliente que cubre todas las necesidades de tu hogar.
					</p>
					<p>
						Los beneficios que CINSA te ofrece son &uacute;nicos ya que tenemos
						tecnolog&iacute;a en nuestra fabricaci&oacute;n de refacciones originales, 
						lo cual nos hace muy durables y ahorradores, CINSA es tu
						compra inteligente.
					</p>
				</div>

				<div class="col-xs-12 col-sm-6">
					<img src="../img/cinsa/nosotrosimg.png" height="570" width="573" alt="Nosotros" class="img-responsive">
				</div>
			</div>
		</div>
	</section>
	<?php $this->load->view("partials/lowermenu"); ?>

</body>
</html>