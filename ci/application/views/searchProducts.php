 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>
</head>
<body id="template-catalogo" class="template-interior" style="background-image: url(<?= !is_object($bg) ? base_url('/img/calorex/bg.png') : $pageProperties["domain"].$bg->src ?>); background-repeat: no-repeat;background-position: 0px -30px;background-size: 100% 270px;">
	<?php $this->load->view("partials/topmenu"); ?>
	
	<section id="page-header" class="hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<!-- <h1><?php echo $catalogSubtitle; ?></h1> -->
					<h1><?php echo $catalogTitle; ?></h1>
					<h2><?php echo $this->input->get("q"); ?></h2>
				</div>
			</div>
		</div>
	</section>

	<section id="main-content">
		<div class="container">
			<div class="row">
				<?php $this->load->view('partials/sidebar'); ?>
				<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9" id="page-content">
					<div class="row" id="lead-breadcrumb">
						<div class="hidden-xs col-sm-4 col">
							<section id="breadcrumb" class="hidden-xs" style="height: 40px;">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;">
									<ol class="breadcrumb" style="margin-bottom:0px;">
										<li>
											<a href="<?=base_url()?>"><?php echo ucfirst(strtolower($navigation->brand->front_name)); ?></a>
										</li>
										<li class="active"><?=strtoupper(str_replace('|', '/', str_replace('_', '-', str_replace('-', ' ', $catalogTitle))))?></li>
									</ol>	
								</div>
							</section>
						</div>
						<!-- <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col">
							<label>Ordenar Por</label>
							<select name="order">
								<option value="1">Popularidad</option>
								<option value="2">Precio (Bajo)</option>
								<option value="3">Precio (Alto)</option>
								option
							</select>
						</div> -->
					</div><!-- .row -->

					<div class="row is-flex" id="products">
						<?php 
						if(count($products) > 0):
						foreach($products as $product): 							//echo $product->brand_id." == ".$pageProperties["brand_id"]." && ".$product->family->slug." != refacciones && ".$product->family->slug." == ".$this->uri->segment(2)."<br />";
							if($product->brand_id == $pageProperties["brand_id"] && strcmp($product->family->slug, "refacciones") != 0):
						?>
							<?php  
							//$url_base = 'calentadores/'.strtolower(remove_accents($product->sector->name))."/". str_replace(' ', '-', strtolower(remove_accents($product->family->name)));
							$url_base = base_url($product->business_unit->slug.'/'.$product->brand->slug.'/'.$product->family->slug.'/'.$product->line->slug.'/'.$product->slug);
							//$first_model = array_shift($product->models);
							//$productLink = base_url($url_base."/".(str_replace('/', '|', str_replace(' ', '-', str_replace('-', '_', remove_accents($first_model->front_name)))))); 
							$productImg = false;
							/*if(isset($product->images) && is_array($product->images)){
								foreach($product->images as $img){
									if ($img->location === "listing") {
										$productImg = $img;
									}
								}
							} */
							?>

							<div class="product col-xs-6 col-sm-4 col-md-4 col-lg-4">
								<div class="product-content" style="padding:10px;">
									<div class="row is-flex">
										<div class="col-xs-5">
											<a class="thumbnail-cont" href="<?php echo $url_base; ?>">
											<?php if(isset($product->featuredImage) && !is_null(isset($product->featuredImage))): ?>
												<img src="<?=$pageProperties["domain"]?><?=$product->featuredImage->image->filepath.'/'.$product->featuredImage->image->filename?>" alt="Featured Image">
											<?php else: ?>
												<?php if($product->gallery): ?>
													<img src="<?=$pageProperties["domain"]?><?=$product->gallery->images[0]->filepath . '/' . $product->gallery->images[0]->filename?>" alt="" class="img-responsive">
												<?php endif; ?>
											<?php endif; ?>
											</a>
										</div>
										<div class="col-xs-7" style="position:relative;height:180px;">
											<div class="slogan"><?=$product->name;?></div>
											<br />
											<br />
											<div class="product-name"><a href="<?php echo $url_base; ?>"><?=str_replace(strtoupper($pageProperties['skin'])." ", "", $product->name)?></a></div>
										</div>
									</div>
									<!--div class="subtitle-overlay" style="display: none;"><a href="<?php echo $url_base; ?>"><?php echo $product->name; ?><br /><br /><span class="btn btn-primary" style="border:1px solid #fff;background-color:transparent;">VER M&Aacute;S</span></a></div-->
								</div>
								<div class="category-title"><a href="<?php echo $url_base; ?>"><span class="btn btn-primary ver-mas">VER M&Aacute;S</span></a></div>
							</div>
						
						<?php
							endif;
						endforeach; 
						endif;
						?>

					</div><!-- #products -->
				</div><!-- #page-content -->
			</div>
		</div>
	</section>
	
	<?php $this->load->view("partials/lowermenu"); ?>

</body>
</html>