<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>	


<!--div id="s1-chat-status-container"><a class="s1-status-icon" href="#" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 && window.event.preventDefault) window.event.preventDefault(); this.chat = window.open('https://contactocc.s1gateway.com/integrations/chats/chat_calorex/index.html', 'S1Gateway', 'toolbar=0,scrollbars=0,location=0,status=0,menubar=0,width=402,height=590,resizable=0'); this.chat.focus(); var windowwidth = 402; var windowheight = 590; var screenwidth = screen.availWidth; var screenheight = screen.availHeight; this.chat.moveTo(screenwidth - windowwidth,screenheight - windowheight);this.chat.opener=window;return false;">Ayuda en línea</a></div-->
</head>
<body style="background: #FFF; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
<!--<body style="background-image: url(<?php echo $pageProperties['bodyBackground']; ?>); background-repeat: none; background-size: cover;">
	-->
	<?php $this->load->view("partials/topmenu"); ?>

	<section id="jumbotron" style="margin-bottom:0px;padding-bottom:10px;">
		<div class="container">
			<div class="row">
				<div class="col-xs-offset-2 col-xs-12 col-sm-10 col-md-8 col-lg-8" style="margin-top:-10px;">
					
					<div data-cycle-slides=">a" class="cycle-slideshow" data-cycle-fx="carousel">
						<?php //foreach($pageProperties['slider'] as $slide): ?>
						<?php if($slides): ?>
						<?php foreach($slides as $slide): ?>
						<a href="<?=$slide->link?>">
							<img src="<?=$pageProperties["domain"].$slide->src?>" height="300px" style="width:100%" class="center-block hidden-xs">
							<img src="<?=$pageProperties["domain"].$slide->src?>" class="img-responsive center-block visible-xs">
						</a>
						<?php endforeach; ?>
						<?php endif; ?>
					</div>

				</div>
			</div>
		</div>
	</section><!-- #jumbotron -->

	<section id="products-carousel">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<!--h4>LÍNEA <span>RESIDENCIAL</span></h4-->
					<div class="row">
						<div class="owl-carousel owl-theme" id="products-carousel-container">
							<?php if($products): ?>
							<?php foreach($products as $product): ?>
			 					<div class="product">
									<a href="<?=$product->link?>"><img src="<?=$pageProperties["domain"].$product->src?>" class="img-responsive"></a>
									<div class="product-overlay">
										<h3>
											<a href="<?=$product->link?>">
												<?=$product->title?>
												<br /><br />
												<span class="btn btn-primary" style="border:1px solid #fff;background-color:transparent;">VER M&Aacute;S</span>
											</a>
										</h3>
									</div>
								</div>
							<?php endforeach; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="index-banners" style="margin:10px 0px;padding:10px 0px;">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-lg-12" style="">
				<?php foreach($banners as $banner): ?>
				<div class="col-xs-12 col-md-4">
					<a href="<?=$banner->link?>"><img src="<?=$pageProperties["domain"].$banner->src?>" class="img-responsive"></a>
				</div>
				<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<style>
/*
@media (max-width: 496px) {
 .owl-item{
    width: 6% !important;
 }
.owl-pagination div:nth-child(1) {
}
.owl-pagination div:nth-child(2) {
}
.owl-pagination div:nth-child(3) {
  display:none !important;
}
.owl-pagination div:nth-child(4) {
  display:none !important;
}
 
 
 .owl-carousel .owl-wrapper {
    width: 500px !important;
}
 .owl-item {
    width: 120px !important;
}

}   
*/
</style>
	<?php $this->load->view("partials/lowermenu"); ?>

</body>
<script>
var bgs = [];
var bgCount = 0;
$(document).ready(function(){
	$.ajax({
		url : 'main/getIndexBackgrounds',
		dataType : 'json',
		success : function(data){
			bgs = data;
			setBackground();
			setInterval(setBackground, 10000);
		}
	});
});
function setBackground(){
	if(bgCount >= bgs.length)
		bgCount = 0;
		
	$("body").css("background", "url(<?=$pageProperties["domain"]?>"+bgs[bgCount].src+")");
	bgCount++;
}
</script>
</html>