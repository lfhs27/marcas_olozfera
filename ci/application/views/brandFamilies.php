 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>
</head>
<body id="template-catalogo" class="template-interior" style="background-image: url(<?=isset($bg)?base_url.$bg:"../img/calorex/bannerCX-deposito.png"?>); background-repeat: no-repeat;">
	<?php $this->load->view("partials/topmenu"); ?>

	<section id="page-header" class="hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1>LOS BAÑOS MÁS CONFORTABLES</h1>
					<h2>Línea Depósito</h2>
				</div>
			</div>
		</div>
	</section>

	<section id="main-content">
		<div class="container">
			<div class="row">
				<?php $this->load->view('partials/sidebar'); ?>
				<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9" id="page-content">
					<div class="row" id="lead-breadcrumb">
						<div class="hidden-xs col-sm-4 col">
							<h1>Línea Residencial</h1>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col">
							<label>Ordenar Por</label>
							<select name="order">
								<option value="1">Popularidad</option>
								<option value="2">Precio (Bajo)</option>
								<option value="3">Precio (Alto)</option>
								option
							</select>
						</div>
					</div><!-- .row -->

					<div class="row" id="products">
						<?php foreach($navigation->brand->sectors[0]->products as $product): ?>
							<?php var_dump($product); ?>
							<?php 
								$productLink = base_url('marca/' . $navigation->brand->id . '/producto/' . $product->id);
								$listingImage = false;

								foreach($product->images as $img):
									if ($img->location === "listing") {
										$listingImage = $img;
									}
								endforeach;
							?>
							<div class="product col-xs-6 col-sm-4 col-md-4 col-lg-4">
								<div class="product-content">
									<?php if($listingImage): ?>
									<a class="thumbnail-cont" href="<?php echo $productLink; ?>"><img src="http://srgisazr1-0002.cloudapp.net/<?php echo $listingImage->path .'/'. $listingImage->filename; ?>" alt="Depósito"></a>
									<?php endif; ?>
									<div class="subtitle-overlay" style="display: none;"><a href="<?php echo $productLink; ?>"><?php echo $product->slogan; ?><br /><br /><span class="btn btn-primary" style="border:1px solid #fff;background-color:transparent;">VER M&Aacute;S</span></a></div>
								</div>
								<div class="category-title"><a href="<?php echo $productLink; ?>"><?=str_replace(ucfirst($pageProperties['skin'])." ", "", $product->name)?></a></div>
							</div>
						
						<?php endforeach; ?>

					</div><!-- #products -->
				</div><!-- #page-content -->
			</div>
		</div>
	</section>
	
	<?php $this->load->view("partials/lowermenu"); ?>

</body>
</html>