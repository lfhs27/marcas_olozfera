﻿ <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>
</head>
<body id="template-catalogo" class="template-interior" style="background-image: url(<?= !is_object($bg) ? base_url('/img/calorex/bg.png') : $pageProperties['domain'].$bg->src ?>); background-repeat: no-repeat;background-position: 0px -30px;background-size: 100% 270px;">
	<?php $this->load->view("partials/topmenu"); ?>
	
	<section id="page-header" class="hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<!-- <h1><?php //echo $catalogSubtitle; ?></h1> -->
					<h1><?php //echo $lines[0]->slogan ?></h1>
					<h2><?php //echo $catalogTitle; ?></h2>
				</div>
			</div>
		</div>
	</section>

	<section id="main-content">
		<div class="container">
			<div class="row">
				<!--section id="breadcrumb" class="hidden-xs" style="height: 50px;">
					<div class="container">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=base_url()?>"><?php //echo ucfirst(strtolower($navigation->brand->name)); ?></a>
								</li>
								<li class="active"><?=ucwords(str_replace('|', '/', str_replace('_', '-', str_replace('-', ' ', $catalogTitle))))?></li>
							</ol>	
						</div>
					</div>
				</section-->
				<?php $this->load->view('partials/sidebar'); ?>
				<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9" id="page-content">
					<div class="row" id="lead-breadcrumb">
						<div class="hidden-xs col-sm-4 col">
							<h1>Línea Residencial</h1>
						</div>
						<!-- <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col">
							<label>Ordenar Por</label>
							<select name="order">
								<option value="1">Popularidad</option>
								<option value="2">Precio (Bajo)</option>
								<option value="3">Precio (Alto)</option>
								option
							</select>
						</div> -->
					</div><!-- .row -->

					<div class="row" id="products">
						<?php 
						/*print_r("<pre>");
						print_r($lines);
						print_r("</pre>");*/
						if(count($lines) > 0):
						foreach($lines as $line): 
							if($line->brand_id == $pageProperties["brand_id"] && strcmp($line->family->slug, "refacciones") != 0 && !strpos($line->name, "GEN 2")):
						?>
							<?php  
							$url_base = base_url('calentadores/'.$line->brand->slug.'/'.$line->family->slug.'/'.$line->slug);
							$lineImg = false;
							?>

							<div class="product col-xs-6 col-sm-4 col-md-4 col-lg-4">
								<div class="product-content" style="padding:10px;">												<?php 												/*print_r("<pre>");												print_r($line); 												print_r("</pre>");*/												?>
									<div class="row">
										<div class="row">
											<div class="col-xs-12">
												<a class="thumbnail-cont" href="<?php echo $url_base; ?>" style="text-align:center;">
												<?php if(COUNT($line->products) > 0): ?>
													<?php
													$firstProduct = null;
													foreach($line->products as $product){
														if(strpos($product->slug, "refaccion") === false){
															$firstProduct = $product;
															break;
														}
													}
													?>
													<?php if(!is_null($firstProduct)): ?>
														<?php if(isset($firstProduct->featuredImage) && !is_null(isset($firstProduct->featuredImage))): ?>
															<img src="<?=$pageProperties["domain"]?><?=$firstProduct->featuredImage->image->filepath.'/'.$firstProduct->featuredImage->image->filename?>" alt="Featured Image" style="height:150px;display:inline;" />
														<?php else: ?>
															<?php if($firstProduct->gallery): ?>
																<img src="<?=$pageProperties["domain"]?><?=$firstProduct->gallery->images[0]->filepath . '/' . $firstProduct->gallery->images[0]->filename?>" alt="" style="height:150px;display:inline;" class="img-responsive">
															<?php endif; ?>
														<?php endif; ?>
													<?php endif; ?>
												<?php endif; ?>
												</a>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12" style="position:relative;text-align:center;">
												<div class="product-name" style="position:relative;"><a href="<?php echo $url_base; ?>"><?=str_replace(strtoupper($pageProperties['skin'])." ", "", $line->name)?></a></div>
											</div>
										</div>
									</div>
									<!--div class="subtitle-overlay" style="display: none;"><a href="<?php echo $url_base; ?>"><?php echo $line->name; ?><br /><br /><span class="btn btn-primary" style="border:1px solid #fff;background-color:transparent;">VER M&Aacute;S</span></a></div-->
								</div>
								<div class="category-title"><a href="<?php echo $url_base; ?>"><span class="btn btn-primary ver-mas">VER M&Aacute;S</span></a></div>
							</div>
						
						<?php
							endif;
						endforeach; 
						endif;
						?>

					</div><!-- #products -->
				</div><!-- #page-content -->
			</div>
		</div>
	</section>
	
	<?php $this->load->view("partials/lowermenu"); ?>

</body>
</html>