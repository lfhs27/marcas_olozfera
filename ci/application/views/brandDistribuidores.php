 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=$pageProperties["skin"] == "calorex" ? "Calorex" : "Cinsa Boilers"?></title>
	<link rel="icon" href="<?=base_url()?>img/<?=$pageProperties["skin"]?>/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<?php $this->load->view('partials/head.php',['skin' => $pageProperties['skin']]); ?>
</head>
<body id="template-default" class="template-interior template-catalogo-producto template-header-sm">
	<?php $this->load->view("partials/topmenu"); ?>
	<input type="hidden" id="mapColor" value="<?=$pageProperties["skin"] == "calorex" ? "#F44336" : "#249CD8"?>" />
	<input type="hidden" id="mapSelectedColor" value="<?=$pageProperties["skin"] == "calorex" ? "#CC0000" : "#204471"?>" />
	<section id="page-content">
		<div class="container">
			<div class="row top_fix" style="margin-top:0px;">
				<div class="col-xs-12 col-md-6">
					<h2 style="text-shadow: 1px 2px 0px #C0C0C0;">Distribuidores</h2>
					<div style="color:#A5A4A4;font-size:18px;padding-bottom:15px;">Elige un estado o da click sobre el mapa</div>
					<select class="form-control" id="estados" style="margin-bottom:20px;">
						<option value="0">Selecciona un Estado</option>
						<?php foreach($states as $state): ?>
						<option value="<?=$state->id?>"><?=$state->name?></option>
						<?php endforeach; ?>
					</select>
					<select class="form-control" id="municipios" style="display:none;">
						<option value="0"> -- </option>
					</select>
				</div>
				<div class="col-xs-12 col-md-6">
					<div id="mapdiv" style="height: 500px;"></div>
				</div>
			</div>
		</div>		<div class="container-fluid">			<div class="row" style="background-color:#F2F2F2;">				<div class="col-xs-6">					<h2 style="margin:15px;" id="place-name"></h2>				</div>			</div>		</div>
		<div class="container-fluid">
			<div class="row">				<div class="col-xs-4" style="height:500px; overflow-y:scroll;">					<h3 style="margin:20px 15px 0px 15px;font-size:22px;font-weight:bold;text-align:center;">Selecciona una tienda</h3>					<div id="lista-filtros" style="padding-top:15px;"></div>					<div id="lista-direcciones" style=""></div>				</div>				<div class="col-xs-8">
					<div id="mapGoogle" style="height:500px;"></div>				</div>
			</div>
		</div>
		<!--div class="container-fluid">
			<div class="row" style="background-color:#F2F2F2;">
				<div class="col-xs-offset-1 col-xs-6">
					<h2 style="margin-top:15px;" id="place-name"></h2>
				</div>
			</div>
		</div-->
		<div class="container">
			<!-- AQUI ESTABAN LOS FILTROS Y LISTAS -->
		</div>
	</section>	
	<?php $this->load->view("partials/lowermenu"); ?>
<script>
var mapGoogle;
var geocoder;
function initMap() {
	geocoder = new google.maps.Geocoder();
	mapGoogle = new google.maps.Map(document.getElementById('mapGoogle'), {
		scrollwheel: false,
		zoom: 5
	});
	$.ajax({
		url : "main/getCalorexAddresses",
		dataType : "json", 
		success : function(data){
			if($.isArray(data)){
				/*$.each(data, function(i, val){
					codeAddress(val.direccion+", "+val.municipio.municipio+", "+val.municipio.estado.name);
				});*/
			}
		}
	});
	setAddress("Mexico");
	//codeAddress("Perif�rico Poniente Manuel G�mez Mor�n �#7371, Parque Industrial Vallarta, Zapopan, Jalisco");
	//codeAddress("Avenida Ni�os H�roes #1601, Moderna, Guadalajara, Jalisco");
}
function codeAddress(val) {	var address = val.direccion+", "+val.municipio.municipio+", "+val.municipio.estado.name;	
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			//mapGoogle.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
				map: mapGoogle,
				position: results[0].geometry.location
			});  						var info_html = '';			info_html += '<div class="col-xs-12">';			info_html += '	<h2 style="font-size: 16px;margin:20px 0px 5px 0px;">'+val.nombre_comercial+'</h2>';			info_html += '	'+val.sucursal+'<br />';			info_html += '	'+val.direccion+'<br />';			info_html += '	'+val.municipio.municipio+', '+val.municipio.estado.name+'<br />';			info_html += 	val.telefono;			info_html += '</div>';						var infowindow = new google.maps.InfoWindow({				content: info_html			});
			var marker = new google.maps.Marker({
				position: results[0].geometry.location,
				draggable: false,
				map: mapGoogle,
				title:val.nombre_comercial
			});						marker.addListener('click', function() {				infowindow.open(mapGoogle, marker);			});
		} else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {   
            setTimeout(function() {
                codeAddress(val);
            }, 200);
        } else {
			//alert("Geocode was not successful for the following reason: " + status);
		}
	});
}
function setAddress(address) {
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			mapGoogle.setCenter(results[0].geometry.location);
		} else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {    
            setTimeout(function() {
                setAddress(address);
            }, 200);
        } else {
			//alert("Geocode was not successful for the following reason: " + status);
		}
	});
}

var map;

AmCharts.ready(function() {
	$.ajax({
		url : 'main/getCalorexStates',
		dataType : 'json',
		success : function(data){
			dataAreas = $.map(data, function(val, i){
				return {
					id : val.map_code,
					value : val.id,
					color : $("#mapColor").val()
				};
			});
			
			map = new AmCharts.AmMap();
			map.backgroundColor = "#FFF";
			map.backgroundAlpha = 0;


			map.balloon.color = "#000000";

			var dataProvider = {
				mapVar: AmCharts.maps.mexicoLow,
				getAreasFromMap:true,
				areas : dataAreas,
				unlistedAreasColor: "#C0C0C0"
			};

			map.areasSettings = {
			   autoZoom: false,
			   selectable: true,
			   unlistedAreasColor: "#C0C0C0",
				selectedColor: $("#mapSelectedColor").val()
			};
			map.dataProvider = dataProvider;

			map.addListener( 'clickMapObject', function( event ) {
				// deselect the area by assigning all of the dataProvider as selected object
				map.selectedObject = map.dataProvider; console.log(event.mapObject);
				estado = event.mapObject.enTitle;
				setAddress(estado+", Mexico");
				mapGoogle.setZoom(8);
				$("#estados").val(event.mapObject.value);
				$("#municipios").hide();
				
				var posicion = $("#mapGoogle").offset().top;
				$("html, body").animate({
					scrollTop: posicion
				 }, 2000);	

				 
				$("#place-name").html(estado);
				setInfoEstado(event.mapObject.value);
			} );

			//map.smallMap = new AmCharts.SmallMap();

			map.write("mapdiv");
		}
	});
});
function regresar(){
	$('#lista-filtros label').show();
	$('#lista-filtros .regresar').hide();
	$.each($('#lista-filtros input[type=checkbox]'), function(i, val){
		$(val).prop("checked", false);
		$(val).parent().css("color", "#BCBCBC");
		$(val).parent().removeClass("active");
		$(".comercio[data-nombre]").hide();
	});
}
$(document).ready(function(){	$('body').on('change','#lista-filtros input[type=checkbox]', function(e) {
	elm = $(e.target);
	nombre = elm.val();
	if(elm.prop("checked")){
		//elm.parent().css("color", $("#mapColor").val());
		elm.parent().addClass("active");
		elm.parent().parent().find(".regresar").show();	
		$('#lista-filtros label').not('.regresar').hide();
		$(".comercio[data-nombre="+nombre.replace(/ /g, "-")+"]").show();
	}else{
		//elm.parent().css("color", "#BCBCBC");
		$(".comercio[data-nombre="+nombre.replace(/ /g, "-")+"]").hide();
	}
});
	$("#estados").change(function(e){
		elm = $(this);
		id = $(this).val();
		
		$.ajax({
			url : "getCalorexMunicipios/"+id,
			dataType : "json",
			success : function(data){
				html = '<option value="0">Elegir municipio</option>';
				if($.isArray(data)){
					$.each(data, function(i, val){
						html += '<option value="'+val.municipio+'" data-id="'+val.id+'">'+val.municipio+'</option>';
					});
				}
				$("#municipios").html(html);
				$("#municipios").show();
				
				$("#place-name").html(elm.find(":selected").html());
				$.ajax({
					url : "getCalorexAddressesEstado/"+id,
					dataType : "json",
					success : function(data){
						html = '';
						var filtros_html = '<label class="regresar" style="margin:5px 15px;display:none" onclick="regresar();">< Regresar</label>';						var filtros = [];						if($.isArray(data)){							$.each(data, function(i, val){								if($.inArray(val.nombre_comercial.toUpperCase(), filtros) < 0)									filtros.push(val.nombre_comercial.toUpperCase());								html += print_direccion(i, val);								codeAddress(val);							});						}						$.each(filtros, function(i, val){							filtros_html += '<label style="margin:5px 15px;width:calc(100% - 30px);"><input type="checkbox" name="filtros" value="'+val+'" /> '+val.toUpperCase()+'</label>';						});						$("#lista-filtros").html(filtros_html);						$("#lista-direcciones").html(html);
					}
				});
			}
		});
		setInfoEstado(id);
	});
	
	$("#municipios").change(function(e){
		id = $(this).find(":selected").attr("data-id");
		setAddress($(this).val()+", "+$("#estados").find(":selected").html()+", Mexico");
		
		$("#place-name").html($(this).find(":selected").html()+", "+$("#estados").find(":selected").html());
				
		mapGoogle.setZoom(10);
		var posicion = $("#mapGoogle").offset().top;
		$("html, body").animate({
			scrollTop: posicion
		 }, 2000);	
				
		$.ajax({
			url : "getCalorexAddressesMunicipio/"+id,
			dataType : "json",
			success : function(data){
				html = '';				var filtros_html = '<label class="regresar" style="margin:5px 15px;display:none" onclick="regresar();">< Regresar</label>';				var filtros = [];
				if($.isArray(data)){
					$.each(data, function(i, val){
						if($.inArray(val.nombre_comercial.toUpperCase(), filtros) < 0)
							filtros.push(val.nombre_comercial.toUpperCase());
						html += print_direccion(i, val);
						codeAddress(val);
					});
				}
				$.each(filtros, function(i, val){
					filtros_html += '<label style="margin:5px 15px;width:calc(100% - 30px);"><input type="checkbox" name="filtros" value="'+val+'" /> '+val.toUpperCase()+'</label>';
				});
				$("#lista-filtros").html(filtros_html);
				$("#lista-direcciones").html(html);
			}
		});
	});
});
function print_direccion(i, obj){
	html = '';
	html += '<div class="col-xs-12 col-md-12 comercio" data-nombre="'+obj.nombre_comercial.replace(/ /g, "-").toUpperCase()+'" style="font-size:16px;display:none;">';
	html += '	<h2 style="font-size: 16px;margin:20px 0px 5px 0px;">'+obj.nombre_comercial+'</h2>';
	html += '	'+obj.sucursal+'<br />';
	html += '	'+obj.direccion+'<br />';
	html += '	'+obj.municipio.municipio+', '+obj.municipio.estado.name+'<br />';
	html += 	obj.telefono;
	html += '</div>';
		/*
	if((i+1) % 3 == 0)
		html += '<div class="row"></div>';	*/
	
	return html;
}
function setInfoEstado(estado){
	$.ajax({
		url : "getCalorexAddressesEstado/"+estado,
		dataType : "json",
		success : function(data){			html = '';			var filtros_html = '<label class="regresar" style="margin:5px 15px;display:none" onclick="regresar();">< Regresar</label>';			var filtros = [];			if($.isArray(data)){				$.each(data, function(i, val){					if($.inArray(val.nombre_comercial.toUpperCase(), filtros) < 0)						filtros.push(val.nombre_comercial.toUpperCase());					html += print_direccion(i, val);					codeAddress(val);				});			}			$.each(filtros, function(i, val){				filtros_html += '<label style="margin:5px 15px;width:calc(100% - 30px);"><input type="checkbox" name="filtros" value="'+val+'" /> '+val.toUpperCase()+'</label>';			});			$("#lista-filtros").html(filtros_html);			$("#lista-direcciones").html(html);
		}
	});
}String.prototype.ucfirst = function() {    return this.charAt(0).toUpperCase() + this.slice(1);}
</script>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkv6klvWC-aGno2-EU1oqTSjEeutQ0EXI&callback=initMap">
</script>
</body>
</html>