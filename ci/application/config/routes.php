<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';

// Redirecciones temporales
$route['calentadores/residencial/deposito/G_10'] = 'Main/tempg10';


// http://www.calorex.com.mx/calentadores/calorex/deposito/deposito-timer/calorex-g-10-timer-gas-lp
$route['calentadores/residencial/deposito-timer/G_10-TIMER'] = 'Main/temp2';

// http://www.calorex.com.mx/calentadores/calorex/de-paso/de-paso-standard/calorex-coxdp-06-gas-lp
$route['calentadores/residencial/de-paso/COXDP_06'] = 'Main/temp3';

// http://www.calorex.com.mx/calentadores/calorex/de-paso/de-paso-sin-piloto/calorex-coxdpe-06-gas-lp
$route['calentadores/residencial/de-paso-sin-piloto/COXDPE_06'] = 'Main/temp4';

// http://www.calorex.com.mx/calentadores/calorex/de-paso/de-paso-evolution/calorex-coxpsp-06-gas-lp
$route['calentadores/residencial/de-paso-evolution/COXPSP_06'] = 'Main/temp5';

// http://www.calorex.com.mx/calentadores/calorex/deposito/deposito-electrico/e-10-standard
$route['calentadores/residencial/deposito-electrico/E_10'] = 'Main/temp6';

// http://www.calorex.com.mx/calentadores/calorex/instantaneo/instantaneo-standard/coxdpi-07-b
$route['calentadores/residencial/instantaneo/COXDPI_07'] = 'Main/temp7';

// http://www.calorex.com.mx/calentadores/calorex/instantaneo/instantaneo-modulante/calorex-instantaneo-modulante-coxdpi-14-lp
$route['calentadores/residencial/instantaneo-modulante/COXDPI_14'] = 'Main/temp8';

// http://www.calorex.com.mx/calentadores/calorex/instantaneo/instantaneo-condensacion/calorex-condensacion-cd-150-s
$route['calentadores/residencial/instantaneo-condensacion/CD_150-S'] = 'Main/temp9';

// http://www.calorex.com.mx/calentadores/calorex/instantaneo/instantaneo-punto-de-uso/cox-ie-351
$route['calentadores/residencial/instantaneo-punto-de-uso/COX_IE-351-%7C-127V'] = 'Main/temp10';
$route['calentadores/residencial/instantaneo-punto-de-uso/COX_IE-351-|-127V'] = 'Main/temp10';

// http://www.calorex.com.mx/calentadores/calorex/solar/solar/calorex-solar-termosifon-150
$route['calentadores/residencial/solar/TERMOSIFON-150'] = 'Main/temp11';

// END Redirecciones temporales

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['marca/(:num)'] = 'Main/brand/$1'; 
$route['blog'] = 'Main/tips';
$route['blog/(:any)'] = 'Main/tip/$1';
$route['buscar'] = 'Main/buscar';
$route['getCalorexAddressesEstado/(:num)'] = 'Main/getCalorexAddressesEstado/$1';
$route['getCalorexMunicipios/(:num)'] = 'Main/getCalorexMunicipios/$1';
$route['getCalorexAddressesMunicipio/(:num)'] = 'Main/getCalorexAddressesMunicipio/$1';
$route['residencial/(:any)'] = 'Main/buscarLinea/$1';
$route['distribuidores'] = 'Main/distribuidores';
$route['nosotros'] = 'Main/nosotros';
$route['solicita_ayuda'] = 'Main/solicita_ayuda';
$route['solicita_asesoria'] = 'Main/solicita_asesoria';
$route['solicita_dudas'] = 'Main/solicita_dudas';
$route['solicita_garantia'] = 'Main/solicita_garantia';
$route['solicita_distribuidor'] = 'Main/solicita_distribuidor';
$route['extiende_garantia'] = 'Main/extiende_garantia';
$route['aviso_privacidad'] = 'Main/aviso_privacidad';
$route['marca/(:num)/familia/(:num)'] = 'Main/family/$1/$2';
//$route['calentadores/(:any)/(:any)/(:any)'] = 'Main/productByName/$1/$2/$3';
//$route['calentadores/(:any)/(:any)/(:any)/(:any)'] = 'Main/productByName/$1/$2/$3/$4';
$route['producto/(:num)'] = 'Main/product/$1';
$route['producto/(:num)/(:num)'] = 'Main/product/$1/$2';
$route['calentador-ideal'] = 'Main/calentadorIdeal';
$route['(:any)/(:any)/(:any)'] = 'Main/familyByName/$1/$2/$3';
$route['(:any)/(:any)/(:any)/(:any)'] = 'Main/lineByName/$1/$2/$3/$4';
$route['(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Main/productByName/$1/$2/$3/$4/$5';